Collection of pony files for ponysay

[Featured artists](ARTISTS.md)

![Rhyhorn](examples/rhyhorn.png))
![Surprised Ruby](examples/ruby-surprised.png))

## Usage

Clone the repository, and add the ponies you want in `/usr/share/ponysay/extraponies`.

## How to contribute

Feel free to ask for more ponies !

### Create a .pony file

  1. Create an image (with dimensions fitting the terminal)
  2. Use `util-say` : `./img2ponysay -- custom-pony.png > custom-pony.pony`
  3. `ponysay-tool --edit custom-pony.pony`

And then, you can create a Merge Request !

## Extra

Other repositories of ponies :
  * https://github.com/neqo/ponysay-chars
  * https://github.com/20goto10/wackyponies


