$$$
NAME: Zubat
OTHER NAMES: ズバット, 주뱃, 超音蝠, Nosferapti
APPEARANCE: Pokémon Generation I
KIND: Bat Pokémon
GROUP: flying
BALLOON: top
COAT: purple
DISPLAY: full
BALLOON TOP: 3
BALLOON BOTTOM: 0
POSE: master
BASED ON: Zubat
SOURCE: PokéAPI Sprites
MEDIA: Pokémon
LICENSE: Copyright The Pokémon Company
FREE: no
WIDTH: 75
HEIGHT: 22


$$$
$balloon5$
                                        $\$                                    [00m
                                         $\$                                   [00m
                                          $\$                                  [00m
                                           $\$          [00m
                   [38;5;67m▄▄▄▄[39m                [38;5;233m▄▄▄▄▄[48;5;233;38;5;67m▄▄▄▄▄[49;38;5;233m▄[39m    [00m
               [38;5;67m▄▄[48;5;67;38;5;189m▄▄[48;5;189m█[38;5;110m▄[38;5;67m▄[49m▀[39m          [38;5;233m▄▄▄[48;5;233;38;5;110m▄▄▄[48;5;110;38;5;67m▄[48;5;189m▄[38;5;60m▄[48;5;110m▄▄[48;5;60;38;5;126m▄▄[38;5;89m▄[49;38;5;60m▀▀[39m     [00m
            [38;5;67m▄[48;5;67;38;5;189m▄▄[48;5;189m█[38;5;110m▄▄[48;5;110m██[38;5;60m▄[49m▀[39m        [38;5;233m▄[48;5;233;38;5;110m▄[38;5;189m▄[48;5;110m▄[38;5;67m▄[48;5;189;38;5;60m▄▄[48;5;60;38;5;126m▄▄▄[48;5;126m████[38;5;89m▄[49m▀[39m        [00m
          [38;5;67m▄[48;5;67;38;5;189m▄[48;5;189m█[38;5;110m▄[48;5;110m██████[48;5;60;38;5;60m█[49;39m         [48;5;60;38;5;60m█[48;5;110;38;5;110m█[48;5;67;38;5;89m▄[48;5;60m▄[38;5;126m▄[48;5;126m███████[38;5;89m▄[49m▀[39m          [00m
        [38;5;67m▄[48;5;67;38;5;189m▄[48;5;189m█[38;5;110m▄[48;5;110m█████████[48;5;60;38;5;60m█[49;39m       [48;5;60;38;5;60m█[48;5;110;38;5;189m▄[48;5;67;38;5;89m▄[48;5;89;38;5;126m▄[48;5;126m████████[38;5;89m▄[49m▀[39m           [00m
       [38;5;67m▄[48;5;67;38;5;189m▄[48;5;189m█[48;5;110;38;5;110m██████[38;5;189m▄▄[38;5;67m▄[48;5;189m▄[38;5;60m▄[48;5;110m▄[48;5;60m█[49;39m      [48;5;60;38;5;60m█[48;5;189;38;5;110m▄[48;5;89;38;5;89m█[48;5;126m▄[38;5;126m███████[38;5;89m▄[49m▀[39m            [00m
      [38;5;67m▄[48;5;67;38;5;189m▄[48;5;189m█[48;5;110;38;5;110m███[38;5;189m▄[38;5;60m▄[48;5;189m▄▄[48;5;67;38;5;67m█[48;5;60m▄▄[48;5;67;38;5;60m▄▄[48;5;60;38;5;189m▄▄[48;5;67m▄[49;38;5;67m▄[39m   [38;5;60m▄[48;5;60;38;5;189m▄[48;5;110;38;5;110m█[48;5;67;38;5;60m▄▄▄▄▄[48;5;89;38;5;67m▄▄▄[48;5;126m▄[48;5;89m▄[49;38;5;60m▄[39m            [00m
      [48;5;60;38;5;60m█[48;5;189;38;5;110m▄[48;5;110m███[48;5;67;38;5;67m█[48;5;60m▄[48;5;67m████[38;5;60m▄[48;5;60;38;5;189m▄[48;5;189m█[38;5;110m▄[38;5;126m▄[48;5;126m█[48;5;89m▄[48;5;67;38;5;60m▄[49;39m   [48;5;60;38;5;60m█[48;5;110;38;5;110m█[48;5;89;38;5;89m██[48;5;126;38;5;126m████[48;5;60m▄▄▄[38;5;89m▄[49;38;5;60m▀[39m             [00m
       [38;5;60m▀▀[48;5;67m▄[48;5;110;38;5;67m▄[38;5;110m█[48;5;67m▄▄[38;5;67m██[38;5;60m▄[48;5;60;38;5;189m▄[48;5;189m█[38;5;110m▄[48;5;126;38;5;126m██[38;5;176m▄[48;5;176m█[48;5;89;38;5;89m█[49;39m  [38;5;60m▄[48;5;60;38;5;189m▄[48;5;110;38;5;110m█[48;5;89;38;5;89m█[48;5;126;38;5;126m███[38;5;89m▄▄[49m▀▀▀[39m               [00m
           [38;5;60m▀[38;5;67m▀[48;5;110;38;5;60m▄[38;5;110m█[48;5;67;38;5;60m▄[48;5;60;38;5;110m▄[48;5;189;38;5;189m█[38;5;110m▄[48;5;126;38;5;89m▄[38;5;126m█[48;5;176;38;5;176m██[38;5;89m▄[49m▀[39m  [48;5;60;38;5;60m█[48;5;189;38;5;110m▄[48;5;89;38;5;89m█[48;5;126;38;5;126m██[38;5;89m▄[49m▀[39m                    [00m
    [38;5;67m▄▄▄▄▄[39m     [38;5;60m▀[48;5;60m█[48;5;110;38;5;110m██[48;5;89;38;5;60m▄[38;5;89m██[48;5;176;38;5;126m▄[38;5;176m█[48;5;89;38;5;89m█[49;39m  [48;5;60;38;5;60m█[48;5;189;38;5;110m▄[48;5;110;38;5;89m▄[48;5;89;38;5;126m▄[48;5;126;38;5;89m▄[49m▀[39m                      [00m
   [48;5;67;38;5;67m█[48;5;189;38;5;89m▄[38;5;126m▄[38;5;110m▄[48;5;110;38;5;189m▄[38;5;110m█[48;5;60m▄[48;5;67m▄[49;38;5;60m▄▄[48;5;60;38;5;110m▄▄[48;5;67m▄[48;5;110m██[48;5;60;38;5;60m█[48;5;89;38;5;89m██[48;5;126;38;5;126m█[38;5;89m▄[49m▀[39m [48;5;60;38;5;67m▄[48;5;189;38;5;110m▄[48;5;110;38;5;89m▄[48;5;89m█[48;5;126m▄[49m▀[39m                       [00m
    [38;5;89m▀[48;5;126m▄[38;5;176m▄[48;5;89;38;5;89m█[48;5;110m▄[38;5;67m▄[48;5;67;38;5;189m▄[48;5;189m███[48;5;110;38;5;110m█████[48;5;67m▄[48;5;60m▄[48;5;89;38;5;67m▄[48;5;60m▄[38;5;110m▄[48;5;67;38;5;67m█[48;5;110;38;5;110m█[38;5;60m▄[48;5;89m▄[48;5;60;38;5;110m▄[38;5;67m▄[49;38;5;60m▄[39m            [38;5;67m▄▄▄▄▄[48;5;67;38;5;110m▄▄▄▄▄[49;38;5;233m▄[39m[00m
      [38;5;89m▀[48;5;176m▄[48;5;89;38;5;67m▄[48;5;67;38;5;189m▄[48;5;189m██[38;5;110m▄[48;5;110;38;5;67m▄[38;5;60m▄▄▄[38;5;67m▄[38;5;110m███████████[48;5;67;38;5;67m██[48;5;60;38;5;60m█[49;38;5;67m▄▄▄▄▄[48;5;67;38;5;110m▄▄▄▄▄▄[48;5;110m█[38;5;67m▄[38;5;233m▄▄▄[48;5;67m▄[49m▀▀▀▀[39m [00m
        [48;5;67;38;5;60m▄[48;5;189;38;5;110m▄[48;5;110;38;5;60m▄[48;5;60;38;5;233m▄▄[48;5;231m▄[48;5;188;38;5;231m▄[48;5;233;38;5;233m███[48;5;60m▄[48;5;110;38;5;60m▄[38;5;110m████████[38;5;67m▄[48;5;67m███[38;5;233m▄[48;5;110;38;5;67m▄▄[38;5;233m▄▄▄[48;5;67m▄[49m▀▀▀▀▀▀[39m         [00m
         [48;5;241;38;5;241m█[48;5;231;38;5;231m█[48;5;233;38;5;60m▄[48;5;241m▄[38;5;233m▄[38;5;241m██[48;5;233;38;5;233m█[38;5;188m▄[38;5;233m█[48;5;60;38;5;60m█[48;5;110;38;5;110m███[38;5;67m▄▄▄[48;5;67m█████[38;5;233m▄[48;5;233;38;5;67m▄▄▄[48;5;60m▄▄[49;38;5;60m▄▄[39m               [00m
          [38;5;241m▀[39m  [38;5;241m▄[48;5;233;38;5;188m▄[48;5;241;38;5;233m▄[48;5;233m█[48;5;231;38;5;60m▄[48;5;188;38;5;67m▄[48;5;67;38;5;110m▄[48;5;110m█[38;5;60m▄[48;5;67;38;5;233m▄▄[38;5;67m████[38;5;233m▄▄[49m▀▀▀▀[48;5;67m▄▄▄▄[38;5;67m█[48;5;60m▄▄▄[49;38;5;60m▄▄▄[39m         [00m
             [38;5;241m▀[48;5;231;38;5;233m▄[48;5;60;38;5;110m▄▄[48;5;110m█[38;5;233m▄[48;5;67m▄[49m▀[39m   [38;5;233m▀▀▀▀[39m          [38;5;233m▀▀▀[48;5;67m▄▄▄[38;5;67m█[48;5;60m▄▄▄[49;38;5;60m▄[39m     [00m
               [38;5;233m▀▀▀[39m                          [38;5;233m▀▀▀▀[39m      [00m
                                                      [00m
