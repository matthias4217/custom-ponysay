$$$
NAME: Totodile
OTHER NAMES: ワニノコ, 리아코, Waninoko, 小鋸鱷, Kaiminus, Karnimani
APPEARANCE: Pokémon Generation II
KIND: Big Jaw Pokémon
GROUP: water1, monster
BALLOON: top
COAT: blue
DISPLAY: full
BALLOON TOP: 3
BALLOON BOTTOM: 0
POSE: master
BASED ON: Totodile
SOURCE: PokéAPI Sprites
MEDIA: Pokémon
LICENSE: Copyright The Pokémon Company
FREE: no
WIDTH: 72
HEIGHT: 21


$$$
$balloon5$
         $\$                                                              [00m
          $\$                                                             [00m
           $\$                                                            [00m
            $\$                           [00m
           [38;5;110m▄▄[48;5;32;38;5;117m▄▄[38;5;32m█[49m▄[48;5;32;38;5;110m▄[48;5;60m▄▄[49;38;5;32m▄[38;5;233m▄[39m                  [00m
          [48;5;110;38;5;110m█[48;5;117;38;5;117m██[38;5;60m▄[48;5;60;38;5;110m▄[48;5;110m███[38;5;32m▄▄[38;5;110m██[48;5;233;38;5;60m▄[49;39m                 [00m
         [48;5;32;38;5;32m█[48;5;117;38;5;110m▄[48;5;110m█[48;5;60;38;5;60m█[48;5;110;38;5;110m██[38;5;60m▄[48;5;60m█[48;5;32;38;5;231m▄[48;5;231m██[48;5;32;38;5;249m▄[48;5;110;38;5;60m▄[38;5;110m█[48;5;233;38;5;233m█[49;38;5;88m▄▄▄▄▄[39m           [00m
     [38;5;110m▄▄[39m [38;5;60m▄[48;5;32;38;5;110m▄[48;5;110m███[38;5;60m▄[48;5;60m█[38;5;233m▄[48;5;249;38;5;231m▄[48;5;131;38;5;131m█[48;5;231;38;5;210m▄[38;5;231m██[48;5;60;38;5;60m█[48;5;32m▄[48;5;60;38;5;110m▄[48;5;167;38;5;233m▄[38;5;167m██[38;5;88m▄[49m▀[39m           [00m
   [38;5;32m▄[48;5;110;38;5;117m▄[48;5;117m█[38;5;110m▄[48;5;60m▄▄[48;5;32m▄[48;5;110m██[48;5;60;38;5;60m███[48;5;233;38;5;233m█[48;5;210;38;5;210m█[48;5;233;38;5;233m█[48;5;131;38;5;131m█[48;5;231;38;5;231m█[38;5;249m▄[48;5;60;38;5;60m█[38;5;110m▄[48;5;110m█[48;5;233;38;5;60m▄[48;5;167;38;5;167m█[38;5;88m▄[49m▀[39m            [00m
   [48;5;60;38;5;60m█[48;5;117;38;5;110m▄[48;5;110m█[38;5;32m▄▄[38;5;110m█████[48;5;233m▄▄▄[48;5;249m▄[48;5;233m▄[48;5;210m▄[48;5;231m▄[48;5;32m▄[48;5;110;38;5;60m▄[38;5;110m███[48;5;233;38;5;233m█[48;5;88;38;5;167m▄[49;38;5;88m▄▄[39m           [00m
   [38;5;233m▀[48;5;110m▄[38;5;110m█[48;5;60m▄[48;5;110m█[38;5;60m▄[38;5;233m▄[49;38;5;60m▀[38;5;233m▀[38;5;60m▀[48;5;110m▄[38;5;233m▄▄▄▄▄[48;5;60m▄[48;5;233m█[48;5;110;38;5;110m████[48;5;60m▄[48;5;167;38;5;233m▄[38;5;167m██[48;5;88m▄[38;5;233m▄[49;38;5;60m▄▄[39m       [00m
    [48;5;231;38;5;233m▄[49m▀▀▀[48;5;231m▄[48;5;233m█[49;39m   [48;5;233;38;5;233m█[48;5;249;38;5;131m▄[48;5;88m▄[48;5;131m██[38;5;231m▄[48;5;88;38;5;233m▄[48;5;233;38;5;110m▄[48;5;110m█[38;5;32m▄[38;5;110m███[48;5;233;38;5;32m▄[38;5;110m▄▄[48;5;60m▄[48;5;117;38;5;32m▄[48;5;60;38;5;110m▄[48;5;117;38;5;60m▄[48;5;233;38;5;233m█[49;39m      [00m
        [38;5;32m▄[48;5;32;38;5;117m▄[49;38;5;32m▄▄[48;5;233;38;5;233m█[48;5;249;38;5;131m▄[48;5;131;38;5;210m▄[48;5;210m██[38;5;131m▄[48;5;131;38;5;231m▄[48;5;233;38;5;233m█[48;5;110;38;5;110m██[48;5;60;38;5;60m█[48;5;110;38;5;137m▄[48;5;137;38;5;221m▄[48;5;32;38;5;32m█[48;5;110;38;5;110m██████[48;5;117;38;5;233m▄[49m▀[39m      [00m
        [38;5;32m▀[48;5;117m▄[38;5;117m█[48;5;110;38;5;233m▄[48;5;233;38;5;231m▄[48;5;231;38;5;249m▄[48;5;210;38;5;210m██[38;5;88m▄[48;5;131;38;5;231m▄[48;5;88;38;5;233m▄[48;5;60;38;5;110m▄[48;5;110m█[38;5;60m▄[48;5;60;38;5;221m▄[48;5;221m██[48;5;60;38;5;60m█[48;5;110;38;5;110m████[48;5;32;38;5;32m█[48;5;117;38;5;233m▄[49m▀[39m       [00m
        [38;5;60m▄[48;5;32;38;5;110m▄[48;5;110m█[48;5;233m▄[38;5;233m█[38;5;110m▄[48;5;131;38;5;233m▄[48;5;88m▄[48;5;231m▄[38;5;60m▄[48;5;60;38;5;110m▄[48;5;110m█[38;5;60m▄[48;5;60;38;5;137m▄[48;5;221;38;5;221m████[48;5;32m▄[48;5;110;38;5;60m▄▄[38;5;233m▄[48;5;233;38;5;32m▄[49;38;5;233m▄[39m   [38;5;88m▄[48;5;88;38;5;167m▄[48;5;233;38;5;233m█[49;39m  [00m
         [38;5;60m▀▀▀[38;5;110m▀[38;5;60m▀[48;5;110;38;5;233m▄▄▄▄▄[48;5;233;38;5;137m▄[48;5;137m█[48;5;221;38;5;221m██[38;5;137m▄▄[48;5;137;38;5;110m▄▄[48;5;110;38;5;32m▄▄[48;5;32;38;5;110m▄[38;5;32m██[48;5;233m▄[49;38;5;233m▄[48;5;88;38;5;167m▄[48;5;167m██[48;5;233;38;5;233m█[49;39m  [00m
             [38;5;32m▄[48;5;60;38;5;110m▄[48;5;233;38;5;233m█[48;5;137;38;5;32m▄[38;5;137m█[48;5;233;38;5;233m█[48;5;137;38;5;137m█[48;5;221;38;5;221m█[38;5;137m▄[48;5;137;38;5;110m▄[48;5;110m██[38;5;60m▄[48;5;32;38;5;110m▄[48;5;110m███[48;5;60m▄[48;5;32;38;5;60m▄[38;5;32m███[48;5;233m▄▄▄[48;5;60m▄[49;38;5;233m▄[39m[00m
            [38;5;60m▄[48;5;32;38;5;110m▄[48;5;110m██[48;5;233;38;5;233m█[48;5;32;38;5;32m█[48;5;137m▄[48;5;233m▄[48;5;137m▄[48;5;110m▄[38;5;110m██[38;5;60m▄[48;5;60;38;5;110m▄[48;5;110m█████[48;5;60m▄[48;5;32;38;5;60m▄[38;5;32m█████[38;5;233m▄[49m▀[39m[00m
            [48;5;60;38;5;60m█[48;5;110;38;5;32m▄[48;5;32m███[48;5;60m▄[48;5;32;38;5;233m▄[38;5;60m▄[38;5;32m███[48;5;110m▄[48;5;60;38;5;233m▄[48;5;110;38;5;32m▄[38;5;110m██████[48;5;233;38;5;233m█[48;5;32m▄▄▄[49;38;5;60m▀[38;5;233m▀[39m  [00m
           [38;5;233m▄[38;5;32m▄[48;5;60;38;5;233m▄[38;5;32m▄[48;5;32;38;5;60m▄▄[38;5;32m███[48;5;233;38;5;233m█[49;38;5;60m▀[48;5;32;38;5;233m▄▄[48;5;233m█[48;5;32m▄[38;5;32m██[48;5;110m▄▄▄[48;5;32;38;5;233m▄[49m▀[39m       [00m
           [38;5;233m▀[48;5;110m▄[48;5;32;38;5;60m▄[48;5;110;38;5;110m█[48;5;32;38;5;32m█[38;5;60m▄[38;5;233m▄[49;38;5;60m▀[38;5;233m▀[39m      [48;5;233;38;5;233m█[48;5;60;38;5;32m▄[48;5;32;38;5;110m▄[38;5;32m█[48;5;60;38;5;110m▄[48;5;233;38;5;32m▄[38;5;110m▄[49;38;5;233m▄[39m      [00m
              [38;5;233m▀▀[39m           [38;5;233m▀[48;5;110;38;5;60m▄[48;5;60;38;5;233m▄[48;5;110m▄[48;5;60m▄[48;5;110m▄[49m▀[39m      [00m
                                        [00m
