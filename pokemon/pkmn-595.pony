$$$
NAME: Joltik
OTHER NAMES: Wattzapf, 파쪼옥, バチュル, Statitik, 電電蟲
APPEARANCE: Pokémon Generation V
KIND: Attaching Pokémon
GROUP: bug
BALLOON: top
COAT: yellow
DISPLAY: full
BALLOON TOP: 3
BALLOON BOTTOM: 0
POSE: master
BASED ON: Joltik
SOURCE: PokéAPI Sprites
MEDIA: Pokémon
LICENSE: Copyright The Pokémon Company
FREE: no
WIDTH: 75
HEIGHT: 19


$$$
$balloon5$
                   $\$                                                        [00m
                    $\$                                                       [00m
                     $\$                                                      [00m
                      $\$                     [00m
                     [38;5;94m▄▄[48;5;94;38;5;221m▄▄[48;5;233;38;5;233m█[49;39m   [38;5;233m▄[38;5;94m▄[48;5;94;38;5;221m▄[49;38;5;94m▄[39m           [00m
                 [38;5;94m▄[39m [38;5;94m▄[48;5;94;38;5;221m▄[48;5;221m███[48;5;178;38;5;94m▄[48;5;94;38;5;221m▄[48;5;233m▄[48;5;94;38;5;233m▄[48;5;233;38;5;221m▄[48;5;221m███[48;5;233;38;5;178m▄[49;38;5;233m▄[39m          [00m
                [48;5;94;38;5;94m█[48;5;221;38;5;221m█[48;5;94;38;5;178m▄[48;5;221;38;5;221m███████[48;5;94m▄[48;5;221m██████[48;5;233m▄[49;38;5;94m▄▄[38;5;233m▄[39m       [00m
             [38;5;233m▄[38;5;94m▄[48;5;94;38;5;221m▄[48;5;221m███████████████████[38;5;233m▄[49m▀[39m       [00m
            [48;5;94;38;5;94m█[48;5;221;38;5;221m█[48;5;233m▄[48;5;221;38;5;94m▄[38;5;221m█[38;5;94m▄[48;5;94;38;5;221m▄[48;5;221;38;5;233m▄[38;5;221m██████████[38;5;178m▄[38;5;94m▄▄▄[38;5;221m█[48;5;233m▄[48;5;94m▄[38;5;233m▄[49;39m      [00m
            [48;5;233;38;5;233m█[48;5;221;38;5;221m██[48;5;94m▄[38;5;233m▄[48;5;178;38;5;221m▄[48;5;221m█[48;5;94m▄[48;5;221m█████[38;5;94m▄[48;5;94;38;5;221m▄[48;5;233;38;5;233m█[48;5;221;38;5;94m▄[48;5;94;38;5;221m▄▄[48;5;221m██[48;5;233;38;5;233m█[48;5;221;38;5;221m█[38;5;178m▄[48;5;233;38;5;94m▄[49;39m       [00m
        [38;5;94m▄▄[48;5;233;38;5;233m█[48;5;94;38;5;221m▄[48;5;233m▄[48;5;221m████[38;5;236m▄[38;5;101m▄[38;5;221m████████[48;5;94m▄[48;5;221m████[48;5;233;38;5;233m█[48;5;221;38;5;221m█[38;5;178m▄[48;5;178m██[48;5;94;38;5;233m▄[49;39m      [00m
       [48;5;94;38;5;94m█[48;5;178;38;5;178m█[38;5;94m▄▄[48;5;233m▄[48;5;221;38;5;221m█[38;5;101m▄[38;5;236m▄[38;5;221m█[48;5;101;38;5;236m▄[48;5;254;38;5;69m▄[48;5;236;38;5;236m█[48;5;221;38;5;221m███[38;5;101m▄[48;5;236;38;5;254m▄[48;5;101;38;5;236m▄[48;5;221;38;5;221m██████[48;5;94;38;5;233m▄[48;5;233;38;5;221m▄[48;5;94m▄[48;5;178;38;5;233m▄[38;5;178m█[38;5;233m▄[48;5;233;38;5;178m▄[49;38;5;94m▄▄[39m    [00m
     [38;5;94m▄[48;5;94;38;5;178m▄[48;5;178m█[48;5;233;38;5;233m█[48;5;221;38;5;178m▄[38;5;221m██[48;5;101;38;5;101m█[48;5;254;38;5;254m█[38;5;62m▄[48;5;236m▄[48;5;101;38;5;178m▄[48;5;236;38;5;221m▄[48;5;101m▄[48;5;221m███[48;5;236;38;5;101m▄[48;5;69;38;5;236m▄[48;5;101;38;5;221m▄[48;5;221;38;5;254m▄[48;5;236m▄[38;5;62m▄[48;5;221;38;5;236m▄[38;5;221m███[38;5;233m▄[48;5;233;38;5;94m▄[48;5;178;38;5;178m██[48;5;94m▄[48;5;178m███[48;5;94m▄[49;38;5;94m▄[39m  [00m
     [48;5;94;38;5;94m█[48;5;178;38;5;178m█[38;5;233m▄[48;5;94;38;5;178m▄[48;5;233;38;5;233m█[48;5;221;38;5;178m▄[38;5;221m█[48;5;101;38;5;178m▄[48;5;62;38;5;62m█[48;5;233;38;5;233m█[48;5;62;38;5;62m█[48;5;101;38;5;101m█[48;5;221;38;5;221m███████[48;5;178;38;5;236m▄[48;5;254;38;5;62m▄[48;5;62;38;5;233m▄[38;5;62m█[48;5;236;38;5;236m█[48;5;221;38;5;221m█████[48;5;94m▄▄[48;5;178;38;5;233m▄[38;5;178m████[48;5;94m▄[49;38;5;94m▄[39m [00m
     [38;5;233m▄[48;5;233;38;5;178m▄[48;5;221;38;5;221m███[48;5;233m▄[48;5;221;38;5;233m▄[38;5;221m█[48;5;236m▄[48;5;62;38;5;236m▄▄[48;5;101;38;5;221m▄[48;5;221m███████[48;5;236;38;5;236m█[48;5;62;38;5;62m█[48;5;233m▄[48;5;62;38;5;236m▄[48;5;101;38;5;221m▄[48;5;221m████[38;5;178m▄[38;5;233m▄[48;5;233;38;5;221m▄[48;5;94m▄[48;5;178;38;5;94m▄▄[38;5;178m███[38;5;94m▄[49m▀[39m[00m
   [38;5;233m▄[48;5;94;38;5;178m▄[48;5;233m▄[48;5;221m▄▄[38;5;221m██[38;5;178m▄[48;5;233;38;5;233m█[48;5;221;38;5;221m█[48;5;94;38;5;178m▄[48;5;178;38;5;94m▄[48;5;221;38;5;178m▄[38;5;221m█████████[48;5;236m▄▄[48;5;221m███[38;5;233m▄▄[48;5;233;38;5;178m▄[38;5;221m▄[48;5;221m████[38;5;178m▄[48;5;233m▄[48;5;178;38;5;233m▄[48;5;94;38;5;69m▄[48;5;233;38;5;233m█[49;39m [00m
    [38;5;233m▀[48;5;233;38;5;236m▄[48;5;178;38;5;233m▄[38;5;94m▄[38;5;178m███[38;5;94m▄[49;38;5;233m▀▀[39m [48;5;233;38;5;233m█[48;5;221;38;5;221m██[38;5;94m▄[48;5;233;38;5;233m█[48;5;221;38;5;221m███[38;5;233m▄[48;5;94;38;5;178m▄[48;5;178m█[38;5;94m▄[48;5;94m█[48;5;221;38;5;233m▄[38;5;221m█[48;5;233;38;5;233m█[48;5;178;38;5;94m▄[48;5;94m█[48;5;221;38;5;221m███[38;5;178m▄▄[48;5;178m██[48;5;94;38;5;94m█[49;38;5;233m▀[39m  [00m
     [38;5;236m▀[48;5;69m▄[48;5;233;38;5;69m▄[48;5;178;38;5;233m▄[48;5;233;38;5;69m▄[38;5;236m▄[49;38;5;94m▀[39m    [48;5;94;38;5;94m█[48;5;221;38;5;233m▄[49m▀[39m [48;5;94;38;5;233m▄[48;5;221;38;5;221m█[38;5;233m▄[49m▀[38;5;94m▀▀[39m   [38;5;233m▀▀[39m [38;5;94m▀[38;5;233m▀[48;5;178m▄[38;5;178m█[38;5;94m▄[38;5;178m██[38;5;233m▄[38;5;178m█[48;5;233;38;5;233m█[49;39m  [00m
      [38;5;236m▀[48;5;69;38;5;233m▄▄[49m▀[39m           [38;5;233m▀[39m            [38;5;233m▀▀[48;5;94;38;5;236m▄[48;5;178;38;5;69m▄[48;5;233m▄[48;5;69m█[48;5;236;38;5;236m█[49;39m   [00m
                                     [38;5;233m▀[48;5;69m▄[48;5;236m▄[49;39m    [00m
                                            [00m
