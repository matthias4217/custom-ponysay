$$$
NAME: Inkay
OTHER NAMES: Sepiatop, Iscalar, 오케이징, 好啦魷, マーイーカ
APPEARANCE: Pokémon Generation VI
KIND: Revolving Pokémon
GROUP: water1, water2
BALLOON: top
COAT: blue
DISPLAY: full
BALLOON TOP: 3
BALLOON BOTTOM: 0
POSE: master
BASED ON: Inkay
SOURCE: PokéAPI Sprites
MEDIA: Pokémon
LICENSE: Copyright The Pokémon Company
FREE: no
WIDTH: 73
HEIGHT: 25


$$$
$balloon5$
                     $\$                                                   [00m
                      $\$                                                  [00m
                       $\$                                                 [00m
                        $\$                     [00m
                       [38;5;59m▄▄[48;5;59;38;5;231m▄[38;5;252m▄[38;5;231m▄[49;38;5;59m▄[39m                 [00m
                     [38;5;59m▄[48;5;59;38;5;231m▄[48;5;231m██[48;5;252;38;5;252m█[48;5;231;38;5;231m███[48;5;59m▄[49;38;5;233m▄[39m               [00m
                   [38;5;59m▄[48;5;59;38;5;231m▄[48;5;231m███[48;5;252;38;5;252m█[48;5;231;38;5;231m██████[48;5;233m▄[49;38;5;233m▄[39m             [00m
                 [38;5;59m▄[48;5;59;38;5;231m▄[48;5;231m██[38;5;59m▄▄[48;5;59;38;5;231m▄▄▄▄[48;5;231;38;5;59m▄▄[38;5;231m████[48;5;233m▄[49;38;5;233m▄[39m           [00m
               [38;5;233m▄[48;5;233;38;5;252m▄[48;5;231;38;5;231m██[38;5;59m▄[48;5;59;38;5;231m▄[48;5;231m██[38;5;224m▄▄▄▄[38;5;231m██[48;5;59m▄[48;5;231;38;5;59m▄[38;5;231m████[48;5;233;38;5;252m▄[49;38;5;233m▄[39m         [00m
              [48;5;233;38;5;233m█[48;5;252;38;5;252m██[38;5;59m▄[48;5;59;38;5;231m▄[48;5;231m█[38;5;224m▄[48;5;224m██████[48;5;217;38;5;217m██[48;5;231m▄[38;5;231m█[48;5;59m▄[48;5;231;38;5;59m▄[38;5;252m▄[48;5;252m███[48;5;233m▄[49;38;5;233m▄[39m       [00m
               [48;5;233;38;5;233m█[48;5;59;38;5;231m▄[48;5;231m█[38;5;217m▄[48;5;224;38;5;224m███████[38;5;217m▄[48;5;217m████[48;5;231m▄[38;5;231m█[48;5;59m▄[48;5;252;38;5;59m▄[38;5;252m██[38;5;233m▄[49m▀[39m       [00m
             [38;5;59m▄[48;5;59;38;5;231m▄[48;5;231m█[38;5;217m▄[48;5;217;38;5;228m▄[48;5;228m██[48;5;224m▄[38;5;217m▄▄▄▄[48;5;217;38;5;228m▄[48;5;228m██[48;5;217m▄[38;5;217m████[48;5;231m▄[38;5;231m█[48;5;59m▄[48;5;233;38;5;233m█[49;39m         [00m
           [38;5;59m▄[48;5;59;38;5;231m▄[48;5;231m█[38;5;217m▄[48;5;217m██[48;5;228;38;5;228m████[48;5;217;38;5;217m████[48;5;228;38;5;228m████[48;5;217;38;5;217m████[38;5;228m▄[38;5;217m█[48;5;231m▄[38;5;231m█[48;5;233m▄[49;38;5;233m▄[39m       [00m
          [38;5;59m▄[48;5;59;38;5;231m▄[48;5;231m█[48;5;228;38;5;228m█[48;5;217;38;5;217m████[48;5;228m▄▄[48;5;217m██████[48;5;228m▄▄[48;5;217m█████[48;5;228;38;5;228m██[48;5;217;38;5;217m█[48;5;231m▄[38;5;231m█[48;5;233m▄[49;38;5;233m▄[39m      [00m
         [38;5;59m▄[48;5;59;38;5;231m▄[48;5;231m█[48;5;217;38;5;217m█[48;5;228m▄[48;5;217m██[38;5;95m▄▄▄[48;5;95;38;5;110m▄▄▄▄▄▄▄[38;5;252m▄[48;5;217;38;5;95m▄▄▄[38;5;217m███[48;5;228m▄[38;5;228m█[48;5;217;38;5;217m██[48;5;231m▄[38;5;231m█[48;5;233m▄[49;38;5;233m▄[39m     [00m
         [48;5;59;38;5;59m█[48;5;231;38;5;231m█[48;5;217;38;5;217m███[38;5;95m▄[48;5;95;38;5;252m▄[48;5;252;38;5;236m▄[48;5;236m█[48;5;231;38;5;231m█[48;5;236;38;5;236m█[48;5;110;38;5;110m█[38;5;175m▄▄[38;5;110m█[38;5;252m▄[48;5;252;38;5;236m▄[48;5;236m█[48;5;231;38;5;231m█[48;5;236;38;5;236m█[48;5;252m▄[48;5;95;38;5;252m▄[38;5;24m▄[48;5;217;38;5;95m▄[38;5;217m█████[48;5;231;38;5;231m██[48;5;233;38;5;233m█[49;39m     [00m
         [38;5;59m▀[48;5;231m▄[48;5;252;38;5;252m█[48;5;217;38;5;217m█[38;5;24m▄[48;5;95;38;5;110m▄[48;5;231;38;5;231m█[48;5;236;38;5;236m█[38;5;59m▄▄[38;5;236m█[48;5;95;38;5;175m▄[48;5;217;38;5;95m▄▄[48;5;95;38;5;175m▄[48;5;231;38;5;231m█[48;5;236;38;5;236m█[38;5;59m▄▄[38;5;236m█[38;5;252m▄[48;5;231;38;5;231m█[48;5;24;38;5;24m█[48;5;110;38;5;110m█[48;5;95;38;5;95m█[48;5;217;38;5;217m███[38;5;252m▄[48;5;231m▄[38;5;231m█[48;5;233;38;5;233m█[49;39m     [00m
          [38;5;59m▀[48;5;252m▄[38;5;252m█[48;5;24;38;5;24m█[48;5;110;38;5;117m▄[48;5;231;38;5;24m▄[38;5;231m█[48;5;236m▄▄[48;5;231;38;5;110m▄[48;5;175;38;5;117m▄[48;5;217;38;5;175m▄▄[48;5;175;38;5;117m▄[48;5;110m▄[48;5;231;38;5;110m▄[48;5;236;38;5;231m▄▄[48;5;252m▄[48;5;231;38;5;24m▄[48;5;110;38;5;117m▄[48;5;117m██[48;5;59;38;5;59m█[48;5;252;38;5;252m██[48;5;233;38;5;233m█[48;5;252;38;5;252m██[48;5;233;38;5;233m█[49;39m      [00m
           [48;5;59;38;5;59m█[48;5;252;38;5;252m█[48;5;24;38;5;24m█[48;5;117;38;5;117m██[48;5;24m▄▄▄[48;5;117m███████[48;5;24m▄▄▄[48;5;117m████[48;5;59;38;5;110m▄[48;5;252;38;5;59m▄[38;5;252m█[48;5;233;38;5;233m█[48;5;252m▄[49m▀[39m       [00m
          [48;5;59;38;5;59m█[48;5;252;38;5;252m██[48;5;24;38;5;24m█[48;5;117;38;5;110m▄[48;5;24;38;5;24m█[48;5;117;38;5;117m██[38;5;24m▄[38;5;117m████████[38;5;24m▄[38;5;117m██[38;5;110m▄▄[48;5;24;38;5;24m█[48;5;110;38;5;110m█[48;5;59;38;5;59m█[48;5;252;38;5;252m█[48;5;233m▄[49;38;5;233m▄[39m        [00m
        [38;5;59m▄[48;5;59;38;5;231m▄[48;5;231m██[48;5;233;38;5;233m█[48;5;24;38;5;24m█[48;5;110;38;5;110m█[48;5;24;38;5;24m█[48;5;110;38;5;110m██[48;5;24;38;5;24m█[48;5;117;38;5;110m▄▄▄▄▄▄▄▄[48;5;24;38;5;24m█[48;5;110;38;5;110m████[48;5;24;38;5;24m█[48;5;110;38;5;110m█[48;5;233;38;5;233m█[48;5;252;38;5;252m█[38;5;231m▄[48;5;233m▄[49;38;5;233m▄[39m       [00m
    [38;5;59m▄▄[48;5;59;38;5;231m▄▄[48;5;231m███[38;5;233m▄[49m▀[39m [38;5;24m▀[48;5;110;38;5;233m▄[48;5;24m▄[48;5;110;38;5;110m██[48;5;24;38;5;24m█[48;5;110;38;5;110m███████[48;5;24;38;5;24m█[48;5;110;38;5;110m███[48;5;24;38;5;24m█[48;5;110;38;5;110m█[48;5;233;38;5;233m█[49;39m [48;5;233;38;5;233m█[48;5;231;38;5;231m███[48;5;233m▄[49;38;5;233m▄▄[39m    [00m
   [48;5;59;38;5;59m█[48;5;231;38;5;231m██████[38;5;233m▄[49m▀[39m     [38;5;233m▀[48;5;110m▄[38;5;110m█[48;5;24m▄[48;5;110;38;5;24m▄[38;5;110m█████[48;5;24;38;5;24m█[48;5;110;38;5;110m██[48;5;24;38;5;233m▄[48;5;110m▄[49m▀[39m   [48;5;233;38;5;233m█[48;5;231;38;5;231m█████[48;5;233m▄▄[49;38;5;233m▄[39m [00m
   [48;5;59;38;5;59m█[48;5;231;38;5;231m███[38;5;233m▄▄[49m▀[39m         [38;5;233m▀▀[48;5;110m▄[48;5;24;38;5;110m▄[48;5;110;38;5;24m▄[38;5;110m██[48;5;24;38;5;24m█[48;5;110;38;5;110m██[48;5;233;38;5;233m█[49;39m       [38;5;233m▀[48;5;231m▄[38;5;231m██████[48;5;233;38;5;233m█[49;39m[00m
   [38;5;59m▀[38;5;233m▀▀▀[39m               [38;5;233m▀[48;5;110m▄[48;5;24m▄[48;5;110;38;5;110m█[48;5;24;38;5;24m█[48;5;110;38;5;233m▄[48;5;233m█[49;39m          [38;5;233m▀▀[48;5;231m▄▄▄▄[48;5;233m█[49;39m[00m
                         [38;5;233m▀▀[39m                   [00m
                                              [00m
