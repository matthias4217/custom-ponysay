$$$
NAME: Froakie
OTHER NAMES: 개구마르, Grenousse, 呱呱泡蛙, ケロマツ, Froxy
APPEARANCE: Pokémon Generation VI
KIND: Bubble Frog Pokémon
GROUP: water1
BALLOON: top
COAT: blue
DISPLAY: full
BALLOON TOP: 3
BALLOON BOTTOM: 0
POSE: master
BASED ON: Froakie
SOURCE: PokéAPI Sprites
MEDIA: Pokémon
LICENSE: Copyright The Pokémon Company
FREE: no
WIDTH: 67
HEIGHT: 21


$$$
$balloon5$
      $\$                                                            [00m
       $\$                                                           [00m
        $\$                                                          [00m
         $\$                         [00m
         [38;5;67m▄[48;5;67;38;5;117m▄▄▄▄[49;38;5;67m▄[39m      [38;5;67m▄[38;5;67m▄[48;5;67;38;5;117m▄▄▄[38;5;67m▄[49;38;5;67m▄[39m       [00m
        [48;5;67;38;5;67m█[48;5;117;38;5;67m▄[48;5;67;38;5;222m▄▄[48;5;117;38;5;67m▄[38;5;117m██[48;5;67;38;5;67m█[49;39m    [48;5;67;38;5;67m█[48;5;67;38;5;67m█[48;5;117;38;5;179m▄[48;5;67;38;5;222m▄▄[48;5;117;38;5;67m▄[38;5;117m██[48;5;67;38;5;67m▄[49;39m      [00m
       [48;5;67;38;5;67m█[48;5;67;38;5;179m▄[48;5;186;38;5;222m▄[48;5;222m█[38;5;179m▄[48;5;186;38;5;222m▄[48;5;67;38;5;179m▄[48;5;117;38;5;117m█[48;5;67;38;5;67m█[48;5;67;38;5;67m█[49;39m  [48;5;67;38;5;67m█[48;5;67;38;5;67m█[48;5;179;38;5;186m▄[48;5;222;38;5;222m███[48;5;186m▄[48;5;67;38;5;67m█[48;5;117;38;5;117m██[48;5;67;38;5;67m█[49;39m     [00m
       [48;5;67;38;5;67m█[48;5;186;38;5;222m▄[48;5;222m█[48;5;179;38;5;234m▄[48;5;234;38;5;146m▄[48;5;179;38;5;234m▄[38;5;179m█[48;5;67;38;5;67m██[48;5;234;38;5;234m█[49;39m  [48;5;67;38;5;67m█[48;5;179;38;5;179m█[48;5;186;38;5;234m▄[48;5;234;38;5;146m▄[48;5;179;38;5;234m▄[48;5;222;38;5;222m██[48;5;186m▄[48;5;117;38;5;67m▄[48;5;67m█[48;5;234;38;5;234m█[49;39m     [00m
       [48;5;67;38;5;67m█[48;5;222;38;5;222m██[48;5;234;38;5;234m█[48;5;231;38;5;231m█[48;5;234;38;5;234m█[48;5;179;38;5;179m█[48;5;67;38;5;67m██[48;5;234;38;5;234m█[49m▄▄[48;5;67;38;5;67m█[48;5;179;38;5;179m█[48;5;234;38;5;234m█[48;5;231;38;5;231m█[48;5;234;38;5;234m█[48;5;222;38;5;222m███[48;5;67;38;5;67m██[48;5;234;38;5;234m█[49;39m     [00m
       [38;5;67m▀[48;5;186m▄[48;5;222;38;5;222m█[48;5;234;38;5;179m▄[48;5;146;38;5;234m▄[48;5;234;38;5;146m▄[48;5;67m▄[48;5;67;38;5;67m▄[48;5;234;38;5;25m▄[48;5;25m██[38;5;67m▄[48;5;67m▄[48;5;179;38;5;179m█[48;5;234;38;5;234m█[48;5;146m▄[48;5;234m█[48;5;222;38;5;222m██[48;5;186;38;5;67m▄[48;5;67m█[48;5;234;38;5;234m█[48;5;146;38;5;189m▄[49;38;5;146m▄[39m    [00m
      [38;5;67m▄[48;5;67;38;5;117m▄▄[48;5;186m▄[48;5;222;38;5;186m▄[48;5;146;38;5;231m▄[48;5;231m██[48;5;146m▄[48;5;231m██[48;5;146m▄[48;5;117;38;5;146m▄[38;5;117m█[48;5;179m▄[38;5;179m█[48;5;234;38;5;222m▄[48;5;179m▄[48;5;222m█[48;5;186;38;5;67m▄[48;5;67m█[38;5;117m▄▄[48;5;67m▄[48;5;189;38;5;67m▄[48;5;146;38;5;146m█[49m▄[39m  [00m
     [48;5;234;38;5;234m█[48;5;117;38;5;74m▄[38;5;117m████[48;5;231m▄[38;5;231m██[38;5;146m▄[38;5;231m██[38;5;146m▄[48;5;146;38;5;117m▄[48;5;117m███[48;5;179m▄[48;5;186m▄[48;5;67m▄[48;5;117m████[38;5;74m▄[48;5;67;38;5;67m█[48;5;234;38;5;234m█[48;5;231;38;5;231m█[48;5;146m▄[49;38;5;146m▄[39m[00m
      [38;5;234m▀[48;5;67m▄[48;5;117m▄[38;5;74m▄[38;5;117m███[38;5;67m▄▄▄▄[38;5;67m▄[38;5;117m██████[38;5;74m▄[38;5;67m▄▄[48;5;74m▄[48;5;67;38;5;234m▄▄[48;5;234;38;5;189m▄[48;5;189m█[48;5;231m▄▄[48;5;102;38;5;102m█[49;39m[00m
         [48;5;234;38;5;102m▄[38;5;189m▄[48;5;67;38;5;67m▄[38;5;234m▄▄[38;5;67m███[48;5;67m▄▄[48;5;67m████[38;5;234m▄▄▄[48;5;234;38;5;189m▄▄[48;5;189m████[38;5;102m▄[49m▀[39m [00m
         [38;5;102m▀[48;5;189m▄[38;5;189m█[38;5;146m▄[38;5;189m█[48;5;234m▄▄▄▄▄▄▄▄[48;5;102m▄[48;5;189m█[38;5;102m▄[48;5;146;38;5;67m▄[48;5;189;38;5;189m████[48;5;102;38;5;102m█[49m▀[39m   [00m
          [38;5;67m▄[48;5;67;38;5;117m▄[48;5;67;38;5;67m█[48;5;102;38;5;234m▄[48;5;189;38;5;102m▄▄[48;5;146;38;5;189m▄[48;5;189;38;5;231m▄▄[38;5;189m██[38;5;102m▄▄[48;5;102;38;5;67m▄[48;5;67;38;5;234m▄[38;5;67m█[48;5;67;38;5;117m▄[48;5;189;38;5;67m▄[38;5;234m▄[48;5;102m▄[49;38;5;67m▄[39m    [00m
         [48;5;67;38;5;67m█[48;5;117;38;5;117m█[38;5;234m▄[48;5;234;38;5;67m▄[48;5;67m█[48;5;67m▄[48;5;102;38;5;102m█[48;5;231;38;5;231m████[48;5;189;38;5;189m█[48;5;102;38;5;102m█[48;5;67;38;5;67m███[48;5;234;38;5;234m█[48;5;117;38;5;67m▄[48;5;67;38;5;117m▄[48;5;67;38;5;67m▄[38;5;67m██[48;5;234;38;5;234m█[49;39m   [00m
        [48;5;67;38;5;67m█[48;5;117;38;5;117m█[48;5;67;38;5;234m▄[48;5;234m█[48;5;67;38;5;67m██[38;5;67m▄[38;5;67m█[48;5;102m▄[48;5;231;38;5;102m▄▄[48;5;189m▄[48;5;102;38;5;67m▄[48;5;67m███[48;5;67m▄[48;5;67m█[48;5;234;38;5;234m█[48;5;117;38;5;117m█[48;5;67;38;5;67m█[48;5;67;38;5;67m█[48;5;234;38;5;234m█[49;39m    [00m
    [38;5;234m▄▄▄[48;5;234;38;5;146m▄[48;5;189;38;5;231m▄[48;5;231m█[48;5;234;38;5;234m█[49;38;5;67m▄[48;5;234m▄[48;5;67m▄[38;5;67m█[48;5;67m▄▄[48;5;67;38;5;67m▄▄[38;5;67m████[38;5;67m▄▄[48;5;67;38;5;234m▄[48;5;234;38;5;189m▄[48;5;117;38;5;231m▄[48;5;67;38;5;234m▄[48;5;234;38;5;67m▄[49m▄[39m    [00m
   [48;5;234;38;5;234m█[48;5;189;38;5;189m█[38;5;234m▄[48;5;146;38;5;231m▄[48;5;231m█[38;5;146m▄[38;5;189m▄[48;5;189;38;5;231m▄[48;5;234m▄[48;5;67;38;5;234m▄▄▄▄[49m▀▀[39m [38;5;234m▀▀[48;5;234m█[38;5;231m▄▄▄[48;5;146;38;5;189m▄[48;5;231;38;5;231m██[48;5;234;38;5;189m▄[48;5;67;38;5;234m▄▄[48;5;67;38;5;67m▄[49;38;5;67m▄[39m  [00m
    [38;5;234m▀▀[48;5;231m▄[48;5;189m▄[49m▀▀[48;5;231m▄▄[49m▀[39m        [38;5;234m▀[48;5;231m▄[48;5;189m▄[48;5;146m▄[48;5;231;38;5;231m██[48;5;189;38;5;234m▄[48;5;231m▄[38;5;231m██[48;5;234;38;5;234m█[49m▀[39m  [00m
                         [38;5;234m▀▀[39m  [38;5;234m▀▀[39m    [00m
                                   [00m
