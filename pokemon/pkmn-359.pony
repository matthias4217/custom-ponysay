$$$
NAME: Absol
OTHER NAMES: アブソル, 阿勃梭魯, 앱솔
APPEARANCE: Pokémon Generation III
KIND: Disaster Pokémon
GROUP: ground
BALLOON: top
COAT: white
DISPLAY: full
BALLOON TOP: 3
BALLOON BOTTOM: 0
POSE: master
BASED ON: Absol
SOURCE: PokéAPI Sprites
MEDIA: Pokémon
LICENSE: Copyright The Pokémon Company
FREE: no
WIDTH: 75
HEIGHT: 33


$$$
$balloon5$
        $\$                                                                    [00m
         $\$                                                                   [00m
          $\$                                                                  [00m
           $\$                                            [00m
            $\$   [38;5;60m▄▄▄▄▄[38;5;237m▄▄▄[39m                                [00m
           [38;5;60m▄▄[48;5;60;38;5;103m▄▄▄[48;5;103m██[38;5;60m▄[38;5;16m▄▄▄[48;5;60m▄[48;5;60m▄[48;5;237m▄▄[49m▄[39m                             [00m
         [38;5;60m▄[48;5;60;38;5;103m▄[48;5;103m███[38;5;237m▄[48;5;60m▄[49m▀▀▀[39m                                   [38;5;237m▄▄[39m[00m
       [38;5;237m▄[48;5;60;38;5;103m▄[48;5;103m██[38;5;60m▄[38;5;16m▄[49;38;5;237m▀[39m                                    [38;5;237m▄▄[48;5;237;38;5;103m▄▄[48;5;103;38;5;16m▄[49m▀[39m[00m
      [38;5;237m▄[48;5;237;38;5;103m▄[48;5;103m█[38;5;60m▄[48;5;60;38;5;60m▄[48;5;16;38;5;16m█[49;39m      [38;5;103m▄▄▄▄▄▄[39m                        [38;5;237m▄[48;5;237;38;5;103m▄[48;5;103m██[38;5;16m▄[49m▀[39m  [00m
      [48;5;237;38;5;237m█[48;5;103;38;5;103m█[38;5;60m▄[48;5;60;38;5;60m█[48;5;16;38;5;16m█[49;39m   [38;5;60m▄[38;5;103m▄[48;5;103;38;5;231m▄▄[48;5;231m██████[48;5;103m▄▄[49;38;5;103m▄[38;5;60m▄[39m                  [38;5;237m▄[48;5;237;38;5;103m▄[48;5;103m███[48;5;16;38;5;16m█[49;39m    [00m
     [48;5;237;38;5;237m█[48;5;103;38;5;103m█[48;5;60;38;5;60m▄[48;5;60m█[48;5;16;38;5;16m█[49;39m  [38;5;60m▄[48;5;60;38;5;231m▄[48;5;231m██[38;5;146m▄[38;5;103m▄[38;5;60m▄[38;5;253m▄[38;5;231m████████[48;5;60m▄[49;38;5;60m▄[39m              [38;5;237m▄[48;5;237;38;5;103m▄[48;5;103m██[38;5;60m▄▄[48;5;16;38;5;16m█[49;39m     [00m
     [48;5;16;38;5;16m█[48;5;60;38;5;60m▄[48;5;60m██[48;5;16m▄[49;38;5;16m▄▄[48;5;16;38;5;231m▄[48;5;231m██[48;5;146;38;5;103m▄[48;5;103m██[48;5;60;38;5;237m▄[48;5;146;38;5;146m█[48;5;231;38;5;231m█████████[48;5;146m▄[48;5;103;38;5;60m▄[49;39m            [38;5;237m▄[48;5;237;38;5;103m▄[48;5;103m█[38;5;60m▄[48;5;60;38;5;60m▄[48;5;60m█[48;5;16;38;5;16m█[49;39m      [00m
     [48;5;16;38;5;16m█[48;5;60;38;5;60m█[38;5;237m▄[38;5;16m▄[38;5;60m██[48;5;16;38;5;16m█[48;5;231;38;5;231m██[48;5;253;38;5;146m▄[48;5;103;38;5;103m██[48;5;60;38;5;60m█[48;5;237;38;5;237m█[48;5;146;38;5;253m▄[48;5;231;38;5;231m█████████[48;5;253;38;5;146m▄[48;5;60m▄[49;38;5;60m▄[39m           [48;5;237;38;5;237m█[48;5;103;38;5;103m█[48;5;60;38;5;60m█[48;5;60;38;5;60m███[48;5;237m▄[49;38;5;237m▄[39m     [00m
      [38;5;16m▀[38;5;237m▀[39m [38;5;16m▀[48;5;60;38;5;237m▄[48;5;16;38;5;16m█[48;5;231;38;5;146m▄[38;5;253m▄[48;5;146;38;5;146m█[48;5;60;38;5;237m▄▄[48;5;237;38;5;103m▄[48;5;253;38;5;231m▄[48;5;231m████████[38;5;253m▄[48;5;253;38;5;146m▄[48;5;146m██[48;5;60;38;5;16m▄[49;39m [38;5;103m▄[38;5;60m▄▄[39m      [48;5;16;38;5;16m█[48;5;103;38;5;60m▄[48;5;60;38;5;60m▄[48;5;60m██[38;5;16m▄▄▄[48;5;16m█[49;39m     [00m
           [38;5;237m▀[48;5;16;38;5;60m▄[48;5;60;38;5;95m▄[48;5;231;38;5;60m▄[48;5;103;38;5;253m▄▄[48;5;231m▄[38;5;146m▄[38;5;237m▄[48;5;253m▄[48;5;237;38;5;60m▄[38;5;95m▄[48;5;103;38;5;16m▄[48;5;253;38;5;103m▄[38;5;146m▄[48;5;146m█████[48;5;16;38;5;16m█[48;5;60;38;5;231m▄[48;5;103m▄[48;5;231m█[38;5;60m▄[49m▀[39m     [48;5;16;38;5;16m█[48;5;60;38;5;60m█[38;5;16m▄[49m▀▀[39m         [00m
          [38;5;60m▄[48;5;60;38;5;231m▄[48;5;60;38;5;60m█[48;5;167;38;5;60m▄[48;5;231;38;5;167m▄[48;5;237;38;5;60m▄[38;5;103m▄[38;5;60m▄▄[48;5;60;38;5;237m▄[48;5;237;38;5;167m▄[48;5;231m▄[48;5;167;38;5;210m▄[48;5;231;38;5;60m▄[48;5;16;38;5;60m▄[48;5;146;38;5;60m▄[38;5;146m████[38;5;16m▄[48;5;16;38;5;146m▄[48;5;146;38;5;231m▄[48;5;103m▄[48;5;60;38;5;60m█[49m▄▄[39m [38;5;60m▄▄▄▄[48;5;16;38;5;237m▄[48;5;60;38;5;16m▄[49m▀[39m           [00m
         [48;5;60;38;5;16m▄[48;5;231m▄[48;5;103;38;5;231m▄[48;5;60m▄[48;5;103;38;5;60m▄[38;5;103m██[38;5;60m▄[48;5;60m███[48;5;60;38;5;103m▄▄▄[48;5;60;38;5;60m█[38;5;60m▄[48;5;16;38;5;16m█[48;5;146;38;5;146m████[48;5;16;38;5;16m█[48;5;146;38;5;146m██[48;5;231;38;5;103m▄[38;5;60m▄[48;5;253;38;5;16m▄[48;5;16;38;5;146m▄[48;5;60;38;5;231m▄[48;5;231m████[48;5;60m▄▄[49;38;5;60m▄[39m           [00m
       [38;5;60m▄[48;5;60;38;5;231m▄[48;5;231m█[38;5;103m▄[38;5;60m▄[38;5;231m█[48;5;60m▄[48;5;60;38;5;60m▄[48;5;103;38;5;60m▄[48;5;60;38;5;16m▄[38;5;237m▄[38;5;60m███[48;5;103m▄[38;5;60m▄[48;5;60;38;5;16m▄[48;5;16;38;5;146m▄[38;5;16m█[48;5;146;38;5;146m███[48;5;16;38;5;16m█[48;5;146;38;5;146m██[38;5;60m▄[38;5;146m██[48;5;60m▄[48;5;146;38;5;60m▄[48;5;253;38;5;146m▄[48;5;231;38;5;231m██████[48;5;253m▄[48;5;60;38;5;60m█[49;39m          [00m
       [38;5;16m▀▀[48;5;16m█[48;5;103;38;5;231m▄[48;5;231;38;5;103m▄[38;5;16m▄[48;5;60m▄[48;5;231;38;5;231m█[48;5;60m▄[38;5;146m▄[48;5;60;38;5;16m▄▄▄[48;5;60m▄[48;5;16;38;5;146m▄▄[48;5;146m█[38;5;60m▄[48;5;60;38;5;146m▄[48;5;146m█[38;5;16m▄[48;5;16;38;5;146m▄[48;5;146m███[48;5;60;38;5;60m█[48;5;16;38;5;146m▄▄▄[48;5;60m▄[48;5;146m█[48;5;253m▄[48;5;231;38;5;253m▄[38;5;231m█████[48;5;253;38;5;146m▄[48;5;60;38;5;16m▄[49;39m         [00m
         [38;5;16m▀▀▀[39m [48;5;16;38;5;16m█[48;5;231;38;5;231m███[48;5;253m▄[48;5;146m▄[38;5;146m█████[48;5;16;38;5;16m█[48;5;146m▄[48;5;16;38;5;146m▄[48;5;146m████[48;5;60;38;5;60m█[48;5;146;38;5;146m█████████[48;5;253m▄[48;5;231m▄▄[48;5;146m█[48;5;103;38;5;103m█[48;5;16;38;5;16m█[49;39m         [00m
              [48;5;16;38;5;16m█[48;5;231;38;5;231m█████[48;5;146;38;5;146m██████████[38;5;60m▄[48;5;60;38;5;146m▄[48;5;146m████████████[38;5;103m▄[48;5;60;38;5;60m█[49;39m          [00m
               [48;5;16;38;5;16m█[48;5;231;38;5;231m█[48;5;103;38;5;60m▄[48;5;231;38;5;231m██[48;5;146;38;5;146m██████████[48;5;60;38;5;60m█[48;5;146;38;5;146m█[48;5;60;38;5;60m█[48;5;146;38;5;146m██[38;5;16m▄▄[49;38;5;60m▀[48;5;146m▄[38;5;146m█████[48;5;103m▄[48;5;16;38;5;16m█[49;39m          [00m
               [38;5;60m▄[48;5;16;38;5;146m▄[48;5;60;38;5;16m▄[48;5;231;38;5;231m█[38;5;146m▄[48;5;146m██[38;5;103m▄[38;5;146m█████[38;5;60m▄[48;5;60;38;5;146m▄[48;5;146m██[48;5;60;38;5;16m▄[48;5;16;38;5;60m▄[48;5;60m█[48;5;16;38;5;16m█[49;39m  [38;5;60m▀[48;5;146;38;5;16m▄[38;5;146m█████[48;5;16;38;5;103m▄[49;38;5;16m▄▄[39m [38;5;237m▄[48;5;237;38;5;60m▄[49;38;5;16m▄[39m    [00m
               [48;5;60;38;5;60m█[48;5;146;38;5;146m██[48;5;16;38;5;16m█[48;5;146;38;5;146m███[48;5;103m▄[48;5;146;38;5;103m▄[38;5;146m███[38;5;60m▄[48;5;60m█[48;5;146;38;5;146m███[48;5;16;38;5;16m█[48;5;60;38;5;60m███[48;5;16;38;5;16m█[49;39m  [38;5;16m▀[48;5;146m▄[38;5;146m███[38;5;103m▄[48;5;103m███[48;5;16m▄[48;5;60m▄[38;5;60m▄[48;5;16;38;5;16m█[49;39m    [00m
              [48;5;60;38;5;60m█[48;5;146;38;5;146m████[48;5;16;38;5;16m█[48;5;146m▄[38;5;146m██[48;5;60;38;5;60m█[48;5;146;38;5;103m▄[38;5;146m█[38;5;103m▄[48;5;60;38;5;16m▄[48;5;146;38;5;146m████[48;5;16m▄[48;5;60;38;5;16m▄[38;5;60m█[38;5;16m▄[49m▀[39m   [38;5;60m▀[48;5;60;38;5;16m▄[48;5;103m▄▄[38;5;103m███████[48;5;16;38;5;16m█[49;39m    [00m
             [48;5;60;38;5;60m█[48;5;253;38;5;231m▄[48;5;146;38;5;253m▄[38;5;146m██[48;5;16;38;5;16m█[49;39m [38;5;16m▀[48;5;146m▄[38;5;146m█[48;5;16;38;5;16m█[48;5;103m▄[48;5;146;38;5;146m█[48;5;60;38;5;16m▄[48;5;16;38;5;60m▄[48;5;60;38;5;16m▄[48;5;146;38;5;146m█[38;5;253m▄[48;5;253;38;5;231m▄▄[48;5;16;38;5;16m█[48;5;60m▄[49m▀[39m        [38;5;16m▀▀[38;5;60m▀[48;5;103;38;5;16m▄[38;5;103m███[48;5;16m▄[49;38;5;16m▄[39m   [00m
           [38;5;60m▄[48;5;60;38;5;231m▄[48;5;231m██[48;5;253m▄[48;5;146;38;5;146m█[48;5;16;38;5;237m▄[49m▄[39m  [38;5;16m▀▀[39m [38;5;16m▄[48;5;16m█[48;5;60;38;5;60m██[48;5;16m▄[48;5;253;38;5;16m▄[48;5;231;38;5;231m███[48;5;60m▄[48;5;16;38;5;60m▄[49;39m            [48;5;16;38;5;60m▄[48;5;103m▄[38;5;103m███[48;5;16;38;5;16m█[49;39m   [00m
         [38;5;60m▄[48;5;60;38;5;231m▄[48;5;231m█████[48;5;60;38;5;16m▄▄[49m▀[39m    [48;5;16;38;5;16m█[48;5;60;38;5;60m█[38;5;16m▄[38;5;60m██[38;5;16m▄[48;5;16m█[48;5;231;38;5;253m▄[38;5;231m███[48;5;60m▄[49;38;5;60m▄[39m        [38;5;237m▄[48;5;237;38;5;60m▄▄[48;5;103;38;5;103m█[38;5;16m▄[48;5;237;38;5;60m▄[48;5;103;38;5;237m▄[38;5;103m█[48;5;237;38;5;60m▄[48;5;16m▄[49;38;5;16m▄[39m [00m
    [38;5;16m▄[48;5;16;38;5;103m▄[38;5;60m▄[48;5;60m█[38;5;253m▄[48;5;231;38;5;231m█████[38;5;16m▄[49m▀[39m       [38;5;16m▀▀[39m [38;5;16m▀▀[39m  [48;5;16;38;5;16m█[48;5;231;38;5;231m████[48;5;60m▄[49;38;5;60m▄[39m       [48;5;237;38;5;16m▄[48;5;60m▄[49m▀[48;5;16m█[48;5;60;38;5;60m█[38;5;16m▄[48;5;60m▄[49m▀▀[48;5;60m▄[48;5;16m█[49;39m [00m
   [38;5;60m▄[48;5;60;38;5;103m▄▄[48;5;146;38;5;60m▄[38;5;146m█[38;5;237m▄▄[48;5;253;38;5;146m▄[48;5;231m▄[38;5;253m▄[38;5;16m▄[49m▀[39m               [48;5;16;38;5;16m█[48;5;231;38;5;231m██████[48;5;60m▄▄[49;38;5;60m▄[39m       [38;5;16m▀▀[39m       [00m
   [48;5;237;38;5;237m█[48;5;103;38;5;60m▄[48;5;60;38;5;16m▄▄[48;5;237;38;5;237m█[48;5;60;38;5;103m▄[48;5;103;38;5;60m▄[48;5;237;38;5;237m█[48;5;146;38;5;16m▄▄[49m▀[39m               [48;5;237;38;5;237m█[48;5;253;38;5;60m▄[48;5;231;38;5;231m████[38;5;253m▄[38;5;237m▄[38;5;146m▄▄[48;5;146;38;5;237m▄[48;5;237;38;5;103m▄[48;5;16m▄[49;38;5;16m▄[39m             [00m
   [38;5;16m▀▀[39m  [48;5;16;38;5;16m█[48;5;60m▄[49m▀▀[39m                 [48;5;237;38;5;237m█[48;5;103;38;5;103m█[38;5;60m▄[48;5;60;38;5;237m▄[48;5;146;38;5;146m███[48;5;237;38;5;237m█[48;5;60;38;5;60m▄[38;5;60m█[48;5;146;38;5;16m▄[48;5;237m▄[48;5;60m▄[48;5;103;38;5;60m▄[48;5;16;38;5;16m█[49;39m             [00m
                            [48;5;16;38;5;16m█[48;5;60m▄[48;5;237m▄[49m▀▀▀▀▀[48;5;237m▄[38;5;237m█[48;5;16;38;5;16m█[49;39m  [38;5;16m▀▀[39m             [00m
                                     [38;5;16m▀[39m                  [00m
                                                        [00m
