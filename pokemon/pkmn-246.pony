$$$
NAME: Larvitar
OTHER NAMES: 애버라스, Embrylex, 幼基拉斯, Yogiras, ヨーギラス
APPEARANCE: Pokémon Generation II
KIND: Rock Skin Pokémon
GROUP: monster
BALLOON: top
COAT: green
DISPLAY: full
BALLOON TOP: 3
BALLOON BOTTOM: 0
POSE: master
BASED ON: Larvitar
SOURCE: PokéAPI Sprites
MEDIA: Pokémon
LICENSE: Copyright The Pokémon Company
FREE: no
WIDTH: 68
HEIGHT: 25


$$$
$balloon5$
                $\$                                                   [00m
                 $\$                                                  [00m
                  $\$                                                 [00m
                   $\$                 [00m
                   [38;5;107m▄[48;5;107;38;5;187m▄[48;5;240;38;5;151m▄[38;5;240m█[49;39m              [00m
                  [48;5;107;38;5;107m█[48;5;187;38;5;187m█[48;5;151;38;5;151m██[48;5;240;38;5;240m█[49;39m              [00m
                 [48;5;107;38;5;240m▄[48;5;187;38;5;187m█[48;5;151;38;5;151m██[38;5;233m▄[49;38;5;240m▀[39m              [00m
                [48;5;240;38;5;240m█[48;5;187;38;5;187m█[48;5;151;38;5;151m███[48;5;233;38;5;233m█[49;39m               [00m
               [48;5;240;38;5;240m█[48;5;187;38;5;187m█[48;5;151;38;5;151m███[38;5;233m▄[49m▀[39m               [00m
           [38;5;107m▄▄▄[48;5;240;38;5;187m▄[48;5;187;38;5;151m▄[48;5;151m████[48;5;233;38;5;233m█[49;39m                [00m
       [38;5;107m▄▄[48;5;107;38;5;187m▄▄[48;5;151m▄[48;5;187m██[38;5;151m▄[48;5;151m██████[48;5;240;38;5;240m█[49;39m               [00m
     [38;5;107m▄[48;5;107;38;5;187m▄[48;5;187m██[48;5;151;38;5;107m▄[48;5;107;38;5;151m▄[48;5;151;38;5;187m▄[38;5;151m█[38;5;240m▄▄[38;5;233m▄▄[38;5;151m█████[48;5;240;38;5;240m█[49;39m              [00m
    [48;5;107;38;5;240m▄[48;5;151;38;5;151m████[48;5;107;38;5;187m▄[48;5;187;38;5;107m▄[48;5;107;38;5;124m▄[48;5;94;38;5;203m▄[48;5;231;38;5;215m▄[38;5;231m█[48;5;233;38;5;240m▄[48;5;151;38;5;151m███████[48;5;240;38;5;240m█[49;39m             [00m
   [48;5;240;38;5;240m█[48;5;151;38;5;151m██████[48;5;233m▄[48;5;203;38;5;233m▄[48;5;94m▄[48;5;215m▄[48;5;231m▄[48;5;107;38;5;151m▄[48;5;151m████████[48;5;233;38;5;233m█[49;39m            [00m
   [48;5;233;38;5;233m█[48;5;240;38;5;151m▄▄[38;5;107m▄[48;5;151;38;5;240m▄[38;5;151m████[48;5;240m▄[48;5;233;38;5;233m█[48;5;151;38;5;151m██████████[48;5;233;38;5;107m▄[49;38;5;233m▄[39m           [00m
    [38;5;233m▀[48;5;151m▄[38;5;151m███████[48;5;107m▄[48;5;151m██████████[48;5;107;38;5;107m█[48;5;233;38;5;233m█[49;39m           [00m
      [38;5;233m▀▀[48;5;151m▄[38;5;240m▄[38;5;151m██████████████[48;5;107;38;5;240m▄[38;5;107m█[48;5;233;38;5;233m█[49;39m          [00m
       [38;5;240m▄[48;5;240;38;5;124m▄[48;5;151;38;5;151m█████████[38;5;107m▄[38;5;151m█[48;5;240;38;5;107m▄[48;5;151m▄[38;5;151m█[48;5;107;38;5;240m▄[48;5;233;38;5;233m█[48;5;240m▄[48;5;233;38;5;107m▄[49;38;5;233m▄[39m [38;5;240m▄▄▄▄[39m    [00m
      [48;5;240;38;5;240m█[48;5;124;38;5;124m█[48;5;215;38;5;215m█[48;5;124;38;5;94m▄[48;5;151;38;5;151m███[38;5;240m▄[38;5;151m███[48;5;240;38;5;240m█[48;5;151;38;5;151m███[48;5;240;38;5;240m█[48;5;151;38;5;151m█[48;5;233;38;5;107m▄[38;5;233m██[48;5;240;38;5;240m█[48;5;233;38;5;233m█[48;5;240;38;5;151m▄[48;5;151m█[38;5;240m▄▄[48;5;240;38;5;151m▄▄▄[49;38;5;240m▄[39m [00m
     [48;5;240;38;5;240m█[48;5;151;38;5;233m▄[48;5;203;38;5;203m█[48;5;215;38;5;124m▄[48;5;203;38;5;94m▄[48;5;94m█[48;5;151;38;5;151m█[48;5;240;38;5;240m█[48;5;233;38;5;233m█[48;5;240m▄[48;5;151;38;5;240m▄[38;5;151m█[48;5;240;38;5;240m█[48;5;151;38;5;107m▄[38;5;151m██[48;5;233;38;5;233m█[48;5;151;38;5;151m██[48;5;107;38;5;107m█[48;5;233m▄[48;5;107m█[48;5;233;38;5;233m█[48;5;107;38;5;151m▄[48;5;240m▄[48;5;151m█████[48;5;240;38;5;240m█[49;39m [00m
       [48;5;124;38;5;94m▄[48;5;215;38;5;203m▄▄[48;5;203m█[48;5;94;38;5;94m█[48;5;151;38;5;151m█[48;5;233;38;5;240m▄[48;5;240;38;5;151m▄[48;5;151m█[38;5;240m▄[38;5;151m█[48;5;240m▄[48;5;107;38;5;233m▄[48;5;151m▄[48;5;233;38;5;151m▄[48;5;151m██[48;5;107;38;5;107m██[38;5;233m▄[48;5;233;38;5;107m▄[48;5;107m█[48;5;151m▄[38;5;240m▄▄[38;5;233m▄[48;5;240m▄▄[49;38;5;240m▄[39m [00m
       [38;5;94m▀[48;5;124m▄[38;5;203m▄[48;5;94m▄[38;5;94m█[48;5;151;38;5;151m██[38;5;240m▄[48;5;240;38;5;151m▄[48;5;151m███████[48;5;107;38;5;107m███[48;5;233;38;5;233m█[48;5;107;38;5;107m████[48;5;151m▄▄▄▄[38;5;151m█[48;5;240;38;5;240m█[49;39m[00m
         [48;5;94;38;5;233m▄[38;5;240m▄[48;5;107m▄[38;5;107m██[48;5;233;38;5;233m█[48;5;151;38;5;107m▄▄[38;5;151m███[38;5;107m▄▄[48;5;107m███[48;5;233;38;5;233m█[48;5;107;38;5;107m███[48;5;240m▄[38;5;240m█[48;5;107;38;5;233m▄[38;5;240m▄[38;5;233m▄▄▄[49m▀[39m[00m
       [38;5;240m▄[48;5;233;38;5;187m▄[48;5;240m▄[48;5;107m▄[38;5;107m█[48;5;240m▄▄[38;5;233m▄[48;5;233m█[48;5;107m▄[38;5;107m███████[48;5;233;38;5;233m█[48;5;240m▄[48;5;107;38;5;107m█[48;5;240m▄[48;5;107;38;5;233m▄[38;5;107m███[48;5;233m▄[49;38;5;233m▄[39m   [00m
       [38;5;233m▀▀▀▀▀▀▀[39m  [38;5;240m▄[48;5;233;38;5;107m▄[48;5;240m▄[48;5;107m████[48;5;233;38;5;233m█[49;39m  [38;5;233m▀▀▀▀[48;5;107m▄▄▄[49m▀[39m   [00m
               [48;5;240;38;5;233m▄[48;5;187m▄▄▄[48;5;107m▄▄[49m▀▀[39m              [00m
                                     [00m
