$$$
NAME: Charmander
OTHER NAMES: Salamèche, ヒトカゲ, 파이리, Hitokage, 小火龍, Glumanda
APPEARANCE: Pokémon Generation I
KIND: Lizard Pokémon
GROUP: monster, dragon
BALLOON: top
COAT: red
DISPLAY: full
BALLOON TOP: 3
BALLOON BOTTOM: 0
POSE: master
BASED ON: Charmander
SOURCE: PokéAPI Sprites
MEDIA: Pokémon
LICENSE: Copyright The Pokémon Company
FREE: no
WIDTH: 69
HEIGHT: 25


$$$
$balloon5$
     $\$                                                               [00m
      $\$                                                              [00m
       $\$                                                             [00m
        $\$                                [00m
         $\$  [38;5;167m▄▄[38;5;88m▄▄▄[39m                        [00m
         [38;5;167m▄[48;5;167;38;5;221m▄[38;5;208m▄[48;5;208m█████[48;5;88m▄▄[49;38;5;233m▄[39m                     [00m
        [48;5;167;38;5;167m█[48;5;221;38;5;221m███[48;5;208;38;5;208m███████[48;5;233m▄[49;38;5;233m▄[39m                    [00m
       [48;5;167;38;5;167m█[48;5;208;38;5;208m██[48;5;221m▄[48;5;208m████[38;5;167m▄[48;5;167;38;5;208m▄[48;5;208m███[48;5;233;38;5;167m▄[49;38;5;233m▄[39m                   [00m
       [48;5;60;38;5;31m▄[48;5;167;38;5;167m█[48;5;208;38;5;208m██████[48;5;167m▄[48;5;208;38;5;60m▄[48;5;60m██[48;5;167;38;5;88m▄[38;5;167m█[48;5;233;38;5;233m█[49;39m                   [00m
       [48;5;231;38;5;233m▄[48;5;208;38;5;208m████████[48;5;231;38;5;231m█[48;5;233;38;5;60m▄[48;5;31;38;5;39m▄[48;5;88;38;5;88m█[48;5;167;38;5;167m█[48;5;233;38;5;233m█[49;39m             [38;5;160m▄[39m     [00m
      [48;5;88;38;5;88m█[48;5;60;38;5;221m▄[48;5;208m▄▄[38;5;208m██████[48;5;88m▄[48;5;31;38;5;167m▄[48;5;39m▄[48;5;167m██[48;5;233;38;5;233m█[49;39m             [48;5;160;38;5;160m█[48;5;220;38;5;220m█[49;39m    [00m
      [38;5;88m▀[48;5;221m▄[38;5;208m▄[48;5;208m██[48;5;167m▄[48;5;208m███[38;5;167m▄[48;5;167;38;5;88m▄▄[48;5;88;38;5;167m▄[48;5;167m█[38;5;233m▄[49m▀[39m     [38;5;88m▄▄[39m     [48;5;160;38;5;160m█[48;5;214;38;5;220m▄[48;5;220m█[49;38;5;214m▄[39m   [00m
    [38;5;88m▄▄[39m [38;5;233m▀▀[48;5;208m▄▄▄▄[48;5;88;38;5;160m▄▄[48;5;233;38;5;88m▄[48;5;231;38;5;167m▄[48;5;167m███[48;5;233;38;5;88m▄[49m▄▄[48;5;88;38;5;208m▄▄▄▄[48;5;167;38;5;167m█[48;5;233;38;5;233m█[49;39m     [48;5;160;38;5;160m█[48;5;214;38;5;220m▄[48;5;220;38;5;160m▄[38;5;220m█[48;5;160;38;5;214m▄[49;39m  [00m
    [48;5;233;38;5;233m█[48;5;167;38;5;167m█[48;5;88;38;5;208m▄▄▄[48;5;233;38;5;167m▄[48;5;167;38;5;233m▄▄[48;5;88;38;5;208m▄▄[38;5;167m▄[48;5;167;38;5;233m▄▄[48;5;233;38;5;167m▄[48;5;167m██[48;5;208m▄[38;5;208m██[38;5;167m▄▄▄[48;5;167m█[38;5;233m▄▄[49m▀[39m   [48;5;160;38;5;160m██[38;5;214m▄[38;5;160m█[48;5;220m▄[38;5;220m█[48;5;160;38;5;214m▄[49;38;5;160m▄[39m[00m
   [38;5;233m▀[48;5;167m▄▄[38;5;167m█[48;5;208m▄▄[48;5;167m███[48;5;233m▄▄▄[48;5;167m█████████[38;5;233m▄▄[49m▀[39m      [48;5;160;38;5;160m█[48;5;220;38;5;214m▄[38;5;220m█[48;5;160m▄[48;5;220m███[48;5;160;38;5;160m█[49;39m[00m
      [38;5;233m▀▀[48;5;167m▄▄[38;5;167m██[48;5;88;38;5;88m█[48;5;167;38;5;179m▄[48;5;179m███[48;5;167m▄[38;5;167m██[38;5;208m▄[48;5;88m▄[38;5;233m▄[49m▀[39m         [38;5;160m▀[48;5;214m▄[38;5;214m█[38;5;160m▄[48;5;220;38;5;214m▄▄[48;5;214;38;5;160m▄[48;5;160m█[49;39m[00m
          [38;5;233m▀[48;5;88;38;5;88m█[48;5;167;38;5;179m▄[48;5;222;38;5;222m█████[48;5;179m▄[48;5;208;38;5;208m███[48;5;233;38;5;167m▄[49;38;5;233m▄[39m           [38;5;160m▀[48;5;160;38;5;88m▄[48;5;88m█[48;5;160;38;5;160m█[49m▀[39m [00m
           [48;5;88;38;5;88m█[48;5;222;38;5;222m███████[48;5;208;38;5;208m████[48;5;233m▄[49;38;5;233m▄[39m           [48;5;88;38;5;88m█[48;5;221;38;5;208m▄[48;5;233;38;5;233m█[49;39m  [00m
          [48;5;88;38;5;88m█[48;5;179;38;5;222m▄[48;5;222m███████[48;5;179m▄[48;5;208;38;5;208m████[48;5;233;38;5;233m█[49;39m        [38;5;88m▄▄[48;5;88;38;5;208m▄[48;5;208m█[38;5;167m▄[48;5;233;38;5;233m█[49;39m  [00m
        [38;5;88m▄[48;5;88;38;5;208m▄[38;5;167m▄[48;5;222;38;5;179m▄[38;5;222m███████[38;5;179m▄[48;5;167;38;5;167m█[48;5;208;38;5;208m████[48;5;233;38;5;167m▄[49;38;5;233m▄[38;5;88m▄▄▄[48;5;88;38;5;208m▄▄▄[48;5;208m██[38;5;167m▄[48;5;167;38;5;179m▄[48;5;233;38;5;233m█[49;39m   [00m
       [38;5;233m▄[48;5;88;38;5;208m▄[48;5;208m██[48;5;167;38;5;88m▄[48;5;179;38;5;179m█[48;5;222m▄▄▄▄[48;5;179m██[48;5;167;38;5;167m█[48;5;208;38;5;208m██████[48;5;233;38;5;167m▄[48;5;167;38;5;233m▄[38;5;167m███[48;5;208m▄▄[48;5;167;38;5;179m▄[48;5;179m█[38;5;233m▄[49m▀[39m    [00m
       [48;5;233;38;5;233m█[48;5;208;38;5;208m████[48;5;233;38;5;167m▄[48;5;179;38;5;233m▄[38;5;179m█████[48;5;167;38;5;88m▄[48;5;208;38;5;167m▄[38;5;208m███[38;5;167m▄[48;5;167m██[48;5;233;38;5;233m█[48;5;167;38;5;179m▄▄▄[48;5;179m█[38;5;233m▄▄[49m▀[39m      [00m
        [38;5;233m▀[48;5;208m▄[38;5;167m▄[48;5;167m███[48;5;233m▄▄[48;5;179;38;5;233m▄▄▄[48;5;88m▄[48;5;167;38;5;88m▄[38;5;167m█████[38;5;233m▄[48;5;233;38;5;88m▄[48;5;179m▄▄[49m▀▀[39m         [00m
        [38;5;233m▄[48;5;233;38;5;167m▄[48;5;167m████[48;5;233;38;5;233m█[49m▀[39m     [48;5;88;38;5;88m█[48;5;167;38;5;208m▄▄▄[38;5;167m█[48;5;233;38;5;233m█[49;39m              [00m
       [38;5;233m▀[48;5;231m▄[48;5;88m▄[48;5;231m▄[48;5;167m▄▄[49m▀[39m       [38;5;88m▀[48;5;167;38;5;233m▄[48;5;208;38;5;231m▄[38;5;88m▄[38;5;231m▄[48;5;167;38;5;88m▄[48;5;233;38;5;231m▄[49;38;5;233m▄[39m            [00m
                       [38;5;233m▀▀▀▀▀[39m             [00m
                                         [00m
