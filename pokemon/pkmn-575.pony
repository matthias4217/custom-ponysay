$$$
NAME: Gothorita
OTHER NAMES: Hypnomorba, 고디보미, Mesmérella, ゴチミル, 哥德小童
APPEARANCE: Pokémon Generation V
KIND: Manipulate Pokémon
GROUP: humanshape
BALLOON: top
COAT: purple
DISPLAY: full
BALLOON TOP: 3
BALLOON BOTTOM: 0
POSE: master
BASED ON: Gothorita
SOURCE: PokéAPI Sprites
MEDIA: Pokémon
LICENSE: Copyright The Pokémon Company
FREE: no
WIDTH: 74
HEIGHT: 30


$$$
$balloon5$
$\$                                                                         [00m
 $\$                                                                        [00m
  $\$                                                                       [00m
   $\$                                             [00m
    $\$ [38;5;234m▄▄[39m  [38;5;243m▄▄[39m                                     [00m
    [38;5;243m▄[48;5;243;38;5;249m▄[48;5;249;38;5;231m▄[48;5;231m█[48;5;234;38;5;243m▄[48;5;243;38;5;234m▄[48;5;249;38;5;249m██[48;5;243;38;5;243m█[49;39m [38;5;234m▄▄[39m  [38;5;243m▄[48;5;243;38;5;249m▄▄[49;38;5;243m▄[39m           [38;5;243m▄[38;5;234m▄[39m     [38;5;243m▄▄[39m [48;5;243;38;5;231m▄▄[49;38;5;243m▄[39m   [00m
   [38;5;243m▄[48;5;243;38;5;249m▄[48;5;231;38;5;231m████[48;5;234;38;5;243m▄[48;5;249;38;5;249m██[48;5;243m▄[48;5;234;38;5;234m█[48;5;231;38;5;231m██[48;5;234m▄[49;38;5;234m▄[48;5;243;38;5;249m▄[48;5;249m██[48;5;243m▄[49;38;5;243m▄[39m      [38;5;243m▄[48;5;234;38;5;231m▄▄[48;5;243;38;5;234m▄[48;5;249;38;5;249m█[48;5;243m▄[48;5;234;38;5;234m█[49;39m   [48;5;243;38;5;243m█[48;5;231;38;5;231m██[48;5;243m▄[48;5;231;38;5;243m▄[38;5;231m██[48;5;234;38;5;243m▄[49;39m  [00m
   [48;5;243;38;5;243m█[48;5;231;38;5;231m█████[38;5;235m▄[48;5;249m▄[48;5;235;38;5;238m▄[48;5;234;38;5;234m█[48;5;231;38;5;231m████[48;5;243m▄[48;5;249;38;5;249m███[38;5;235m▄[48;5;243m▄[49m▄▄▄[39m  [38;5;243m▄[48;5;249;38;5;231m▄[48;5;231m███[48;5;234;38;5;234m█[48;5;249;38;5;235m▄[48;5;243m▄[48;5;235;38;5;238m▄▄[38;5;235m█[48;5;231;38;5;231m█████[48;5;243;38;5;249m▄[48;5;231;38;5;231m██[48;5;234;38;5;243m▄[49;39m [00m
   [48;5;235;38;5;235m█[48;5;231;38;5;243m▄[38;5;231m██[38;5;238m▄[48;5;235m▄[48;5;238m███[48;5;234m▄[48;5;231;38;5;234m▄[38;5;249m▄[38;5;231m██[38;5;249m▄[48;5;243;38;5;235m▄[48;5;235;38;5;238m▄▄[48;5;238m█████[48;5;235m▄[38;5;235m█[48;5;243;38;5;249m▄[48;5;231;38;5;231m████[48;5;234;38;5;234m█[48;5;238;38;5;238m████[48;5;235m▄[48;5;249;38;5;234m▄[48;5;231;38;5;249m▄[38;5;231m███[38;5;249m▄[48;5;249m██[48;5;243;38;5;243m█[48;5;234;38;5;234m█[49;39m[00m
    [38;5;235m▀[38;5;243m▀[48;5;235;38;5;235m█[48;5;238;38;5;238m██████[48;5;234m▄[48;5;249;38;5;234m▄[38;5;249m██[38;5;234m▄[48;5;234;38;5;238m▄[48;5;238m████████[48;5;235;38;5;235m█[48;5;249;38;5;249m██[48;5;231m▄▄[48;5;249;38;5;234m▄[48;5;234;38;5;238m▄[48;5;238m█████[48;5;234m▄[48;5;249;38;5;234m▄[38;5;249m████[48;5;243;38;5;234m▄[48;5;249;38;5;249m█[48;5;243;38;5;235m▄[49;39m [00m
     [48;5;234;38;5;234m█[48;5;238;38;5;238m█████████[48;5;234;38;5;235m▄[38;5;238m▄[48;5;238m██████████[48;5;235m▄[48;5;243;38;5;235m▄[48;5;249;38;5;249m██[38;5;234m▄[48;5;234;38;5;238m▄[48;5;238m████████[48;5;234m▄[48;5;249;38;5;234m▄▄[48;5;234;38;5;238m▄[48;5;235;38;5;234m▄[49;38;5;235m▀[39m  [00m
     [48;5;234;38;5;234m█[48;5;235;38;5;235m█[48;5;238m▄[38;5;238m██████[38;5;235m▄[48;5;235;38;5;237m▄[48;5;238;38;5;238m█████████████[48;5;234m▄[38;5;234m█[48;5;238;38;5;235m▄[38;5;238m███████████[38;5;235m▄[48;5;235m█[48;5;234;38;5;234m█[49;39m   [00m
      [48;5;234;38;5;234m█[48;5;235;38;5;235m███[48;5;238m▄▄▄▄[48;5;234;38;5;234m█[48;5;139;38;5;235m▄[48;5;238;38;5;139m▄[38;5;238m█[48;5;139;38;5;175m▄[48;5;238;38;5;238m██████[48;5;139;38;5;175m▄[48;5;238;38;5;238m███[48;5;139;38;5;175m▄[48;5;234;38;5;238m▄[48;5;235;38;5;234m▄[38;5;235m██[48;5;238m▄▄▄▄▄▄[48;5;235m████[38;5;234m▄[49m▀[39m   [00m
       [38;5;234m▀[48;5;235m▄▄[38;5;235m████[48;5;234;38;5;234m█[48;5;249;38;5;249m█[48;5;234;38;5;231m▄[38;5;24m▄[48;5;235;38;5;231m▄[48;5;139;38;5;235m▄[48;5;238;38;5;238m███[38;5;235m▄[48;5;139m▄[48;5;175m▄[48;5;235;38;5;231m▄[38;5;24m▄[38;5;249m▄[38;5;231m▄[48;5;238;38;5;238m█[48;5;234;38;5;235m▄[48;5;235;38;5;234m▄[38;5;235m██████████[38;5;234m▄[49m▀[39m    [00m
         [38;5;234m▄[48;5;235;38;5;235m█[38;5;234m▄[49m▀▀▀[48;5;243m▄[48;5;231;38;5;243m▄[48;5;24;38;5;231m▄[48;5;75;38;5;24m▄[48;5;24;38;5;75m▄[48;5;139;38;5;139m█[48;5;238;38;5;238m█[38;5;139m▄[48;5;249m▄[48;5;231;38;5;231m█[48;5;24;38;5;75m▄[48;5;75;38;5;24m▄[48;5;24;38;5;75m▄[48;5;231;38;5;231m█[38;5;249m▄[48;5;235;38;5;235m██[48;5;234;38;5;234m█[49m▀▀[48;5;235m▄▄[38;5;235m██[38;5;234m▄▄[49m▀▀[39m      [00m
        [48;5;234;38;5;234m█[48;5;235;38;5;238m▄[38;5;235m█[48;5;234m▄[49;38;5;234m▄[39m  [38;5;234m▀[48;5;238m▄[48;5;249;38;5;238m▄[48;5;231;38;5;139m▄[48;5;139;38;5;175m▄[48;5;175;38;5;95m▄▄[38;5;175m█[48;5;139m▄[48;5;249;38;5;139m▄[48;5;231m▄[38;5;231m█[38;5;249m▄[38;5;235m▄[48;5;235m██[48;5;234;38;5;234m█[49m▀[39m    [48;5;234;38;5;234m█[48;5;235;38;5;235m█[48;5;234m▄[49;38;5;234m▄[39m        [00m
       [48;5;234;38;5;234m█[48;5;238;38;5;238m████[48;5;234;38;5;234m█[49;39m    [38;5;234m▀[48;5;238m▄[48;5;243m▄[48;5;209;38;5;95m▄[38;5;209m█[48;5;95m▄[48;5;175;38;5;175m█[38;5;139m▄[38;5;235m▄▄[48;5;235m█[38;5;234m▄▄[49m▀[39m     [48;5;234;38;5;234m█[48;5;235;38;5;238m▄▄[48;5;238m█[48;5;234;38;5;235m▄[49;38;5;234m▄[39m       [00m
       [38;5;234m▀[48;5;235m▄[48;5;238m▄▄[48;5;234m█[49m▀[39m    [48;5;243;38;5;243m█[48;5;249;38;5;231m▄[48;5;231m█[48;5;234;38;5;249m▄[48;5;95;38;5;139m▄[38;5;235m▄[48;5;235;38;5;231m▄[48;5;231m██[48;5;234m▄[38;5;234m█[49;39m        [48;5;234;38;5;234m█[48;5;238;38;5;235m▄[38;5;238m██[38;5;235m▄[48;5;234;38;5;234m█[49;39m       [00m
          [38;5;235m▄▄▄▄[39m  [38;5;235m▄[48;5;249;38;5;249m█[48;5;231;38;5;231m███[48;5;234;38;5;249m▄[48;5;231;38;5;231m████[38;5;249m▄[48;5;234;38;5;235m▄[49;38;5;234m▄[39m   [38;5;235m▄▄▄▄[39m [38;5;234m▀▀▀▀[39m        [00m
          [38;5;235m▀[38;5;238m▀[48;5;238;38;5;234m▄▄[48;5;234;38;5;235m▄[38;5;238m▄[48;5;238m█[48;5;234;38;5;234m█[48;5;249;38;5;249m█[48;5;231m▄[48;5;249;38;5;234m▄[48;5;234m█[48;5;249;38;5;249m█[48;5;231m▄▄[48;5;249m█[48;5;234;38;5;234m█[48;5;235m▄[38;5;235m██[48;5;234;38;5;238m▄▄[48;5;238m█[38;5;234m▄[38;5;235m▄[49;38;5;234m▀[39m             [00m
              [38;5;234m▀▀▀[38;5;235m▄[48;5;234m▄[49;38;5;234m▀[48;5;234m█[48;5;235;38;5;238m▄[48;5;234m▄[48;5;249;38;5;234m▄▄[49m▀[39m [38;5;234m▀▀[48;5;238m▄▄[49m▀[38;5;235m▀[39m                [00m
                [48;5;243;38;5;249m▄[48;5;231;38;5;231m██[48;5;234;38;5;249m▄[48;5;238;38;5;235m▄▄[48;5;235;38;5;231m▄[48;5;249m▄[48;5;231m█[48;5;234m▄[49;38;5;234m▄[38;5;235m▄[39m                     [00m
               [48;5;235;38;5;235m█[48;5;231;38;5;231m████[48;5;235m▄[48;5;231m████[38;5;249m▄[48;5;234;38;5;234m█[48;5;238;38;5;238m█[48;5;235m▄[49;38;5;235m▄[39m                   [00m
              [48;5;234;38;5;234m█[48;5;238;38;5;238m█[48;5;234m▄[48;5;231;38;5;234m▄▄[48;5;234;38;5;238m▄▄[48;5;231;38;5;234m▄[38;5;249m▄▄[38;5;234m▄[48;5;234;38;5;238m▄[48;5;238m██[38;5;234m▄[49m▀[39m                   [00m
               [38;5;234m▀[48;5;238m▄▄[38;5;235m▄[38;5;238m██████[38;5;235m▄[38;5;234m▄[48;5;234m█[49m▀[39m                    [00m
                  [38;5;234m▀[48;5;234m█[38;5;235m▄▄[49;38;5;234m▀[48;5;234m█[48;5;235;38;5;235m██[48;5;234;38;5;234m█[49;39m                      [00m
                   [38;5;234m▀[48;5;235m▄[38;5;235m█[48;5;234;38;5;234m█[48;5;235;38;5;235m██[48;5;234;38;5;234m█[49;39m                       [00m
                    [38;5;234m▀[48;5;235m▄[48;5;234;38;5;235m▄[48;5;235m█[48;5;234;38;5;234m█[38;5;235m▄[49;38;5;234m▄[39m                      [00m
                    [38;5;235m▄[48;5;234;38;5;238m▄[48;5;238m██[48;5;234m▄[48;5;235;38;5;235m█[48;5;234;38;5;234m█[49;39m                      [00m
                    [48;5;235;38;5;235m█[48;5;238;38;5;238m███[38;5;235m▄[48;5;234;38;5;234m█[49;39m                       [00m
                     [38;5;234m▀▀▀▀[39m                        [00m
                                                 [00m
