$$$
NAME: Ampharos
OTHER NAMES: 전룡, 電龍, デンリュウ, Pharamp, Denryu
APPEARANCE: Pokémon Generation II
KIND: Light Pokémon
GROUP: ground, monster
BALLOON: top
COAT: yellow
DISPLAY: full
BALLOON TOP: 3
BALLOON BOTTOM: 0
POSE: master
BASED ON: Ampharos
SOURCE: PokéAPI Sprites
MEDIA: Pokémon
LICENSE: Copyright The Pokémon Company
FREE: no
WIDTH: 75
HEIGHT: 36


$$$
$balloon5$
           $\$                                                               [00m
            $\$                                                              [00m
             $\$                                                             [00m
              $\$                                           [00m
              [38;5;233m▄[48;5;233;38;5;66m▄[38;5;233m██[49m▄[39m                                       [00m
              [48;5;233;38;5;233m█[48;5;66;38;5;221m▄[48;5;221m█[38;5;66m▄▄[48;5;233m▄[49;38;5;233m▄[39m    [38;5;233m▄▄[48;5;233;38;5;66m▄▄▄[38;5;233m█[49m▄[39m                          [00m
              [48;5;95;38;5;95m█[48;5;221;38;5;66m▄[48;5;66;38;5;166m▄▄▄[38;5;172m▄[48;5;172;38;5;227m▄▄▄[49;38;5;233m▄[48;5;233;38;5;221m▄[48;5;227;38;5;227m█[48;5;66;38;5;66m██[38;5;233m▄[48;5;233m███[49;39m                          [00m
              [38;5;88m▄[48;5;166;38;5;202m▄[48;5;202;38;5;88m▄[48;5;231m▄[48;5;202;38;5;202m█[48;5;166;38;5;227m▄[48;5;227m██[48;5;95;38;5;172m▄[48;5;233;38;5;233m█[48;5;221;38;5;172m▄[48;5;227;38;5;221m▄[48;5;95;38;5;172m▄[48;5;233;38;5;95m▄[38;5;233m███[49;39m                           [00m
            [38;5;172m▄[48;5;172;38;5;227m▄[48;5;227m█[48;5;166m▄[48;5;202m▄▄[48;5;227m████[48;5;221m▄[48;5;66;38;5;221m▄[48;5;233;38;5;233m█[48;5;95m▄[48;5;172;38;5;95m▄▄[38;5;233m▄[49m▀[39m                            [00m
           [38;5;172m▄[48;5;172;38;5;227m▄[48;5;227m█████[48;5;172;38;5;233m▄[48;5;233;38;5;231m▄[48;5;172;38;5;233m▄[48;5;227;38;5;227m███[48;5;172m▄[48;5;233m▄[38;5;221m▄[38;5;172m▄[48;5;95;38;5;95m█[49;39m                             [00m
          [38;5;172m▄[48;5;172;38;5;227m▄[48;5;227m██████[48;5;172m▄[48;5;233m▄[48;5;95m▄[48;5;227m████[48;5;221;38;5;172m▄[48;5;172m██[48;5;95;38;5;95m█[49;39m                             [00m
          [48;5;95;38;5;233m▄[48;5;227;38;5;227m█████████[38;5;221m▄[38;5;95m▄[38;5;172m▄[48;5;221m▄[48;5;172m███[48;5;95;38;5;233m▄[49;39m                              [00m
           [38;5;233m▀[38;5;95m▀[48;5;221;38;5;233m▄▄[38;5;95m▄[38;5;172m▄▄[48;5;172m█[48;5;95m▄[48;5;233m▄[48;5;172m█████[48;5;95;38;5;233m▄[49;39m                               [00m
                [38;5;233m▀▀▀▀[38;5;95m▀[48;5;172m▄[38;5;172m███[38;5;221m▄[48;5;233;38;5;233m█[49;39m                               [00m
                      [48;5;233;38;5;66m▄[48;5;221m▄[38;5;233m▄[48;5;172m▄[48;5;233m█[49;39m                               [00m
                     [48;5;95;38;5;95m█[48;5;233;38;5;66m▄[48;5;221m▄[38;5;233m▄▄[48;5;233m█[49;39m                               [00m
                    [48;5;95;38;5;95m█[48;5;233;38;5;66m▄[48;5;221m▄▄[38;5;233m▄▄[48;5;233m██[49;39m                              [00m
                   [48;5;95;38;5;95m█[48;5;221;38;5;221m█[48;5;172m▄[48;5;66m▄[48;5;233m▄▄[48;5;172m▄[48;5;221m█[48;5;95m▄[49;38;5;95m▄[39m                             [00m
              [38;5;95m▄▄▄▄[48;5;95;38;5;221m▄[48;5;221;38;5;227m▄[48;5;227;38;5;231m▄[48;5;231m██[48;5;227m▄[48;5;221;38;5;227m▄[38;5;221m███[48;5;95m▄[49;38;5;172m▄[39m                            [00m
      [38;5;95m▄▄▄[38;5;172m▄[48;5;95;38;5;227m▄▄▄[48;5;172m▄[48;5;227m███[48;5;172;38;5;66m▄[48;5;145;38;5;231m▄[48;5;231m██████[48;5;221;38;5;227m▄[38;5;221m████[48;5;172m▄[49;38;5;172m▄▄[39m                    [38;5;166m▄▄▄[39m  [00m
   [38;5;95m▄[48;5;95;38;5;227m▄▄[48;5;227m██████████[48;5;172;38;5;66m▄[48;5;145;38;5;231m▄[48;5;231m████████[48;5;221;38;5;227m▄[38;5;221m█[38;5;172m▄[38;5;221m████[48;5;172m▄▄[38;5;227m▄[49;38;5;172m▄▄[39m             [38;5;166m▄[48;5;166;38;5;231m▄[48;5;231m█[48;5;202;38;5;166m▄▄[48;5;88;38;5;202m▄[49;38;5;88m▄[39m[00m
    [38;5;233m▀▀[38;5;95m▀[48;5;221;38;5;233m▄[48;5;227m▄▄▄▄▄[38;5;95m▄[49m▀[48;5;95;38;5;233m▄[48;5;66;38;5;231m▄[48;5;231m██████████[48;5;221;38;5;227m▄[48;5;233;38;5;221m▄[48;5;221;38;5;233m▄[38;5;221m████[38;5;227m▄[48;5;227m███[48;5;172m▄▄[49;38;5;172m▄▄[39m         [48;5;166;38;5;166m█[48;5;202;38;5;202m█[48;5;166;38;5;166m███[48;5;202;38;5;202m█[48;5;88;38;5;88m█[49;39m[00m
               [48;5;233;38;5;66m▄[48;5;231;38;5;231m████████████[48;5;221;38;5;221m██[48;5;233m▄[48;5;95m▄[48;5;227;38;5;233m▄[38;5;95m▄[38;5;221m▄[38;5;227m███████[48;5;172m▄[49;38;5;95m▄[39m        [38;5;88m▀[48;5;202;38;5;52m▄▄[38;5;233m▄[49;38;5;52m▀[39m [00m
              [48;5;233;38;5;233m█[48;5;231;38;5;231m█████████████[48;5;227m▄[48;5;221;38;5;221m█████[48;5;95;38;5;95m█[49;38;5;233m▀[48;5;221;38;5;95m▄[48;5;227;38;5;233m▄▄▄▄[38;5;95m▄[49;38;5;172m▀[38;5;233m▀[39m         [48;5;233;38;5;233m██[49;39m   [00m
              [48;5;233;38;5;66m▄[48;5;231;38;5;231m██████████████[48;5;221;38;5;227m▄[38;5;221m█████[48;5;95;38;5;95m█[49;39m                [48;5;233;38;5;233m█[48;5;66;38;5;66m█[48;5;233;38;5;233m█[49;39m   [00m
             [48;5;95;38;5;95m█[48;5;66;38;5;66m█[48;5;231;38;5;231m███████████████[48;5;221;38;5;221m██[38;5;227m▄▄▄[38;5;172m▄[48;5;233;38;5;233m█[49;39m             [38;5;95m▄[48;5;95;38;5;221m▄[48;5;233;38;5;66m▄[48;5;66;38;5;233m▄[48;5;233m█[49;39m   [00m
            [48;5;95;38;5;95m█[48;5;221;38;5;221m█[48;5;66;38;5;66m█[48;5;231;38;5;231m███████████████[48;5;172;38;5;172m█[48;5;227;38;5;227m█████[48;5;172m▄[48;5;233;38;5;233m█[49;39m         [38;5;233m▄▄[48;5;95;38;5;66m▄[48;5;221m▄[38;5;95m▄[48;5;172;38;5;172m█[48;5;233;38;5;233m█[49;39m    [00m
           [48;5;95;38;5;95m█[48;5;221;38;5;221m███[48;5;66;38;5;233m▄[48;5;254;38;5;254m█[48;5;231m▄[38;5;231m██████████[38;5;254m▄[48;5;172;38;5;172m█[48;5;227;38;5;227m███████[48;5;221m▄[48;5;233;38;5;95m▄[49;38;5;233m▄▄▄▄▄[48;5;233;38;5;66m▄▄▄[48;5;221;38;5;233m▄[38;5;221m█[48;5;66m▄[38;5;233m▄[48;5;233m██[49;39m     [00m
           [48;5;95;38;5;95m█[48;5;221;38;5;221m████[48;5;66;38;5;233m▄[48;5;254;38;5;254m███[48;5;231m▄▄▄▄▄[48;5;254m████[48;5;172;38;5;95m▄[48;5;227;38;5;227m████████[48;5;172;38;5;221m▄[48;5;95;38;5;95m█[48;5;221;38;5;221m█████[48;5;66m▄[38;5;233m▄▄[48;5;233m█[48;5;172;38;5;172m█[48;5;233;38;5;233m█[49m▀[39m      [00m
           [38;5;95m▀[48;5;221m▄[38;5;172m▄[38;5;221m█[38;5;172m▄[48;5;172m█[48;5;233m▄[48;5;254;38;5;233m▄[38;5;254m██████████[48;5;95;38;5;95m█[48;5;221;38;5;221m█[48;5;227m▄▄[38;5;227m████[38;5;221m▄[48;5;221m█[48;5;233;38;5;233m█[48;5;172;38;5;172m█[48;5;221m▄▄▄[48;5;172m██[48;5;233;38;5;233m███[49m▀[39m        [00m
            [38;5;233m▀[48;5;172m▄[38;5;172m█████[48;5;233m▄[48;5;254;38;5;233m▄[38;5;254m█████████[48;5;95;38;5;95m█[48;5;221;38;5;221m███████[48;5;172;38;5;233m▄[48;5;233;38;5;172m▄[48;5;172m████[38;5;233m▄[48;5;233m██[49m▀[39m          [00m
              [38;5;233m▀[38;5;95m▀[48;5;172;38;5;233m▄[38;5;95m▄[38;5;172m███[48;5;233;38;5;95m▄[49;38;5;66m▀[48;5;254;38;5;233m▄[38;5;66m▄[38;5;254m█████[38;5;145m▄[48;5;95m▄[48;5;221;38;5;233m▄[38;5;221m████[38;5;233m▄[48;5;233;38;5;172m▄[48;5;172m█[38;5;95m▄[38;5;233m▄▄[49m▀▀[39m             [00m
                [38;5;233m▄[48;5;233;38;5;172m▄[48;5;172m██[48;5;95;38;5;233m▄[49;39m    [38;5;233m▀▀[38;5;66m▀[48;5;145;38;5;233m▄▄▄▄▄[48;5;95m▄[48;5;221m▄[38;5;221m██[48;5;233;38;5;233m█[49m▀▀[38;5;95m▀[39m                 [00m
             [38;5;95m▄[48;5;95;38;5;221m▄▄[48;5;221m███[48;5;172m▄[38;5;172m█[48;5;233;38;5;233m█[49;39m           [38;5;233m▄[48;5;233;38;5;227m▄[48;5;221;38;5;221m██[48;5;172;38;5;227m▄[48;5;233;38;5;95m▄[49;39m                   [00m
            [48;5;95;38;5;95m█[48;5;172;38;5;231m▄▄[48;5;221;38;5;172m▄[38;5;221m█[38;5;95m▄[38;5;233m▄[48;5;172m▄[49m▀[39m           [38;5;233m▄[48;5;95;38;5;221m▄[48;5;227m▄[38;5;227m███[38;5;221m▄[48;5;95;38;5;233m▄[49;39m                  [00m
             [38;5;233m▀▀▀▀[38;5;95m▀[39m              [48;5;233;38;5;233m█[48;5;221;38;5;172m▄[38;5;221m█[38;5;172m▄▄▄[38;5;221m█[48;5;233;38;5;233m█[49;39m                  [00m
                                 [38;5;233m▀[48;5;172m▄[48;5;231m▄▄▄[49m▀[39m                   [00m
                                                          [00m
