$$$
NAME: Buneary
OTHER NAMES: Laporeille, 이어롤, 捲捲耳, Mimirol, ミミロル, Haspiror
APPEARANCE: Pokémon Generation IV
KIND: Rabbit Pokémon
GROUP: ground, humanshape
BALLOON: top
COAT: brown
DISPLAY: full
BALLOON TOP: 3
BALLOON BOTTOM: 0
POSE: master
BASED ON: Buneary
SOURCE: PokéAPI Sprites
MEDIA: Pokémon
LICENSE: Copyright The Pokémon Company
FREE: no
WIDTH: 67
HEIGHT: 32


$$$
$balloon5$
 $\$                                                                 [00m
  $\$                                                                [00m
   $\$                                                               [00m
    $\$                              [00m
     $\$ [38;5;101m▄▄▄[39m                         [00m
      [48;5;101;38;5;101m█[48;5;180;38;5;187m▄[48;5;187m██[48;5;101m▄[49;38;5;101m▄[48;5;101;38;5;187m▄[38;5;180m▄[49;38;5;101m▄[39m                    [00m
   [38;5;101m▄[48;5;101;38;5;180m▄[38;5;187m▄[48;5;180m▄[48;5;187m███[48;5;180m▄[48;5;187m██[48;5;180m▄[38;5;180m█[48;5;101;38;5;101m█[49;39m                   [00m
   [48;5;101;38;5;101m█[48;5;187;38;5;187m██████████[38;5;180m▄[48;5;180m█[48;5;101;38;5;101m█[49;39m                  [00m
   [48;5;101;38;5;101m█[48;5;187;38;5;180m▄[38;5;187m█[38;5;180m▄[38;5;187m██████[38;5;180m▄[48;5;180m██[48;5;101;38;5;101m█[49;39m                  [00m
    [48;5;101;38;5;101m█[48;5;180;38;5;180m█[48;5;187m▄▄▄[48;5;180m██[48;5;187m▄[48;5;180m██[38;5;101m▄[49m▀[39m                   [00m
     [38;5;101m▀[48;5;180m▄▄▄[48;5;101;38;5;137m▄[48;5;180;38;5;101m▄▄▄[48;5;101;38;5;137m▄[48;5;233;38;5;237m▄[49;39m                    [00m
      [48;5;233;38;5;233m█[48;5;137;38;5;137m█[38;5;95m▄[38;5;95m▄[38;5;137m████[48;5;95;38;5;95m█[48;5;237;38;5;237m█[49;39m                   [00m
      [38;5;233m▀[48;5;95m▄[48;5;211;38;5;211m██[48;5;95;38;5;95m█[48;5;137;38;5;137m██[48;5;95;38;5;95m██[48;5;233;38;5;233m█[49;39m                   [00m
       [48;5;233;38;5;233m█[48;5;211;38;5;168m▄▄[48;5;168m█[48;5;95;38;5;95m█[48;5;95;38;5;95m███[48;5;233;38;5;233m█[49;39m                   [00m
        [48;5;233;38;5;233m█[48;5;168;38;5;168m██[48;5;95;38;5;95m█[48;5;95;38;5;95m██[38;5;233m▄[49m▀[39m                   [00m
        [38;5;233m▀[48;5;168m▄[38;5;168m█[48;5;95m▄[48;5;95;38;5;95m▄[38;5;95m█[48;5;233;38;5;237m▄[49m▄▄▄▄▄[39m   [38;5;237m▄[38;5;233m▄▄▄[39m        [00m
         [38;5;233m▀[48;5;168;38;5;237m▄[38;5;95m▄[48;5;95;38;5;137m▄[48;5;237m▄[48;5;95m▄[48;5;137m█████[48;5;237m▄[38;5;233m▄[48;5;233;38;5;137m▄[48;5;137m███[38;5;101m▄[48;5;233;38;5;187m▄[48;5;101m▄▄[49;38;5;101m▄▄▄[39m  [00m
        [38;5;237m▄[48;5;237;38;5;137m▄[48;5;137;38;5;180m▄[38;5;137m█████████[48;5;95;38;5;237m▄[38;5;137m▄[48;5;137m███[48;5;101;38;5;101m█[48;5;187;38;5;187m█████[48;5;180m▄[48;5;187m█[48;5;101m▄[49;38;5;101m▄[39m[00m
       [48;5;237;38;5;95m▄[48;5;137;38;5;137m█[48;5;95m▄[48;5;187;38;5;95m▄[48;5;95;38;5;137m▄[48;5;137m██████[38;5;180m▄[38;5;137m█[48;5;237;38;5;237m█[48;5;137;38;5;137m███[38;5;101m▄[48;5;101;38;5;187m▄[48;5;180m▄[48;5;187m██████[38;5;180m▄[48;5;101;38;5;101m█[49;39m[00m
      [48;5;237;38;5;233m▄[48;5;137;38;5;137m█[38;5;95m▄[38;5;237m▄[38;5;137m███████[48;5;95m▄[48;5;187;38;5;95m▄[48;5;95;38;5;137m▄[48;5;237;38;5;95m▄[48;5;137m▄[38;5;137m██[48;5;101;38;5;101m█[48;5;187;38;5;180m▄[38;5;187m████[38;5;180m▄▄[48;5;180m█[48;5;101m▄[49;38;5;101m▄[39m[00m
      [48;5;233;38;5;233m█[48;5;95;38;5;95m█[48;5;237;38;5;237m█[48;5;231;38;5;233m▄[48;5;95;38;5;95m█[48;5;137;38;5;137m████████[38;5;237m▄[38;5;95m▄[48;5;237m▄[48;5;137m▄[38;5;137m██[48;5;101m▄[48;5;180;38;5;101m▄[48;5;101;38;5;180m▄[48;5;180m███[38;5;101m▄[38;5;180m█[38;5;101m▄[49m▀[39m[00m
       [48;5;236;38;5;233m▄[48;5;95;38;5;95m█[48;5;237m▄[48;5;95m█[48;5;95;38;5;168m▄[38;5;211m▄[48;5;137;38;5;95m▄[38;5;137m██[38;5;95m▄▄[48;5;237;38;5;237m█[48;5;231;38;5;233m▄[48;5;233m█[48;5;95;38;5;95m█[48;5;233m▄[48;5;95;38;5;233m▄[38;5;95m███[48;5;101m▄[48;5;180m▄▄[48;5;101;38;5;237m▄[38;5;233m▄[49;38;5;101m▀[39m  [00m
        [38;5;233m▀[48;5;95m▄[38;5;237m▄[38;5;95m█[48;5;95;38;5;237m▄[48;5;95;38;5;95m██████[48;5;237m▄[48;5;95m████[48;5;233m▄[48;5;237;38;5;237m█[48;5;95;38;5;233m▄▄[48;5;237;38;5;237m█[49;38;5;233m▀▀[39m    [00m
          [38;5;233m▀▀[48;5;95m▄[48;5;237m▄[48;5;95;38;5;237m▄▄▄[48;5;237;38;5;95m▄[48;5;95m███[38;5;237m▄[38;5;233m▄[49;38;5;237m▀[38;5;233m▀[39m          [00m
              [38;5;237m▀[48;5;233m▄[38;5;95m▄[48;5;237m▄[48;5;95m█[48;5;237m▄[48;5;233m▄[49;38;5;237m▄▄[39m            [00m
            [38;5;237m▄▄[48;5;237;38;5;137m▄[48;5;137;38;5;95m▄[38;5;137m██[48;5;95m▄▄▄▄[48;5;137m█[48;5;237m▄▄▄[49;38;5;237m▄▄[38;5;233m▄[39m      [00m
         [38;5;237m▄[48;5;237;38;5;95m▄[38;5;137m▄[48;5;137m██[38;5;237m▄[48;5;237;38;5;137m▄[48;5;137m█[38;5;101m▄▄[38;5;187m▄[38;5;101m▄[48;5;237;38;5;180m▄[48;5;137;38;5;233m▄[38;5;237m▄[38;5;137m███[38;5;95m▄[48;5;95;38;5;137m▄[48;5;233;38;5;233m█[49;39m     [00m
         [48;5;237;38;5;237m█[48;5;137;38;5;95m▄[38;5;137m██[38;5;237m▄[48;5;101;38;5;180m▄[48;5;187;38;5;187m█[48;5;101m▄[48;5;187m████[38;5;180m▄[38;5;187m█[48;5;233;38;5;180m▄▄[48;5;137;38;5;233m▄▄▄[49m▀[39m      [00m
          [38;5;237m▀[38;5;233m▀▀[38;5;101m▀[48;5;180m▄[48;5;187;38;5;187m██████[38;5;180m▄[48;5;180;38;5;187m▄[48;5;187;38;5;180m▄[48;5;180m█[38;5;101m▄[38;5;180m█[48;5;233;38;5;233m█[49;39m       [00m
             [48;5;233;38;5;233m█[48;5;180;38;5;101m▄[38;5;180m█[48;5;187m▄▄▄▄[48;5;180m█[48;5;187m▄[48;5;180m████[48;5;233;38;5;101m▄[49;38;5;233m▄[39m       [00m
              [38;5;233m▀[48;5;101m▄[48;5;180;38;5;101m▄[38;5;233m▄[48;5;101m▄[48;5;180;38;5;101m▄[38;5;180m███[48;5;101;38;5;233m▄[48;5;180;38;5;101m▄[48;5;101;38;5;180m▄[48;5;180m█[48;5;101m▄[48;5;233;38;5;233m█[49;39m      [00m
               [38;5;233m▄[48;5;233;38;5;180m▄[48;5;180m███[48;5;233m▄[38;5;101m▄[38;5;233m██[48;5;180;38;5;180m██[38;5;187m▄[48;5;187;38;5;101m▄[38;5;187m█[48;5;233;38;5;233m█[49;39m     [00m
               [38;5;233m▀[48;5;101m▄[48;5;187m▄[48;5;101m▄[48;5;187m▄[48;5;101m▄[49m▀[39m  [38;5;233m▀[48;5;187m▄[48;5;101m▄[48;5;187m▄[49m▀[39m      [00m
                                   [00m
