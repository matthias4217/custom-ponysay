$$$
NAME: Wailmer
OTHER NAMES: ホエルコ, 吼吼鯨, 고래왕자, Whalko
APPEARANCE: Pokémon Generation III
KIND: Ball Whale Pokémon
GROUP: ground, water2
BALLOON: top
COAT: blue
DISPLAY: full
BALLOON TOP: 3
BALLOON BOTTOM: 0
POSE: master
BASED ON: Wailmer
SOURCE: PokéAPI Sprites
MEDIA: Pokémon
LICENSE: Copyright The Pokémon Company
FREE: no
WIDTH: 75
HEIGHT: 27


$$$
$balloon5$
                            $\$                                                           [00m
                             $\$                                                          [00m
                              $\$                                                         [00m
                               $\$                                           [00m
                            [38;5;60m▄▄▄▄[48;5;60;38;5;67m▄▄▄▄▄[38;5;67m▄▄▄▄▄▄[49;38;5;60m▄▄▄▄[39m                            [00m
                        [38;5;60m▄▄[48;5;60;38;5;67m▄▄[48;5;67m█[38;5;67m▄[48;5;67m█████████[48;5;67m▄[38;5;67m█████[48;5;60m▄[38;5;60m█[48;5;60m▄▄[49;38;5;236m▄▄[39m                        [00m
                      [38;5;60m▄[48;5;60;38;5;67m▄[48;5;67m█████[48;5;67m▄[38;5;67m█████████[38;5;67m▄[48;5;67m███████[48;5;60;38;5;60m████[48;5;236m▄▄[49;38;5;236m▄[39m                     [00m
                    [38;5;60m▄[48;5;60;38;5;67m▄[48;5;67m██████████[48;5;67m▄▄▄▄▄[48;5;67m███████████[48;5;60;38;5;60m██████[48;5;236m▄[49;38;5;16m▄[39m                   [00m
                   [38;5;236m▄[48;5;60;38;5;67m▄[48;5;67m███[48;5;16m▄[48;5;67m███████████████████████[48;5;60;38;5;60m████████[48;5;16m▄[49;38;5;16m▄[39m                 [00m
                [38;5;60m▄▄[48;5;60;38;5;67m▄[48;5;236;38;5;236m█[48;5;60;38;5;60m█[48;5;67m▄[38;5;67m████████████████[38;5;16m▄[38;5;67m████████[48;5;60;38;5;60m███████████[48;5;16;38;5;16m█[49;39m                [00m
         [38;5;60m▄▄▄▄▄[48;5;60;38;5;67m▄▄[48;5;67;38;5;67m▄[48;5;67;38;5;60m▄[48;5;60m██[48;5;236;38;5;236m█[38;5;250m▄[48;5;60;38;5;236m▄[48;5;67;38;5;60m▄▄[38;5;67m███████████████████[38;5;60m▄▄[48;5;60m█████████████[48;5;16;38;5;16m█[49;39m               [00m
    [38;5;16m▄[48;5;60;38;5;67m▄▄▄▄[48;5;67m█[38;5;67m▄[38;5;60m▄▄▄[48;5;67m▄[48;5;60m███[48;5;60;38;5;16m▄[48;5;16;38;5;223m▄[48;5;236;38;5;95m▄[48;5;250;38;5;250m█[38;5;231m▄[48;5;236;38;5;250m▄[48;5;60;38;5;236m▄▄[38;5;60m██[48;5;67m▄▄▄[38;5;67m███████[38;5;60m▄▄▄▄[48;5;60m██████████████████[48;5;16;38;5;16m█[49;39m              [00m
   [38;5;16m▄[48;5;16;38;5;67m▄[48;5;67m█[38;5;67m▄[48;5;67;38;5;60m▄[48;5;60m█████████[48;5;16;38;5;16m█[48;5;230;38;5;230m█[48;5;223m▄[48;5;95;38;5;223m▄[48;5;250;38;5;95m▄[48;5;231;38;5;231m█[48;5;250;38;5;250m█[48;5;231;38;5;231m█[48;5;245;38;5;250m▄[48;5;236;38;5;231m▄[38;5;245m▄[38;5;250m▄[48;5;60;38;5;236m▄▄▄▄▄[38;5;60m▄▄[38;5;60m███[38;5;60m▄[38;5;236m▄▄[38;5;60m▄[38;5;60m██████████████████[48;5;16;38;5;16m█[49;39m             [00m
   [48;5;16;38;5;16m█[48;5;67;38;5;67m▄[48;5;67;38;5;60m▄[48;5;60m██████████[38;5;16m▄[48;5;16;38;5;223m▄[48;5;230;38;5;230m█[38;5;180m▄[48;5;180;38;5;230m▄[48;5;95;38;5;223m▄[48;5;250;38;5;95m▄[48;5;231;38;5;231m█[48;5;250;38;5;250m█[48;5;231;38;5;231m█[48;5;250;38;5;250m█[38;5;231m▄[48;5;231m█[48;5;250;38;5;250m█[48;5;231;38;5;231m█[48;5;245;38;5;250m▄[48;5;250;38;5;231m▄[48;5;245;38;5;250m▄[48;5;250;38;5;231m▄[48;5;236;38;5;250m▄[38;5;231m▄[38;5;250m▄[38;5;231m▄[48;5;60;38;5;250m▄[48;5;250;38;5;231m▄[38;5;236m▄[48;5;236;38;5;95m▄[38;5;236m█[48;5;60m▄[38;5;60m██[38;5;60m▄[38;5;60m██████████████[48;5;16;38;5;16m█[49;39m            [00m
   [38;5;16m▀[48;5;60m▄[38;5;236m▄▄▄▄▄[48;5;60;38;5;60m█[48;5;236;38;5;60m▄▄[48;5;60m▄[48;5;60m█[48;5;16;38;5;16m█[48;5;180;38;5;223m▄[48;5;223m█[38;5;180m▄[48;5;180;38;5;223m▄[48;5;230;38;5;230m██[48;5;223m▄[48;5;95;38;5;223m▄[48;5;250;38;5;95m▄[48;5;231m▄[48;5;250;38;5;250m█[48;5;231;38;5;231m█[48;5;250;38;5;250m█[48;5;231;38;5;231m█[38;5;250m▄[48;5;250;38;5;231m▄[48;5;231;38;5;250m▄[48;5;250;38;5;231m▄[48;5;231;38;5;250m▄[48;5;250;38;5;231m▄[48;5;231;38;5;250m▄[48;5;250;38;5;231m▄[48;5;231;38;5;236m▄[48;5;250m▄[48;5;236;38;5;223m▄[48;5;223m████[48;5;236m▄[48;5;60;38;5;236m▄[48;5;16;38;5;16m█[48;5;67;38;5;60m▄[38;5;67m█[48;5;60m▄▄▄[38;5;60m██████[38;5;67m▄▄[48;5;236m▄[48;5;16;38;5;236m▄[49m▄[39m           [00m
    [38;5;16m▀[48;5;60m▄[38;5;60m████[38;5;60m▄[38;5;236m▄▄[48;5;236;38;5;60m▄[38;5;16m▄[48;5;180;38;5;223m▄[48;5;223m█[38;5;101m▄[48;5;101;38;5;223m▄[48;5;223m██[48;5;230m▄[38;5;230m██[38;5;180m▄[48;5;180;38;5;230m▄[48;5;95m▄▄[38;5;180m▄[48;5;231;38;5;95m▄[48;5;250m▄[48;5;231m▄[48;5;250m▄[48;5;231m▄[48;5;250m▄[48;5;236;38;5;223m▄▄[38;5;101m▄[48;5;223;38;5;223m█████████[48;5;16m▄[48;5;60;38;5;16m▄[48;5;67;38;5;67m███████████████[48;5;236m▄▄[49;38;5;236m▄▄[39m       [00m
      [38;5;16m▀▀[48;5;236m▄▄[48;5;60m▄[48;5;60;38;5;236m▄[38;5;60m▄▄[48;5;16;38;5;16m█[48;5;223;38;5;223m██[48;5;101;38;5;101m█[48;5;223;38;5;223m█████[38;5;180m▄[48;5;101;38;5;101m█[48;5;230;38;5;223m▄[38;5;230m████[38;5;223m▄▄[48;5;223m████[48;5;101;38;5;101m█[48;5;223;38;5;223m███████████[48;5;16;38;5;16m█[48;5;67;38;5;60m▄[38;5;67m▄[38;5;67m████████████████[48;5;67m▄[48;5;236;38;5;67m▄▄[49;38;5;236m▄[39m    [00m
           [38;5;16m▀▀▀[48;5;16m█[48;5;223;38;5;180m▄[48;5;95;38;5;95m█[48;5;223;38;5;223m██████[48;5;101;38;5;101m█[48;5;223;38;5;223m████████████[48;5;101;38;5;101m█[48;5;223;38;5;223m████████████[48;5;16;38;5;16m█[48;5;67;38;5;60m▄[48;5;67;38;5;67m▄[38;5;67m█████████████████[48;5;67;38;5;67m██[48;5;236m▄[49;38;5;236m▄[39m  [00m
              [48;5;16;38;5;16m█[48;5;180;38;5;180m█[48;5;95;38;5;95m█[48;5;223;38;5;223m██████[48;5;101;38;5;95m▄[48;5;223;38;5;223m████████████[48;5;101;38;5;101m█[48;5;223;38;5;223m███████████[38;5;180m▄[48;5;180;38;5;223m▄[48;5;16;38;5;16m█[48;5;67;38;5;67m█[48;5;67m▄[38;5;67m██[48;5;60;38;5;60m█[48;5;67;38;5;67m███████████[48;5;60m▄[48;5;67;38;5;236m▄[48;5;67;38;5;67m████[48;5;236m▄[49;38;5;236m▄[39m[00m
              [38;5;95m▀[48;5;16;38;5;16m█[48;5;95;38;5;180m▄[48;5;180m█[48;5;223;38;5;223m█████[48;5;95;38;5;95m█[48;5;223;38;5;223m█████████████[48;5;95;38;5;95m█[48;5;223;38;5;223m█████████[48;5;180m▄[48;5;223;38;5;180m▄[48;5;180;38;5;223m▄[48;5;16;38;5;16m█[48;5;67;38;5;60m▄[38;5;67m██[48;5;67m▄▄[48;5;236;38;5;236m█[48;5;67;38;5;67m▄[38;5;67m█████[38;5;236m▄[38;5;67m█[38;5;67m▄▄[48;5;67m██[48;5;236;38;5;60m▄[48;5;67m▄[38;5;67m██[48;5;67;38;5;60m▄[48;5;236;38;5;236m█[49;39m[00m
               [38;5;16m▀[48;5;180m▄[38;5;180m██[48;5;223m▄[38;5;223m███[48;5;95;38;5;95m█[48;5;223;38;5;223m█████████████[48;5;95m▄[48;5;223;38;5;95m▄[38;5;223m███████[38;5;180m▄[48;5;180;38;5;223m▄[48;5;223;38;5;180m▄[48;5;180;38;5;223m▄[38;5;180m█[48;5;16;38;5;16m█[48;5;67;38;5;67m▄[38;5;67m███[48;5;60m▄[48;5;67;38;5;236m▄[38;5;67m█████[48;5;236;38;5;60m▄[48;5;67m▄[38;5;67m█████[48;5;236m▄[48;5;67;38;5;236m▄[48;5;67;38;5;60m▄[48;5;60m█[48;5;16;38;5;16m█[49;39m[00m
                [38;5;16m▀[48;5;180m▄[38;5;180m███[48;5;223m▄▄[48;5;95;38;5;223m▄[48;5;223;38;5;95m▄[38;5;223m█████████████[48;5;95m▄[48;5;223;38;5;95m▄[38;5;223m███[38;5;180m▄▄[48;5;180m███[48;5;223m▄[48;5;180m███[48;5;60;38;5;16m▄[48;5;67;38;5;60m▄[48;5;67m▄[38;5;67m▄[38;5;67m█[48;5;236;38;5;236m█[48;5;67;38;5;67m██████[48;5;16;38;5;16m█[48;5;67;38;5;67m██████[48;5;236;38;5;67m▄[48;5;60;38;5;236m▄[48;5;16;38;5;16m█[49;39m [00m
                 [38;5;95m▀[48;5;95;38;5;16m▄[48;5;180;38;5;180m█████[48;5;95;38;5;95m█[48;5;223;38;5;180m▄▄▄[38;5;223m████████[38;5;180m▄▄▄[48;5;95m▄[48;5;180;38;5;95m▄[38;5;180m████████████[48;5;16m▄▄[48;5;60;38;5;16m▄▄[48;5;236m▄[48;5;67;38;5;60m▄[48;5;67m▄[38;5;67m▄[38;5;67m███[38;5;67m▄[48;5;16;38;5;16m█[48;5;67;38;5;67m██[38;5;67m▄[38;5;60m▄[48;5;67m▄[48;5;60;38;5;236m▄[48;5;16;38;5;16m█[49;39m  [00m
                   [38;5;16m▀[48;5;180m▄[38;5;180m████[48;5;95;38;5;95m█[48;5;180;38;5;180m██████████████[48;5;95m▄[48;5;180;38;5;95m▄[38;5;180m███████████████[48;5;95;38;5;16m▄[49m▀▀[48;5;60m▄▄▄▄▄[49m▀[48;5;60m▄▄▄▄[49m▀[38;5;236m▀[39m   [00m
                     [38;5;16m▀[48;5;180m▄▄[38;5;180m██[48;5;95;38;5;95m█[48;5;180;38;5;180m███████████████[48;5;95m▄[48;5;180;38;5;95m▄▄[38;5;180m██████████[38;5;16m▄[49m▀[39m                  [00m
                        [38;5;16m▀▀[48;5;180m▄[48;5;95m▄[48;5;180;38;5;95m▄[38;5;180m████████████████[48;5;95m▄▄[48;5;180;38;5;95m▄▄▄[38;5;180m█[38;5;16m▄▄[49m▀▀[39m                    [00m
                            [38;5;16m▀▀[48;5;95;38;5;95m█[48;5;180;38;5;16m▄▄▄[38;5;180m██████████[38;5;16m▄▄▄▄[49m▀▀▀[39m                        [00m
                                  [38;5;16m▀▀▀▀▀▀▀▀▀▀[39m                               [00m
                                                                           [00m
