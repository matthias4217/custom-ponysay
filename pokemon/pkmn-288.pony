$$$
NAME: Vigoroth
OTHER NAMES: ヤルキモノ, Yarukimono, Muntier, 過動猿, 발바로
APPEARANCE: Pokémon Generation III
KIND: Wild Monkey Pokémon
GROUP: ground
BALLOON: top
COAT: white
DISPLAY: full
BALLOON TOP: 3
BALLOON BOTTOM: 0
POSE: master
BASED ON: Vigoroth
SOURCE: PokéAPI Sprites
MEDIA: Pokémon
LICENSE: Copyright The Pokémon Company
FREE: no
WIDTH: 75
HEIGHT: 31


$$$
$balloon5$
                           $\$                                                             [00m
                            $\$                                                            [00m
                             $\$                                                           [00m
                              $\$                                       [00m
                              [38;5;95m▄[48;5;95;38;5;203m▄[38;5;95m█[49;39m                                     [00m
                           [38;5;95m▄[48;5;95;38;5;203m▄▄[48;5;203m█[48;5;95;38;5;95m█[38;5;203m▄[38;5;95m█[49;39m                                    [00m
                         [38;5;95m▄[48;5;95;38;5;203m▄[48;5;203m███[48;5;131m▄[48;5;203m██[48;5;233;38;5;233m█[48;5;240;38;5;188m▄[49;38;5;240m▄▄[39m                                 [00m
                        [38;5;95m▄[48;5;95;38;5;203m▄[48;5;203m██████[38;5;131m▄[48;5;233;38;5;233m█[48;5;188;38;5;188m█[38;5;231m▄[38;5;233m▄[48;5;240m▄[48;5;233;38;5;231m▄▄[49;38;5;233m▄[39m                             [00m
                        [48;5;233;38;5;233m█[48;5;203;38;5;131m▄[38;5;203m███[38;5;131m▄▄[48;5;131m█[48;5;95;38;5;233m▄[48;5;233;38;5;95m▄[48;5;137m▄[48;5;231;38;5;137m▄[38;5;231m███[38;5;240m▄[48;5;233m▄[49;39m                             [00m
                       [48;5;238;38;5;238m█[48;5;233;38;5;95m▄[48;5;131;38;5;95m▄[38;5;131m█████[38;5;233m▄[48;5;233;38;5;231m▄[48;5;188m▄[48;5;95m▄[38;5;95m█[48;5;137m▄[48;5;231;38;5;231m█[48;5;188m▄[48;5;238;38;5;188m▄[48;5;188m█[48;5;238m▄[49;38;5;233m▄[39m                           [00m
                       [48;5;95;38;5;238m▄[48;5;188;38;5;231m▄[48;5;231;38;5;240m▄[48;5;233;38;5;203m▄▄[38;5;131m▄▄[38;5;95m▄[48;5;231;38;5;95m▄[48;5;240;38;5;233m▄[48;5;231;38;5;231m█[38;5;95m▄[48;5;95m█[38;5;137m▄[48;5;231;38;5;231m█[38;5;254m▄[48;5;188;38;5;188m███[48;5;233m▄[49;38;5;233m▄[39m                          [00m
                      [48;5;240;38;5;233m▄[48;5;238;38;5;231m▄[48;5;188;38;5;137m▄[48;5;233m▄[48;5;203;38;5;95m▄[48;5;131m▄▄[38;5;244m▄[48;5;244;38;5;254m▄[48;5;95;38;5;231m▄▄▄▄[48;5;244;38;5;254m▄[48;5;231;38;5;188m▄[48;5;254m▄[48;5;188m█████[48;5;233m▄[49;38;5;240m▄[39m                         [00m
                      [48;5;233;38;5;233m█[48;5;231;38;5;244m▄[38;5;188m▄[48;5;254;38;5;238m▄[48;5;244;38;5;233m▄[48;5;238;38;5;131m▄▄▄▄[48;5;244m▄[48;5;254;38;5;238m▄[48;5;244;38;5;231m▄[38;5;238m▄[48;5;238;38;5;131m▄[48;5;233m▄[38;5;233m█[48;5;188;38;5;238m▄[38;5;188m██████[48;5;240m▄▄▄[49;38;5;240m▄▄▄[39m                   [00m
                  [38;5;238m▄▄[48;5;238;38;5;231m▄▄[48;5;231m█[48;5;233m▄▄[48;5;188;38;5;233m▄[48;5;233;38;5;231m▄▄[48;5;131;38;5;233m▄[38;5;131m█[38;5;95m▄[48;5;95;38;5;131m▄[48;5;131m█[48;5;233m▄[48;5;131m███[48;5;233;38;5;233m█[48;5;238;38;5;238m█[48;5;188;38;5;188m███[38;5;244m▄[38;5;188m████████[48;5;240m▄▄[49;38;5;240m▄▄[39m               [00m
               [38;5;233m▄[48;5;233;38;5;231m▄▄[48;5;231m█████████[38;5;233m▄[48;5;233;38;5;131m▄[48;5;131;38;5;95m▄[48;5;95;38;5;131m▄[48;5;131;38;5;168m▄[48;5;168m███[48;5;131m▄[48;5;233;38;5;233m█[48;5;95;38;5;95m█[48;5;238;38;5;233m▄[48;5;188;38;5;188m██[48;5;238m▄[48;5;188;38;5;238m▄[38;5;188m███████████[38;5;231m▄[48;5;240m▄▄[49;38;5;240m▄▄[39m           [00m
            [38;5;240m▄[48;5;240;38;5;188m▄[38;5;231m▄[48;5;231m███████████[38;5;238m▄[48;5;233;38;5;238m▄[48;5;131;38;5;131m█[48;5;95;38;5;95m█[48;5;168;38;5;238m▄▄[38;5;233m▄[38;5;238m▄[48;5;238;38;5;231m▄[48;5;168;38;5;233m▄[48;5;233;38;5;95m▄[48;5;95;38;5;238m▄[48;5;233;38;5;188m▄[48;5;188m█[48;5;238m▄[38;5;244m▄[48;5;188m▄[38;5;188m█████████[38;5;231m▄[48;5;231m██████[48;5;240m▄[38;5;188m▄[49;38;5;240m▄[39m        [00m
         [38;5;240m▄[48;5;240;38;5;188m▄[38;5;231m▄[48;5;231m██████████████[48;5;233;38;5;233m█[48;5;188;38;5;95m▄[48;5;233;38;5;137m▄▄[48;5;137m█[38;5;95m▄▄[48;5;238m▄▄[48;5;95m█[38;5;233m▄[48;5;233;38;5;188m▄[48;5;188m██[38;5;244m▄[38;5;238m▄▄[48;5;244m▄[48;5;188;38;5;188m███████[48;5;231;38;5;231m███████████[48;5;244m▄[49;38;5;244m▄[39m      [00m
       [38;5;240m▄[48;5;240;38;5;231m▄[48;5;231m█████████████[38;5;188m▄[38;5;233m▄▄[38;5;231m██[48;5;233m▄[48;5;95;38;5;233m▄▄▄▄▄▄[48;5;233;38;5;188m▄▄[48;5;188m██[48;5;244m▄▄[48;5;188;38;5;254m▄[38;5;188m████[48;5;240m▄▄[38;5;233m▄[48;5;188m▄▄[48;5;231m▄[38;5;231m█████████████[48;5;244;38;5;188m▄[49;38;5;244m▄[39m    [00m
      [38;5;240m▄[48;5;240;38;5;188m▄[48;5;231;38;5;231m█████████[38;5;188m▄[38;5;233m▄[48;5;188m▄[49m▀▀▀[39m  [48;5;233;38;5;233m█[48;5;231;38;5;231m█[38;5;244m▄[38;5;231m███[48;5;188m▄▄[38;5;254m▄▄▄▄[38;5;231m▄[48;5;254m▄▄[48;5;231m█[38;5;254m▄[48;5;188;38;5;188m█████[48;5;233;38;5;233m█[49;39m   [38;5;233m▀▀▀[48;5;188m▄[48;5;231m▄[38;5;231m█████████[48;5;244;38;5;188m▄[49;38;5;244m▄[39m   [00m
     [38;5;240m▄[48;5;240;38;5;188m▄[48;5;231;38;5;231m████████[38;5;233m▄[49m▀▀[39m       [38;5;240m▄[48;5;240;38;5;231m▄[48;5;231m██[38;5;244m▄[38;5;188m▄[38;5;231m█████████[38;5;254m▄[48;5;188;38;5;188m████[38;5;137m▄[48;5;137;38;5;95m▄[48;5;233;38;5;233m█[49;39m       [38;5;233m▄▀[48;5;188m▄[48;5;231;38;5;188m▄▄[38;5;231m████[38;5;188m▄[48;5;188m█[48;5;244m▄[49;38;5;244m▄[39m  [00m
    [38;5;240m▄[48;5;240;38;5;188m▄[48;5;231;38;5;231m█████[38;5;188m▄▄[48;5;188m█[48;5;233;38;5;233m█[49;39m          [38;5;238m▀[48;5;238;38;5;233m▄[48;5;233;38;5;231m▄[48;5;244m▄[48;5;188m▄[48;5;231m█████████[38;5;188m▄[48;5;188m█████[48;5;95m▄▄[48;5;233m▄[49;38;5;233m▄[39m     [48;5;238;38;5;238m█[48;5;254;38;5;254m█[48;5;233;38;5;233m█[49m▀[48;5;188m▄[38;5;188m████████[48;5;244m▄[49;38;5;240m▄[39m [00m
    [48;5;240;38;5;240m█[48;5;188;38;5;188m██[48;5;231m▄▄[48;5;188m████[48;5;233;38;5;233m█[49;39m             [38;5;233m▀[38;5;238m▀[48;5;231;38;5;233m▄▄[38;5;188m▄[38;5;231m██[38;5;254m▄[38;5;188m▄▄[48;5;254m▄[48;5;188m████████[38;5;137m▄[38;5;95m▄[48;5;233;38;5;233m█[49;39m  [38;5;238m▄▄[48;5;238;38;5;254m▄[48;5;254m█[38;5;244m▄[48;5;233;38;5;233m█[49;39m [38;5;233m▀[48;5;188m▄[38;5;188m████████[48;5;240;38;5;240m█[49;39m [00m
   [48;5;240;38;5;240m█[48;5;188;38;5;188m████████[48;5;233;38;5;233m█[49;39m                  [48;5;233;38;5;233m█[48;5;188;38;5;188m██████████████[48;5;95m▄▄[48;5;233;38;5;233m█[48;5;238;38;5;188m▄▄[48;5;188;38;5;254m▄[48;5;254m█[38;5;188m▄[38;5;244m▄[48;5;233;38;5;254m▄[48;5;254m█[48;5;233;38;5;233m█[49;39m [38;5;233m▀[48;5;188m▄[38;5;188m████████[48;5;240;38;5;240m█[49;39m[00m
   [48;5;233;38;5;233m█[48;5;188;38;5;188m██████[38;5;238m▄[48;5;233;38;5;233m█[49;39m                [38;5;244m▄▄[48;5;240;38;5;231m▄▄[48;5;233;38;5;233m█[48;5;188;38;5;244m▄[38;5;188m██████████████[48;5;233m▄▄[48;5;188;38;5;233m▄▄[38;5;188m███[48;5;254m▄[48;5;188;38;5;233m▄[49m▀[39m  [48;5;233;38;5;233m█[48;5;188;38;5;188m████████[48;5;240;38;5;240m█[49;39m[00m
   [38;5;233m▀[48;5;240m▄[48;5;188;38;5;244m▄[38;5;238m▄▄▄[48;5;240;38;5;244m▄[48;5;244m█[48;5;233m▄[49;38;5;233m▄[39m             [38;5;240m▄[48;5;244;38;5;231m▄[48;5;231m█████[48;5;244m▄[48;5;188m▄[38;5;188m██████████[38;5;231m▄[48;5;254m▄[48;5;231m███[48;5;188m▄▄[48;5;233;38;5;188m▄[48;5;188;38;5;233m▄[38;5;188m█[38;5;233m▄[49m▀[39m    [48;5;233;38;5;233m█[48;5;188;38;5;188m███████[48;5;240;38;5;238m▄[49;39m[00m
    [38;5;238m▀▀[48;5;244;38;5;233m▄▄[48;5;233;38;5;240m▄[48;5;240;38;5;233m▄[48;5;244m▄[38;5;244m█[48;5;238m▄[49;38;5;238m▄[39m           [48;5;240;38;5;233m▄[48;5;231;38;5;254m▄[38;5;231m█████[38;5;188m▄[48;5;254m▄[48;5;188m████████████[48;5;231m▄[38;5;231m██████[48;5;233;38;5;188m▄[48;5;238;38;5;233m▄[49;39m       [48;5;233;38;5;233m█[48;5;238;38;5;244m▄[48;5;240m▄[48;5;188;38;5;233m▄[38;5;238m▄[38;5;244m▄[48;5;238m▄[48;5;233;38;5;233m█[49;39m[00m
        [38;5;238m▀▀▀▀[38;5;233m▀▀[39m           [38;5;233m▀[48;5;188m▄[38;5;188m█[48;5;254m▄▄▄[48;5;188m█████[38;5;233m▄▄[38;5;238m▄▄▄▄[38;5;233m▄▄[38;5;188m████[48;5;231m▄▄▄▄[48;5;188m██[48;5;233;38;5;233m█[49;39m      [48;5;233;38;5;233m█[48;5;244;38;5;244m██[38;5;233m▄[48;5;233;38;5;238m▄[48;5;244;38;5;244m██[48;5;233;38;5;233m█[49;39m [00m
                           [38;5;233m▀▀[48;5;188;38;5;238m▄▄[38;5;244m▄[38;5;188m██[48;5;244m▄[48;5;233m▄[49;38;5;233m▄[39m       [38;5;233m▀▀[48;5;188m▄[38;5;244m▄[38;5;188m███[38;5;233m▄▄[49m▀[39m      [48;5;233;38;5;233m█[48;5;244m▄[49;38;5;238m▀[48;5;233m▄[48;5;238;38;5;244m▄[48;5;244;38;5;238m▄[38;5;233m▄[49m▀[39m  [00m
                         [38;5;233m▄[48;5;238;38;5;244m▄▄[38;5;240m▄[48;5;188;38;5;233m▄[38;5;238m▄[38;5;188m███[38;5;233m▄▄[49m▀[39m       [38;5;233m▄[48;5;233;38;5;188m▄[48;5;188m██[48;5;244m▄▄[48;5;238m▄▄[49;38;5;238m▄▄[39m         [38;5;233m▀▀[39m     [00m
                        [48;5;238;38;5;233m▄[48;5;244m▄[48;5;240m▄[48;5;233;38;5;238m▄[38;5;244m▄[48;5;244m█[38;5;240m▄[48;5;238;38;5;233m▄[48;5;188m▄[49m▀[39m          [38;5;233m▀[48;5;188m▄▄[38;5;188m██[38;5;238m▄[38;5;244m▄[38;5;233m▄▄[48;5;238;38;5;244m▄[48;5;233m▄[49;38;5;233m▄[39m              [00m
                          [48;5;233;38;5;233m█[48;5;244m▄[49m▀▀▀[39m                [38;5;233m▀▀[48;5;244m▄▄▄[48;5;233;38;5;244m▄[48;5;238m▄[48;5;244;38;5;233m▄▄[48;5;233m█[49;39m             [00m
                                                    [38;5;233m▀▀▀[39m               [00m
                                                                      [00m
