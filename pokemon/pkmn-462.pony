$$$
NAME: Magnezone
OTHER NAMES: 自爆磁怪, Magnézone, 자포코일, ジバコイル, Jibacoil
APPEARANCE: Pokémon Generation IV
KIND: Magnet Area Pokémon
GROUP: mineral
BALLOON: top
COAT: gray
DISPLAY: full
BALLOON TOP: 3
BALLOON BOTTOM: 0
POSE: master
BASED ON: Magnezone
SOURCE: PokéAPI Sprites
MEDIA: Pokémon
LICENSE: Copyright The Pokémon Company
FREE: no
WIDTH: 75
HEIGHT: 31


$$$
$balloon5$
                                        $\$                                                [00m
                                         $\$                                               [00m
                                          $\$                                              [00m
                                           $\$                                   [00m
                                           [38;5;136m▄[48;5;136;38;5;221m▄▄▄[49;38;5;60m▄[39m                               [00m
                                          [48;5;136;38;5;60m▄[48;5;221;38;5;221m█[48;5;231;38;5;231m██[48;5;221;38;5;221m██[48;5;60;38;5;233m▄[49;39m                              [00m
                                          [38;5;60m▀[48;5;221;38;5;233m▄[38;5;221m███[38;5;233m▄[49m▀[39m                              [00m
                                            [48;5;233;38;5;233m█[48;5;136;38;5;136m█[48;5;233;38;5;233m█[49;39m                                [00m
                                            [48;5;233;38;5;60m▄[48;5;136;38;5;221m▄[48;5;233;38;5;233m█[49;39m                                [00m
                                           [48;5;60;38;5;233m▄[48;5;136;38;5;221m▄[48;5;221m█[48;5;233;38;5;136m▄[49;38;5;60m▄[39m                               [00m
                                          [38;5;233m▄[48;5;233;38;5;221m▄[48;5;221m███[48;5;233;38;5;233m█[49;39m                    [38;5;60m▄▄▄[39m        [00m
                     [38;5;233m▄▄▄▄[39m        [38;5;244m▄▄▄[38;5;231m▄▄[48;5;60m▄[48;5;233;38;5;145m▄▄▄[48;5;60;38;5;244m▄▄▄▄▄▄[48;5;233m▄[38;5;60m▄▄[49;38;5;233m▄▄▄[39m           [38;5;60m▄[48;5;60m█[38;5;145m▄[48;5;145m███[48;5;60m▄[48;5;233m▄[49;38;5;233m▄[39m     [00m
                   [38;5;233m▄[48;5;233;38;5;244m▄[48;5;145m▄▄▄▄[48;5;233;38;5;145m▄[49;38;5;233m▄[39m [38;5;60m▄▄[48;5;60;38;5;145m▄▄[48;5;244m▄[48;5;145m██[48;5;231m▄[38;5;231m███[38;5;145m▄[48;5;145m████[48;5;244m▄▄[38;5;244m██[48;5;238;38;5;238m█[48;5;244;38;5;244m██[48;5;60m▄[48;5;244;38;5;238m▄[38;5;244m█[48;5;233m▄▄[38;5;60m▄[49;38;5;233m▄▄[39m      [48;5;233;38;5;233m█[48;5;145;38;5;60m▄[48;5;60m████[48;5;145m▄▄[48;5;60;38;5;145m▄[38;5;233m▄[49;39m    [00m
                   [38;5;233m▀[48;5;244m▄[48;5;238;38;5;238m█[38;5;244m▄[38;5;238m█[48;5;244m▄[38;5;233m▄[48;5;238;38;5;145m▄▄[48;5;145;38;5;244m▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄[38;5;238m▄▄▄▄[48;5;244m▄▄[48;5;238m█[48;5;244m▄▄[48;5;238m█[48;5;244m▄▄[38;5;244m█[48;5;60m▄[48;5;244;38;5;60m▄[48;5;60;38;5;244m▄[48;5;233;38;5;60m▄[38;5;244m▄[49;38;5;233m▄[39m    [38;5;233m▀[48;5;238;38;5;238m█[48;5;145;38;5;145m█[38;5;244m▄[48;5;60;38;5;145m▄[38;5;233m▄[38;5;60m██[48;5;233;38;5;233m█[49;39m    [00m
                 [38;5;60m▄[38;5;233m▄[48;5;233m█[48;5;238m▄[48;5;244;38;5;238m▄[48;5;238;38;5;233m▄[48;5;233;38;5;110m▄[48;5;238m▄[48;5;60m▄▄[38;5;103m▄[48;5;103;38;5;231m▄[48;5;231m█[38;5;167m▄▄▄▄[38;5;231m████████[48;5;103;38;5;103m█[48;5;110;38;5;110m██████████[48;5;103m▄[38;5;103m█[48;5;60m▄[48;5;238m▄▄▄[48;5;60m▄[48;5;244;38;5;60m▄[38;5;238m▄[48;5;238;38;5;233m▄[49m▄▄▄[48;5;238m▄[48;5;244;38;5;145m▄▄[48;5;145;38;5;244m▄[48;5;244;38;5;238m▄[48;5;233m▄[49m▀▀[39m     [00m
              [38;5;233m▄[48;5;60;38;5;110m▄[48;5;233m▄[48;5;110m███[38;5;60m▄[48;5;238;38;5;110m▄[48;5;110m████[48;5;103;38;5;103m█[38;5;231m▄[48;5;231m█[48;5;167;38;5;167m█[48;5;131;38;5;95m▄[48;5;95;38;5;167m▄▄[48;5;131;38;5;95m▄[48;5;167;38;5;167m█[48;5;231;38;5;231m██████[48;5;103;38;5;103m█[48;5;110;38;5;110m████████████[48;5;103m▄[48;5;103;38;5;103m████[38;5;60m▄[48;5;233;38;5;110m▄[48;5;103;38;5;103m▄[48;5;110m▄▄▄[38;5;110m██[48;5;233m▄▄[48;5;145;38;5;233m▄[48;5;233;38;5;238m▄[49;39m        [00m
            [38;5;233m▄[48;5;233;38;5;110m▄[48;5;110;38;5;231m▄[48;5;231m██[48;5;110;38;5;103m▄[48;5;103m▄[48;5;103;38;5;233m▄[48;5;233;38;5;110m▄[48;5;110m█████[48;5;103;38;5;103m█[48;5;231;38;5;231m██[48;5;167;38;5;167m█[48;5;95;38;5;131m▄[48;5;167;38;5;95m▄▄[48;5;95;38;5;131m▄[48;5;167;38;5;167m█[48;5;231;38;5;231m████[38;5;103m▄[48;5;103;38;5;110m▄[48;5;110m██████████████[48;5;103m▄[48;5;103;38;5;103m█[38;5;238m▄[48;5;60;38;5;110m▄[48;5;110m█[48;5;103;38;5;231m▄[48;5;231m████[48;5;103m▄[48;5;110;38;5;110m███[38;5;103m▄[48;5;233;38;5;103m▄[49;38;5;233m▄[39m       [00m
           [38;5;60m▄[48;5;60;38;5;103m▄[48;5;231;38;5;231m█[48;5;233m▄[48;5;231m█[38;5;103m▄[48;5;103m█[38;5;233m▄[48;5;233m█[48;5;110m▄▄▄▄▄▄▄[48;5;103m▄[48;5;231m▄▄[48;5;167m▄▄▄▄[48;5;231m▄▄▄[48;5;103m▄[48;5;110m▄▄▄▄▄▄▄▄▄[38;5;238m▄▄▄▄▄▄[38;5;110m███[48;5;103;38;5;233m▄[48;5;238;38;5;110m▄[48;5;110m█[48;5;103;38;5;103m█[48;5;231;38;5;231m██[48;5;233m▄[48;5;231m███[48;5;103;38;5;103m█[48;5;110;38;5;110m█[38;5;103m▄[48;5;103m██[48;5;233m▄[49;38;5;233m▄[39m      [00m
      [38;5;233m▄▄[48;5;238;38;5;238m█[48;5;233;38;5;110m▄▄▄▄▄▄[38;5;103m▄[38;5;238m▄[48;5;238;38;5;110m▄▄▄[48;5;110;38;5;103m▄[38;5;103m▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄[38;5;103m▄[38;5;110m█[48;5;103m▄[48;5;103m▄[48;5;238m▄▄▄▄▄▄▄▄▄[48;5;233m▄▄▄▄▄▄▄▄[48;5;238;38;5;238m█[48;5;103;38;5;233m▄[48;5;233m█[49m▄▄[38;5;238m▄[39m   [00m
     [38;5;233m▀[48;5;103m▄[48;5;110m▄▄[48;5;103;38;5;238m▄[48;5;103m▄▄▄▄[38;5;233m▄▄▄▄[48;5;238m▄[38;5;103m▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄[48;5;103;38;5;238m▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄[38;5;103m█[48;5;103m▄[48;5;110m▄▄▄[38;5;103m▄[48;5;238;38;5;110m▄[48;5;233;38;5;103m▄▄[49;38;5;233m▄[39m[00m
          [48;5;233;38;5;238m▄[48;5;103m▄[38;5;103m█████[38;5;103m▄[48;5;233;38;5;103m▄[48;5;103;38;5;233m▄[38;5;103m████████████████████████████████████[48;5;233m▄[48;5;238;38;5;238m█[48;5;103;38;5;103m███████████[48;5;238m▄▄▄[38;5;233m▄[48;5;103m▄▄[49m▀▀▀[39m [00m
          [38;5;233m▄[48;5;233m█[38;5;145m▄▄▄[38;5;233m█[48;5;103m▄▄[38;5;103m█[48;5;233m▄[48;5;103;38;5;233m▄[38;5;103m▄▄[38;5;103m███████████████████████████████████[48;5;233m▄[48;5;103;38;5;233m▄▄[48;5;233;38;5;145m▄▄▄▄[48;5;103;38;5;233m▄▄[38;5;103m███[38;5;60m▄[48;5;238;38;5;238m█[49;39m       [00m
       [38;5;233m▄▄[48;5;233;38;5;145m▄[48;5;145m██[38;5;60m▄[38;5;238m▄[48;5;238m███[48;5;233m▄[38;5;233m█[48;5;103m▄[48;5;233;38;5;103m▄[48;5;103;38;5;233m▄[48;5;110;38;5;110m██[48;5;103m▄[38;5;103m██████████████████████[38;5;103m▄[38;5;103m█[38;5;103m▄[48;5;103;38;5;103m▄[48;5;103;38;5;103m▄[48;5;103;38;5;103m▄[48;5;103;38;5;103m▄[38;5;103m██[38;5;233m▄[48;5;238;38;5;238m█[48;5;233;38;5;145m▄[48;5;145m███[38;5;238m▄[48;5;60m▄[48;5;238m███[48;5;233m▄[48;5;103;38;5;233m▄[49;38;5;238m▀▀[39m        [00m
    [38;5;233m▄▄[48;5;233;38;5;145m▄[48;5;60m▄[48;5;145m█[38;5;238m▄▄[48;5;238m███████[48;5;233m▄[38;5;233m█[49;38;5;238m▀[39m [38;5;233m▀[48;5;110m▄▄[38;5;110m█[48;5;103m▄[48;5;103m▄[38;5;103m▄[38;5;103m████████████████[38;5;103m▄[48;5;103;38;5;103m▄[48;5;103;38;5;103m▄[48;5;103;38;5;103m▄[48;5;103;38;5;103m▄[48;5;103;38;5;103m▄[48;5;103;38;5;103m▄[48;5;103;38;5;103m▄[48;5;103;38;5;103m▄[38;5;233m▄[48;5;233;38;5;145m▄[48;5;238m▄[48;5;145m███[38;5;238m▄[48;5;238m███[38;5;233m▄▄[38;5;238m██[48;5;233m▄[49;38;5;233m▄[39m         [00m
   [48;5;233;38;5;233m█[48;5;145;38;5;60m▄▄▄[38;5;238m▄[48;5;238m█████[38;5;233m▄[48;5;233m██[48;5;238;38;5;238m███[48;5;233;38;5;233m█[49;39m    [38;5;233m▀▀[48;5;103m▄[48;5;110m▄[38;5;103m▄[38;5;110m█[48;5;103m▄[48;5;103m▄▄[38;5;103m▄[38;5;103m███████████[38;5;103m▄[48;5;103;38;5;103m▄[48;5;103;38;5;103m▄[48;5;103;38;5;103m▄[48;5;103;38;5;103m▄[48;5;103;38;5;103m▄[48;5;103;38;5;103m▄[48;5;103;38;5;103m▄[48;5;233;38;5;233m█[48;5;145;38;5;145m███[38;5;238m▄[48;5;60m▄[48;5;238m███[38;5;233m▄[48;5;233m████[48;5;238;38;5;238m██[48;5;233;38;5;233m█[49;39m         [00m
   [48;5;233;38;5;233m█[48;5;60;38;5;60m███[48;5;238;38;5;238m███[38;5;233m▄▄[48;5;233;38;5;60m▄[48;5;60m██[48;5;233;38;5;238m▄[48;5;238m██[38;5;233m▄[49m▀[39m        [38;5;233m▀▀[48;5;103m▄[48;5;110m▄▄[38;5;103m▄[38;5;110m█████[48;5;103m▄[48;5;103m▄▄▄▄▄▄[48;5;103m▄[48;5;103m▄[48;5;103m▄[48;5;103m▄[48;5;103m▄[48;5;110m██[48;5;233;38;5;233m█[48;5;60;38;5;60m███[48;5;238;38;5;238m███[38;5;233m▄[49m▀[39m [48;5;233;38;5;233m█[48;5;238;38;5;238m██[48;5;233;38;5;233m█[48;5;238;38;5;238m██[48;5;233;38;5;233m█[49;39m         [00m
    [48;5;233;38;5;233m█[48;5;60;38;5;60m███[48;5;238;38;5;233m▄[48;5;233m█[38;5;145m▄[48;5;60m▄▄[38;5;238m▄[48;5;238m███[38;5;233m▄[49m▀[39m              [38;5;233m▀▀▀▀▀[48;5;103m▄[48;5;110m▄▄▄▄▄▄▄▄▄▄[48;5;103m▄[49m▀▀▀▀[48;5;233m█[48;5;60;38;5;60m███[48;5;238;38;5;238m█[38;5;233m▄[49m▀▄[48;5;233;38;5;60m▄[48;5;60m██[38;5;238m▄[48;5;238m██[38;5;233m▄[49m▀[39m         [00m
    [38;5;233m▀▀[48;5;233m██[38;5;145m▄[48;5;145m██[38;5;238m▄[48;5;238m███[38;5;233m▄[49m▀[39m                                     [38;5;233m▀[48;5;60m▄▄▄[49m▀▄[48;5;233;38;5;145m▄[48;5;60m▄▄[38;5;238m▄[48;5;238m███[38;5;233m▄[49m▀[39m          [00m
     [48;5;233;38;5;233m█[48;5;145;38;5;60m▄▄▄[38;5;238m▄[48;5;238m███[38;5;233m▄[49m▀[39m                                         [38;5;233m▄[48;5;233m█[38;5;145m▄[48;5;145m██[38;5;238m▄[48;5;238m████[38;5;233m▄[49m▀[39m           [00m
     [38;5;233m▀[48;5;60m▄[38;5;60m██[48;5;238m▄[38;5;238m█[38;5;233m▄[49m▀[39m                                           [48;5;233;38;5;233m█[48;5;145m▄[38;5;60m▄▄[48;5;238;38;5;238m████[38;5;233m▄[49m▀[39m             [00m
      [48;5;233;38;5;233m█[48;5;60m▄▄▄[49m▀[39m                                              [48;5;233;38;5;233m█[48;5;60;38;5;60m██[48;5;238m▄[38;5;238m█[38;5;233m▄[49m▀[39m               [00m
                                                          [48;5;233;38;5;233m█[48;5;60m▄▄[49m▀[39m                 [00m
                                                                               [00m
