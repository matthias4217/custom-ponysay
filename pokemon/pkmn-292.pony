$$$
NAME: Shedinja
OTHER NAMES: Munja, 껍질몬, 脫殼忍者, Nukenin, Ninjatom, ヌケニン
APPEARANCE: Pokémon Generation III
KIND: Shed Pokémon
GROUP: mineral
BALLOON: top
COAT: brown
DISPLAY: full
BALLOON TOP: 3
BALLOON BOTTOM: 0
POSE: master
BASED ON: Shedinja
SOURCE: PokéAPI Sprites
MEDIA: Pokémon
LICENSE: Copyright The Pokémon Company
FREE: no
WIDTH: 72
HEIGHT: 25


$$$
$balloon5$
      $\$                                                                 [00m
       $\$                                                                [00m
        $\$                                                               [00m
         $\$                                    [00m
        [38;5;103m▄[38;5;146m▄[48;5;146;38;5;231m▄[38;5;146m█[38;5;103m▄▄[49m▀▀[39m     [38;5;146m▀▀[48;5;146m█[38;5;188m▄[48;5;103m▄[49;38;5;146m▄[38;5;103m▄[39m                  [00m
      [38;5;103m▄[48;5;103;38;5;231m▄[48;5;231m███[48;5;103;38;5;146m▄[38;5;103m█[49;39m           [48;5;146;38;5;146m█[48;5;188;38;5;231m▄[38;5;188m█[48;5;146m▄[48;5;103;38;5;103m█[49;39m                 [00m
      [38;5;103m▀[48;5;188m▄[48;5;231;38;5;188m▄[38;5;231m███[48;5;188m▄[48;5;146m▄▄▄▄▄▄▄▄▄[48;5;188m▄[48;5;231m██[38;5;188m▄[48;5;188m█[38;5;103m▄[49m▀[39m                 [00m
     [38;5;239m▄▄[39m [38;5;103m▀[38;5;146m▀[48;5;188;38;5;103m▄[48;5;231m▄[38;5;146m▄[38;5;188m▄▄▄▄▄▄▄▄[38;5;146m▄[48;5;188;38;5;103m▄▄▄[49;38;5;146m▀[38;5;103m▀[39m                   [00m
   [38;5;239m▄[48;5;239;38;5;137m▄[48;5;180;38;5;180m█[48;5;239m▄[49;38;5;239m▄[39m     [38;5;103m▀▀▀▀▀▀▀▀[39m                   [38;5;239m▄▄▄[39m   [00m
   [38;5;233m▀[48;5;137m▄[38;5;137m█[48;5;180m▄[48;5;239;38;5;180m▄[49;38;5;239m▄[39m                           [38;5;239m▄▄[48;5;239;38;5;222m▄▄[48;5;222;38;5;180m▄▄[48;5;233;38;5;233m█[49;39m   [00m
     [48;5;233;38;5;239m▄[48;5;137;38;5;137m█[48;5;180m▄[48;5;102;38;5;239m▄[48;5;239;38;5;233m▄[49;39m  [38;5;239m▄▄▄[38;5;233m▄▄▄[38;5;101m▄[48;5;101;38;5;222m▄[48;5;239;38;5;137m▄[49;38;5;239m▄[39m       [38;5;239m▄[48;5;239;38;5;222m▄[48;5;233m▄[49;38;5;233m▄[38;5;239m▄[48;5;239;38;5;137m▄[38;5;222m▄[48;5;222m█[38;5;180m▄▄[48;5;180m█[38;5;233m▄[48;5;137m▄[49m▀[39m   [00m
    [48;5;239;38;5;233m▄[48;5;137;38;5;137m██[48;5;239;38;5;239m█[48;5;222;38;5;222m█[48;5;180;38;5;180m█[48;5;239m▄▄[48;5;180;38;5;137m▄[48;5;101;38;5;180m▄▄[48;5;137;38;5;137m█[38;5;239m▄[48;5;101;38;5;180m▄[48;5;222;38;5;222m█[38;5;180m▄[48;5;137;38;5;137m█[48;5;239m▄[49;38;5;233m▄[39m   [38;5;239m▄[48;5;239;38;5;137m▄[38;5;180m▄[48;5;222m▄[48;5;180m██[48;5;233m▄[48;5;222m▄▄[48;5;180m██[38;5;137m▄[38;5;233m▄[49m▀[39m      [00m
     [38;5;233m▀[48;5;137;38;5;239m▄[48;5;239;38;5;137m▄[48;5;137;38;5;180m▄[48;5;180;38;5;222m▄[48;5;222m█[48;5;137;38;5;137m█[48;5;222;38;5;222m█[48;5;180m▄[38;5;180m██[48;5;137m▄[38;5;137m█[48;5;180m▄[48;5;137m██[38;5;239m▄▄[48;5;233m▄[48;5;239;38;5;233m▄[38;5;180m▄[48;5;137;38;5;239m▄[38;5;137m██[48;5;180;38;5;180m██████[38;5;233m▄[49m▀▀[38;5;239m▄▄▄▄[48;5;239;38;5;222m▄▄▄[38;5;233m▄[49;39m[00m
    [38;5;101m▄[48;5;101;38;5;222m▄[48;5;239;38;5;239m█[48;5;180;38;5;180m█[48;5;222;38;5;222m██[48;5;137;38;5;137m█[48;5;222;38;5;222m███[48;5;180;38;5;180m███[48;5;137;38;5;137m██[38;5;239m▄[48;5;239;38;5;222m▄[48;5;222m██[48;5;137m▄[48;5;239;38;5;137m▄[48;5;180;38;5;233m▄[38;5;180m█[48;5;239m▄[48;5;137;38;5;137m██[48;5;180m▄▄[38;5;233m▄[48;5;233;38;5;239m▄[38;5;137m▄[48;5;239;38;5;180m▄[38;5;222m▄▄[48;5;222m█[38;5;180m▄▄▄[48;5;180m█[38;5;233m▄▄[49m▀[39m[00m
    [48;5;239;38;5;233m▄[48;5;101;38;5;239m▄[48;5;233;38;5;233m█[48;5;180;38;5;180m██[48;5;222m▄[48;5;137;38;5;137m█[48;5;222;38;5;180m▄▄[48;5;180m███[38;5;137m▄[48;5;137m██[48;5;239;38;5;233m▄[48;5;233m█[48;5;101m▄[48;5;222m▄▄[48;5;137m▄[48;5;233m█[48;5;137;38;5;137m█[48;5;180m▄[48;5;137m████████[48;5;180;38;5;180m█████[48;5;233;38;5;233m█[49m▀[39m   [00m
    [38;5;239m▀[48;5;222m▄[48;5;239;38;5;180m▄[48;5;180;38;5;101m▄[38;5;180m██[48;5;137;38;5;137m█[48;5;180;38;5;180m████[38;5;137m▄[48;5;137m███[48;5;239m▄[48;5;233;38;5;239m▄[48;5;101;38;5;180m▄[48;5;180;38;5;137m▄[48;5;137m█[38;5;233m▄[48;5;233;38;5;137m▄[48;5;137m█████[38;5;239m▄[38;5;137m██[38;5;233m▄[49m▀▀▀[48;5;180m▄▄[49m▀▀[39m    [00m
      [38;5;239m▀[48;5;233;38;5;233m█[48;5;137;38;5;137m█[48;5;180m▄[48;5;101;38;5;101m█[48;5;180;38;5;137m▄▄[48;5;137m████████[48;5;233m▄▄[38;5;239m▄[48;5;102;38;5;102m█[48;5;233m▄[48;5;137;38;5;233m▄▄[38;5;137m███[48;5;239m▄[48;5;137;38;5;239m▄[38;5;137m█[48;5;239m▄▄▄[49;38;5;239m▄▄▄[39m      [00m
    [38;5;239m▄▄[48;5;239;38;5;137m▄[48;5;137;38;5;233m▄[48;5;233;38;5;180m▄[48;5;137;38;5;233m▄[48;5;101;38;5;137m▄[48;5;137;38;5;101m▄[38;5;137m████[38;5;239m▄[38;5;233m▄▄▄▄[48;5;239;38;5;102m▄[48;5;233m▄[48;5;102;38;5;233m▄[48;5;233;38;5;180m▄[48;5;239;38;5;137m▄[48;5;102;38;5;239m▄[38;5;102m█[48;5;233m▄[48;5;137;38;5;233m▄[38;5;137m██[48;5;239;38;5;239m█[48;5;137;38;5;137m███████[48;5;239m▄▄[49;38;5;239m▄[39m   [00m
    [48;5;233;38;5;233m█[48;5;137;38;5;137m██[48;5;233;38;5;233m█[48;5;222;38;5;222m██[48;5;239m▄[48;5;101;38;5;239m▄[48;5;137;38;5;101m▄[38;5;137m██[38;5;239m▄[48;5;233;38;5;102m▄[48;5;102m█████[48;5;233;38;5;239m▄[48;5;222;38;5;222m██[48;5;180;38;5;180m█[48;5;137;38;5;137m█[48;5;233;38;5;233m█[48;5;102;38;5;102m█[48;5;233m▄[48;5;137;38;5;233m▄[38;5;137m█[48;5;239;38;5;233m▄[48;5;137;38;5;137m████[48;5;233;38;5;233m█[49m▀[48;5;137m▄▄[38;5;137m█[48;5;233;38;5;233m█[49;39m   [00m
     [38;5;233m▀▀[48;5;233m█[48;5;222;38;5;180m▄[38;5;222m█[38;5;180m▄[48;5;180;38;5;137m▄[48;5;233;38;5;239m▄[38;5;249m▄▄[48;5;239m▄[48;5;102;38;5;247m▄▄▄[38;5;101m▄[38;5;239m▄[48;5;239m█[48;5;222;38;5;222m██[48;5;180;38;5;180m█[38;5;137m▄[48;5;137m█[48;5;233;38;5;233m█[48;5;102;38;5;102m██[48;5;233;38;5;239m▄[48;5;137;38;5;137m█[48;5;233;38;5;233m█[49m▀▀[48;5;137m▄▄[49m▀[39m   [38;5;233m▀▀[39m   [00m
        [48;5;233;38;5;233m█[48;5;180;38;5;137m▄[48;5;137m██[48;5;233;38;5;233m█[48;5;239;38;5;249m▄▄[48;5;101m▄▄▄▄[48;5;247m▄[38;5;247m█[48;5;239;38;5;233m▄[48;5;180;38;5;180m██[38;5;137m▄[48;5;137m█[48;5;239;38;5;233m▄[48;5;102;38;5;102m██[38;5;239m▄[48;5;239;38;5;102m▄[48;5;233;38;5;233m█[49;39m              [00m
         [38;5;233m▀[48;5;137m▄▄[48;5;233m█[48;5;247;38;5;101m▄[48;5;249;38;5;249m███████[48;5;233m▄[48;5;137;38;5;233m▄[38;5;137m█[38;5;101m▄[48;5;239;38;5;233m▄[48;5;102;38;5;239m▄[48;5;239;38;5;102m▄▄[48;5;102m█[38;5;233m▄[49m▀[39m              [00m
             [48;5;233;38;5;233m█[48;5;101;38;5;101m█[38;5;249m▄▄▄▄▄▄[48;5;102m▄[48;5;249m█[48;5;233;38;5;247m▄[38;5;239m▄[48;5;247;38;5;249m▄[38;5;247m█[48;5;239;38;5;233m▄[48;5;102;38;5;102m█[38;5;233m▄[49m▀[39m               [00m
             [38;5;239m▀[48;5;247;38;5;101m▄[48;5;101;38;5;247m▄[48;5;247;38;5;233m▄[48;5;249;38;5;102m▄[38;5;101m▄[48;5;101;38;5;249m▄▄[48;5;247;38;5;239m▄[38;5;102m▄[48;5;102m█[48;5;239m▄[48;5;249;38;5;233m▄[48;5;247m▄[48;5;233;38;5;102m▄[48;5;102;38;5;233m▄[49m▀[39m                [00m
               [38;5;101m▀[38;5;239m▀[38;5;233m▀[48;5;101;38;5;239m▄[48;5;249;38;5;247m▄▄[48;5;247;38;5;102m▄[48;5;233m▄[48;5;102;38;5;239m▄[38;5;233m▄▄▄[49m▀[39m                  [00m
                   [38;5;233m▀[48;5;102m▄▄▄▄▄[49m▀[39m                    [00m
                                              [00m
