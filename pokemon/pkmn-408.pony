$$$
NAME: Cranidos
OTHER NAMES: Kranidos, 頭蓋龍, 두개도스, Koknodon, ズガイドス, Zugaidos
APPEARANCE: Pokémon Generation IV
KIND: Head Butt Pokémon
GROUP: monster
BALLOON: top
COAT: blue
DISPLAY: full
BALLOON TOP: 3
BALLOON BOTTOM: 0
POSE: master
BASED ON: Cranidos
SOURCE: PokéAPI Sprites
MEDIA: Pokémon
LICENSE: Copyright The Pokémon Company
FREE: no
WIDTH: 71
HEIGHT: 31


$$$
$balloon5$
         $\$                                                             [00m
          $\$                                                            [00m
           $\$                                                           [00m
            $\$                        [00m
           [38;5;24m▄▄[48;5;24;38;5;67m▄▄▄▄▄[49;38;5;24m▄▄[39m                 [00m
        [38;5;61m▄[48;5;61;38;5;67m▄▄[48;5;67;38;5;111m▄▄▄[38;5;67m██████[48;5;233m▄[49;38;5;233m▄[39m               [00m
      [38;5;24m▄[48;5;61;38;5;67m▄[48;5;67m█[38;5;111m▄[48;5;111;38;5;231m▄[48;5;231m██[48;5;111;38;5;111m██[48;5;67;38;5;67m███████[48;5;233;38;5;61m▄[49;38;5;233m▄[39m             [00m
     [38;5;24m▄[48;5;24;38;5;67m▄[48;5;67m██[48;5;111m▄[48;5;231;38;5;111m▄▄[48;5;111m█[38;5;67m▄[48;5;67m███████[48;5;61;38;5;61m███[48;5;233;38;5;233m█[49;38;5;243m▄▄[39m          [00m
    [38;5;24m▄[48;5;24;38;5;67m▄[48;5;67m██████████[38;5;24m▄▄[38;5;61m▄▄[48;5;61m███[38;5;24m▄[48;5;24;38;5;249m▄[48;5;233;38;5;233m█[48;5;249;38;5;249m█[48;5;243m▄[38;5;253m▄[49;38;5;243m▄[38;5;233m▄[39m      [00m
    [48;5;24;38;5;24m█[48;5;67;38;5;67m█████████[38;5;24m▄[48;5;24;38;5;249m▄[48;5;249;38;5;246m▄[48;5;246m█[48;5;24m▄[48;5;61;38;5;24m▄[38;5;61m██[38;5;24m▄[48;5;24;38;5;249m▄[48;5;246;38;5;66m▄[48;5;66m█[48;5;238;38;5;233m▄[48;5;249;38;5;249m█[48;5;253;38;5;253m█[38;5;238m▄[49m▀[39m      [00m
   [48;5;238;38;5;238m█[48;5;24;38;5;253m▄[48;5;61;38;5;246m▄[48;5;67;38;5;24m▄[38;5;61m▄▄▄[38;5;24m▄▄[48;5;61;38;5;249m▄[48;5;24m▄[48;5;249;38;5;246m▄[48;5;246m█████[48;5;24m▄▄[48;5;249m▄[48;5;66;38;5;238m▄[38;5;231m▄▄[48;5;233;38;5;249m▄[48;5;249;38;5;238m▄[48;5;243m▄[49;39m        [00m
   [48;5;238;38;5;238m█[48;5;253;38;5;249m▄[38;5;253m████[38;5;249m▄[38;5;66m▄[38;5;246m▄[48;5;66m▄[48;5;246m███[38;5;238m▄▄[48;5;238;38;5;231m▄▄[48;5;66;38;5;66m█[48;5;246;38;5;246m█[48;5;238;38;5;238m█[48;5;231;38;5;231m█████[48;5;249m▄[48;5;238m▄[49;38;5;243m▄[39m      [00m
   [48;5;233;38;5;233m█[48;5;249;38;5;249m█[48;5;246m▄[48;5;249;38;5;246m▄[48;5;66m▄[48;5;246m█████[38;5;66m▄[48;5;238;38;5;231m▄▄[48;5;253;38;5;203m▄[48;5;124;38;5;124m█[48;5;231;38;5;231m██[48;5;66;38;5;66m█[48;5;246;38;5;246m█[48;5;238;38;5;66m▄[48;5;231;38;5;253m▄▄▄[48;5;253m█[38;5;249m▄[38;5;233m▄[49m▀[39m       [00m
    [48;5;238;38;5;233m▄[48;5;249;38;5;249m█[48;5;246;38;5;246m████████[48;5;253;38;5;66m▄[48;5;231;38;5;253m▄[38;5;231m██[38;5;66m▄[48;5;253;38;5;246m▄[48;5;246m██[48;5;66;38;5;66m█[48;5;238m▄[48;5;253m▄[38;5;233m▄[49m▀▀[39m         [00m
    [38;5;238m▀[48;5;249;38;5;233m▄[38;5;246m▄[48;5;246m█[48;5;238m▄[48;5;246m███[38;5;238m▄[38;5;246m██████[38;5;66m▄▄[48;5;66m██[38;5;238m▄[49;38;5;233m▀[39m            [00m
      [38;5;238m▀[48;5;249;38;5;233m▄[48;5;246;38;5;246m██[48;5;238;38;5;233m▄[48;5;233;38;5;66m▄[48;5;66m█[48;5;233m▄[48;5;246;38;5;233m▄▄▄[48;5;233;38;5;66m▄[48;5;66m███[38;5;238m▄[38;5;233m▄[49m▀[39m             [00m
        [38;5;233m▀▀[38;5;238m▀[48;5;66;38;5;233m▄▄[38;5;238m▄[38;5;66m███[38;5;238m▄[38;5;246m▄[38;5;24m▄[48;5;24;38;5;61m▄[48;5;233;38;5;24m▄[49;39m               [00m
              [38;5;233m▀[48;5;233m█[38;5;246m▄[48;5;246m███[48;5;61m▄[38;5;61m█[48;5;233;38;5;67m▄[49;38;5;233m▄[39m          [38;5;24m▄[39m  [00m
            [38;5;238m▄▄[48;5;233;38;5;66m▄[48;5;249;38;5;249m██[48;5;246m▄▄▄[48;5;66;38;5;61m▄[48;5;67;38;5;67m███[48;5;24m▄[48;5;233m▄[49;38;5;24m▄[38;5;233m▄[39m   [38;5;233m▄▄[48;5;233;38;5;67m▄[48;5;67m█[48;5;24;38;5;233m▄[49;39m [00m
       [38;5;238m▄▄[48;5;238;38;5;249m▄▄▄[48;5;249m█[38;5;246m▄[48;5;66;38;5;249m▄[48;5;249m█████[48;5;61m▄▄[38;5;246m▄[38;5;66m▄[48;5;67;38;5;67m████[48;5;24m▄[48;5;233m▄▄[48;5;67m███[38;5;61m▄▄[48;5;233;38;5;233m█[49;39m[00m
    [38;5;238m▄[48;5;238;38;5;249m▄[38;5;246m▄[48;5;249m▄[38;5;249m██[38;5;246m▄▄[48;5;246m█[48;5;66;38;5;66m█[48;5;249;38;5;249m███[38;5;238m▄[48;5;238;38;5;246m▄[48;5;249;38;5;249m███[48;5;246;38;5;233m▄[38;5;246m█[48;5;61;38;5;61m█[48;5;67;38;5;67m████████[48;5;61m▄[48;5;246;38;5;61m▄[38;5;246m█[48;5;233;38;5;233m█[49;39m[00m
   [48;5;238;38;5;233m▄[48;5;253;38;5;231m▄[48;5;246;38;5;246m█[48;5;231;38;5;238m▄[48;5;238m█[48;5;246;38;5;246m██[38;5;233m▄▄[48;5;66;38;5;66m█[48;5;249;38;5;249m███[38;5;238m▄[48;5;238;38;5;249m▄[48;5;249m███[38;5;233m▄[48;5;233;38;5;246m▄[48;5;246m█[48;5;61m▄▄[38;5;61m██[38;5;67m▄[48;5;67m█[48;5;24m▄[48;5;67;38;5;24m▄[38;5;61m▄▄[48;5;61m█[48;5;233;38;5;233m█[49;39m [00m
    [48;5;233;38;5;233m█[48;5;246;38;5;231m▄[38;5;233m▄[49m▀▀▀[39m [48;5;233;38;5;233m█[48;5;66;38;5;238m▄[48;5;249;38;5;249m██[48;5;238;38;5;238m█[48;5;249;38;5;249m███[38;5;246m▄[38;5;233m▄[48;5;233;38;5;246m▄[48;5;246m█[38;5;238m▄[48;5;238;38;5;67m▄[48;5;61m▄[48;5;67m██████[48;5;233;38;5;61m▄[48;5;246;38;5;233m▄[38;5;246m█[48;5;233;38;5;233m█[49;39m [00m
     [38;5;233m▀▀[39m   [48;5;233;38;5;233m█[48;5;61;38;5;61m█[48;5;233;38;5;233m█[48;5;246;38;5;238m▄[48;5;238;38;5;253m▄[48;5;249;38;5;246m▄[38;5;249m█[38;5;238m▄[38;5;233m▄[48;5;233;38;5;246m▄[48;5;246;38;5;66m▄▄[48;5;66;38;5;233m▄[48;5;24;38;5;67m▄[48;5;67m█[38;5;61m▄[48;5;61;38;5;249m▄[48;5;67;38;5;61m▄[38;5;67m███[38;5;61m▄[48;5;61m█[48;5;233m▄[38;5;233m█[49;39m  [00m
          [48;5;233;38;5;233m█[48;5;61;38;5;61m█[48;5;24;38;5;24m█[48;5;238;38;5;233m▄[48;5;233;38;5;66m▄[48;5;231;38;5;233m▄[48;5;233;38;5;66m▄[48;5;231;38;5;233m▄[48;5;233;38;5;66m▄[48;5;66m███[48;5;233;38;5;233m█[48;5;67;38;5;61m▄[48;5;61;38;5;249m▄[48;5;249m██[48;5;61m▄[48;5;67;38;5;67m██[38;5;61m▄[48;5;61;38;5;246m▄[48;5;246m█[48;5;61;38;5;61m█[48;5;233;38;5;233m█[49;39m  [00m
           [48;5;233;38;5;238m▄[48;5;66m▄[38;5;66m█[48;5;233m▄[48;5;66;38;5;233m▄▄[38;5;66m█████[48;5;233m▄[48;5;249;38;5;233m▄[38;5;246m▄▄▄▄[48;5;61m▄[48;5;67;38;5;61m▄[48;5;61;38;5;246m▄[48;5;246;38;5;66m▄[38;5;233m▄[49m▀[39m   [00m
            [38;5;233m▀▀[48;5;66m▄▄[38;5;66m█[48;5;233m▄▄[48;5;66;38;5;233m▄▄▄▄▄[48;5;233m█[48;5;66m▄[48;5;246m▄[38;5;66m▄▄▄[48;5;66m█[48;5;233;38;5;233m█[49;39m     [00m
                [38;5;233m▀[48;5;233m█[48;5;66;38;5;66m██[38;5;233m▄[49m▀[39m     [48;5;233;38;5;233m█[48;5;66;38;5;66m██[38;5;238m▄[48;5;233m▄[49;38;5;233m▄[39m    [00m
                [48;5;233;38;5;233m█[48;5;238;38;5;246m▄[38;5;66m▄▄[48;5;233m▄[49;38;5;233m▄[39m    [38;5;233m▄[48;5;233m█[48;5;238;38;5;66m▄[38;5;246m▄[48;5;66;38;5;66m██[48;5;233;38;5;233m█[49;39m    [00m
            [38;5;238m▄▄[48;5;233m▄▄[48;5;66;38;5;246m▄[48;5;246;38;5;238m▄[48;5;66;38;5;246m▄[38;5;66m█[38;5;233m▄[48;5;249m▄[49;38;5;238m▀[39m  [38;5;233m▄[48;5;238;38;5;246m▄[48;5;246;38;5;66m▄[48;5;66;38;5;246m▄[48;5;246m██[48;5;66;38;5;66m█[48;5;233;38;5;249m▄[49;38;5;238m▄[39m   [00m
           [38;5;238m▀[48;5;253;38;5;233m▄[48;5;246m▄[48;5;233;38;5;253m▄[48;5;253m█[38;5;233m▄[48;5;238;38;5;253m▄[48;5;66;38;5;233m▄[49m▀[39m  [38;5;238m▄[48;5;238;38;5;253m▄[48;5;233m▄[48;5;246;38;5;238m▄[48;5;66;38;5;231m▄[48;5;253;38;5;253m█[48;5;66;38;5;246m▄[48;5;246;38;5;253m▄▄[48;5;238;38;5;233m▄[49m▀[38;5;238m▀[39m   [00m
              [38;5;233m▀▀▀▀[39m    [38;5;233m▀▀▀▀[48;5;253m▄[48;5;246m▄[49m▀[48;5;253m▄[48;5;233m█[49;39m      [00m
                                     [00m
