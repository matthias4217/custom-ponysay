$$$
NAME: Pancham
OTHER NAMES: Pandespiègle, 판짱, 頑皮熊貓, Pam-Pam, ヤンチャム
APPEARANCE: Pokémon Generation VI
KIND: Playful Pokémon
GROUP: ground, humanshape
BALLOON: top
COAT: white
DISPLAY: full
BALLOON TOP: 3
BALLOON BOTTOM: 0
POSE: master
BASED ON: Pancham
SOURCE: PokéAPI Sprites
MEDIA: Pokémon
LICENSE: Copyright The Pokémon Company
FREE: no
WIDTH: 65
HEIGHT: 24


$$$
$balloon5$
        $\$                                                        [00m
         $\$                                                       [00m
          $\$                                                      [00m
           $\$                   [00m
            $\$    [38;5;101m▄▄[39m            [00m
             [48;5;101;38;5;101m█[38;5;230m▄[49;38;5;101m▄▄[48;5;101;38;5;144m▄[48;5;230;38;5;230m█[48;5;101;38;5;101m█[49;39m  [38;5;101m▄▄[39m       [00m
            [38;5;101m▄[48;5;101m█[48;5;230;38;5;230m██[48;5;101m▄[48;5;230m███[48;5;101;38;5;144m▄[48;5;144;38;5;230m▄[48;5;230m█[48;5;101;38;5;101m█[49;39m       [00m
       [38;5;236m▄▄▄[48;5;101;38;5;230m▄▄[48;5;230m█[48;5;144m▄[48;5;230m████████[38;5;144m▄[48;5;101m▄[48;5;236;38;5;101m▄[38;5;240m▄▄[49;38;5;236m▄[39m   [00m
     [38;5;236m▄[48;5;236;38;5;240m▄[48;5;240m███[48;5;101m▄[48;5;230;38;5;101m▄[38;5;230m██████████[48;5;144m▄[48;5;230m██[48;5;101m▄[48;5;240;38;5;101m▄[38;5;240m█[48;5;236;38;5;236m█[49;39m  [00m
    [38;5;236m▄[48;5;236;38;5;240m▄[48;5;240m████[38;5;101m▄[48;5;101;38;5;230m▄[48;5;230m██████████████[48;5;101m▄[48;5;240;38;5;101m▄[48;5;235;38;5;235m█[49;39m  [00m
    [48;5;236;38;5;236m█[48;5;240;38;5;240m████[48;5;101;38;5;101m█[48;5;230;38;5;230m█████████████████[48;5;101;38;5;144m▄[48;5;235;38;5;233m▄[49;39m  [00m
     [48;5;236;38;5;235m▄[48;5;240;38;5;240m██[48;5;101;38;5;101m█[48;5;230;38;5;230m███[38;5;101m▄[38;5;230m███████████[38;5;144m▄[48;5;101;38;5;236m▄[48;5;230;38;5;240m▄[48;5;144;38;5;144m█[48;5;233;38;5;233m█[49;39m  [00m
     [48;5;233;38;5;233m█[48;5;236;38;5;230m▄▄[48;5;230m██[38;5;144m▄[48;5;101;38;5;240m▄[48;5;240;38;5;235m▄[48;5;236;38;5;233m▄[48;5;144;38;5;236m▄[48;5;230;38;5;101m▄[38;5;230m███████[38;5;236m▄[48;5;236;38;5;233m▄[48;5;144;38;5;231m▄[48;5;240;38;5;240m█[48;5;101;38;5;101m█[48;5;233m▄[49m▄▄[39m[00m
    [38;5;101m▄[48;5;101m█[48;5;144m▄[48;5;230;38;5;144m▄▄[38;5;230m█[48;5;101;38;5;101m█[48;5;240;38;5;240m█[48;5;233;38;5;233m██[38;5;235m▄[48;5;144;38;5;231m▄[48;5;236;38;5;240m▄[48;5;144;38;5;230m▄[48;5;230m█[38;5;144m▄[38;5;240m▄▄[38;5;144m▄[48;5;235;38;5;240m▄[48;5;233;38;5;235m▄[48;5;231;38;5;144m▄[48;5;240;38;5;240m█[48;5;101;38;5;144m▄[48;5;230;38;5;233m▄▄[49m▀[39m[00m
   [38;5;101m▀[48;5;230;38;5;233m▄▄[38;5;144m▄[48;5;144m███[48;5;101m▄[48;5;240;38;5;240m█[48;5;235m▄[38;5;144m▄[48;5;231m▄[48;5;144;38;5;240m▄[48;5;240;38;5;144m▄[48;5;230m▄▄[48;5;240m▄[38;5;236m▄▄[48;5;236;38;5;144m▄[48;5;144m█[48;5;240m▄▄[48;5;144;38;5;233m▄[49m▀[39m   [00m
      [38;5;233m▀▀▀[48;5;144m▄▄[38;5;144m█[48;5;240m▄▄▄[48;5;144m█[38;5;101m▄[38;5;144m█████[38;5;101m▄[48;5;101;38;5;65m▄[48;5;144m▄[49;38;5;233m▀[39m     [00m
        [38;5;235m▄[48;5;235;38;5;240m▄▄[48;5;233m▄▄[38;5;236m▄[48;5;144m▄[38;5;235m▄▄▄[48;5;101m▄▄[38;5;233m▄[48;5;28;38;5;28m█[48;5;34;38;5;34m███[48;5;65m▄[49;38;5;65m▄[39m    [00m
       [48;5;235;38;5;233m▄[48;5;240;38;5;240m████[38;5;233m▄[48;5;236;38;5;240m▄▄▄[38;5;236m█████[48;5;233m▄[38;5;235m▄[48;5;34;38;5;233m▄▄▄[49;38;5;65m▀[39m    [00m
       [38;5;233m▀[48;5;240m▄[38;5;240m██[38;5;236m▄[48;5;233m▄[48;5;240;38;5;233m▄[38;5;239m▄[48;5;244;38;5;244m█[48;5;240;38;5;239m▄[38;5;240m█[38;5;239m▄[48;5;239;38;5;244m▄[48;5;240;38;5;240m█[48;5;244;38;5;244m█[48;5;235;38;5;239m▄[48;5;236;38;5;233m▄[38;5;236m█[48;5;233;38;5;235m▄[49;39m     [00m
       [38;5;101m▄[48;5;233;38;5;233m█[38;5;236m▄[48;5;236;38;5;235m▄[38;5;236m█[38;5;233m▄[48;5;233;38;5;236m▄[48;5;244;38;5;244m███[48;5;240;38;5;239m▄[48;5;244;38;5;244m██[48;5;239m▄[48;5;244m██[48;5;233;38;5;233m█[38;5;236m▄[49;39m      [00m
      [48;5;101;38;5;101m█[48;5;144;38;5;144m██[48;5;233;38;5;235m▄[38;5;244m▄▄[48;5;244m██████████[48;5;239;38;5;236m▄[38;5;239m██[48;5;236m▄[49;38;5;236m▄[39m    [00m
      [38;5;233m▀[48;5;144m▄▄[48;5;240;38;5;239m▄[48;5;239m█[48;5;244m▄▄[38;5;244m██████[38;5;233m▄▄[48;5;235;38;5;239m▄[48;5;239m████[48;5;233;38;5;233m█[49;39m    [00m
        [48;5;233;38;5;233m█[48;5;239;38;5;239m█████[38;5;233m▄▄[49m▀▀▀▄[48;5;233;38;5;239m▄[48;5;239m███[38;5;233m▄[49m▀[39m     [00m
        [38;5;233m▄[48;5;233;38;5;236m▄[48;5;239m▄▄[38;5;235m▄[48;5;233;38;5;233m█[49;39m     [38;5;233m▀[48;5;239m▄▄[48;5;235;38;5;236m▄▄[48;5;236;38;5;235m▄[48;5;233;38;5;233m█[49;39m     [00m
        [38;5;233m▀[48;5;235m▄[48;5;236m▄[48;5;235m▄[48;5;236m▄[49m▀[39m        [38;5;233m▀▀▀[39m      [00m
                               [00m
