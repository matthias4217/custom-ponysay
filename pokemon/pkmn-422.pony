$$$
NAME: Shellos
OTHER NAMES: 깝질무, Schalellos, Karanakushi, 無殼海兔, カラナクシ, Sancoki
APPEARANCE: Pokémon Generation IV
KIND: Sea Slug Pokémon
GROUP: water1, indeterminate
BALLOON: top
COAT: purple
DISPLAY: full
BALLOON TOP: 3
BALLOON BOTTOM: 0
POSE: master
BASED ON: Shellos
SOURCE: PokéAPI Sprites
MEDIA: Pokémon
LICENSE: Copyright The Pokémon Company
FREE: no
WIDTH: 67
HEIGHT: 22


$$$
$balloon5$
       $\$                                                           [00m
        $\$                                                          [00m
         $\$                                                         [00m
          $\$                       [00m
        [38;5;96m▄▄▄[48;5;96;38;5;233m▄[48;5;233;38;5;211m▄▄[38;5;205m▄[49;38;5;233m▄[39m                  [00m
       [48;5;96;38;5;96m█[48;5;205;38;5;205m██[48;5;233;38;5;96m▄[48;5;205;38;5;205m█[48;5;211m▄▄[48;5;205m██[48;5;96;38;5;233m▄[48;5;233;38;5;211m▄▄[38;5;205m▄[49;38;5;233m▄[39m             [00m
      [48;5;96;38;5;96m█[48;5;205;38;5;205m██[48;5;233;38;5;233m█[48;5;205;38;5;211m▄▄[38;5;205m███[48;5;233;38;5;96m▄[48;5;205;38;5;205m█[48;5;211m▄▄[48;5;205m██[48;5;233;38;5;233m█[49;38;5;96m▄[39m           [00m
      [38;5;96m▀[48;5;211;38;5;95m▄[38;5;211m█[48;5;233;38;5;233m█[48;5;211;38;5;211m████[48;5;233;38;5;233m█[48;5;211;38;5;211m██[48;5;205m▄▄[38;5;205m█[38;5;233m▄[48;5;233;38;5;205m▄[48;5;96;38;5;96m█[49;39m           [00m
      [38;5;95m▄[48;5;95;38;5;211m▄[48;5;211;38;5;95m▄[38;5;211m█[48;5;132;38;5;132m█[48;5;211;38;5;211m███[48;5;233;38;5;132m▄[48;5;211;38;5;211m████[38;5;233m▄[48;5;233;38;5;132m▄[48;5;205;38;5;96m▄[49m▀[39m           [00m
    [38;5;95m▄[48;5;95;38;5;222m▄[48;5;218;38;5;218m████[48;5;211m▄[38;5;211m███████[48;5;95m▄[48;5;211m█[48;5;233;38;5;233m█[49;39m             [00m
   [38;5;95m▄[48;5;95;38;5;222m▄[48;5;218;38;5;211m▄[38;5;218m███[38;5;211m▄▄[48;5;211m█[38;5;222m▄[48;5;222;38;5;233m▄[38;5;137m▄[48;5;211;38;5;222m▄[38;5;211m█████[48;5;233;38;5;233m█[49;39m            [00m
   [48;5;95;38;5;233m▄[48;5;222;38;5;137m▄[48;5;211;38;5;211m██[38;5;132m▄[38;5;211m███[38;5;222m▄[48;5;137;38;5;233m▄[48;5;231m▄[48;5;137;38;5;137m█[48;5;222;38;5;222m█[48;5;211;38;5;211m████[38;5;132m▄[48;5;233;38;5;233m█[49;39m            [00m
   [48;5;233;38;5;233m█[48;5;222;38;5;239m▄[48;5;137;38;5;222m▄[48;5;211m▄[38;5;137m▄▄▄▄[48;5;137;38;5;222m▄[48;5;233m▄▄[48;5;222;38;5;137m▄[48;5;211;38;5;211m████[38;5;132m▄[48;5;233;38;5;233m█[49;39m             [00m
    [38;5;233m▀▀[48;5;222m▄▄[38;5;137m▄▄▄▄[48;5;137;38;5;222m▄▄[48;5;222;38;5;239m▄[48;5;239m█[48;5;211;38;5;132m▄[48;5;132m██[38;5;233m▄[49m▀[39m [38;5;95m▄▄[39m [38;5;95m▄▄[39m       [00m
        [38;5;233m▀▀[48;5;239m▄[38;5;249m▄▄▄[38;5;243m▄[48;5;137;38;5;239m▄[48;5;239;38;5;137m▄[48;5;132;38;5;239m▄[38;5;132m█[48;5;233m▄[49;38;5;233m▄[48;5;95m▄[48;5;132;38;5;95m▄[48;5;95m█[38;5;211m▄[48;5;211m█[48;5;95;38;5;95m█[49;39m       [00m
           [48;5;233;38;5;233m█[48;5;249;38;5;255m▄▄[38;5;249m█[48;5;239m▄[48;5;137;38;5;239m▄[48;5;239;38;5;137m▄[48;5;132;38;5;239m▄[38;5;132m███[48;5;233;38;5;211m▄▄[48;5;132m▄[48;5;211;38;5;233m▄[48;5;233;38;5;132m▄[38;5;211m▄[49;38;5;233m▄[38;5;95m▄[48;5;95;38;5;132m▄[38;5;95m█[49;39m  [00m
          [38;5;243m▄[48;5;243;38;5;255m▄[48;5;255m████[48;5;239m▄[48;5;222;38;5;222m█[48;5;239;38;5;239m█[48;5;132;38;5;211m▄[48;5;211m████[48;5;96m▄[48;5;211m██[38;5;132m▄[48;5;233m▄[48;5;132;38;5;233m▄[38;5;95m▄[49m▀[39m  [00m
         [38;5;243m▄[48;5;243;38;5;255m▄[48;5;255m██████[48;5;137;38;5;137m█[48;5;222;38;5;222m█[48;5;239;38;5;239m█[48;5;211;38;5;211m███████[48;5;96m▄[48;5;211m██[48;5;233m▄[49;38;5;233m▄[39m  [00m
         [48;5;233;38;5;233m█[48;5;255;38;5;255m████████[48;5;137;38;5;137m█[48;5;222;38;5;222m█[48;5;239m▄[48;5;211;38;5;239m▄▄[38;5;211m█████[38;5;132m▄▄[48;5;132m█[48;5;239;38;5;239m█[48;5;233;38;5;137m▄[49;38;5;233m▄[39m[00m
         [38;5;233m▀[48;5;243m▄[48;5;255;38;5;249m▄▄[38;5;255m████[38;5;249m▄▄[48;5;233m▄[48;5;137;38;5;233m▄[48;5;222;38;5;239m▄[48;5;137;38;5;137m█[48;5;239m▄▄▄▄[48;5;132;38;5;239m▄▄▄[48;5;239;38;5;137m▄[48;5;137m█[38;5;233m▄[49m▀[39m[00m
         [38;5;233m▄[48;5;249;38;5;249m█[48;5;243m▄[48;5;249m██████████[48;5;233m▄▄[48;5;239m▄▄[48;5;137;38;5;233m▄▄[38;5;239m▄[38;5;137m██[38;5;233m▄[49m▀[39m [00m
         [38;5;233m▀[48;5;249m▄▄[48;5;243;38;5;243m█[49;38;5;233m▀▀[48;5;243m▄[48;5;249m▄▄[38;5;243m▄[38;5;249m██[38;5;243m▄[48;5;233;38;5;233m█[49m▀▀[48;5;249m▄▄▄▄[49m▀▀[39m   [00m
                  [38;5;233m▀▀▀▀[39m            [00m
                                  [00m
