$$$
NAME: Mantine
OTHER NAMES: Démanta, Mantain, Mantax, マンタイン, 만타인, 巨翅飛魚
APPEARANCE: Pokémon Generation II
KIND: Kite Pokémon
GROUP: water1
BALLOON: top
COAT: purple
DISPLAY: full
BALLOON TOP: 3
BALLOON BOTTOM: 0
POSE: master
BASED ON: Mantine
SOURCE: PokéAPI Sprites
MEDIA: Pokémon
LICENSE: Copyright The Pokémon Company
FREE: no
WIDTH: 75
HEIGHT: 31


$$$
$balloon5$
                                                    $\$                                  [00m
                                                     $\$                                 [00m
                                                      $\$                                [00m
                                                       $\$                    [00m
                                                     [38;5;233m▄▄▄[48;5;233;38;5;69m▄▄[38;5;60m▄▄[49;38;5;233m▄▄[39m              [00m
                                                  [38;5;233m▄[48;5;233;38;5;69m▄▄[48;5;69m██[38;5;189m▄▄▄[48;5;60;38;5;146m▄▄[38;5;60m██[48;5;233;38;5;233m█[49;39m             [00m
                                                [38;5;237m▄[48;5;237;38;5;69m▄[48;5;69m████[48;5;189;38;5;189m██[38;5;146m▄▄[48;5;146m███[48;5;60;38;5;237m▄[49;38;5;233m▀[39m             [00m
                                              [38;5;237m▄[48;5;237;38;5;69m▄[48;5;69m████[38;5;60m▄[48;5;60m█[48;5;146;38;5;146m██████[48;5;237;38;5;237m█[49;39m               [00m
                                             [48;5;237;38;5;237m█[48;5;69;38;5;69m████[38;5;60m▄[48;5;146;38;5;146m███████[38;5;233m▄[49;38;5;237m▀[39m                [00m
                                            [48;5;233;38;5;237m▄[48;5;69;38;5;69m███[38;5;60m▄[48;5;60m██[48;5;146;38;5;146m██████[38;5;233m▄[49m▀[39m                 [00m
                                           [48;5;233;38;5;237m▄[48;5;69;38;5;69m█[38;5;60m▄▄[48;5;60;38;5;146m▄▄▄▄[48;5;146m█████[38;5;233m▄[49m▀[39m                  [00m
                 [38;5;233m▄▄▄▄[39m                     [48;5;233;38;5;233m█[48;5;69;38;5;69m█[38;5;60m▄[48;5;60m██[48;5;146;38;5;146m█████████[48;5;233;38;5;233m█[49;39m                   [00m
                [48;5;233;38;5;233m█[48;5;237;38;5;69m▄[48;5;69m███[48;5;233m▄[38;5;60m▄[49;38;5;237m▄[39m                [38;5;233m▄[48;5;233;38;5;60m▄[48;5;60m█████[48;5;146;38;5;146m████████[48;5;233;38;5;233m█[49;39m                    [00m
                [38;5;233m▀[48;5;69m▄▄[38;5;69m███[48;5;60;38;5;60m██[48;5;237m▄[49;38;5;237m▄[39m            [38;5;233m▄[48;5;233;38;5;60m▄[48;5;60m███[38;5;146m▄▄▄▄[48;5;146m███████[38;5;233m▄[49m▀[39m                    [00m
                   [38;5;237m▀▀[48;5;60m▄[38;5;60m███[48;5;237m▄[49;38;5;233m▄[39m    [38;5;237m▄▄▄▄▄[48;5;233;38;5;60m▄▄[48;5;60m█████[48;5;146;38;5;146m███████████[48;5;233;38;5;233m█[49;39m                     [00m
                      [38;5;233m▀[48;5;60m▄[38;5;60m██[48;5;233;38;5;233m█[49;38;5;237m▄[48;5;237;38;5;69m▄▄▄[48;5;69m█████[48;5;60m▄▄[38;5;60m██████[48;5;146;38;5;146m██████████[48;5;233;38;5;233m█[49;39m                     [00m
      [38;5;233m▄[48;5;233;38;5;237m▄[38;5;69m▄▄[49;38;5;237m▄[39m           [38;5;237m▄[48;5;233;38;5;233m█[48;5;60;38;5;69m▄▄[48;5;69m██████[38;5;189m▄▄▄▄▄▄[48;5;60m▄[38;5;60m█████[48;5;146m▄[38;5;146m█████████[48;5;233;38;5;233m█[49;39m                     [00m
      [48;5;233;38;5;233m█[48;5;69;38;5;69m███[48;5;237m▄[49;38;5;237m▄[39m        [38;5;233m▄[48;5;233;38;5;69m▄[48;5;69m█████[38;5;189m▄[48;5;189m████████████[48;5;146;38;5;146m██[48;5;60m▄▄▄[48;5;146m████████[48;5;233;38;5;233m█[49;39m                      [00m
      [38;5;233m▀[48;5;69;38;5;237m▄[38;5;69m███[48;5;60;38;5;60m█[48;5;237m▄[49;38;5;237m▄[39m    [38;5;233m▄[48;5;233;38;5;69m▄[48;5;69m██████[48;5;189;38;5;189m██[38;5;233m▄[48;5;233;38;5;231m▄[38;5;60m▄[48;5;189;38;5;233m▄[38;5;189m████████[48;5;146;38;5;146m████████████[38;5;233m▄[49m▀[39m                      [00m
       [38;5;233m▀[48;5;60m▄[48;5;69;38;5;60m▄▄[48;5;60m███[48;5;237m▄▄[49;38;5;237m▄[48;5;237m█[48;5;60;38;5;69m▄[48;5;69m██████[48;5;189;38;5;189m███[48;5;233m▄[48;5;60;38;5;233m▄▄[48;5;233;38;5;189m▄[48;5;189m████████[48;5;146;38;5;146m███████████[38;5;237m▄[48;5;233;38;5;60m▄[49;38;5;233m▄[39m                      [00m
         [38;5;233m▀▀[48;5;60m▄▄▄[38;5;60m███[48;5;69;38;5;69m██████[38;5;189m▄[48;5;189m███████████████[48;5;60;38;5;60m█[48;5;146;38;5;146m█[38;5;60m▄[38;5;146m████████[48;5;237m▄[48;5;60m▄[38;5;60m█[48;5;233m▄[49;38;5;233m▄[39m                     [00m
              [38;5;233m▀▀[48;5;233m█[48;5;69;38;5;69m██[38;5;189m▄▄▄[48;5;189m██████[48;5;233;38;5;233m█[48;5;189;38;5;189m█████████[48;5;60;38;5;60m█[38;5;146m▄[48;5;146;38;5;60m▄[48;5;60m█[48;5;146;38;5;146m████████[38;5;189m▄[48;5;189;38;5;146m▄[48;5;146;38;5;189m▄[48;5;60;38;5;146m▄[48;5;233;38;5;233m█[49m▄[39m                    [00m
                [48;5;233;38;5;233m█[48;5;189;38;5;189m██[38;5;233m▄▄[38;5;189m█████[38;5;233m▄[48;5;233;38;5;189m▄[48;5;60m▄▄[48;5;189m██████[38;5;146m▄[48;5;146m███[48;5;60m▄[48;5;146m██████████[48;5;189m▄[48;5;146m██[48;5;237;38;5;237m█[48;5;146;38;5;146m█[48;5;233;38;5;233m█[49;39m                   [00m
                [38;5;233m▀[48;5;189;38;5;237m▄[38;5;233m▄[48;5;231;38;5;60m▄[48;5;60m█[48;5;233;38;5;233m█[48;5;189;38;5;189m██[48;5;233m▄▄[48;5;60;38;5;60m█[48;5;189;38;5;189m███████[38;5;146m▄[48;5;146m██████████████████[38;5;237m▄[48;5;237;38;5;146m▄[48;5;146m██[48;5;233;38;5;231m▄▄▄▄▄[49;38;5;60m▄[38;5;233m▄▄[39m           [00m
                 [48;5;233;38;5;233m█[48;5;189;38;5;189m█[48;5;233m▄▄[48;5;189m██████████[38;5;146m▄▄[48;5;146m███████████████████[38;5;237m▄[48;5;237;38;5;60m▄[48;5;146m▄[49m▀▀[48;5;60m█[38;5;146m▄[48;5;231;38;5;60m▄▄▄[38;5;231m███[48;5;233m▄▄[49;38;5;233m▄[39m        [00m
                [38;5;237m▄▄[48;5;233;38;5;60m▄[48;5;189;38;5;233m▄[38;5;189m██████[38;5;146m▄▄▄[48;5;146m█████████████████████[38;5;237m▄[49m▀[39m      [38;5;60m▀[48;5;146;38;5;233m▄▄[38;5;146m██[48;5;60m▄[48;5;231m▄▄[38;5;231m██[48;5;233;38;5;60m▄[49;39m       [00m
            [38;5;237m▄▄[48;5;237;38;5;60m▄▄[48;5;60m████[48;5;237m▄[48;5;146;38;5;237m▄▄[38;5;146m████[38;5;60m▄▄[38;5;146m██████████████████[38;5;237m▄▄[49m▀[39m           [38;5;233m▀[48;5;146m▄[38;5;146m███[48;5;231m▄[38;5;231m██[48;5;233;38;5;233m█[49;39m      [00m
       [38;5;237m▄▄▄[48;5;237;38;5;60m▄▄[48;5;60m████[48;5;146;38;5;146m███[48;5;60m▄▄▄[48;5;146m█[48;5;237m▄▄▄[48;5;60;38;5;237m▄[48;5;146m▄▄[48;5;60m▄[38;5;146m▄[48;5;146m████████[38;5;237m▄▄[38;5;233m▄▄▄[49;38;5;237m▀▀▀[39m               [48;5;233;38;5;60m▄[48;5;146;38;5;146m████[48;5;231m▄[38;5;231m█[48;5;60;38;5;60m█[49;39m      [00m
    [38;5;237m▄[48;5;237;38;5;60m▄▄[48;5;60m████[48;5;146;38;5;146m███████████████████[48;5;233m▄▄▄[38;5;233m██[49m▀▀▀▀[39m                       [38;5;60m▀[48;5;146m▄[38;5;146m██████[48;5;60;38;5;60m█[49;39m     [00m
   [38;5;237m▄[48;5;237;38;5;69m▄[48;5;60;38;5;146m▄[48;5;146m██████[38;5;233m▄▄▄▄▄[49;38;5;237m▀▀▀▀▀▀▀▀[48;5;146m▄▄▄▄▄[49;38;5;233m▀▀▀[39m                              [38;5;60m▀[48;5;146m▄[38;5;146m█████[48;5;60m▄[49;38;5;60m▄▄[38;5;146m▄[48;5;60m▄[38;5;233m▄[49;39m[00m
   [48;5;237;38;5;237m█[48;5;146;38;5;146m███[38;5;237m▄[38;5;233m▄[49m▀▀▀[39m                                                     [38;5;60m▀▀[48;5;146m▄▄▄▄▄▄[49m▀[38;5;233m▀[39m [00m
    [38;5;233m▀▀▀[39m                                                                     [00m
                                                                            [00m
