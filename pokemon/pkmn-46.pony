$$$
NAME: Paras
OTHER NAMES: パラス, 派拉斯, 파라스
APPEARANCE: Pokémon Generation I
KIND: Mushroom Pokémon
GROUP: bug, plant
BALLOON: top
COAT: red
DISPLAY: full
BALLOON TOP: 3
BALLOON BOTTOM: 0
POSE: master
BASED ON: Paras
SOURCE: PokéAPI Sprites
MEDIA: Pokémon
LICENSE: Copyright The Pokémon Company
FREE: no
WIDTH: 75
HEIGHT: 22


$$$
$balloon5$
         $\$                                                                     [00m
          $\$                                                                    [00m
           $\$                                                                   [00m
            $\$                                   [00m
             $\$  [38;5;167m▄▄▄[38;5;237m▄[39m                            [00m
             [38;5;167m▄[48;5;167;38;5;210m▄▄[48;5;210m███[48;5;142;38;5;221m▄[48;5;237;38;5;142m▄▄[49;38;5;237m▄[39m          [38;5;167m▄▄▄▄[39m           [00m
            [48;5;167;38;5;167m█[48;5;210;38;5;210m█[48;5;221;38;5;221m███[48;5;210;38;5;210m██[38;5;167m▄[48;5;142m▄▄[48;5;167m█[48;5;237;38;5;237m█[49;39m      [38;5;167m▄[48;5;167;38;5;210m▄▄[48;5;210;38;5;221m▄▄[38;5;210m██[48;5;167m▄▄[49;38;5;237m▄[39m        [00m
           [48;5;167;38;5;237m▄[48;5;210;38;5;221m▄▄[38;5;210m██[38;5;167m▄▄[48;5;167;38;5;142m▄▄[38;5;167m██[38;5;233m▄[49m▀[39m     [48;5;237;38;5;237m█[48;5;210;38;5;167m▄[38;5;210m█[48;5;221;38;5;221m██[38;5;210m▄[48;5;210m███[38;5;167m▄[48;5;167m█[48;5;237;38;5;237m█[49;39m       [00m
           [48;5;237;38;5;237m█[48;5;142;38;5;142m██[48;5;167;38;5;167m███[48;5;142;38;5;142m██[38;5;233m▄[48;5;167m▄[48;5;233;38;5;235m▄[49m▄▄[48;5;235;38;5;209m▄▄▄▄[38;5;130m▄[48;5;237m▄[48;5;167;38;5;233m▄[38;5;167m██[48;5;210m▄▄▄[48;5;167m███[38;5;142m▄[48;5;142m█[48;5;233;38;5;233m█[49;39m      [00m
            [38;5;237m▀[48;5;167;38;5;233m▄▄▄▄[48;5;233;38;5;235m▄[38;5;240m▄[48;5;240;38;5;130m▄[38;5;235m▄[48;5;130m▄▄[48;5;209m▄▄▄[38;5;209m██[38;5;130m▄[48;5;130m█[48;5;233m▄[48;5;167;38;5;233m▄[38;5;167m█[38;5;142m▄▄[38;5;167m█[38;5;142m▄▄[38;5;167m█[48;5;142m▄[38;5;142m█[48;5;233;38;5;233m█[49;39m      [00m
               [38;5;235m▄[48;5;235;38;5;130m▄[48;5;130;38;5;235m▄[48;5;235;38;5;130m▄▄[48;5;130;38;5;95m▄▄▄[38;5;130m███[48;5;235m▄▄[48;5;130;38;5;235m▄▄[38;5;130m█[48;5;142;38;5;240m▄[48;5;233m▄[48;5;142;38;5;233m▄[38;5;142m█[48;5;167;38;5;167m█[48;5;142m▄▄[48;5;167m██[48;5;233;38;5;233m█[49;39m       [00m
              [38;5;240m▄[48;5;235m▄[48;5;130m▄[48;5;95;38;5;215m▄▄▄[48;5;215m███[48;5;95m▄▄▄[48;5;130;38;5;95m▄▄[38;5;130m██[48;5;235m▄[48;5;130;38;5;235m▄[48;5;240;38;5;130m▄▄[48;5;233m▄[38;5;235m▄[48;5;167;38;5;233m▄▄▄[49m▀[39m        [00m
        [38;5;130m▄▄[48;5;130;38;5;215m▄▄[48;5;95;38;5;240m▄[48;5;240;38;5;146m▄[48;5;231;38;5;231m██[48;5;146;38;5;240m▄[48;5;240;38;5;252m▄[48;5;215;38;5;240m▄[48;5;209;38;5;215m▄[48;5;215m██[48;5;209m▄[48;5;215m██[38;5;209m▄[48;5;209m██[48;5;95m▄[48;5;130;38;5;95m▄[38;5;130m█[48;5;235m▄[48;5;130;38;5;235m▄[38;5;130m██[48;5;235m▄[48;5;130;38;5;235m▄[38;5;130m█[48;5;235m▄[49;38;5;235m▄[39m        [00m
      [38;5;130m▄[48;5;130;38;5;215m▄[48;5;215m████[48;5;240;38;5;240m█[48;5;146;38;5;252m▄[48;5;233;38;5;240m▄[38;5;233m█[38;5;240m▄[48;5;252;38;5;252m█[48;5;240;38;5;240m█[48;5;215;38;5;209m▄[38;5;130m▄[38;5;209m▄▄[48;5;209m██[48;5;130m▄[48;5;209;38;5;240m▄[48;5;240;38;5;231m▄▄[38;5;146m▄[48;5;95;38;5;240m▄[48;5;130;38;5;130m█[48;5;235m▄[48;5;130;38;5;235m▄[38;5;130m██[48;5;235;38;5;235m█[48;5;130;38;5;130m██[48;5;235;38;5;235m█[49m▄[38;5;233m▄▄[39m     [00m
     [48;5;130;38;5;130m█[48;5;215;38;5;215m█████[38;5;209m▄[48;5;209m█[48;5;240;38;5;235m▄[48;5;252;38;5;240m▄▄▄[48;5;240;38;5;209m▄[48;5;209m█████[48;5;130m▄[48;5;209m█[48;5;240;38;5;240m█[48;5;146;38;5;146m█[48;5;231;38;5;233m▄▄[48;5;240m▄[48;5;252;38;5;252m█[48;5;240;38;5;240m█[48;5;130;38;5;130m█[48;5;235;38;5;235m█[48;5;130;38;5;95m▄▄[48;5;95;38;5;215m▄▄[48;5;233m▄[48;5;235;38;5;233m▄[48;5;130;38;5;130m█[38;5;95m▄[38;5;130m█[48;5;233m▄[49;38;5;233m▄▄[39m  [00m
    [48;5;130;38;5;130m█[48;5;215;38;5;215m█████[38;5;209m▄[48;5;209m██[48;5;235;38;5;130m▄[48;5;130;38;5;235m▄[38;5;130m██[48;5;209;38;5;95m▄[48;5;130;38;5;215m▄▄[38;5;209m▄[48;5;209;38;5;130m▄▄▄[38;5;209m█[48;5;240m▄[48;5;252;38;5;240m▄[48;5;240;38;5;252m▄[48;5;233m▄[48;5;240m▄[48;5;252;38;5;240m▄[48;5;240;38;5;95m▄[48;5;95;38;5;215m▄▄[48;5;215m█████[48;5;209;38;5;209m█[48;5;233;38;5;233m█[49;39m [38;5;95m▀[48;5;130m▄▄▄[48;5;233;38;5;130m▄[49;38;5;233m▄[39m[00m
   [48;5;95;38;5;95m█[48;5;215;38;5;215m█████[38;5;209m▄[48;5;209m██[38;5;130m▄[48;5;233;38;5;233m█[49;39m [38;5;235m▀[48;5;235;38;5;233m▄[48;5;231;38;5;240m▄[48;5;215;38;5;231m▄[38;5;233m▄[48;5;209m▄[38;5;209m██[48;5;95;38;5;231m▄[48;5;130;38;5;95m▄[38;5;130m██[48;5;240m▄▄[38;5;95m▄[48;5;95;38;5;215m▄[48;5;215m███████[48;5;209;38;5;209m██[48;5;233;38;5;233m█[38;5;130m▄[49;38;5;233m▄[39m   [48;5;95;38;5;95m█[48;5;233;38;5;233m█[49;39m[00m
   [48;5;95;38;5;95m█[48;5;209;38;5;209m█[48;5;215m▄▄[48;5;209m███[38;5;130m▄[38;5;233m▄[49m▀[39m   [38;5;233m▀[48;5;231m▄[48;5;240m▄[49m▀[48;5;233m█[48;5;231;38;5;240m▄▄[48;5;240;38;5;233m▄[48;5;95;38;5;235m▄[48;5;130m▄▄[49m▀[48;5;235;38;5;95m▄[48;5;95;38;5;215m▄[48;5;215m███████[48;5;209;38;5;209m██[48;5;130;38;5;233m▄[49m▀[48;5;130;38;5;95m▄▄[48;5;233;38;5;130m▄[49;38;5;233m▄[39m [38;5;95m▀[48;5;233;38;5;233m█[49;39m[00m
    [48;5;95;38;5;95m█[48;5;209;38;5;235m▄▄[38;5;209m█[38;5;130m▄[48;5;130;38;5;233m▄[49m▀[39m          [38;5;233m▀▀[39m    [38;5;95m▄[48;5;95;38;5;209m▄[48;5;215;38;5;215m██████[38;5;209m▄[48;5;209m██[38;5;233m▄[49m▀[39m   [48;5;95;38;5;95m█[48;5;233;38;5;233m█[49;39m   [00m
    [38;5;95m▀[48;5;209;38;5;233m▄[38;5;209m█[48;5;233;38;5;233m█[49m▀[39m                  [48;5;95;38;5;95m█[48;5;209;38;5;209m██[48;5;215m▄▄▄[48;5;209m███[38;5;130m▄[48;5;130;38;5;233m▄[49m▀[39m    [48;5;95;38;5;233m▄[49m▀[39m   [00m
     [38;5;233m▀[48;5;209m▄[48;5;233m█[49;39m                   [38;5;95m▀[48;5;209m▄[48;5;95;38;5;209m▄▄[48;5;209;38;5;95m▄[38;5;209m██[38;5;130m▄[48;5;130;38;5;233m▄[49m▀[39m           [00m
                           [48;5;95;38;5;95m█[48;5;209;38;5;209m██[38;5;130m▄[48;5;130;38;5;233m▄[48;5;95;38;5;235m▄[49;38;5;233m▀▀[39m             [00m
                          [48;5;95;38;5;233m▄[48;5;209m▄[48;5;130m▄[49m▀▀[39m                 [00m
                                                [00m
