$$$
NAME: Lucario
OTHER NAMES: 路卡利歐, 루카리오, ルカリオ
APPEARANCE: Pokémon Generation IV
KIND: Aura Pokémon
GROUP: ground, humanshape
BALLOON: top
COAT: blue
DISPLAY: full
BALLOON TOP: 3
BALLOON BOTTOM: 0
POSE: master
BASED ON: Lucario
SOURCE: PokéAPI Sprites
MEDIA: Pokémon
LICENSE: Copyright The Pokémon Company
FREE: no
WIDTH: 74
HEIGHT: 35


$$$
$balloon5$
                    $\$                                                     [00m
                     $\$                                                    [00m
                      $\$                                                   [00m
                       $\$                         [00m
                        $\$ [38;5;24m▄[39m                      [00m
                         [48;5;24;38;5;24m█[48;5;75;38;5;75m█[48;5;24;38;5;24m█[49;39m                     [00m
                [48;5;24;38;5;233m▄[38;5;75m▄[49;38;5;24m▄[39m     [38;5;67m▄[48;5;24;38;5;75m▄[48;5;75m██[48;5;233;38;5;233m█[49;39m                    [00m
                [48;5;233;38;5;233m█[48;5;75;38;5;75m██[48;5;24;38;5;67m▄[49;38;5;24m▄[39m   [48;5;24;38;5;24m█[48;5;75m▄[38;5;75m██[48;5;24;38;5;67m▄[49;38;5;233m▄[39m                   [00m
                [38;5;233m▀[48;5;75m▄[48;5;237;38;5;237m█[48;5;67m▄[48;5;233;38;5;67m▄[49;38;5;233m▄[39m  [48;5;24;38;5;24m█[48;5;237;38;5;237m██[48;5;75;38;5;75m█[48;5;67;38;5;67m█[48;5;233;38;5;233m█[49;39m                   [00m
                 [48;5;233;38;5;233m█[48;5;240;38;5;67m▄[48;5;237;38;5;24m▄[48;5;24;38;5;237m▄[48;5;237;38;5;240m▄▄▄[48;5;24;38;5;24m█[48;5;67;38;5;75m▄[48;5;237;38;5;67m▄[48;5;67m██[48;5;233;38;5;233m█[49;39m                   [00m
                  [48;5;233;38;5;233m█[48;5;75;38;5;240m▄[48;5;240;38;5;244m▄[48;5;244m█[48;5;240;38;5;75m▄[48;5;75m█████[48;5;67;38;5;240m▄[48;5;240m█[48;5;233;38;5;233m█[49;38;5;237m▄[39m                 [00m
                 [48;5;237;38;5;233m▄[48;5;75;38;5;240m▄[48;5;244;38;5;244m██[48;5;240;38;5;75m▄[48;5;75m███[38;5;240m▄▄[48;5;240m████[48;5;233;38;5;233m█[48;5;237;38;5;237m█[49m▄▄[39m              [00m
                 [48;5;233;38;5;237m▄[48;5;244;38;5;244m███[48;5;75m▄[48;5;244;38;5;240m▄[48;5;240m█[38;5;233m▄[38;5;131m▄[48;5;233;38;5;231m▄[48;5;240;38;5;240m█████[48;5;233;38;5;233m█[48;5;237;38;5;237m███[49m▄[39m            [00m
               [38;5;237m▄[48;5;237;38;5;244m▄[48;5;244m███[38;5;240m▄[48;5;240m█[48;5;233m▄[48;5;131;38;5;167m▄[48;5;233m▄[48;5;167;38;5;231m▄[48;5;231;38;5;240m▄[48;5;240m██[38;5;67m▄[48;5;67m█[48;5;233;38;5;233m█[48;5;237m▄[38;5;237m██[38;5;240m▄▄[48;5;233m▄[49;38;5;233m▄[39m          [00m
              [48;5;237;38;5;233m▄[48;5;240;38;5;240m█[48;5;244m▄▄[48;5;240m███[38;5;67m▄▄▄▄▄▄[48;5;67m███[48;5;233;38;5;233m█[48;5;237;38;5;237m█[48;5;233m▄[48;5;237;38;5;233m▄[48;5;240;38;5;240m████[48;5;233m▄[49;38;5;233m▄[39m         [00m
               [48;5;233;38;5;237m▄[38;5;240m▄▄[48;5;67;38;5;233m▄▄▄▄▄[48;5;237;38;5;67m▄▄[48;5;67m█[38;5;24m▄[38;5;233m▄[48;5;24m▄[49m▀[48;5;237m▄[38;5;237m██[48;5;233m▄[48;5;240;38;5;233m▄[38;5;240m████[48;5;233;38;5;233m█[49;39m         [00m
       [38;5;240m▄▄▄[39m     [38;5;233m▀[48;5;237m▄[48;5;240m▄[38;5;237m▄▄[48;5;237;38;5;233m▄[48;5;240m▄[48;5;24;38;5;143m▄[38;5;67m▄▄[48;5;67m█[48;5;233;38;5;186m▄[49;38;5;233m▄▄[39m  [38;5;233m▀[48;5;237m▄▄▄[49m▀[48;5;240m▄▄[48;5;237m▄[49m▀[39m         [00m
    [38;5;233m▄[48;5;233;38;5;244m▄[48;5;237m▄[38;5;240m▄[48;5;244;38;5;237m▄[38;5;244m█[48;5;237;38;5;240m▄[49;38;5;237m▄[39m      [38;5;233m▄[48;5;233;38;5;240m▄[48;5;186;38;5;237m▄[38;5;143m▄[48;5;237;38;5;186m▄[48;5;143m▄[48;5;237m▄[48;5;186m█[38;5;143m▄[48;5;143;38;5;186m▄[48;5;233m▄[49;38;5;233m▄▄[39m        [38;5;237m▄[38;5;233m▄[48;5;240;38;5;244m▄▄[49;38;5;237m▄[39m     [00m
   [38;5;233m▄[48;5;233;38;5;244m▄[48;5;237m▄[48;5;244;38;5;240m▄[38;5;237m▄[48;5;240;38;5;240m█[48;5;233;38;5;233m█[48;5;240m▄[49;38;5;237m▀[39m     [48;5;240;38;5;237m▄[48;5;244;38;5;244m█[38;5;233m▄[48;5;233;38;5;231m▄▄[48;5;143;38;5;188m▄[48;5;186;38;5;244m▄[38;5;186m██[48;5;143;38;5;237m▄[48;5;237;38;5;244m▄[48;5;240m▄[38;5;240m█[38;5;24m▄[48;5;233;38;5;233m█[49;39m      [48;5;237;38;5;233m▄[48;5;240;38;5;244m▄[48;5;244m█[48;5;237m▄[48;5;244;38;5;233m▄[48;5;240;38;5;240m█[48;5;237;38;5;237m█[49;39m    [00m
   [38;5;233m▀[48;5;240;38;5;237m▄[48;5;244;38;5;240m▄[38;5;233m▄[48;5;240m▄[48;5;233;38;5;240m▄[48;5;240m█[48;5;237;38;5;237m█[49;39m     [38;5;24m▄[48;5;237;38;5;67m▄[48;5;240;38;5;233m▄[38;5;240m█[48;5;233m▄[48;5;188;38;5;233m▄▄[48;5;237;38;5;186m▄[48;5;186m██[48;5;143;38;5;237m▄[48;5;244;38;5;240m▄▄[48;5;24;38;5;24m█[48;5;67;38;5;67m█[48;5;237m▄[49;38;5;237m▄[39m    [48;5;233;38;5;233m█[48;5;244;38;5;244m█[48;5;240m▄[48;5;237m▄[48;5;244;38;5;233m▄[48;5;240;38;5;240m█[38;5;237m▄[49;38;5;233m▀[39m    [00m
    [38;5;237m▀[48;5;240m▄[38;5;240m█████[48;5;237m▄[49;38;5;237m▄[39m  [38;5;24m▄[48;5;24;38;5;75m▄[48;5;67;38;5;67m█[38;5;24m▄[49;38;5;233m▀▀[48;5;237m▄[48;5;186;38;5;186m███[38;5;143m▄[48;5;237m▄[48;5;240;38;5;237m▄[38;5;233m▄[49m▀[48;5;24m▄[48;5;75;38;5;75m█[48;5;237m▄[49;38;5;24m▄[38;5;237m▄[48;5;237;38;5;67m▄[38;5;237m█[48;5;240;38;5;240m█[48;5;233m▄[48;5;244m▄▄[48;5;240m██[48;5;237;38;5;188m▄[48;5;240;38;5;237m▄[49;39m    [00m
      [38;5;233m▀[48;5;240m▄▄[38;5;240m██[48;5;75;38;5;75m██[48;5;67m▄[48;5;24m▄[48;5;75m█[48;5;67;38;5;233m▄[49m▀[39m  [38;5;233m▄[48;5;233;38;5;143m▄[48;5;143m█[48;5;186m▄[48;5;143m█[48;5;237;38;5;237m█[49m▀[39m    [38;5;233m▀[48;5;75;38;5;24m▄[38;5;75m█[38;5;67m▄[48;5;240;38;5;240m██████[38;5;233m▄[48;5;188;38;5;237m▄[48;5;231m▄[38;5;188m▄[48;5;240m▄[49;38;5;240m▄[39m  [00m
         [38;5;233m▀▀[48;5;240m▄[48;5;75m▄▄[48;5;67;38;5;24m▄[49;38;5;233m▀[39m   [38;5;233m▄[48;5;233;38;5;143m▄[48;5;143m████[48;5;237m▄[49;38;5;233m▄[39m     [38;5;233m▀[48;5;75m▄[38;5;237m▄[38;5;233m▄[48;5;67m▄[48;5;240m▄[49m▀▀▀[39m   [38;5;237m▀▀▀[39m  [00m
                [38;5;233m▄[38;5;237m▄[48;5;233;38;5;240m▄[48;5;240m█[48;5;143;38;5;237m▄[48;5;186;38;5;186m█[38;5;237m▄[48;5;143;38;5;186m▄[48;5;237;38;5;240m▄[48;5;240m██[48;5;233m▄[38;5;24m▄[49;38;5;233m▄▄[39m            [38;5;67m▄▄[39m    [00m
             [38;5;67m▄[48;5;67;38;5;75m▄[48;5;24m▄[48;5;75m█[48;5;24m▄[48;5;240;38;5;237m▄▄[38;5;240m█[48;5;237m▄[48;5;240m███[38;5;237m▄▄[48;5;237;38;5;75m▄[48;5;75m███[48;5;233m▄[49;38;5;233m▄[39m        [38;5;67m▄[48;5;67;38;5;75m▄[48;5;75m██[48;5;24m▄[49;38;5;24m▄[39m  [00m
           [38;5;24m▄[48;5;67;38;5;75m▄[48;5;75m██████[48;5;67;38;5;67m█[48;5;233m▄▄▄[48;5;237m▄[48;5;67m█[38;5;75m▄[48;5;75m█████[48;5;67m▄[48;5;75;38;5;67m▄[48;5;233;38;5;233m█[49;39m   [38;5;24m▄▄[48;5;24;38;5;75m▄▄[48;5;75m█[38;5;67m▄▄[48;5;67m███[48;5;24;38;5;24m█[49;39m [00m
          [38;5;24m▄[48;5;24;38;5;67m▄[48;5;75;38;5;75m██████[48;5;67;38;5;67m██████[48;5;75m▄[38;5;75m██████[48;5;67;38;5;67m█[48;5;75m▄[48;5;67m█[48;5;233;38;5;233m█[48;5;24;38;5;67m▄▄[48;5;67m████[38;5;233m▄[49m▀▀[48;5;67m▄▄[38;5;67m██[48;5;24;38;5;24m█[49;39m[00m
          [48;5;24;38;5;24m█[48;5;67;38;5;67m█[48;5;75m▄▄[38;5;75m██[38;5;67m▄[48;5;67m██[48;5;24m▄[48;5;233;38;5;24m▄[49;38;5;233m▀[48;5;67m▄[38;5;24m▄[38;5;67m█[48;5;75m▄[38;5;75m████[48;5;67;38;5;67m████[48;5;233m▄[48;5;67;38;5;233m▄[38;5;24m▄▄▄[49;38;5;233m▀▀[39m     [38;5;233m▀[48;5;67m▄[48;5;24m▄[49;39m[00m
           [38;5;24m▀[48;5;67;38;5;233m▄[38;5;67m█████[38;5;24m▄▄[49m▀[39m   [38;5;233m▀[48;5;67m▄[38;5;24m▄[48;5;75;38;5;67m▄[48;5;67m███████[48;5;233;38;5;233m█[49;39m             [00m
             [38;5;233m▀[48;5;233m█[38;5;237m▄▄▄▄[49;39m        [38;5;233m▀[48;5;67m▄▄▄[38;5;24m▄[48;5;24;38;5;240m▄[48;5;233m▄[38;5;237m▄[49;39m              [00m
               [48;5;233;38;5;233m█[48;5;237;38;5;237m███[48;5;233;38;5;233m█[49;39m          [38;5;237m▀[48;5;240;38;5;233m▄[38;5;240m██[48;5;237m▄[49m▄[39m             [00m
             [38;5;237m▄[48;5;233;38;5;240m▄[48;5;237;38;5;237m███[48;5;233m▄[49;39m             [38;5;233m▀[48;5;240;38;5;237m▄[38;5;240m██[48;5;237m▄[49;38;5;237m▄[39m           [00m
        [38;5;233m▄[38;5;237m▄[48;5;233;38;5;244m▄[48;5;237m▄▄[48;5;244m█[48;5;240m▄[38;5;240m█[48;5;237;38;5;237m█[49;38;5;233m▀[39m               [38;5;237m▀[48;5;237;38;5;233m▄[48;5;240;38;5;240m██[38;5;244m▄[48;5;237m▄[49;38;5;240m▄▄[39m        [00m
     [38;5;233m▄[48;5;233;38;5;244m▄[48;5;237;38;5;237m█[48;5;244;38;5;244m█[38;5;240m▄[48;5;240;38;5;244m▄[48;5;237m▄[48;5;244m██[38;5;240m▄[48;5;233;38;5;233m█[49;39m                  [38;5;233m▀[48;5;240;38;5;237m▄[48;5;244;38;5;244m█████[48;5;240m▄[48;5;237m▄[49;38;5;237m▄[39m     [00m
     [38;5;233m▀[48;5;240m▄[48;5;237m▄[48;5;240m▄[48;5;237m▄[48;5;244m▄▄[48;5;240m▄[49m▀▀[39m                    [48;5;233;38;5;237m▄[48;5;244;38;5;240m▄[38;5;244m█[48;5;240m▄[48;5;244;38;5;237m▄[38;5;244m█[48;5;240m▄[48;5;244;38;5;237m▄[38;5;240m▄[48;5;233;38;5;233m█[49;39m    [00m
                                    [38;5;233m▀[48;5;240m▄▄[48;5;237m▄[48;5;240m▄▄[48;5;237;38;5;237m█[49;38;5;233m▀[39m     [00m
                                                 [00m
