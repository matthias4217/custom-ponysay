$$$
NAME: Rattata
OTHER NAMES: Rattfratz, 꼬렛, 小拉達, Koratta, コラッタ
APPEARANCE: Pokémon Generation I
KIND: Mouse Pokémon
GROUP: ground
BALLOON: top
COAT: purple
DISPLAY: full
BALLOON TOP: 3
BALLOON BOTTOM: 0
POSE: master
BASED ON: Rattata
SOURCE: PokéAPI Sprites
MEDIA: Pokémon
LICENSE: Copyright The Pokémon Company
FREE: no
WIDTH: 71
HEIGHT: 25


$$$
$balloon5$
                         $\$                                             [00m
                          $\$                                            [00m
                           $\$                                           [00m
                            $\$          [00m
                            [38;5;176m▄[48;5;176;38;5;182m▄▄▄▄[49;38;5;96m▄▄[39m    [00m
                           [48;5;176;38;5;176m█[48;5;182;38;5;182m███[48;5;96m▄▄[48;5;176;38;5;96m▄[38;5;176m█[48;5;96m▄[49;38;5;236m▄[39m  [00m
                            [38;5;96m▀[48;5;182;38;5;236m▄[38;5;96m▄[38;5;176m▄[48;5;176m██[48;5;96;38;5;236m▄[48;5;176;38;5;176m█[48;5;96m▄[48;5;236;38;5;236m█[49;39m [00m
                              [38;5;236m▀▀▀▀[38;5;96m▀[48;5;176m▄[38;5;176m█[48;5;236m▄[49;38;5;233m▄[39m[00m
                                   [38;5;96m▀[48;5;176m▄[38;5;176m█[48;5;233;38;5;233m█[49;39m[00m
                                    [48;5;96;38;5;96m█[48;5;176;38;5;176m█[48;5;233;38;5;233m█[49;39m[00m
                                   [38;5;236m▄[48;5;96;38;5;176m▄[48;5;176m█[48;5;233;38;5;233m█[49;39m[00m
       [38;5;96m▄▄▄[39m           [38;5;176m▄▄▄[39m           [48;5;236;38;5;236m█[48;5;176;38;5;176m█[48;5;233;38;5;233m█[49;39m [00m
      [48;5;96;38;5;236m▄[48;5;182m▄[38;5;182m█[48;5;176;38;5;176m█[48;5;96m▄[49;38;5;96m▄[39m       [38;5;176m▄[48;5;176;38;5;182m▄[48;5;182m█[38;5;236m▄[38;5;176m▄[48;5;96m▄[49;38;5;96m▄[39m        [38;5;236m▄[48;5;236;38;5;96m▄[48;5;176;38;5;233m▄[49m▀[39m [00m
      [48;5;96;38;5;96m█[48;5;137;38;5;222m▄[48;5;236;38;5;236m█[48;5;176;38;5;176m█[38;5;236m▄[48;5;96m▄[48;5;236;38;5;182m▄▄▄▄▄▄[48;5;176;38;5;176m█[48;5;182;38;5;182m█[38;5;236m▄[48;5;96;38;5;137m▄[48;5;137m█[48;5;236m▄[48;5;176;38;5;96m▄[48;5;96;38;5;176m▄[49;38;5;236m▄[39m      [38;5;236m▄[48;5;236;38;5;96m▄[48;5;96;38;5;233m▄[49m▀[39m  [00m
       [48;5;96;38;5;96m█[48;5;222;38;5;222m█[48;5;236;38;5;236m█[48;5;182;38;5;182m██████[38;5;176m▄[48;5;176m█[48;5;182m▄[48;5;176m█[48;5;96;38;5;96m█[48;5;222;38;5;222m██[48;5;137m▄[48;5;96;38;5;96m█[48;5;176;38;5;176m█[48;5;236;38;5;233m▄[49;38;5;96m▄[48;5;96;38;5;176m▄▄▄▄[49;38;5;96m▄[48;5;236m▄[48;5;96;38;5;233m▄[49m▀[39m   [00m
   [48;5;137;38;5;58m▄[38;5;187m▄[49;38;5;137m▄[39m  [48;5;236;38;5;236m█[48;5;176;38;5;176m██[48;5;182m▄▄▄[48;5;176m██[38;5;96m▄[38;5;236m▄[38;5;96m▄[38;5;176m██[48;5;96m▄[48;5;222;38;5;96m▄▄[48;5;96;38;5;176m▄[48;5;176m█[48;5;233;38;5;233m█[48;5;176;38;5;176m███████[48;5;236m▄[49;38;5;236m▄[39m   [00m
    [38;5;58m▀[48;5;187;38;5;96m▄[48;5;96;38;5;176m▄▄[48;5;176m██████[38;5;236m▄[48;5;236;38;5;231m▄[48;5;131;38;5;131m█[48;5;252;38;5;231m▄[48;5;176m▄[38;5;176m██[38;5;236m▄[38;5;233m▄▄▄[48;5;233;38;5;176m▄[48;5;176m█████████[48;5;236;38;5;96m▄[49;38;5;236m▄[39m  [00m
     [48;5;96;38;5;96m█[48;5;182;38;5;176m▄▄[48;5;176;38;5;96m▄[38;5;176m██[38;5;179m▄[38;5;187m▄[48;5;96m▄[48;5;204m▄[48;5;131m▄[48;5;204m▄[48;5;231m▄[38;5;179m▄[48;5;176;38;5;137m▄▄▄[48;5;137;38;5;187m▄▄▄▄▄▄[48;5;176;38;5;58m▄[38;5;176m█████[38;5;96m▄[48;5;96m█[48;5;233;38;5;233m█[49;39m  [00m
      [48;5;96;38;5;58m▄[48;5;176;38;5;187m▄[48;5;179m▄[48;5;187m██[38;5;58m▄[38;5;233m▄▄[38;5;179m▄[38;5;187m███[38;5;179m▄[38;5;137m▄▄[38;5;58m▄▄[48;5;58;38;5;176m▄▄▄▄▄[48;5;176m█████[38;5;96m▄[48;5;96m██[48;5;233;38;5;233m█[49;39m  [00m
       [38;5;58m▀[48;5;58;38;5;252m▄[38;5;231m▄▄[48;5;231m█[48;5;240;38;5;240m█[48;5;233;38;5;233m██[48;5;179;38;5;58m▄[48;5;187;38;5;187m████[48;5;179;38;5;179m█[48;5;176;38;5;176m█[38;5;96m▄[38;5;176m█[38;5;182m▄▄[38;5;176m█[38;5;236m▄[38;5;176m█[48;5;96;38;5;179m▄[48;5;176;38;5;236m▄[38;5;96m▄[48;5;96m█████[48;5;233;38;5;233m█[49;39m [00m
         [48;5;252;38;5;236m▄[48;5;231;38;5;252m▄▄[48;5;240;38;5;233m▄[48;5;233m█[38;5;204m▄[48;5;58m▄[48;5;179;38;5;137m▄[48;5;187;38;5;187m███[48;5;179;38;5;233m▄[48;5;233;38;5;236m▄[48;5;96;38;5;182m▄[48;5;182m████[48;5;233;38;5;233m█[48;5;179;38;5;179m█[38;5;58m▄▄[49;38;5;236m▀▀[48;5;96m▄[38;5;96m███[38;5;179m▄[48;5;233;38;5;233m█[49;39m[00m
       [38;5;236m▄[48;5;236;38;5;96m▄[48;5;96m███[48;5;58;38;5;58m█[48;5;179;38;5;179m█[48;5;204;38;5;137m▄▄[48;5;137;38;5;187m▄[48;5;187m█[38;5;233m▄[49m▀[39m [48;5;236;38;5;236m█[48;5;182;38;5;182m████[48;5;233;38;5;233m█[49;38;5;58m▀▀[39m    [38;5;233m▄[48;5;233;38;5;179m▄[48;5;179;38;5;58m▄[48;5;137;38;5;179m▄[48;5;179;38;5;137m▄[48;5;233;38;5;233m█[49;39m[00m
      [38;5;233m▄[48;5;233;38;5;137m▄[48;5;137;38;5;179m▄[48;5;96m▄[38;5;96m█[38;5;236m▄[49m▀[38;5;58m▀[48;5;179;38;5;233m▄[48;5;187m▄▄[49m▀[39m  [48;5;236;38;5;236m█[48;5;182;38;5;182m███[38;5;176m▄[48;5;176;38;5;233m▄[49m▀[39m      [38;5;233m▀[48;5;58m▄[49m▀[48;5;137m▄[49m▀[39m [00m
      [38;5;233m▀[48;5;179m▄[49m▀[48;5;179m▄[49;38;5;236m▀[39m        [38;5;137m▄[48;5;137;38;5;187m▄[48;5;179m▄[48;5;176;38;5;179m▄[38;5;176m█[38;5;233m▄[49m▀[39m             [00m
                  [48;5;137;38;5;233m▄[48;5;187m▄[48;5;137;38;5;187m▄[48;5;187;38;5;233m▄[38;5;187m█[48;5;233;38;5;233m█[49;39m               [00m
                    [38;5;233m▀[39m [38;5;233m▀[39m                [00m
                                       [00m
