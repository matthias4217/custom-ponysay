$$$
NAME: Makuhita
OTHER NAMES: マクノシタ, Makunoshita, 마크탕, 幕下力士
APPEARANCE: Pokémon Generation III
KIND: Guts Pokémon
GROUP: humanshape
BALLOON: top
COAT: yellow
DISPLAY: full
BALLOON TOP: 3
BALLOON BOTTOM: 0
POSE: master
BASED ON: Makuhita
SOURCE: PokéAPI Sprites
MEDIA: Pokémon
LICENSE: Copyright The Pokémon Company
FREE: no
WIDTH: 75
HEIGHT: 30


$$$
$balloon5$
              $\$                                                            [00m
               $\$                                                           [00m
                $\$                                                          [00m
                 $\$                                 [00m
                  $\$[38;5;95m▄▄▄[39m                             [00m
                   [48;5;95;38;5;233m▄[48;5;222;38;5;222m██[48;5;95m▄▄[49;38;5;233m▄[39m     [38;5;95m▄▄▄[48;5;95;38;5;222m▄▄▄[38;5;233m▄[49;39m              [00m
                   [38;5;233m▀[48;5;222m▄[38;5;222m██[38;5;179m▄[38;5;95m▄[48;5;233;38;5;228m▄[48;5;95m▄[38;5;222m▄[49;38;5;95m▄[48;5;95;38;5;222m▄[48;5;222m███[38;5;179m▄[48;5;222m▄[48;5;179;38;5;233m▄[49m▀[39m              [00m
                    [38;5;233m▀[48;5;222m▄[48;5;179;38;5;179m█[48;5;95;38;5;95m█[48;5;222;38;5;222m██[38;5;222m▄[38;5;95m▄[48;5;95;38;5;222m▄[48;5;222;38;5;222m▄[48;5;222;38;5;179m▄[48;5;179m███[38;5;233m▄[49m▀[39m               [00m
                      [48;5;233;38;5;95m▄[48;5;95;38;5;233m▄[48;5;222;38;5;222m██[48;5;179;38;5;179m█[48;5;95;38;5;95m█[48;5;179;38;5;179m███[38;5;233m▄▄[49m▀[39m                 [00m
                  [38;5;95m▄▄[48;5;95;38;5;228m▄▄[48;5;228m█[48;5;179m▄[48;5;233;38;5;222m▄[48;5;179;38;5;233m▄▄▄[48;5;233;38;5;222m▄▄▄[48;5;179m▄[48;5;95m▄[48;5;233m▄[49;38;5;233m▄▄[39m               [00m
                [38;5;233m▄[48;5;95;38;5;228m▄[48;5;179m▄[48;5;228m██████[48;5;222;38;5;222m███████████[48;5;233m▄[49;38;5;233m▄[39m             [00m
               [48;5;233;38;5;233m█[48;5;222;38;5;222m█[48;5;228m▄[48;5;179m▄[48;5;95;38;5;228m▄[48;5;228;38;5;95m▄[38;5;228m██[38;5;222m▄[48;5;222m████[38;5;95m▄▄[48;5;95;38;5;222m▄▄[48;5;179m▄[48;5;222m█████[48;5;233;38;5;179m▄[49;38;5;233m▄[39m           [00m
              [48;5;233;38;5;233m█[48;5;222;38;5;222m██████[48;5;95;38;5;95m█[48;5;222;38;5;222m█████[48;5;95;38;5;95m█[48;5;179;38;5;222m▄[48;5;222m█████████[48;5;179;38;5;179m█[48;5;233m▄[49;38;5;233m▄[39m          [00m
             [48;5;233;38;5;233m█[48;5;211;38;5;168m▄[48;5;179;38;5;211m▄[48;5;222;38;5;179m▄[38;5;222m█████[48;5;95;38;5;95m█[48;5;222;38;5;222m███[48;5;95;38;5;95m█[48;5;222;38;5;222m████████[38;5;211m▄▄[48;5;179;38;5;168m▄[38;5;179m██[48;5;233;38;5;233m█[49;39m          [00m
             [48;5;233;38;5;233m█[48;5;222;38;5;222m█[48;5;211;38;5;168m▄[38;5;211m█[48;5;222;38;5;179m▄[38;5;222m██[48;5;179;38;5;179m█[48;5;222;38;5;222m█[48;5;179;38;5;179m█[48;5;222;38;5;222m███[48;5;179;38;5;179m█[48;5;222;38;5;222m███[48;5;179;38;5;179m█[48;5;222;38;5;222m██[38;5;211m▄[48;5;211m█[38;5;168m▄[48;5;168;38;5;222m▄[38;5;137m▄[38;5;168m█[48;5;179;38;5;131m▄[48;5;233;38;5;233m█[49;39m          [00m
              [48;5;233;38;5;233m█[48;5;222;38;5;222m█[48;5;211;38;5;168m▄[38;5;211m█[48;5;222;38;5;222m██[48;5;95m▄[48;5;222;38;5;95m▄[38;5;222m██████[38;5;179m▄[38;5;95m▄[48;5;95;38;5;222m▄[48;5;222m██[48;5;211;38;5;211m█[48;5;168;38;5;168m█[48;5;222;38;5;222m███[48;5;168;38;5;168m█[38;5;233m▄[49m▀[39m          [00m
              [38;5;95m▄[48;5;233m▄[48;5;168;38;5;233m▄[38;5;179m▄[48;5;222m▄▄[38;5;222m██[48;5;95m▄▄▄▄▄▄[48;5;179m▄[48;5;222m█[38;5;222m▄[38;5;179m▄▄[48;5;168m▄[38;5;168m██[48;5;222m▄[48;5;137;38;5;131m▄[48;5;131;38;5;233m▄[48;5;233m█[49m▄[39m          [00m
           [38;5;95m▄[48;5;95;38;5;222m▄▄[48;5;222m█[48;5;244;38;5;137m▄[38;5;244m█[48;5;233;38;5;239m▄▄[48;5;179;38;5;233m▄▄[38;5;95m▄[38;5;179m███████████[38;5;233m▄▄[48;5;168m▄[48;5;233;38;5;239m▄[38;5;179m▄[48;5;179m███[48;5;233m▄▄[49;38;5;233m▄[39m       [00m
         [38;5;95m▄[48;5;95;38;5;222m▄[48;5;222m██[38;5;179m▄[38;5;95m▄[38;5;222m█[48;5;244m▄[38;5;137m▄▄[38;5;244m█[48;5;239m▄[38;5;239m█[48;5;233m▄▄▄▄▄▄▄▄▄▄▄[48;5;239m██[38;5;179m▄[48;5;179m██[38;5;95m▄[38;5;179m█████[48;5;233m▄[49;38;5;233m▄[39m     [00m
       [38;5;233m▄[48;5;233;38;5;244m▄[48;5;179m▄[48;5;222;38;5;239m▄[38;5;179m▄[48;5;179m█[48;5;95;38;5;233m▄[48;5;222;38;5;222m█████[48;5;244m▄[38;5;244m███[48;5;239m▄[38;5;239m███████[38;5;137m▄▄[48;5;137;38;5;179m▄[48;5;179m█████[48;5;95m▄[48;5;179;38;5;95m▄[38;5;179m█████[48;5;233m▄[49;38;5;233m▄[39m   [00m
     [38;5;233m▄[48;5;233;38;5;238m▄[48;5;244m▄[38;5;239m▄[38;5;244m█[38;5;239m▄[48;5;239m█[48;5;233;38;5;233m█[48;5;222;38;5;222m███████[48;5;244;38;5;244m██████[48;5;239;38;5;239m████[48;5;137;38;5;137m█[48;5;179;38;5;179m█████████[48;5;95m▄[48;5;179;38;5;95m▄[38;5;137m▄[38;5;239m▄[48;5;137m▄▄▄[48;5;233m▄[49;38;5;233m▄[39m  [00m
   [38;5;233m▄[48;5;233;38;5;239m▄[48;5;239;38;5;244m▄▄[38;5;239m███[48;5;238m▄[48;5;233;38;5;233m█[48;5;222;38;5;222m██[38;5;228m▄[48;5;228m██[48;5;222;38;5;222m███[48;5;244;38;5;244m█████████[48;5;239m▄[48;5;137;38;5;222m▄[48;5;179m▄▄[38;5;179m████████[48;5;233;38;5;233m█[48;5;239m▄▄[38;5;238m▄[38;5;239m███[48;5;233m▄[49;38;5;233m▄[39m [00m
   [48;5;233;38;5;233m█[48;5;244;38;5;244m████[48;5;239;38;5;239m██[38;5;233m▄[48;5;233;38;5;222m▄[48;5;222m██[48;5;228;38;5;228m███[48;5;222;38;5;222m███[48;5;137m▄[48;5;244;38;5;244m███████[38;5;137m▄[48;5;179;38;5;222m▄[48;5;222m████[48;5;179m▄[38;5;179m█████[38;5;233m▄[48;5;233;38;5;239m▄[48;5;238m▄[48;5;239;38;5;244m▄▄[38;5;239m████[48;5;233;38;5;233m█[49;39m [00m
   [48;5;233;38;5;233m█[48;5;239;38;5;239m█[48;5;244m▄▄[48;5;239m███[48;5;233;38;5;233m█[48;5;222;38;5;222m███[48;5;228;38;5;228m██[38;5;222m▄[48;5;222m█████[48;5;244m▄▄▄▄▄[48;5;137m▄[48;5;222m████████[48;5;179;38;5;179m████[48;5;233;38;5;233m█[48;5;239;38;5;239m█[48;5;244;38;5;244m████[48;5;239;38;5;239m████[48;5;233;38;5;233m█[49;39m[00m
   [38;5;233m▀[48;5;239m▄[38;5;239m████[38;5;233m▄[48;5;233m█[48;5;222;38;5;222m██████████████████████████[48;5;179;38;5;179m███[48;5;233;38;5;233m█[48;5;239;38;5;239m██[48;5;244m▄▄[48;5;239m█████[48;5;233;38;5;233m█[49;39m[00m
     [38;5;233m▀▀▀▀[39m  [48;5;233;38;5;233m█[48;5;222;38;5;179m▄[38;5;222m████████████████████████[48;5;179;38;5;179m████[48;5;233m▄[48;5;239;38;5;233m▄[38;5;239m█████[48;5;238;38;5;233m▄[49m▀[39m [00m
            [38;5;233m▀[48;5;179m▄[38;5;179m███[48;5;222m▄▄▄[38;5;222m████████████████[38;5;179m▄[48;5;179m████[38;5;137m▄[38;5;233m▄[49m▀▀▀▀▀[39m   [00m
            [38;5;233m▄▄[48;5;233;38;5;179m▄▄[48;5;95m▄▄▄[48;5;179;38;5;95m▄▄[38;5;233m▄▄[49m▀▀▀▀▀[48;5;222m▄▄▄[38;5;222m█[38;5;179m▄▄[48;5;179m████[38;5;233m▄▄[49m▀▀[39m         [00m
           [48;5;233;38;5;233m█[48;5;179;38;5;222m▄[48;5;222;38;5;95m▄[48;5;95;38;5;179m▄[38;5;222m▄[48;5;179;38;5;179m███[38;5;233m▄[48;5;233m█[49;39m          [38;5;233m▀▀[48;5;233m█[38;5;179m▄▄[48;5;179;38;5;222m▄[38;5;179m█[38;5;222m▄▄[48;5;233;38;5;179m▄[49;38;5;233m▄[39m         [00m
            [38;5;233m▀[48;5;233m█[48;5;222m▄▄▄[49m▀▀[39m              [38;5;233m▀[48;5;179m▄[48;5;222;38;5;179m▄[38;5;222m██[48;5;137;38;5;95m▄[48;5;222;38;5;222m██[48;5;179;38;5;179m█[48;5;233;38;5;233m█[49;39m        [00m
                                   [38;5;233m▀[48;5;179m▄▄▄[48;5;95;38;5;95m█[48;5;222;38;5;233m▄[48;5;179m▄[49m▀[39m        [00m
                                                   [00m
