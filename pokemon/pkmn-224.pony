$$$
NAME: Octillery
OTHER NAMES: 대포무노, Okutank, オクタン, 章魚桶
APPEARANCE: Pokémon Generation II
KIND: Jet Pokémon
GROUP: water1, water2
BALLOON: top
COAT: red
DISPLAY: full
BALLOON TOP: 3
BALLOON BOTTOM: 0
POSE: master
BASED ON: Octillery
SOURCE: PokéAPI Sprites
MEDIA: Pokémon
LICENSE: Copyright The Pokémon Company
FREE: no
WIDTH: 75
HEIGHT: 26


$$$
$balloon5$
                 $\$                                                           [00m
                  $\$                                                          [00m
                   $\$                                                         [00m
                    $\$                               [00m
                     $\$       [38;5;58m▄▄▄▄[39m                   [00m
                    [38;5;58m▄▄[48;5;58;38;5;187m▄▄[38;5;136m▄[48;5;236;38;5;58m▄[38;5;88m▄[38;5;167m▄[48;5;58;38;5;88m▄[48;5;187m▄[38;5;136m▄[38;5;187m██[48;5;58m▄[38;5;136m▄[49;38;5;58m▄[39m                [00m
                  [38;5;58m▄[48;5;58;38;5;136m▄[48;5;187m▄[38;5;187m███[38;5;136m▄[48;5;136m█[48;5;233;38;5;233m█[48;5;167;38;5;167m███[48;5;88m▄▄▄▄▄[48;5;236m▄[38;5;88m▄[49;38;5;236m▄[39m              [00m
                 [38;5;236m▄[48;5;58;38;5;167m▄[48;5;136;38;5;58m▄[38;5;136m███[38;5;58m▄[38;5;233m▄[48;5;233;38;5;88m▄[48;5;88;38;5;167m▄[48;5;167m███████████[48;5;236m▄[49;38;5;233m▄[39m            [00m
                [38;5;236m▄[48;5;236;38;5;167m▄[48;5;167m██[48;5;233m▄▄▄[48;5;88m▄[48;5;167m███████████████[48;5;88;38;5;88m█[48;5;233;38;5;233m█[49;39m           [00m
               [38;5;233m▄[48;5;236;38;5;167m▄[48;5;88;38;5;88m█[48;5;167;38;5;167m█████████[38;5;236m▄▄[48;5;236;38;5;167m▄▄▄[48;5;167;38;5;236m▄▄[38;5;167m████[48;5;88;38;5;88m███[48;5;233;38;5;233m█[49;39m          [00m
               [48;5;233;38;5;233m█[48;5;167;38;5;167m█[48;5;88;38;5;233m▄[48;5;167m▄[48;5;233;38;5;167m▄[48;5;236m▄▄[48;5;167;38;5;236m▄[38;5;167m██[38;5;236m▄[48;5;236;38;5;167m▄[48;5;167m██████[48;5;88m▄[48;5;236;38;5;236m█[48;5;167;38;5;167m█[38;5;88m▄[48;5;88m████[48;5;236m▄[49;38;5;233m▄[39m         [00m
            [38;5;233m▄[48;5;233;38;5;167m▄▄▄[48;5;236m▄[48;5;167m█████[48;5;236;38;5;236m█[48;5;167;38;5;167m██[48;5;236m▄[48;5;233;38;5;240m▄[38;5;231m▄[48;5;236;38;5;233m▄[48;5;167m▄▄[38;5;167m████[48;5;236;38;5;233m▄[48;5;88;38;5;88m██████[48;5;233;38;5;233m█[49;39m         [00m
           [48;5;233;38;5;233m█[48;5;167;38;5;167m█[38;5;233m▄[48;5;236m▄[48;5;167;38;5;236m▄[38;5;88m▄▄▄▄[48;5;88m███[48;5;233;38;5;233m█[48;5;167;38;5;167m██[48;5;240;38;5;240m█[48;5;231;38;5;231m█[48;5;233m▄[48;5;231m██[48;5;233m▄[38;5;240m▄[48;5;167;38;5;167m██[48;5;233;38;5;233m█[48;5;88;38;5;88m█████[38;5;233m▄[49m▀[39m         [00m
           [48;5;233;38;5;233m█[48;5;167m▄[48;5;236;38;5;167m▄[48;5;233;38;5;236m▄[38;5;88m▄[48;5;88m████████[48;5;233;38;5;236m▄[48;5;167;38;5;167m██[48;5;240m▄[48;5;231;38;5;240m▄▄▄[48;5;240;38;5;167m▄[48;5;167m██[48;5;236;38;5;233m▄[48;5;88;38;5;88m█████[38;5;233m▄[49m▀[39m          [00m
            [38;5;233m▀▀[48;5;88m▄▄▄[38;5;88m████████[48;5;236m▄[48;5;233m▄[48;5;167;38;5;236m▄▄▄▄▄[48;5;233;38;5;88m▄▄[48;5;88m█[38;5;167m▄[38;5;88m█[38;5;167m▄[38;5;88m█[38;5;233m▄[49m▀[39m           [00m
                 [38;5;233m▀[48;5;233;38;5;236m▄[38;5;88m▄[48;5;88;38;5;233m▄[38;5;236m▄[38;5;88m████████████[48;5;167m▄[48;5;88;38;5;167m▄[48;5;167;38;5;88m▄[48;5;88;38;5;233m▄[49m▀[39m             [00m
                   [48;5;236;38;5;236m█[48;5;88;38;5;88m████████████████[48;5;233;38;5;233m█[49;39m               [00m
       [38;5;236m▄▄[48;5;236;38;5;167m▄▄▄▄[49;38;5;236m▄[39m      [48;5;236;38;5;233m▄[48;5;88;38;5;88m████████[38;5;236m▄▄▄[38;5;88m███[48;5;233;38;5;233m█[49;39m                [00m
     [38;5;236m▄[48;5;236;38;5;167m▄[48;5;167m██[38;5;88m▄▄[48;5;88m█[38;5;236m▄[49m▀[39m     [38;5;236m▄[48;5;233;38;5;233m█[48;5;167;38;5;167m██[48;5;88m▄▄[38;5;88m█[38;5;236m▄[48;5;236;38;5;167m▄▄[48;5;167m███[48;5;233;38;5;233m█[48;5;88;38;5;88m█[48;5;236;38;5;233m▄[38;5;88m▄▄[49;38;5;236m▄[39m              [00m
    [38;5;58m▄[48;5;58;38;5;187m▄▄[48;5;167;38;5;58m▄[48;5;88;38;5;88m███[38;5;236m▄[49m▀[39m    [38;5;236m▄[48;5;236;38;5;167m▄[48;5;167;38;5;233m▄[48;5;233;38;5;167m▄[48;5;167m███[38;5;236m▄[48;5;236;38;5;167m▄[48;5;167m███[38;5;88m▄▄[48;5;88;38;5;236m▄[48;5;233;38;5;88m▄[48;5;88m█[48;5;233;38;5;236m▄[48;5;88;38;5;88m██[38;5;233m▄[48;5;236m▄[49m▄▄[39m           [00m
   [48;5;58;38;5;58m█[48;5;187;38;5;187m██[38;5;136m▄[48;5;136m█[48;5;58;38;5;233m▄[48;5;88;38;5;88m██[48;5;236;38;5;236m█[49m▄[39m   [38;5;233m▄[48;5;236;38;5;167m▄[48;5;167;38;5;236m▄[48;5;233;38;5;167m▄[48;5;167m███[38;5;236m▄[48;5;236;38;5;58m▄[48;5;58;38;5;187m▄▄[48;5;167;38;5;58m▄[48;5;88;38;5;88m███[48;5;233;38;5;233m█[48;5;88;38;5;88m███[48;5;233m▄▄[48;5;167;38;5;233m▄[38;5;88m▄▄[38;5;167m█[48;5;236m▄[48;5;233;38;5;88m▄[49;38;5;233m▄[39m   [38;5;236m▄[48;5;236;38;5;88m▄[38;5;167m▄[49;38;5;233m▄[39m [00m
   [38;5;233m▀[48;5;136m▄[38;5;136m██[38;5;233m▄[48;5;233;38;5;88m▄[48;5;88m██[48;5;236m▄[48;5;88;38;5;236m▄[48;5;233;38;5;88m▄▄▄[48;5;167;38;5;167m██[48;5;236;38;5;236m█[48;5;167;38;5;167m████[48;5;58;38;5;233m▄[48;5;187;38;5;136m▄[38;5;187m██[38;5;136m▄[48;5;58;38;5;233m▄[48;5;88;38;5;88m██[48;5;233;38;5;233m█[48;5;88;38;5;236m▄▄[38;5;88m█████[38;5;236m▄▄[48;5;236;38;5;167m▄[48;5;233m▄▄▄[49;38;5;236m▄▄[48;5;236;38;5;167m▄[48;5;167m█[48;5;88;38;5;58m▄[48;5;233;38;5;136m▄[49;38;5;233m▄[39m[00m
     [48;5;233;38;5;236m▄[38;5;167m▄[48;5;167;38;5;58m▄[48;5;58;38;5;187m▄▄[48;5;88;38;5;58m▄[38;5;88m██[48;5;236m▄▄[48;5;88m███[48;5;236;38;5;236m█[48;5;167;38;5;167m████[48;5;233;38;5;236m▄[48;5;136;38;5;233m▄[38;5;136m██[38;5;233m▄[48;5;233;38;5;88m▄[48;5;88m██[48;5;236m▄[48;5;167;38;5;233m▄[38;5;167m█[48;5;236m▄[48;5;88;38;5;236m▄[38;5;88m███████[48;5;167m▄▄▄▄▄[48;5;88m█[48;5;58;38;5;58m█[48;5;136;38;5;136m█[48;5;233;38;5;233m█[49;39m[00m
      [48;5;58;38;5;58m█[48;5;187;38;5;187m██[38;5;136m▄[48;5;136m█[48;5;58;38;5;233m▄[48;5;88;38;5;88m███████[48;5;236;38;5;233m▄[48;5;167;38;5;88m▄▄▄[48;5;236;38;5;236m█[48;5;167;38;5;167m█[48;5;233m▄▄[48;5;88;38;5;58m▄▄[38;5;88m███[48;5;236m▄[48;5;167;38;5;236m▄[38;5;88m▄[48;5;236m▄[48;5;88;38;5;233m▄[38;5;88m██████[38;5;236m▄[48;5;58;38;5;187m▄▄▄[48;5;88;38;5;58m▄[38;5;88m█[48;5;58m▄[48;5;136;38;5;233m▄[49m▀[39m[00m
      [38;5;233m▀[48;5;136m▄[38;5;136m██[38;5;233m▄[48;5;233;38;5;88m▄[48;5;88m███[38;5;236m▄▄[49m▀▀▀[38;5;233m▀[48;5;88m▄▄[38;5;88m█[48;5;236;38;5;236m█[48;5;167;38;5;58m▄[48;5;58;38;5;187m▄[48;5;187m██[48;5;58m▄[48;5;88;38;5;58m▄[38;5;88m█████[48;5;233;38;5;233m█[48;5;167;38;5;167m█[48;5;236;38;5;233m▄[48;5;88;38;5;236m▄▄[38;5;88m██[48;5;187;38;5;58m▄[38;5;136m▄[48;5;136m██[38;5;58m▄[48;5;58;38;5;233m▄[49m▀[39m  [00m
        [38;5;233m▀▀▀▀▀▀[39m         [38;5;233m▀[38;5;236m▀[48;5;233;38;5;233m█[48;5;136;38;5;136m█[48;5;187m▄▄[48;5;136m█[48;5;233;38;5;233m█[48;5;88;38;5;88m████[48;5;233;38;5;233m█[48;5;88m▄[49m▀[39m   [38;5;233m▀▀▀▀▀▀[39m     [00m
                          [38;5;233m▀[48;5;136m▄▄[48;5;233m█[48;5;88m▄▄▄[49m▀▀[39m                 [00m
                                                    [00m
