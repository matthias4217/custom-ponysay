$$$
NAME: Dragalge
OTHER NAMES: ドラミドロ, 드래캄, 毒藻龍, Kravarech, Tandrak
APPEARANCE: Pokémon Generation VI
KIND: Mock Kelp Pokémon
GROUP: water1, dragon
BALLOON: top
COAT: brown
DISPLAY: full
BALLOON TOP: 3
BALLOON BOTTOM: 0
POSE: master
BASED ON: Dragalge
SOURCE: PokéAPI Sprites
MEDIA: Pokémon
LICENSE: Copyright The Pokémon Company
FREE: no
WIDTH: 75
HEIGHT: 46


$$$
$balloon5$
                         $\$                                                                  [00m
                          $\$                                                                 [00m
                           $\$                                                                [00m
                            $\$                                                        [00m
                            [38;5;236m▄[48;5;236;38;5;144m▄[38;5;236m█[49;39m       [38;5;16m▄[48;5;16;38;5;132m▄[38;5;16m█[49;39m  [38;5;236m▄▄▄[39m                                       [00m
                          [38;5;236m▄▄[48;5;236;38;5;144m▄[48;5;144m█[48;5;236;38;5;236m█[49;39m     [38;5;16m▄[48;5;16;38;5;205m▄[48;5;132;38;5;132m██[48;5;16;38;5;236m▄[48;5;236;38;5;144m▄▄[48;5;144;38;5;101m▄[48;5;101;38;5;236m▄[49m▀[39m                                       [00m
                         [48;5;236;38;5;236m█[48;5;144;38;5;144m██[38;5;101m▄[48;5;236;38;5;236m█[49;39m    [38;5;16m▄[48;5;16;38;5;205m▄[48;5;205;38;5;132m▄[48;5;132m█[38;5;236m▄[48;5;236;38;5;144m▄[48;5;144m█[38;5;101m▄[48;5;101;38;5;16m▄[49m▀[39m                                         [00m
                        [48;5;236;38;5;236m█[48;5;144;38;5;144m█[38;5;101m▄[48;5;101;38;5;236m▄[49m▀[39m  [38;5;236m▄▄[48;5;236;38;5;205m▄[48;5;205m█[38;5;132m▄[48;5;132m█[38;5;236m▄[48;5;236;38;5;144m▄[48;5;144m█[38;5;101m▄[48;5;16;38;5;16m█[49;39m                                           [00m
                       [38;5;236m▄[48;5;236;38;5;144m▄[48;5;144;38;5;101m▄[48;5;236;38;5;236m█[49;39m [38;5;236m▄[48;5;236;38;5;205m▄▄[48;5;205m███[38;5;236m▄[48;5;132m▄[48;5;236;38;5;144m▄[48;5;144m█[38;5;101m▄[48;5;101m█[48;5;16;38;5;16m█[49;39m                                            [00m
             [48;5;236;38;5;236m█[48;5;95;38;5;205m▄▄▄[49;38;5;236m▄[38;5;95m▄▄[39m   [48;5;236;38;5;236m█[48;5;144;38;5;144m█[48;5;101;38;5;95m▄[48;5;236;38;5;205m▄▄[48;5;205m████[38;5;236m▄[48;5;236;38;5;144m▄[48;5;144m█[38;5;101m▄▄[48;5;101m█[38;5;95m▄[48;5;16m▄[49m▄[38;5;236m▄▄[48;5;236;38;5;205m▄▄▄[38;5;132m▄▄[38;5;236m█[49;39m                                    [00m
              [48;5;236;38;5;236m█[48;5;205;38;5;205m███[48;5;236m▄[48;5;205;38;5;95m▄[48;5;95;38;5;205m▄[49;38;5;95m▄[48;5;236m▄[48;5;144;38;5;101m▄[48;5;95;38;5;95m█[48;5;205;38;5;205m████[38;5;132m▄[38;5;236m▄[48;5;236m█[48;5;144;38;5;95m▄▄[48;5;101m▄[48;5;95;38;5;205m▄▄▄[48;5;205;38;5;95m▄▄[48;5;95;38;5;205m▄[48;5;205m███[38;5;132m▄[48;5;132;38;5;16m▄▄[49;38;5;236m▀[39m                                     [00m
               [38;5;236m▀[48;5;205m▄[48;5;95;38;5;205m▄[48;5;205m█████[48;5;95m▄[48;5;205m██[38;5;132m▄[48;5;132;38;5;236m▄[38;5;95m▄[48;5;95;38;5;205m▄▄[48;5;205m██████[38;5;95m▄▄[38;5;236m▄▄[48;5;132;38;5;16m▄[49m▀▀▀[39m                                        [00m
                 [38;5;236m▀▀[48;5;205m▄▄[38;5;205m████[48;5;132m▄▄[48;5;205m████████[48;5;95m▄▄[48;5;205;38;5;132m▄[38;5;16m▄[48;5;16;38;5;236m▄▄[49;38;5;16m▄▄▄[39m                                         [00m
                  [38;5;95m▄[48;5;95;38;5;205m▄[48;5;205;38;5;95m▄[38;5;205m██████[38;5;132m▄[38;5;16m▄▄▄▄▄[48;5;132m▄▄[48;5;16;38;5;236m▄▄▄[48;5;236;38;5;95m▄[48;5;95m██[38;5;16m▄[49m▀[39m                                          [00m
                 [48;5;95;38;5;95m█[48;5;205m▄[48;5;95;38;5;205m▄[48;5;205m██[38;5;132m▄[38;5;205m█[38;5;132m▄[48;5;132;38;5;16m▄[48;5;16;38;5;236m▄[38;5;95m▄[48;5;95m█████[48;5;236m▄▄[38;5;236m█[38;5;16m▄[48;5;16m██[49m▀▀[39m                                            [00m
               [38;5;16m▄[48;5;236;38;5;132m▄[48;5;205m▄▄[38;5;16m▄▄[48;5;16;38;5;131m▄[38;5;16m█[48;5;132m▄[48;5;16;38;5;95m▄[48;5;236;38;5;16m▄[48;5;95m▄[48;5;16;38;5;237m▄[38;5;139m▄▄[38;5;236m▄[48;5;95;38;5;95m████[38;5;16m▄▄[48;5;236;38;5;95m▄▄[48;5;16m▄▄[49;38;5;16m▄▄[39m                                          [00m
               [38;5;16m▀▀▀▀[38;5;236m▄[48;5;236;38;5;131m▄[48;5;131m█[48;5;16m▄[48;5;95;38;5;16m▄[48;5;16;38;5;205m▄▄[48;5;132;38;5;132m█[48;5;237;38;5;237m█[48;5;218;38;5;218m█[48;5;139;38;5;236m▄[48;5;236;38;5;95m▄[48;5;95;38;5;131m▄[48;5;16;38;5;16m█[49m▀[48;5;95m▄▄[38;5;95m█[48;5;16m▄[38;5;16m█[49m▀▀[39m                   [38;5;236m▄▄[48;5;236;38;5;131m▄▄▄▄▄▄[49;38;5;16m▄[39m                [00m
           [48;5;16;38;5;16m█[48;5;236;38;5;131m▄▄[49;38;5;236m▄▄▄[48;5;236;38;5;131m▄▄[48;5;131m██[38;5;95m▄[48;5;95;38;5;16m▄[48;5;16;38;5;218m▄[48;5;237m▄▄▄[48;5;218m█[38;5;236m▄[48;5;236;38;5;95m▄[48;5;95;38;5;131m▄[48;5;16;38;5;16m█[49;39m    [38;5;16m▀[48;5;95m▄[38;5;95m█[48;5;16m▄[49;38;5;16m▄▄[39m   [38;5;16m▄[48;5;16;38;5;95m▄[38;5;16m█[49;39m       [38;5;236m▄▄▄[48;5;236;38;5;131m▄▄[48;5;131;38;5;95m▄[38;5;16m▄[48;5;95m▄[49m▀▀▀▀▀[48;5;95m▄[48;5;16m█[49;39m               [00m
            [38;5;16m▀▀[48;5;131m▄▄▄[38;5;95m▄▄[48;5;95m█[38;5;16m▄[48;5;16;38;5;139m▄[48;5;139;38;5;16m▄▄[48;5;218m▄[38;5;139m▄▄[38;5;236m▄[48;5;236;38;5;95m▄[48;5;95;38;5;131m▄[48;5;131;38;5;95m▄[48;5;16;38;5;16m█[49;39m      [38;5;16m▀[48;5;95m▄▄[38;5;95m█[48;5;16m▄▄▄[48;5;95m█[48;5;16;38;5;236m▄[49m▄▄▄▄[48;5;236;38;5;131m▄▄▄[38;5;95m▄[48;5;131m▄[38;5;16m▄▄▄[48;5;95;38;5;95m█[48;5;16;38;5;16m█[49;39m                        [00m
                 [38;5;16m▀▀▀▀▀[39m   [38;5;16m▀▀[48;5;95m▄[48;5;131m▄[38;5;131m█[48;5;95;38;5;95m█[48;5;16m▄[49;38;5;16m▄[39m        [38;5;16m▀▀▀[48;5;95m▄▄▄▄[48;5;131m▄▄[48;5;95m▄▄[49m▀▀▀▀[39m   [38;5;16m▀[48;5;95m▄[48;5;16m█[49;39m                       [00m
                             [38;5;16m▀[48;5;131m▄[48;5;95;38;5;131m▄▄[48;5;16;38;5;95m▄▄[49;38;5;16m▄▄▄▄▄[48;5;16;38;5;131m▄▄▄▄▄[48;5;131m██[38;5;16m▄[49m▀[39m [48;5;16;38;5;16m█[48;5;95;38;5;95m█[48;5;16;38;5;16m█[49;39m                                [00m
                               [48;5;16;38;5;16m█[48;5;131;38;5;131m███[48;5;95;38;5;16m▄[48;5;131m▄▄[38;5;139m▄[48;5;16;38;5;218m▄▄▄[38;5;139m▄[48;5;131;38;5;16m▄▄[38;5;95m▄[48;5;16;38;5;131m▄[49;38;5;16m▄▄[48;5;16;38;5;95m▄[48;5;95m█[48;5;16;38;5;16m█[49;39m                                 [00m
                               [38;5;16m▀[48;5;131m▄▄[48;5;16;38;5;139m▄[48;5;218;38;5;218m██[48;5;237;38;5;237m█[48;5;218;38;5;218m██████[48;5;237;38;5;237m█[48;5;16;38;5;139m▄[48;5;131;38;5;16m▄[38;5;95m▄[48;5;95m█[38;5;16m▄[49m▀[39m                                  [00m
                               [38;5;16m▄[48;5;16;38;5;139m▄[48;5;139m█[48;5;218;38;5;237m▄▄[48;5;237;38;5;218m▄[48;5;218m██[38;5;236m▄[48;5;237m▄[48;5;218;38;5;218m███[48;5;237;38;5;237m█[48;5;218;38;5;218m█[48;5;139m▄[48;5;16;38;5;16m█[48;5;95;38;5;95m█[48;5;16;38;5;16m█[49;39m  [38;5;16m▄▄[39m                               [00m
                               [48;5;16;38;5;16m█[48;5;237;38;5;139m▄▄[48;5;139m█[48;5;218m▄▄[38;5;236m▄[48;5;236;38;5;131m▄[48;5;131m█[48;5;236;38;5;236m█[48;5;218;38;5;218m██[38;5;237m▄[48;5;237;38;5;218m▄[48;5;218m██[48;5;139;38;5;139m█[48;5;16;38;5;16m█[48;5;95;38;5;95m█[48;5;16m▄▄[48;5;95;38;5;16m▄[49m▀[39m                               [00m
                            [38;5;236m▄▄▄[48;5;16;38;5;131m▄[48;5;139;38;5;16m▄▄▄[48;5;16;38;5;95m▄▄[48;5;95;38;5;16m▄[48;5;16m█[48;5;131;38;5;131m█[48;5;236m▄[48;5;218;38;5;236m▄[38;5;237m▄[48;5;237;38;5;218m▄[48;5;218m██[38;5;139m▄[48;5;237;38;5;237m█[48;5;16;38;5;16m█[48;5;95;38;5;95m█[48;5;16;38;5;16m█[49m▀[39m                                 [00m
                             [38;5;236m▀[48;5;131m▄[38;5;16m▄[48;5;95;38;5;95m███[48;5;16m▄▄[48;5;139;38;5;16m▄▄[48;5;16m█[48;5;131;38;5;131m█[48;5;236m▄[48;5;218;38;5;16m▄[38;5;139m▄[38;5;218m█[38;5;237m▄[48;5;237;38;5;139m▄[48;5;139m█[48;5;16;38;5;16m██[49;39m                                   [00m
                               [38;5;16m▄[48;5;16;38;5;131m▄▄[38;5;16m██[48;5;95m▄▄▄▄[48;5;16;38;5;95m▄[48;5;131;38;5;16m▄[38;5;131m█[48;5;16m▄[48;5;237;38;5;16m▄[48;5;139;38;5;139m██[38;5;237m▄[48;5;16;38;5;16m█[49;39m                                    [00m
                              [38;5;16m▄[48;5;16;38;5;131m▄[48;5;131;38;5;16m▄[49m▀[39m      [38;5;16m▀[48;5;16m█[38;5;95m▄[48;5;131;38;5;16m▄[38;5;131m█[48;5;16m▄[48;5;237;38;5;16m▄[48;5;16m█[49;39m                                     [00m
                   [38;5;16m▄▄▄▄[39m    [38;5;16m▄▄[48;5;16;38;5;131m▄[48;5;131;38;5;16m▄[49m▀[39m         [38;5;16m▀[48;5;95m▄[38;5;95m█[48;5;16;38;5;16m██[48;5;131m▄▄[48;5;16;38;5;131m▄▄[49;38;5;16m▄▄[39m       [38;5;16m▄▄[39m                        [00m
               [48;5;16;38;5;16m█[38;5;131m▄▄[49;38;5;16m▄▄[48;5;16m█[48;5;131;38;5;236m▄[38;5;95m▄[48;5;16;38;5;131m▄▄▄[38;5;16m█[48;5;95m▄[49m▀▀[39m            [48;5;16;38;5;16m█[48;5;95;38;5;95m█[48;5;16;38;5;16m█[48;5;139;38;5;139m█[48;5;16;38;5;16m█[49m▄[48;5;16m██[48;5;131m▄[38;5;95m▄[48;5;16;38;5;131m▄▄▄[49;38;5;16m▄▄▄[48;5;16;38;5;131m▄[48;5;131m█[48;5;16;38;5;16m█[49;39m  [38;5;16m▄▄[48;5;16;38;5;95m▄[38;5;16m█[49;39m                  [00m
            [38;5;16m▄▄▄▄[48;5;16m█[48;5;131;38;5;236m▄[38;5;95m▄[48;5;95m█[38;5;131m▄[48;5;131m███[38;5;16m▄[49m▀[39m               [38;5;16m▄[48;5;16;38;5;95m▄[48;5;95;38;5;16m▄[48;5;16;38;5;139m▄[48;5;139m████[48;5;237;38;5;237m█[48;5;139;38;5;139m█[48;5;16m▄▄[38;5;237m▄[48;5;131;38;5;16m▄[38;5;95m▄[38;5;131m███[48;5;95m▄[48;5;16;38;5;95m▄▄▄[48;5;95m██[38;5;16m▄[49m▀[39m                  [00m
          [38;5;16m▀▀[48;5;95m▄▄[38;5;95m██████[48;5;131m▄▄[48;5;95;38;5;16m▄[49m▀[39m                 [48;5;16;38;5;16m█[48;5;95;38;5;95m█[48;5;16;38;5;16m█[48;5;139m▄▄▄[48;5;16m███[38;5;95m▄[48;5;139;38;5;16m▄▄[48;5;237;38;5;139m▄[48;5;139m█[48;5;16;38;5;16m█[48;5;131m▄[38;5;131m███[48;5;95m▄[38;5;95m███[48;5;236m▄[48;5;16m▄▄▄▄▄▄[49;38;5;16m▄▄▄▄[39m          [00m
       [38;5;16m▄▄[48;5;16;38;5;95m▄▄▄[48;5;95m███████[38;5;16m▄▄[49m▀[39m                   [38;5;16m▀▀▀▀▀[39m    [38;5;16m▀▀[48;5;95m▄[48;5;16;38;5;95m▄[48;5;139;38;5;16m▄[38;5;139m█[48;5;16;38;5;16m█[49m▀[48;5;131m▄[38;5;131m█[38;5;95m▄[48;5;95m█████████[38;5;16m▄▄▄▄▄[49m▀▀[39m        [00m
    [38;5;16m▄[48;5;16;38;5;95m▄▄[48;5;95m███████[38;5;16m▄▄▄[49m▀▀[39m                                  [48;5;16;38;5;16m█[48;5;95;38;5;95m█[48;5;16;38;5;16m█[38;5;139m▄[49;38;5;16m▄[39m [38;5;16m▀[48;5;95m▄▄[38;5;95m███████████[48;5;16m▄▄▄[49;38;5;16m▄▄▄[39m      [00m
   [38;5;16m▀▀▀▀▀▀▀▀▀▀▀[39m                                        [48;5;16;38;5;16m█[48;5;95;38;5;95m█[48;5;16;38;5;16m██[49;39m    [38;5;16m▀▀▀[48;5;95m▄▄▄[38;5;95m███████████[48;5;16m▄▄[49;38;5;16m▄▄[39m  [00m
                                                      [48;5;16;38;5;16m█[48;5;95;38;5;95m█[48;5;16;38;5;16m█[49m▀[39m          [38;5;16m▀▀▀[48;5;95m▄▄▄▄▄▄▄▄▄▄▄[49m▀▀▀[39m[00m
                                       [38;5;16m▄▄[48;5;16;38;5;95m▄▄▄▄[38;5;131m▄▄▄[49;38;5;16m▄▄[39m   [48;5;16;38;5;16m█[48;5;95;38;5;95m█[38;5;16m▄[49m▀[39m                            [00m
                                         [38;5;16m▀[48;5;16;38;5;236m▄▄[48;5;95m▄▄[48;5;131;38;5;131m████[48;5;16;38;5;95m▄[49;38;5;16m▄[48;5;16;38;5;95m▄[48;5;95m█[38;5;16m▄[49m▀[39m                             [00m
                                     [38;5;236m▄▄[48;5;236;38;5;131m▄▄▄[48;5;131m████████[48;5;95m▄[38;5;95m██[48;5;16;38;5;16m█[49;39m                               [00m
                                  [38;5;236m▄[48;5;236;38;5;131m▄▄[48;5;131m████[38;5;95m▄▄[38;5;236m▄[38;5;95m▄[38;5;131m███████[48;5;95;38;5;95m█[48;5;16;38;5;16m█[49;39m                               [00m
                             [38;5;236m▄▄▄[48;5;236;38;5;131m▄▄[48;5;131m███[38;5;95m▄▄[48;5;95;38;5;16m▄▄[48;5;236;38;5;236m█[38;5;131m▄[48;5;131m█████[48;5;95;38;5;95m█[48;5;131;38;5;236m▄[38;5;131m█[38;5;95m▄[48;5;95;38;5;16m▄[49m▀[39m                               [00m
                              [38;5;236m▀▀[48;5;131m▄[38;5;16m▄▄▄[49;38;5;236m▀[38;5;16m▀▀[39m [38;5;236m▄[48;5;236;38;5;131m▄[48;5;131m███[38;5;95m▄[48;5;95;38;5;16m▄▄[48;5;236;38;5;236m█[48;5;131;38;5;131m█[38;5;95m▄[48;5;95;38;5;16m▄[49m▀[39m                                [00m
                                     [38;5;236m▄[48;5;236;38;5;131m▄▄[48;5;131m███[38;5;95m▄[48;5;95m█[48;5;16;38;5;16m█[49;39m [48;5;236;38;5;16m▄[48;5;131;38;5;131m█[48;5;95;38;5;95m█[48;5;16;38;5;16m█[49;39m                                  [00m
                                    [38;5;236m▄[48;5;236;38;5;131m▄[48;5;131m██[38;5;95m▄▄[48;5;95;38;5;16m▄▄[49m▀[39m  [48;5;16;38;5;16m█[48;5;95;38;5;95m█[48;5;16;38;5;16m█[49;39m                                   [00m
                                   [48;5;16;38;5;16m█[48;5;131;38;5;95m▄[48;5;95;38;5;16m▄[49m▀▀▀▀[39m      [48;5;16;38;5;16m█[49;39m                                    [00m
                                   [38;5;16m▀▀[39m                                                [00m
                                                                                     [00m
