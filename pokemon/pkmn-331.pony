$$$
NAME: Cacnea
OTHER NAMES: サボネア, 선인왕, Sabonea, 刺球仙人掌, Tuska
APPEARANCE: Pokémon Generation III
KIND: Cactus Pokémon
GROUP: plant, humanshape
BALLOON: top
COAT: green
DISPLAY: full
BALLOON TOP: 3
BALLOON BOTTOM: 0
POSE: master
BASED ON: Cacnea
SOURCE: PokéAPI Sprites
MEDIA: Pokémon
LICENSE: Copyright The Pokémon Company
FREE: no
WIDTH: 75
HEIGHT: 22


$$$
$balloon5$
                       $\$                                                        [00m
                        $\$                                                       [00m
                         $\$                                                      [00m
                          $\$                              [00m
                       [38;5;137m▄▄[39m [38;5;137m▄[48;5;233;38;5;220m▄[49;38;5;233m▄[39m [38;5;137m▄[48;5;137;38;5;220m▄[49;38;5;233m▄[39m                        [00m
                       [48;5;233;38;5;233m█[48;5;220;38;5;220m█[48;5;137m▄[48;5;220m██[48;5;178m▄[48;5;233m▄[48;5;220m██[48;5;137;38;5;178m▄[49;38;5;233m▄[38;5;137m▄[48;5;137;38;5;178m▄[48;5;233;38;5;233m█[49;39m                    [00m
                        [48;5;137;38;5;233m▄[48;5;220;38;5;220m█[38;5;178m▄[38;5;22m▄▄▄▄[48;5;178m▄▄[38;5;178m██[38;5;137m▄[49m▀[39m                    [00m
                 [48;5;233;38;5;233m█[38;5;78m▄[48;5;22;38;5;28m▄[49;38;5;22m▄▄[39m [38;5;22m▄[48;5;22;38;5;28m▄[38;5;78m▄▄[48;5;78m██[48;5;28m▄[38;5;28m███[48;5;22;38;5;65m▄[48;5;233;38;5;106m▄[38;5;107m▄[49;38;5;233m▄[38;5;22m▄[38;5;28m▄[48;5;28;38;5;78m▄[48;5;22m▄[48;5;233;38;5;233m█[49;39m               [00m
                 [38;5;22m▀[48;5;22;38;5;233m▄[48;5;78;38;5;28m▄[48;5;28;38;5;22m▄[38;5;107m▄[48;5;22;38;5;150m▄[48;5;78;38;5;65m▄[38;5;28m▄[38;5;78m███[38;5;28m▄[48;5;28;38;5;22m▄[38;5;65m▄[48;5;22;38;5;150m▄[48;5;65m▄[48;5;150m██[48;5;106m▄[48;5;22;38;5;22m█[48;5;28;38;5;28m█[48;5;78m▄▄[48;5;28;38;5;233m▄[49m▀[39m               [00m
               [38;5;233m▄[38;5;22m▄▄[48;5;233;38;5;233m█[48;5;22;38;5;150m▄[48;5;150;38;5;107m▄[38;5;22m▄▄[38;5;107m▄[48;5;65;38;5;150m▄[48;5;28;38;5;22m▄[38;5;65m▄[48;5;22;38;5;150m▄[48;5;65m▄[48;5;150m█[38;5;22m▄[38;5;233m▄▄[38;5;22m▄[38;5;107m▄[38;5;150m█[48;5;22m▄[48;5;28;38;5;233m▄▄[48;5;233;38;5;22m▄[49m▄▄[38;5;233m▄[39m              [00m
               [38;5;233m▀[48;5;28m▄[38;5;22m▄[48;5;233;38;5;150m▄[48;5;150;38;5;233m▄[48;5;233m█[38;5;23m▄[38;5;231m▄[48;5;22;38;5;22m█[48;5;150;38;5;150m█████[48;5;65;38;5;233m▄[48;5;233m█[38;5;231m▄▄[38;5;233m██[48;5;107;38;5;65m▄[48;5;150;38;5;150m█[48;5;106m▄[48;5;22;38;5;22m█[48;5;28;38;5;28m██[38;5;233m▄[49m▀[39m              [00m
                 [48;5;22;38;5;22m█[48;5;150;38;5;150m█[48;5;233;38;5;233m█[48;5;23m▄[48;5;231;38;5;231m█[48;5;233;38;5;22m▄[48;5;22;38;5;107m▄[48;5;150;38;5;150m█████[48;5;233;38;5;65m▄[48;5;23;38;5;233m▄[48;5;233;38;5;231m▄[48;5;231m█[48;5;23;38;5;233m▄[48;5;233m█[48;5;65;38;5;107m▄[48;5;150;38;5;150m███[48;5;22;38;5;106m▄[38;5;107m▄[48;5;65m▄[49;38;5;233m▄[39m              [00m
             [38;5;65m▄[38;5;22m▄[48;5;65;38;5;107m▄[48;5;22;38;5;22m█[48;5;106;38;5;106m█[48;5;150;38;5;150m█[48;5;106m▄[48;5;233m▄▄[48;5;106m▄[48;5;150m███████[48;5;107m▄[48;5;233m▄▄▄[48;5;107m▄[48;5;150;38;5;65m▄▄[38;5;150m█[38;5;106m▄[48;5;107;38;5;107m█[38;5;22m▄▄[48;5;233m▄[49m▄[39m             [00m
         [38;5;65m▄▄[48;5;65;38;5;150m▄▄[48;5;150m█[48;5;106;38;5;106m█[48;5;107;38;5;65m▄[48;5;22;38;5;233m▄[48;5;106;38;5;107m▄[48;5;65;38;5;233m▄[48;5;233m█[48;5;65m▄[48;5;150;38;5;150m█[38;5;65m▄[48;5;65;38;5;233m▄[48;5;233m██[48;5;65m▄[48;5;150;38;5;65m▄[38;5;150m█[48;5;65;38;5;233m▄[48;5;233m██[48;5;65m▄[48;5;150;38;5;150m█[48;5;65;38;5;22m▄[48;5;233;38;5;233m██[48;5;22;38;5;22m█[48;5;107m▄[48;5;22;38;5;107m▄[48;5;107;38;5;22m▄▄[38;5;107m███[48;5;22m▄▄[48;5;65m▄[49;38;5;65m▄▄[39m       [00m
   [48;5;233;38;5;233m█[48;5;65;38;5;28m▄▄[49;38;5;22m▄[38;5;65m▄[48;5;65;38;5;150m▄[48;5;106m▄[48;5;150m██[38;5;65m▄[48;5;65;38;5;150m▄[38;5;106m▄[48;5;107;38;5;107m█[48;5;233;38;5;233m█[48;5;107;38;5;107m█[48;5;233;38;5;65m▄[38;5;233m█[38;5;22m▄[48;5;150;38;5;150m█[48;5;233;38;5;233m██████[48;5;150;38;5;150m█[48;5;233;38;5;65m▄[38;5;233m██[38;5;65m▄[48;5;150;38;5;150m█[48;5;106m▄[48;5;22;38;5;106m▄[38;5;107m▄[48;5;107m█[48;5;22;38;5;233m▄[48;5;107;38;5;65m▄[38;5;107m██[48;5;22m▄[48;5;65m▄[48;5;107;38;5;22m▄[38;5;106m▄[48;5;106;38;5;150m▄[48;5;150m███[48;5;65m▄[38;5;106m▄[49;38;5;65m▄[38;5;233m▄[48;5;22;38;5;28m▄[48;5;233;38;5;233m█[49;39m [00m
    [48;5;233;38;5;233m█[48;5;28;38;5;65m▄[48;5;65;38;5;150m▄[48;5;150m███[38;5;65m▄[48;5;65;38;5;150m▄[48;5;150m█[38;5;106m▄[48;5;107;38;5;107m██[48;5;233;38;5;233m█[48;5;107m▄[38;5;107m█[48;5;65;38;5;65m█[48;5;150;38;5;106m▄[38;5;150m█[48;5;22m▄[48;5;233;38;5;22m▄[38;5;233m██[38;5;22m▄[48;5;22;38;5;150m▄[48;5;150m███[48;5;65;38;5;65m█[48;5;150;38;5;150m██[38;5;106m▄[48;5;106;38;5;107m▄[48;5;107;38;5;65m▄[48;5;65m█[48;5;107m▄[48;5;233;38;5;107m▄[38;5;65m▄[48;5;107;38;5;233m▄[38;5;107m██[48;5;106;38;5;106m█[48;5;22;38;5;150m▄▄[48;5;150;38;5;65m▄[38;5;150m████[48;5;233;38;5;233m█[48;5;28;38;5;65m▄[48;5;65m█[48;5;233;38;5;233m█[49;39m [00m
    [38;5;65m▄[48;5;65;38;5;150m▄[48;5;150m██[38;5;107m▄[48;5;107;38;5;150m▄[48;5;150m██[38;5;106m▄[48;5;107;38;5;107m██[48;5;233;38;5;233m█[49;39m [48;5;233;38;5;233m█[48;5;65;38;5;65m█[48;5;22;38;5;22m█[48;5;107;38;5;107m█[48;5;106m▄[48;5;150;38;5;106m▄▄▄[48;5;106;38;5;28m▄[48;5;28m█[48;5;150m▄[38;5;106m▄▄▄[48;5;106;38;5;107m▄[48;5;22;38;5;65m▄[48;5;106;38;5;107m▄[48;5;107m█[48;5;65;38;5;65m█████[48;5;107;38;5;107m█[48;5;65;38;5;65m▄[48;5;233;38;5;233m█[48;5;107m▄[38;5;107m█[48;5;106m▄[48;5;150;38;5;106m▄[38;5;22m▄[48;5;65;38;5;233m▄[48;5;150m▄[38;5;150m██[48;5;106m▄[48;5;22m▄▄[48;5;233;38;5;107m▄[49;38;5;233m▄[39m[00m
    [48;5;233;38;5;233m█[48;5;150;38;5;106m▄[38;5;65m▄[48;5;65;38;5;28m▄[48;5;22;38;5;65m▄[48;5;106;38;5;233m▄[48;5;150;38;5;106m▄[48;5;106;38;5;107m▄[48;5;107m██[48;5;233;38;5;233m█[49;39m   [48;5;233;38;5;233m█[48;5;65;38;5;65m█[48;5;65;38;5;65m█[48;5;107;38;5;107m██[38;5;65m▄[48;5;28m▄▄▄[48;5;65m██[48;5;107m▄[38;5;107m███[48;5;22m▄[48;5;107;38;5;22m▄[38;5;107m██[48;5;65m▄▄[48;5;107m█[38;5;65m▄[48;5;233;38;5;233m█[49;39m  [38;5;233m▀[48;5;107m▄[48;5;22;38;5;22m█[48;5;233;38;5;28m▄[48;5;28;38;5;65m▄[48;5;65m█[48;5;233;38;5;233m█[48;5;150;38;5;150m███[38;5;106m▄[48;5;107;38;5;107m█[48;5;233;38;5;233m█[49;39m[00m
     [48;5;22;38;5;233m▄[48;5;28;38;5;22m▄[48;5;65m▄[38;5;233m▄[48;5;233;38;5;107m▄[48;5;107m██[38;5;65m▄[48;5;22;38;5;22m█[48;5;233m▄[49;38;5;233m▄[39m   [48;5;233;38;5;22m▄[48;5;65;38;5;233m▄[48;5;107;38;5;22m▄[48;5;65;38;5;107m▄[38;5;65m███████[38;5;107m▄[48;5;107m██[48;5;65;38;5;22m▄[48;5;22;38;5;65m▄▄▄[48;5;233m▄[38;5;22m▄[48;5;107;38;5;233m▄[49m▀[39m    [38;5;233m▀[48;5;233m█[38;5;107m▄▄▄[48;5;22m▄[48;5;65m▄[48;5;106;38;5;65m▄▄[48;5;107m▄[48;5;65;38;5;233m▄[49m▀[39m[00m
      [38;5;233m▀▀[48;5;107m▄▄▄[49m▀▀▀▀▀[39m   [48;5;22;38;5;22m█[48;5;65;38;5;28m▄[48;5;233m▄[38;5;65m▄[48;5;107;38;5;233m▄[48;5;65m▄[48;5;65;38;5;107m▄[38;5;65m▄▄[48;5;65;38;5;107m▄[48;5;107m██[38;5;65m▄[48;5;22;38;5;22m█[48;5;65;38;5;28m▄[48;5;28m██[48;5;65m▄[38;5;65m██[48;5;233;38;5;233m█[49;39m       [38;5;233m▀[48;5;107m▄[38;5;22m▄[38;5;107m████[38;5;233m▄[49m▀[39m [00m
                   [48;5;22;38;5;22m█[48;5;28;38;5;28m█[38;5;65m▄[48;5;22;38;5;22m██[38;5;233m▄[49m▀▀▀▀▀▀▀▀[48;5;65m▄[48;5;28;38;5;65m▄[38;5;28m██[48;5;65m▄[38;5;65m█[48;5;233;38;5;233m█[49;39m        [48;5;233;38;5;233m█[48;5;22m▄▄[49m▀▀▀[39m   [00m
                   [48;5;22;38;5;233m▄[48;5;65m▄[48;5;22m▄[49m▀▀[39m          [38;5;233m▀[48;5;65m▄[48;5;28m▄[38;5;28m█[48;5;65;38;5;65m█[48;5;233;38;5;233m█[49;39m                 [00m
                                     [38;5;233m▀▀[39m                  [00m
                                                         [00m
