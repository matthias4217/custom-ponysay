$$$
NAME: Cresselia
OTHER NAMES: クレセリア, 克雷色利亞, Crecelia, 크레세리아
APPEARANCE: Pokémon Generation IV
KIND: Lunar Pokémon
GROUP: no-eggs
BALLOON: top
COAT: yellow
DISPLAY: full
BALLOON TOP: 3
BALLOON BOTTOM: 0
POSE: master
BASED ON: Cresselia
SOURCE: PokéAPI Sprites
MEDIA: Pokémon
LICENSE: Copyright The Pokémon Company
FREE: no
WIDTH: 75
HEIGHT: 38


$$$
$balloon5$
           $\$                                                                         [00m
            $\$                                                                        [00m
             $\$                                                                       [00m
              $\$                                                         [00m
               $\$[38;5;95m▄▄▄[39m                                                     [00m
               [38;5;233m▀[48;5;230m▄[38;5;137m▄[38;5;230m█[48;5;95m▄▄[38;5;137m▄[49;38;5;233m▄[38;5;95m▄[39m                                                [00m
                 [48;5;233;38;5;233m█[48;5;137;38;5;222m▄[48;5;230;38;5;137m▄[38;5;230m█[38;5;222m▄[48;5;222m█[48;5;95m▄[48;5;233m▄[49;38;5;233m▄[39m                                              [00m
                  [48;5;95;38;5;233m▄[48;5;137;38;5;222m▄[48;5;222;38;5;137m▄[38;5;222m█████[48;5;233;38;5;137m▄[49;38;5;233m▄[39m                                            [00m
                  [38;5;233m▀[48;5;222;38;5;95m▄[48;5;137;38;5;137m█[48;5;222;38;5;222m██████[48;5;233m▄[49;38;5;233m▄[39m                                           [00m
                   [48;5;95;38;5;95m█[48;5;222;38;5;222m█[48;5;137;38;5;137m█[48;5;222;38;5;222m██████[48;5;233m▄[49;38;5;233m▄[39m                                          [00m
                  [38;5;95m▄[48;5;95;38;5;137m▄[48;5;222m▄[48;5;137m█[48;5;222;38;5;222m███████[48;5;233;38;5;137m▄[49;38;5;233m▄[39m                                         [00m
                [38;5;233m▄[38;5;126m▄[48;5;233;38;5;176m▄▄[48;5;137;38;5;233m▄[48;5;222;38;5;222m█████████[48;5;233;38;5;233m█[49;39m                                         [00m
              [38;5;233m▄[48;5;96;38;5;176m▄[48;5;176m███[38;5;126m▄[48;5;126m█[48;5;137;38;5;233m▄[48;5;222;38;5;222m████████[48;5;233;38;5;137m▄[49;38;5;233m▄[39m                                        [00m
             [38;5;233m▄[48;5;233;38;5;126m▄[48;5;126m█[48;5;176m▄▄[48;5;126m██[38;5;238m▄[48;5;233;38;5;137m▄[48;5;222;38;5;95m▄[38;5;222m████████[48;5;233;38;5;233m█[49;39m                                        [00m
            [38;5;95m▄[48;5;233;38;5;233m█[48;5;126;38;5;96m▄[38;5;126m█[38;5;96m▄[38;5;238m▄[48;5;96;38;5;222m▄[48;5;238m▄[48;5;222m██[48;5;233;38;5;233m█[48;5;222;38;5;222m████████[48;5;233;38;5;233m█[49;39m                       [38;5;96m▄▄▄▄[48;5;96;38;5;225m▄▄▄▄▄▄▄[49;38;5;96m▄▄▄[39m   [00m
    [38;5;95m▄▄▄▄[38;5;137m▄[48;5;95m▄▄▄[48;5;137;38;5;233m▄[48;5;95;38;5;222m▄[48;5;238m▄▄[48;5;222m██[38;5;238m▄[48;5;238;38;5;231m▄[38;5;238m█[48;5;126;38;5;222m▄[48;5;233;38;5;233m█[48;5;222;38;5;222m███████[48;5;137;38;5;233m▄[49m▀[39m                   [38;5;96m▄▄[48;5;96;38;5;225m▄▄[48;5;225m███[38;5;176m▄▄▄▄▄[38;5;225m██████[48;5;96m▄[49;38;5;96m▄[39m [00m
   [38;5;95m▀[48;5;230;38;5;233m▄[38;5;222m▄[48;5;222m█[38;5;137m▄[48;5;137m█[38;5;233m▄[48;5;95;38;5;222m▄[48;5;137m▄[48;5;222m█████[48;5;126m▄▄[48;5;238m▄[48;5;222;38;5;104m▄[38;5;95m▄[48;5;233m▄[48;5;230;38;5;230m██[48;5;222m▄[38;5;222m███[38;5;137m▄[48;5;233;38;5;233m█[49;39m                [38;5;96m▄▄[48;5;96;38;5;225m▄▄[48;5;225;38;5;176m▄▄▄[48;5;176m███████████[48;5;225m▄[38;5;225m████[48;5;96m▄[49;38;5;96m▄[39m[00m
     [38;5;233m▀▀[48;5;137m▄▄[38;5;137m█[48;5;233m▄▄[48;5;95m▄[48;5;137;38;5;233m▄[48;5;222m▄[38;5;95m▄[38;5;137m▄▄[48;5;104m▄[48;5;68m▄▄[48;5;233;38;5;233m█[48;5;230;38;5;230m████[48;5;222;38;5;222m███[48;5;233;38;5;233m█[49;39m               [38;5;96m▄[48;5;96;38;5;225m▄[48;5;126;38;5;176m▄[48;5;225m▄[48;5;176m████[38;5;126m▄▄▄▄▄▄[38;5;176m███████[48;5;225m▄[38;5;225m████[48;5;96;38;5;96m█[49;39m[00m
         [38;5;233m▀▀▀[38;5;95m▀[39m   [38;5;95m▀▀▀[48;5;137m▄[48;5;95;38;5;233m▄[48;5;137;38;5;230m▄[48;5;230m███[48;5;222;38;5;222m█[38;5;137m▄[38;5;233m▄[49m▀[39m              [38;5;96m▄[48;5;96;38;5;176m▄[48;5;225m▄[48;5;176m█[38;5;126m▄▄[48;5;126m████████████[48;5;176;38;5;176m██████[48;5;225;38;5;225m████[48;5;96;38;5;96m█[49;39m[00m
                 [38;5;233m▄[48;5;233;38;5;230m▄▄[48;5;230m██[38;5;95m▄[38;5;233m▄[48;5;222m▄[48;5;233;38;5;68m▄[38;5;60m▄[49;39m              [38;5;96m▄[48;5;96;38;5;176m▄[48;5;176m█[38;5;126m▄[48;5;126m██████[38;5;238m▄▄▄▄[38;5;126m███████[48;5;176;38;5;176m█████[48;5;225;38;5;225m███[48;5;96;38;5;96m█[49;39m [00m
                  [38;5;233m▀▀▀[48;5;233;38;5;95m▄[48;5;95;38;5;137m▄[48;5;137m█[48;5;68;38;5;68m██[48;5;60m▄[49;39m            [38;5;96m▄[48;5;96;38;5;176m▄[48;5;176;38;5;126m▄[48;5;126m████[38;5;238m▄▄[49m▀▀▀[39m    [38;5;238m▀[48;5;126m▄[38;5;126m█████[48;5;176;38;5;176m████[48;5;225;38;5;225m███[38;5;96m▄[49m▀[39m [00m
                     [48;5;95;38;5;95m█[48;5;137;38;5;222m▄[38;5;137m██[48;5;68;38;5;68m█[38;5;104m▄[48;5;60;38;5;60m█[49;39m          [48;5;96;38;5;96m█[48;5;176;38;5;126m▄[48;5;126m███[38;5;238m▄[49m▀▀[39m          [48;5;238;38;5;238m█[48;5;126;38;5;126m████[38;5;176m▄[48;5;176m███[38;5;225m▄[48;5;225m██[38;5;96m▄[49m▀[39m  [00m
                     [48;5;95;38;5;95m█[48;5;222;38;5;222m███[48;5;104;38;5;104m██[48;5;60m▄[49;38;5;60m▄[39m       [38;5;96m▄[48;5;96;38;5;176m▄[48;5;176;38;5;126m▄[48;5;126m█[38;5;238m▄[49m▀[39m             [38;5;238m▄[48;5;238;38;5;126m▄[48;5;126m████[48;5;176;38;5;176m███[38;5;225m▄[48;5;225m██[48;5;176;38;5;238m▄[49;38;5;96m▀[39m   [00m
                    [38;5;95m▄[48;5;95;38;5;222m▄[48;5;222m██[38;5;250m▄[48;5;104;38;5;104m███[48;5;60m▄[49;38;5;233m▄[39m     [38;5;96m▄[48;5;96;38;5;176m▄[48;5;126;38;5;126m█[38;5;238m▄[49m▀[39m              [38;5;238m▄[48;5;238;38;5;126m▄[48;5;126m████[48;5;176;38;5;176m███[38;5;225m▄[48;5;225m█[38;5;238m▄[49m▀[39m     [00m
                    [48;5;95;38;5;95m█[48;5;222;38;5;222m███[48;5;104;38;5;104m█████[48;5;60m▄[49;38;5;233m▄[39m   [48;5;96;38;5;96m█[38;5;126m▄[48;5;126;38;5;238m▄[49m▀[39m               [38;5;238m▄[48;5;238;38;5;126m▄[48;5;126m███[38;5;176m▄[48;5;176m██[38;5;225m▄[48;5;225m█[38;5;238m▄[49m▀[39m       [00m
                   [48;5;95;38;5;95m█[48;5;222;38;5;222m███[38;5;250m▄[48;5;104;38;5;104m██████[48;5;60m▄[49;38;5;233m▄[39m [38;5;96m▄[48;5;96m█[48;5;238;38;5;238m█[49;39m               [38;5;238m▄[48;5;238;38;5;126m▄[48;5;126m███[38;5;176m▄[48;5;176m██[38;5;225m▄[48;5;225;38;5;96m▄[38;5;238m▄[49m▀[39m         [00m
                  [38;5;95m▄[48;5;95;38;5;222m▄[48;5;222m███[48;5;104;38;5;104m█████████[48;5;233m▄[48;5;96;38;5;60m▄[48;5;238;38;5;233m▄[49m▄[39m             [38;5;233m▄[48;5;238;38;5;126m▄[48;5;126m██[38;5;176m▄▄[48;5;176m█[38;5;225m▄[38;5;238m▄[48;5;225;38;5;233m▄[49;38;5;238m▀[38;5;233m▀[39m           [00m
                 [38;5;233m▄[48;5;233;38;5;176m▄[38;5;96m▄[48;5;137;38;5;238m▄[48;5;238;38;5;176m▄[38;5;96m▄[48;5;104;38;5;238m▄▄[38;5;104m███████████[48;5;60m▄[48;5;233m▄[38;5;68m▄[49;38;5;60m▄[38;5;233m▄[39m    [38;5;233m▄[38;5;126m▄[48;5;233m▄[48;5;126m█[38;5;176m▄▄[48;5;176;38;5;225m▄[38;5;238m▄[38;5;233m▄[48;5;225m▄[49;38;5;238m▀[38;5;233m▀[39m               [00m
                [48;5;233;38;5;233m█[48;5;176;38;5;176m█[48;5;96m▄[48;5;176;38;5;238m▄[48;5;238m█[48;5;96;38;5;176m▄[48;5;176m█[48;5;96m▄[48;5;176m█[48;5;238m▄[48;5;104;38;5;233m▄[38;5;104m████████████[38;5;238m▄▄[48;5;60m▄[48;5;233m▄[38;5;126m▄[48;5;126m█[38;5;176m▄[38;5;233m▄[48;5;176m▄[38;5;60m▄[49;38;5;233m▀▀▀[39m                    [00m
               [48;5;233;38;5;233m█[48;5;126;38;5;96m▄[38;5;126m█[48;5;176m▄[48;5;238;38;5;238m█[48;5;137;38;5;137m█[48;5;238;38;5;238m█[48;5;176;38;5;176m████[48;5;126;38;5;126m█[48;5;233m▄[48;5;104;38;5;233m▄[38;5;104m███████████[48;5;238m▄[48;5;96;38;5;238m▄[38;5;96m█[48;5;233m▄▄[38;5;104m▄[48;5;104m██[48;5;68m▄[48;5;60;38;5;68m▄▄[49;38;5;60m▄▄▄[39m                  [00m
               [48;5;233;38;5;238m▄[48;5;96;38;5;96m██[38;5;233m▄[48;5;137;38;5;222m▄[48;5;222m█[48;5;137m▄[48;5;233;38;5;233m█[48;5;176;38;5;126m▄▄[48;5;126m███[48;5;238m▄[38;5;233m▄[48;5;104;38;5;104m█████████████[48;5;238m▄[48;5;96;38;5;96m█[48;5;238m▄▄[48;5;104;38;5;233m▄[48;5;68;38;5;68m██████[48;5;60m▄▄▄[49;38;5;60m▄▄▄[39m            [00m
            [38;5;238m▄[38;5;96m▄[48;5;238m▄[48;5;96m█[48;5;233;38;5;233m█[48;5;96;38;5;96m█[48;5;233;38;5;233m█[48;5;222;38;5;222m████[48;5;233;38;5;250m▄[48;5;126;38;5;233m▄[48;5;96;38;5;96m█[48;5;126m▄[48;5;96;38;5;126m▄[48;5;126;38;5;96m▄[38;5;126m█[48;5;238;38;5;238m█[48;5;104;38;5;104m██████████████[48;5;233m▄[48;5;96;38;5;233m▄[38;5;96m█[48;5;238m▄[48;5;233m▄[48;5;68;38;5;233m▄[48;5;137m▄[38;5;137m██[48;5;68m▄▄▄[48;5;104m▄[38;5;222m▄[38;5;250m▄[48;5;60;38;5;104m▄▄▄[49;38;5;60m▄▄[39m       [00m
         [38;5;238m▄[38;5;96m▄[48;5;238;38;5;126m▄[48;5;126m█[38;5;238m▄[48;5;238m█[49m▀[39m [38;5;233m▀[38;5;238m▀[48;5;233;38;5;233m█[48;5;222;38;5;137m▄[38;5;222m███[48;5;104;38;5;250m▄[48;5;233;38;5;104m▄[48;5;96;38;5;238m▄[48;5;126;38;5;233m▄[38;5;126m█[48;5;233m▄[48;5;96;38;5;233m▄[48;5;238;38;5;238m█[48;5;104;38;5;104m███████████[38;5;68m▄[38;5;104m█[48;5;68;38;5;68m██[48;5;233m▄[48;5;96;38;5;233m▄[38;5;96m██[48;5;238m▄[48;5;233m▄[48;5;95;38;5;233m▄[49;38;5;95m▀▀▀[48;5;137m▄▄[48;5;222m▄▄[38;5;233m▄▄▄[48;5;137m▄[49m▀[39m      [00m
       [38;5;238m▄[48;5;238;38;5;126m▄[48;5;126m██[38;5;238m▄[49m▀[39m      [38;5;238m▄[48;5;233m▄[48;5;137;38;5;233m▄[38;5;137m█[48;5;222m▄▄[48;5;104;38;5;222m▄[38;5;250m▄[38;5;104m█[48;5;233m▄[48;5;126;38;5;233m▄[38;5;126m█[48;5;233m▄[48;5;104;38;5;238m▄[38;5;233m▄[38;5;104m█████[38;5;68m▄▄▄[48;5;68;38;5;104m▄[48;5;104;38;5;68m▄[48;5;68;38;5;104m▄[48;5;104;38;5;68m▄[48;5;68m█[38;5;60m▄[49m▀[38;5;233m▀[48;5;96;38;5;238m▄[38;5;96m█[38;5;126m▄[38;5;96m█[48;5;233;38;5;126m▄[49;38;5;233m▄[39m                [00m
     [38;5;238m▄[48;5;238;38;5;126m▄[48;5;126m██[38;5;96m▄[49;38;5;238m▀[39m  [38;5;238m▄▄▄▄[48;5;238;38;5;96m▄▄[48;5;96m█[38;5;238m▄[49;38;5;96m▀[38;5;233m▀[48;5;137m▄▄[38;5;137m██[48;5;68m▄[38;5;68m██[48;5;233m▄[48;5;126;38;5;233m▄[38;5;238m▄[38;5;126m█[48;5;238m▄[48;5;233m▄[48;5;68;38;5;233m▄▄[38;5;68m███[48;5;104m▄[48;5;68m█[48;5;104m▄[48;5;68;38;5;60m▄▄[49m▀[39m    [48;5;238;38;5;238m█[48;5;96;38;5;126m▄[48;5;126;38;5;96m▄[48;5;96;38;5;126m▄[48;5;126m█[48;5;233m▄[49;38;5;233m▄[39m              [00m
    [38;5;233m▄[48;5;238;38;5;126m▄[48;5;126m██[38;5;96m▄[48;5;238m▄▄▄▄[48;5;96m███[38;5;238m▄[38;5;233m▄[49;38;5;238m▀[38;5;233m▀[39m     [38;5;233m▀[38;5;95m▀[48;5;137m▄▄▄[48;5;68m▄[38;5;137m▄[48;5;238;38;5;68m▄[48;5;233m▄[48;5;126;38;5;233m▄[38;5;96m▄[38;5;126m██[48;5;238m▄[48;5;233m▄▄[48;5;68;38;5;238m▄[48;5;60;38;5;233m▄▄[49m▄[39m      [38;5;238m▄[48;5;238m█[48;5;126;38;5;126m████[48;5;233m▄[49;38;5;233m▄[39m             [00m
    [48;5;233;38;5;233m█[48;5;126;38;5;238m▄[48;5;96;38;5;96m███[38;5;238m▄[38;5;233m▄▄▄[49;38;5;238m▀[38;5;233m▀▀[39m               [38;5;60m▀▀▀▀[38;5;233m▀▀[48;5;126m▄[38;5;238m▄[38;5;126m██████[48;5;238m▄▄▄▄▄▄▄[48;5;126m██████[48;5;233m▄[49;38;5;233m▄[39m            [00m
     [38;5;233m▀▀▀▀[39m                              [38;5;233m▀▀[38;5;238m▀[48;5;126;38;5;233m▄▄▄[38;5;238m▄[38;5;126m█████████████[48;5;233;38;5;233m█[49;39m            [00m
                                              [38;5;233m▀▀▀[38;5;238m▀[48;5;126;38;5;233m▄▄▄▄▄▄▄▄[49m▀[39m             [00m
                                                                        [00m
