$$$
NAME: Combusken
OTHER NAMES: 力壯雞, Wakasyamo, Galifeu, 영치코, ワカシャモ, Jungglut
APPEARANCE: Pokémon Generation III
KIND: Young Fowl Pokémon
GROUP: ground
BALLOON: top
COAT: red
DISPLAY: full
BALLOON TOP: 3
BALLOON BOTTOM: 0
POSE: master
BASED ON: Combusken
SOURCE: PokéAPI Sprites
MEDIA: Pokémon
LICENSE: Copyright The Pokémon Company
FREE: no
WIDTH: 75
HEIGHT: 35


$$$
$balloon5$
                            $\$                                               [00m
                             $\$                                              [00m
                              $\$                                             [00m
                               $\$                     [00m
                              [38;5;166m▄▄[48;5;166;38;5;209m▄▄[48;5;234;38;5;234m█[49;39m                  [00m
                            [38;5;166m▄[48;5;166;38;5;209m▄[48;5;209m███[38;5;166m▄[48;5;234;38;5;234m█[49;39m                  [00m
                     [48;5;166;38;5;166m█[48;5;88;38;5;209m▄[49;38;5;88m▄▄[39m  [38;5;166m▄[48;5;166;38;5;209m▄[48;5;209m████[48;5;234;38;5;234m█[49;39m                   [00m
                     [48;5;88;38;5;88m█[48;5;209;38;5;166m▄[38;5;209m██[48;5;88m▄[49;38;5;88m▄[48;5;166;38;5;209m▄[48;5;209m███[38;5;166m▄[48;5;166;38;5;234m▄[49m▀[38;5;88m▄▄▄[48;5;88;38;5;166m▄▄[38;5;234m▄[49;39m             [00m
                     [38;5;88m▀[48;5;166;38;5;234m▄[48;5;209;38;5;166m▄[38;5;209m██[48;5;88m▄[48;5;209;38;5;166m▄[38;5;137m▄[48;5;137;38;5;228m▄[48;5;166;38;5;88m▄[38;5;166m█[48;5;234;38;5;88m▄[48;5;88;38;5;166m▄[48;5;166m████[38;5;234m▄[49m▀[39m             [00m
                      [38;5;234m▀[48;5;166m▄[38;5;166m█[48;5;137;38;5;178m▄[48;5;166;38;5;137m▄[48;5;137;38;5;228m▄[48;5;178m▄[48;5;228m█[48;5;234m▄[48;5;88m▄[48;5;166;38;5;88m▄[38;5;166m███[38;5;88m▄[48;5;88;38;5;234m▄[49m▀[39m              [00m
                     [38;5;137m▄[38;5;178m▄[48;5;137;38;5;228m▄▄[48;5;228m████[38;5;178m▄[48;5;178;38;5;137m▄[48;5;88;38;5;234m▄[48;5;166;38;5;88m▄▄▄[49;38;5;234m▀▀[39m                [00m
                    [48;5;137;38;5;178m▄[48;5;228;38;5;228m█████████[48;5;178m▄[38;5;178m█[48;5;234;38;5;234m█[49;39m                    [00m
                   [48;5;178;38;5;137m▄[48;5;228;38;5;228m███████[38;5;137m▄▄[38;5;228m██[48;5;178;38;5;178m██[48;5;234;38;5;234m█[49;39m                   [00m
                   [38;5;137m▀[48;5;228m▄[38;5;228m████[38;5;137m▄[48;5;137;38;5;231m▄[48;5;167;38;5;167m█[48;5;88;38;5;88m█[48;5;228;38;5;228m█[38;5;178m▄[48;5;178m██[48;5;234;38;5;234m█[49;39m                   [00m
                  [38;5;234m▄[48;5;166;38;5;209m▄▄▄▄[48;5;228;38;5;166m▄[38;5;88m▄[48;5;178;38;5;178m█[48;5;234;38;5;166m▄[48;5;167;38;5;88m▄[48;5;88;38;5;178m▄[48;5;178m████[48;5;234;38;5;137m▄[49;39m                   [00m
                   [38;5;234m▀▀[48;5;234;38;5;137m▄[38;5;178m▄▄[48;5;178m████████[48;5;137;38;5;234m▄[49;39m                    [00m
                      [48;5;137;38;5;234m▄[48;5;178;38;5;178m█████████[48;5;234;38;5;234m█[49;39m                    [00m
                     [38;5;234m▄[48;5;234;38;5;178m▄[48;5;178m█████████[48;5;234m▄[49;38;5;234m▄[39m                   [00m
                  [38;5;137m▄[38;5;234m▄[48;5;137;38;5;228m▄[48;5;178;38;5;178m█[48;5;137;38;5;137m█[48;5;178;38;5;178m███████████[48;5;234;38;5;228m▄[49;38;5;234m▄[39m                 [00m
               [38;5;137m▄[38;5;234m▄[48;5;137;38;5;228m▄[48;5;228m█[38;5;178m▄[48;5;178m█[48;5;137;38;5;234m▄[48;5;178;38;5;166m▄[38;5;178m███[38;5;166m▄[38;5;178m███[38;5;137m▄[38;5;234m▄[38;5;178m█[38;5;228m▄[48;5;228m██[48;5;137m▄[48;5;234;38;5;178m▄[49;38;5;234m▄[39m              [00m
             [38;5;137m▄[48;5;234;38;5;228m▄[48;5;178m▄[48;5;228m█[38;5;178m▄[48;5;178m██[48;5;137;38;5;234m▄[48;5;178;38;5;178m█[48;5;166;38;5;137m▄[48;5;137;38;5;166m▄[48;5;178;38;5;137m▄[48;5;137;38;5;166m▄[48;5;166;38;5;137m▄[48;5;178;38;5;228m▄[38;5;178m████[48;5;234;38;5;234m█[48;5;178m▄[48;5;228;38;5;228m█████[48;5;137m▄▄[49;38;5;137m▄[39m           [00m
          [38;5;234m▄[48;5;137;38;5;178m▄[48;5;234;38;5;228m▄[48;5;228m███[48;5;178;38;5;178m██[38;5;234m▄▄[48;5;234m█[48;5;178;38;5;228m▄[38;5;178m█[48;5;166;38;5;166m██[38;5;137m▄[48;5;178;38;5;178m█[48;5;228;38;5;228m█[48;5;178m▄[38;5;178m███[48;5;234;38;5;234m█[49;39m [48;5;234;38;5;234m█[48;5;228;38;5;228m███████[48;5;137m▄[49;38;5;137m▄▄[39m        [00m
        [38;5;234m▄[48;5;137;38;5;228m▄[48;5;228m████[38;5;178m▄[48;5;178m██[48;5;234m▄[48;5;178;38;5;234m▄[48;5;137;38;5;228m▄[48;5;228m█[48;5;137;38;5;209m▄[48;5;228;38;5;137m▄[48;5;209;38;5;209m██[48;5;178;38;5;137m▄[48;5;228m▄[38;5;228m██[38;5;178m▄[48;5;178m███[48;5;234m▄[38;5;137m▄▄[48;5;228;38;5;234m▄[38;5;228m█████[38;5;178m▄[48;5;178;38;5;228m▄[38;5;178m█[48;5;137m▄[49;38;5;137m▄[38;5;234m▄[39m     [00m
     [38;5;234m▄▄[48;5;234;38;5;228m▄[48;5;228m█████[48;5;178;38;5;178m███[38;5;137m▄[48;5;234;38;5;234m█[48;5;228;38;5;228m█[38;5;137m▄[38;5;228m█[48;5;209;38;5;178m▄[38;5;209m████[38;5;228m▄[48;5;228m███[48;5;178;38;5;178m██[38;5;137m▄[38;5;178m██[48;5;137;38;5;234m▄[48;5;234;38;5;88m▄[48;5;178;38;5;234m▄[48;5;228;38;5;228m███[48;5;178m▄[48;5;228;38;5;178m▄[48;5;178;38;5;228m▄[48;5;228;38;5;178m▄[48;5;178m███[48;5;234m▄[49;38;5;234m▄[39m   [00m
    [38;5;234m▀[48;5;228;38;5;137m▄[38;5;228m████[38;5;178m▄▄[48;5;178m████[48;5;88;38;5;88m█[48;5;166;38;5;209m▄[48;5;234m▄[48;5;209m█[48;5;178;38;5;137m▄[48;5;228;38;5;228m█[48;5;209;38;5;209m███[48;5;228;38;5;178m▄[48;5;178;38;5;137m▄[48;5;234;38;5;209m▄[48;5;228;38;5;234m▄[38;5;178m▄[48;5;178;38;5;234m▄[48;5;234;38;5;209m▄[48;5;166m▄[48;5;137;38;5;166m▄[48;5;178;38;5;234m▄[48;5;234;38;5;166m▄[48;5;88m▄[48;5;234;38;5;234m█[48;5;228;38;5;178m▄▄[48;5;178m█████████[48;5;137m▄▄[48;5;234;38;5;234m█[49;39m [00m
     [48;5;234;38;5;242m▄[48;5;137;38;5;253m▄[48;5;228;38;5;245m▄[48;5;137;38;5;234m▄[48;5;178;38;5;178m██[38;5;137m▄▄[38;5;178m██[48;5;234;38;5;234m█[48;5;166;38;5;166m█[48;5;209;38;5;209m████[48;5;137m▄[48;5;166;38;5;88m▄[48;5;209;38;5;166m▄▄[48;5;137;38;5;88m▄[48;5;209;38;5;209m███[48;5;234m▄[48;5;209m█████[48;5;166;38;5;166m██[48;5;88m▄[48;5;234;38;5;234m█[48;5;178;38;5;178m██████████[48;5;137m▄[48;5;234;38;5;234m█[49;39m  [00m
    [48;5;242;38;5;242m█[48;5;253;38;5;253m█[38;5;245m▄[48;5;234;38;5;234m█[48;5;245;38;5;245m█[48;5;178;38;5;242m▄[48;5;234;38;5;234m█[48;5;245;38;5;245m█[48;5;234;38;5;239m▄[48;5;178;38;5;234m▄▄[49m▀[48;5;166m▄[38;5;166m█[48;5;209m▄[38;5;209m██[38;5;166m▄[48;5;88m▄[48;5;166;38;5;88m▄[38;5;166m█[48;5;88;38;5;234m▄[48;5;209;38;5;166m▄[38;5;209m███████[48;5;166;38;5;166m███[38;5;88m▄[48;5;234;38;5;234m██[48;5;178;38;5;178m██[38;5;234m▄[38;5;239m▄[38;5;178m██[38;5;234m▄[48;5;137;38;5;239m▄[48;5;178;38;5;242m▄[38;5;234m▄▄[48;5;234m█[49;39m [00m
   [48;5;242;38;5;242m█[48;5;245;38;5;245m█[38;5;234m▄[49m▀[48;5;239;38;5;239m█[48;5;245;38;5;245m█[48;5;234;38;5;234m█[48;5;245;38;5;239m▄[49m▀[39m    [38;5;234m▀[48;5;166m▄[38;5;166m██████[48;5;88m▄[48;5;234;38;5;88m▄[48;5;88;38;5;234m▄[48;5;166;38;5;166m█[48;5;209m▄▄▄▄[48;5;166m█████[48;5;234;38;5;234m█[49;39m [38;5;234m▀[48;5;178m▄[49m▀[48;5;242;38;5;242m█[48;5;253;38;5;253m█[48;5;234m▄[48;5;178;38;5;234m▄[48;5;234;38;5;239m▄[48;5;245;38;5;245m█[38;5;253m▄[48;5;239;38;5;245m▄[48;5;253;38;5;253m█[48;5;239m▄[48;5;234;38;5;234m█[49;39m[00m
   [48;5;242;38;5;234m▄[48;5;245m▄[49m▀[39m [38;5;234m▀▀[39m         [38;5;234m▀[48;5;88m▄[48;5;166m▄[38;5;243m▄[38;5;239m▄[48;5;88;38;5;234m▄[49m▀▀[39m [38;5;234m▀[48;5;166m▄▄[38;5;166m████[38;5;234m▄▄[49m▀[39m      [38;5;242m▀[48;5;253m▄[38;5;253m█[48;5;234;38;5;234m█[48;5;239;38;5;239m█[48;5;253;38;5;245m▄[38;5;253m█[48;5;234;38;5;234m█[48;5;253;38;5;242m▄[48;5;234;38;5;234m█[49;39m[00m
             [38;5;234m▄[38;5;239m▄[39m     [48;5;234;38;5;234m█[48;5;239;38;5;243m▄▄[48;5;234;38;5;234m█[49;39m      [38;5;234m▀[48;5;88;38;5;239m▄▄[48;5;234;38;5;234m█[49;39m          [38;5;242m▀[48;5;253m▄[48;5;234;38;5;234m█[49;39m [48;5;239;38;5;239m█[48;5;253;38;5;253m█[48;5;234;38;5;234m█[49m▀[39m [00m
           [38;5;234m▄[48;5;234;38;5;253m▄[48;5;253m█[48;5;245m▄[48;5;239;38;5;239m█[48;5;234m▄[38;5;243m▄▄▄[48;5;243m█[48;5;239m▄▄[48;5;243m█[48;5;234m▄[38;5;239m▄[38;5;253m▄[49;38;5;234m▄[39m  [38;5;234m▄[48;5;234;38;5;243m▄[48;5;243;38;5;239m▄[48;5;239;38;5;243m▄[48;5;234m▄[49;38;5;234m▄[39m             [38;5;234m▀[39m   [00m
           [38;5;234m▀[48;5;253m▄[48;5;234;38;5;239m▄[48;5;239;38;5;253m▄[48;5;253m█[48;5;245;38;5;239m▄[48;5;243m▄[48;5;239;38;5;253m▄[38;5;243m▄▄[48;5;243m██[38;5;234m▄[48;5;239m▄[49m▀[48;5;253m▄[49m▀[39m  [48;5;234;38;5;234m█[48;5;243;38;5;243m█[38;5;239m▄[38;5;243m█[38;5;239m▄[48;5;234;38;5;243m▄[49;38;5;234m▄[39m                [00m
             [48;5;234;38;5;234m█[48;5;253;38;5;245m▄[48;5;245;38;5;234m▄[48;5;239m▄[48;5;253;38;5;253m█[38;5;234m▄[48;5;245m▄[49m▀▀▀[39m      [38;5;234m▄[48;5;234;38;5;239m▄[48;5;239m█[48;5;243;38;5;243m██[48;5;239m▄[48;5;243;38;5;239m▄[38;5;243m█[48;5;234;38;5;245m▄[38;5;253m▄[49;38;5;234m▄[39m             [00m
              [38;5;234m▀[39m [38;5;234m▀[48;5;245m▄[49m▀[39m          [48;5;234;38;5;234m█[48;5;253;38;5;253m█[48;5;245m▄[48;5;243;38;5;234m▄[48;5;239m▄[48;5;243;38;5;253m▄[48;5;245m▄[48;5;239;38;5;239m█[48;5;242;38;5;234m▄[48;5;253;38;5;242m▄[38;5;253m█[48;5;234;38;5;234m█[49;39m            [00m
                             [48;5;234;38;5;234m█[48;5;253;38;5;245m▄[48;5;234;38;5;234m█[49;39m  [48;5;234;38;5;234m█[48;5;253;38;5;245m▄[48;5;242;38;5;234m▄[49;39m [38;5;234m▀[48;5;245m▄[49m▀[39m            [00m
                              [38;5;234m▀[39m    [38;5;234m▀[39m                 [00m
                                                     [00m
