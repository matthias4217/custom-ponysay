$$$
NAME: Chespin
OTHER NAMES: 哈力栗, Marisson, 도치마론, Igamaro, ハリマロン
APPEARANCE: Pokémon Generation VI
KIND: Spiny Nut Pokémon
GROUP: ground
BALLOON: top
COAT: green
DISPLAY: full
BALLOON TOP: 3
BALLOON BOTTOM: 0
POSE: master
BASED ON: Chespin
SOURCE: PokéAPI Sprites
MEDIA: Pokémon
LICENSE: Copyright The Pokémon Company
FREE: no
WIDTH: 68
HEIGHT: 25


$$$
$balloon5$
               $\$                                                    [00m
                $\$                                                   [00m
                 $\$                                                  [00m
                  $\$                  [00m
                  [38;5;23m▄[48;5;23;38;5;149m▄[48;5;236;38;5;236m█[49;39m                [00m
   [38;5;23m▄▄[39m      [38;5;23m▄▄▄[39m   [48;5;23;38;5;23m█[48;5;149;38;5;149m██[48;5;236;38;5;236m█[49;39m  [38;5;23m▄▄▄[38;5;236m▄[39m          [00m
   [48;5;23;38;5;23m█[48;5;149;38;5;149m█[48;5;23m▄[49;38;5;23m▄[39m    [38;5;23m▀[48;5;149m▄[38;5;149m█[48;5;23m▄[49;38;5;23m▄[48;5;23m█[48;5;149;38;5;149m███[48;5;236;38;5;23m▄[48;5;23;38;5;149m▄▄[48;5;149m██[38;5;236m▄[49m▀[39m       [38;5;23m▄▄▄[39m[00m
   [48;5;23;38;5;23m█[48;5;149;38;5;149m███[48;5;23m▄[49;38;5;23m▄[39m   [38;5;23m▀[48;5;149m▄[38;5;149m██[48;5;23m▄[48;5;149;38;5;71m▄[38;5;149m█[48;5;71;38;5;71m█[48;5;149;38;5;149m███[38;5;236m▄[49m▀[39m     [38;5;23m▄▄[48;5;23;38;5;149m▄▄[48;5;149m█[38;5;23m▄[49m▀[39m[00m
    [48;5;23;38;5;23m█[48;5;149;38;5;149m████[48;5;23;38;5;23m█[49;39m   [38;5;236m▄[48;5;236m█[48;5;149;38;5;23m▄[38;5;71m▄[38;5;149m███[38;5;71m▄▄[48;5;236;38;5;23m▄[49;38;5;236m▄[39m    [38;5;23m▄[48;5;23;38;5;149m▄[48;5;149m████[38;5;236m▄[49;38;5;23m▀[39m [00m
     [48;5;23;38;5;23m█[48;5;149;38;5;149m██[38;5;71m▄[48;5;71m█[48;5;236;38;5;236m█[49m▄[48;5;236;38;5;149m▄[48;5;71;38;5;193m▄[48;5;149m▄▄[38;5;149m███████[48;5;71m▄[48;5;236;38;5;71m▄[49;38;5;23m▄▄[48;5;23;38;5;149m▄[48;5;149m████[38;5;236m▄[49m▀[39m   [00m
      [38;5;23m▀[48;5;71m▄[38;5;71m██[48;5;23;38;5;149m▄[48;5;149m█[48;5;193;38;5;193m███[38;5;149m▄[48;5;149m█████████[48;5;71m▄[48;5;149m██[38;5;71m▄[38;5;236m▄▄[49m▀[39m     [00m
        [48;5;236;38;5;236m█[48;5;23;38;5;149m▄[48;5;149m█[38;5;71m▄[38;5;23m▄[48;5;71;38;5;137m▄[48;5;23m▄▄▄▄▄[48;5;71m▄[38;5;173m▄[48;5;149;38;5;23m▄[38;5;71m▄[38;5;149m███[48;5;71;38;5;71m█[48;5;236;38;5;236m█[49m▀[39m        [00m
       [48;5;236;38;5;236m█[48;5;71;38;5;71m█[38;5;23m▄[48;5;23;38;5;216m▄[48;5;216m█[48;5;173m▄[48;5;137;38;5;137m████[38;5;173m▄[38;5;216m▄[48;5;216m████[48;5;23;38;5;173m▄[48;5;149;38;5;23m▄[38;5;149m██[48;5;236;38;5;71m▄[49;38;5;236m▄[39m        [00m
       [48;5;236;38;5;236m█[48;5;71;38;5;71m█[48;5;23;38;5;216m▄[48;5;173;38;5;236m▄[48;5;236;38;5;231m▄[48;5;216;38;5;173m▄[38;5;216m█[48;5;137m▄[48;5;173m▄[48;5;216m██[38;5;173m▄[48;5;236;38;5;231m▄[48;5;137;38;5;236m▄[48;5;216;38;5;216m███[48;5;23;38;5;173m▄[48;5;71;38;5;23m▄[38;5;71m██[48;5;236;38;5;236m█[49;39m        [00m
       [48;5;236;38;5;236m█[48;5;71;38;5;71m█[48;5;216;38;5;216m█[48;5;236;38;5;95m▄[48;5;231;38;5;236m▄[48;5;173;38;5;173m█[48;5;216;38;5;167m▄[38;5;203m▄▄[38;5;167m▄[38;5;216m█[48;5;173;38;5;173m█[48;5;231;38;5;236m▄[48;5;236;38;5;95m▄[48;5;216;38;5;216m██[38;5;173m▄[48;5;173;38;5;137m▄[48;5;236;38;5;236m█[48;5;71;38;5;71m██[48;5;236;38;5;236m█[49;39m        [00m
        [48;5;236;38;5;236m█[48;5;173;38;5;23m▄[48;5;216;38;5;173m▄[48;5;95;38;5;216m▄[48;5;216m█[48;5;95m▄[48;5;167;38;5;95m▄▄[48;5;95;38;5;216m▄[48;5;216m██[48;5;95m▄[48;5;216m██[38;5;137m▄[48;5;137m██[48;5;236;38;5;23m▄[48;5;71;38;5;71m█[38;5;236m▄[49m▀[39m        [00m
       [38;5;23m▄[48;5;236;38;5;149m▄[48;5;23;38;5;71m▄[48;5;137;38;5;236m▄[48;5;173;38;5;173m█[48;5;137m▄[48;5;216m▄[38;5;137m▄▄▄▄[48;5;137;38;5;173m▄▄[48;5;216m▄[48;5;173m█[48;5;137m▄[38;5;236m▄[48;5;23;38;5;23m█[48;5;71;38;5;71m█[38;5;236m▄[49m▀[39m         [00m
      [48;5;23;38;5;23m█[48;5;149;38;5;149m██[38;5;236m▄[49m▀▀[48;5;137m▄[48;5;173m▄[38;5;95m▄[38;5;137m▄[38;5;173m██[38;5;137m▄[38;5;95m▄[38;5;236m▄[48;5;137m▄[49m▀▀[48;5;71m▄[48;5;149;38;5;149m██[48;5;236m▄[49;38;5;236m▄[39m        [00m
      [38;5;236m▀▀▀[39m  [38;5;236m▄[48;5;236;38;5;95m▄[48;5;95m█[38;5;137m▄[48;5;173;38;5;173m████[48;5;95;38;5;137m▄[38;5;95m██[48;5;236m▄▄[49;38;5;236m▄▀[48;5;149m▄▄[48;5;236m█[49;39m        [00m
         [38;5;95m▄[48;5;236;38;5;137m▄[48;5;95m▄[38;5;95m██[48;5;216;38;5;216m██████[48;5;95;38;5;137m▄[38;5;95m█[38;5;137m▄[48;5;137m██[48;5;236m▄[49;38;5;236m▄[39m          [00m
        [48;5;95;38;5;95m█[48;5;137;38;5;137m███[48;5;95;38;5;236m▄[48;5;173;38;5;216m▄[48;5;216m███████[48;5;236;38;5;173m▄[48;5;137;38;5;95m▄[38;5;137m███[48;5;236m▄[49;38;5;236m▄[39m         [00m
        [38;5;236m▀[48;5;137m▄▄[49m▀[48;5;236m█[48;5;216;38;5;173m▄[38;5;216m██████[38;5;173m▄[48;5;173m█[48;5;236;38;5;71m▄[48;5;137;38;5;236m▄[38;5;137m█[48;5;95;38;5;95m█[48;5;137;38;5;137m█[48;5;236;38;5;236m█[49;38;5;95m▄[38;5;236m▄▄[39m      [00m
            [38;5;236m▀[48;5;173m▄[38;5;173m█[38;5;137m▄[38;5;236m▄▄▄[38;5;137m▄[38;5;173m██[48;5;71;38;5;236m▄[48;5;23;38;5;71m▄[48;5;236m▄▄[38;5;167m▄[48;5;167;38;5;203m▄[48;5;203;38;5;95m▄[38;5;236m▄[49m▀[39m      [00m
           [38;5;236m▄[48;5;236;38;5;231m▄[48;5;173;38;5;173m█[48;5;137;38;5;231m▄[48;5;173;38;5;137m▄[48;5;236;38;5;236m█[49;39m  [48;5;236;38;5;236m█[48;5;173;38;5;137m▄[48;5;137;38;5;231m▄[38;5;173m▄[48;5;236;38;5;231m▄[48;5;23;38;5;236m▄[49;38;5;23m▀[38;5;95m▀[38;5;236m▀[39m         [00m
          [48;5;236;38;5;236m█[48;5;231;38;5;137m▄[48;5;137;38;5;231m▄[48;5;231m█[38;5;236m▄▄[49m▀[39m  [38;5;236m▀[48;5;231m▄[38;5;231m█[48;5;173m▄[48;5;231;38;5;137m▄[38;5;231m█[48;5;236;38;5;236m█[49;39m           [00m
           [38;5;236m▀▀▀[39m      [38;5;236m▀[48;5;231m▄▄[48;5;137m▄[48;5;231m▄[49m▀[39m           [00m
                                     [00m
