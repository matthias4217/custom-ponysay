$$$
NAME: Rotom
OTHER NAMES: ロトム, 로토무, Motisma, 洛托姆
APPEARANCE: Pokémon Generation IV
KIND: Plasma Pokémon
GROUP: indeterminate
BALLOON: top
COAT: red
DISPLAY: full
BALLOON TOP: 3
BALLOON BOTTOM: 0
POSE: master
BASED ON: Rotom
SOURCE: PokéAPI Sprites
MEDIA: Pokémon
LICENSE: Copyright The Pokémon Company
FREE: no
WIDTH: 75
HEIGHT: 29


$$$
$balloon5$
                              $\$                                                   [00m
                               $\$                                                  [00m
                                $\$                                                 [00m
                                 $\$                              [00m
                                  $\$[38;5;73m▄[39m                            [00m
                                  [38;5;73m▄[48;5;73;38;5;80m▄[49;38;5;73m▄[39m                           [00m
                                 [38;5;73m▄[48;5;73;38;5;80m▄[48;5;80m█[48;5;73m▄[49;38;5;73m▄[39m                          [00m
                                 [48;5;73;38;5;73m█[48;5;80;38;5;124m▄[38;5;80m██[48;5;73;38;5;73m█[49;39m                          [00m
                                [48;5;73;38;5;73m█[48;5;80;38;5;80m█[48;5;124;38;5;124m█[48;5;95;38;5;95m█[48;5;80;38;5;80m█[48;5;73;38;5;73m█[49;39m                          [00m
                               [38;5;73m▄[48;5;73;38;5;80m▄[48;5;80;38;5;124m▄[48;5;124;38;5;209m▄[48;5;95;38;5;95m█[48;5;80;38;5;80m█[48;5;73;38;5;73m█[49;39m                          [00m
                               [48;5;73;38;5;73m█[48;5;80;38;5;80m█[48;5;124;38;5;124m█[48;5;209;38;5;209m█[48;5;95;38;5;124m▄[48;5;80;38;5;80m█[48;5;73;38;5;73m█[49;39m                          [00m
                              [48;5;73;38;5;73m█[48;5;80;38;5;80m█[38;5;124m▄[48;5;124;38;5;209m▄[48;5;209m█[48;5;124m▄[48;5;80;38;5;80m█[48;5;73m▄[49;38;5;73m▄[39m                         [00m
                           [38;5;73m▄▄[48;5;73;38;5;80m▄▄[48;5;80m█[48;5;95;38;5;95m█[48;5;209;38;5;209m███[48;5;124;38;5;124m█[48;5;80;38;5;80m█[48;5;73m▄[49;38;5;73m▄[39m                        [00m
                         [38;5;73m▄[48;5;73;38;5;80m▄[48;5;80m█[38;5;124m▄▄[48;5;124;38;5;209m▄▄[48;5;209m████[48;5;124m▄[48;5;95m▄[48;5;80;38;5;95m▄[38;5;80m█[48;5;73m▄[49;38;5;73m▄[39m                      [00m
                        [38;5;73m▄[48;5;80;38;5;80m█[38;5;124m▄[48;5;124;38;5;216m▄[48;5;216;38;5;231m▄[48;5;231m█[48;5;216;38;5;216m█[48;5;209;38;5;209m███[38;5;124m▄[38;5;250m▄[38;5;231m▄[38;5;124m▄[38;5;209m█[48;5;233m▄[48;5;80;38;5;233m▄[38;5;80m█[48;5;73;38;5;73m█[49;39m                     [00m
                       [38;5;73m▄[48;5;80;38;5;80m█[38;5;25m▄[48;5;250;38;5;69m▄[48;5;124;38;5;231m▄[48;5;216;38;5;209m▄▄[48;5;209m███[38;5;124m▄[48;5;250;38;5;231m▄[48;5;231;38;5;25m▄[48;5;69m▄[48;5;250;38;5;231m▄[48;5;209;38;5;209m██[48;5;95m▄[48;5;80;38;5;95m▄[38;5;80m█[48;5;73;38;5;73m█[49;39m                    [00m
                       [48;5;73;38;5;73m█[48;5;80;38;5;80m█[48;5;25;38;5;69m▄[38;5;25m█[48;5;233;38;5;231m▄[48;5;124;38;5;209m▄[48;5;209m████[48;5;233;38;5;231m▄[48;5;25;38;5;25m███[48;5;231;38;5;233m▄[48;5;209;38;5;124m▄[38;5;209m█[38;5;124m▄[48;5;233;38;5;233m█[48;5;80;38;5;80m█[48;5;73;38;5;73m█[49;39m                    [00m
                       [48;5;73;38;5;73m█[48;5;80;38;5;80m█[48;5;231;38;5;95m▄[48;5;69;38;5;231m▄[48;5;250;38;5;209m▄[48;5;209m█████[48;5;231m▄[48;5;25;38;5;231m▄[38;5;250m▄[48;5;250;38;5;95m▄[48;5;95;38;5;124m▄[48;5;124m███[48;5;233;38;5;233m█[48;5;80;38;5;80m█[48;5;73;38;5;73m█[49;39m                    [00m
                      [38;5;73m▄▄[48;5;73;38;5;80m▄[48;5;80m█[48;5;95;38;5;233m▄[48;5;124;38;5;124m█[48;5;95m▄[48;5;209;38;5;95m▄[38;5;233m▄▄[48;5;233;38;5;124m▄[48;5;124m█[48;5;95m▄▄[48;5;124m████[48;5;95;38;5;233m▄[48;5;80;38;5;80m█[48;5;73;38;5;73m█[49;39m                     [00m
           [38;5;73m▄▄▄▄[38;5;80m▄[48;5;73m▄▄▄[38;5;159m▄▄[38;5;80m▄[48;5;80m█[38;5;73m▄▄▄▄[48;5;233;38;5;80m▄[48;5;124;38;5;233m▄[38;5;95m▄[38;5;124m███████[38;5;95m▄[38;5;233m▄[48;5;233;38;5;80m▄[48;5;80;38;5;73m▄▄▄[48;5;73;38;5;80m▄▄[49;38;5;73m▄▄▄[39m                [00m
          [38;5;73m▀[48;5;73m█[48;5;80;38;5;80m██[48;5;159m▄[48;5;231;38;5;159m▄[38;5;231m█[38;5;159m▄[38;5;80m▄[48;5;159;38;5;73m▄[48;5;80m▄[49m▀▀[39m    [38;5;73m▀▀[48;5;80m▄[48;5;95;38;5;80m▄[48;5;233m▄[48;5;124;38;5;95m▄[38;5;124m██[38;5;95m▄[48;5;233;38;5;80m▄[48;5;80m█[38;5;73m▄[49m▀[39m   [38;5;73m▀[48;5;80m▄▄[38;5;80m█[38;5;159m▄[48;5;73;38;5;231m▄[38;5;80m▄▄[49;38;5;73m▄▄▄[39m          [00m
            [38;5;73m▀▀[48;5;80m▄▄[38;5;80m█[48;5;73m▄[38;5;73m█[49;39m         [48;5;73;38;5;73m█[48;5;80;38;5;80m██[48;5;95;38;5;95m█[48;5;124;38;5;124m████[48;5;95;38;5;233m▄[48;5;80;38;5;80m██[48;5;73;38;5;73m█[49;39m      [38;5;73m▀[48;5;80m▄▄[48;5;231;38;5;80m▄[38;5;159m▄[48;5;159;38;5;231m▄[48;5;80m▄▄[48;5;73;38;5;159m▄[38;5;80m▄[49;38;5;73m▄▄[39m      [00m
               [38;5;73m▄[48;5;73;38;5;80m▄[48;5;80m█[38;5;159m▄[48;5;73;38;5;80m▄▄[49;38;5;73m▄[39m       [38;5;73m▀[48;5;80m▄▄[48;5;95;38;5;80m▄[48;5;124m▄▄[48;5;95m▄[48;5;80;38;5;73m▄▄[49m▀[39m          [38;5;73m▀[48;5;80m▄[38;5;80m█[48;5;231m▄[48;5;159m▄[48;5;80m█[38;5;73m▄▄[49m▀▀[39m     [00m
           [38;5;73m▄▄[48;5;73;38;5;80m▄▄[48;5;80;38;5;159m▄[38;5;231m▄[48;5;231m███[48;5;80m▄[38;5;159m▄[48;5;73;38;5;80m▄▄[49;38;5;73m▄[39m      [48;5;73;38;5;73m█[48;5;80;38;5;80m███[38;5;73m▄[49m▀[39m         [38;5;73m▄▄[48;5;73;38;5;80m▄▄[48;5;80;38;5;159m▄[38;5;231m▄[38;5;80m█[38;5;73m▄[49m▀[39m         [00m
        [38;5;73m▄▄[48;5;73;38;5;80m▄[48;5;80;38;5;159m▄[38;5;80m█[48;5;159m▄[48;5;231;38;5;231m██[38;5;159m▄[38;5;80m▄▄▄[48;5;159m▄[48;5;80m█[38;5;73m▄▄▄[49m▀[39m      [48;5;73;38;5;73m█[48;5;80;38;5;80m█[38;5;73m▄[49m▀[39m       [38;5;73m▄[48;5;73;38;5;80m▄▄[48;5;80m█[38;5;231m▄[48;5;159m▄[48;5;231m███[48;5;80m▄[38;5;80m█[48;5;73m▄[49;38;5;73m▄[39m        [00m
     [38;5;73m▄▄[48;5;73;38;5;80m▄[48;5;80;38;5;159m▄[38;5;231m▄[48;5;231;38;5;159m▄[38;5;80m▄▄[48;5;80m█[38;5;73m▄▄▄▄[49m▀▀▀▀[39m          [38;5;73m▀[48;5;80m▄[49m▀[39m        [38;5;73m▀▀[48;5;80m▄▄[38;5;80m█[48;5;159m▄[48;5;231m▄▄[38;5;231m███[48;5;80m▄[38;5;80m█[48;5;73m▄[49;38;5;73m▄[39m      [00m
   [38;5;73m▄[48;5;73m█[48;5;80m▄▄▄▄[49m▀▀▀▀▀[39m                                 [38;5;73m▀▀▀[48;5;80m▄▄[38;5;80m█[48;5;159m▄[48;5;80m█[38;5;231m▄[48;5;231m█[48;5;80m▄[48;5;73;38;5;80m▄[49;38;5;73m▄[39m    [00m
                                                    [38;5;73m▀▀[48;5;73m█[48;5;80m▄▄[48;5;231;38;5;80m▄▄[48;5;80;38;5;159m▄[48;5;73;38;5;80m▄[49;38;5;73m▄[39m  [00m
                                                         [38;5;73m▀▀[48;5;80m▄▄[38;5;80m█[48;5;73m▄[49;38;5;73m▄[39m[00m
                                                             [38;5;73m▀▀▀[39m[00m
                                                                [00m
