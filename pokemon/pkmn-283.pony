$$$
NAME: Surskit
OTHER NAMES: Ametama, 溜溜糖球, Gehweiher, 비구술, アメタマ, Arakdo
APPEARANCE: Pokémon Generation III
KIND: Pond Skater Pokémon
GROUP: water1, bug
BALLOON: top
COAT: blue
DISPLAY: full
BALLOON TOP: 3
BALLOON BOTTOM: 0
POSE: master
BASED ON: Surskit
SOURCE: PokéAPI Sprites
MEDIA: Pokémon
LICENSE: Copyright The Pokémon Company
FREE: no
WIDTH: 73
HEIGHT: 25


$$$
$balloon5$
                      $\$                                                  [00m
                       $\$                                                 [00m
                        $\$                                                [00m
                         $\$                     [00m
                          $\$ [38;5;136m▄▄▄[39m                [00m
                          [38;5;136m▄[48;5;136;38;5;222m▄[48;5;222;38;5;16m▄[49m▀[39m                 [00m
                        [38;5;136m▄[48;5;136;38;5;222m▄[48;5;222;38;5;178m▄[48;5;16;38;5;16m█[49;39m                   [00m
                    [38;5;136m▄[38;5;95m▄▄[38;5;136m▄[48;5;136;38;5;222m▄[48;5;222m█[48;5;16;38;5;16m█[49;39m                    [00m
                 [38;5;136m▄[48;5;136;38;5;222m▄▄[48;5;222m███[48;5;136m▄[48;5;222m█[48;5;178;38;5;95m▄[48;5;95;38;5;178m▄[48;5;16m▄▄[49;38;5;16m▄▄[39m                [00m
               [38;5;60m▄[48;5;60;38;5;117m▄[48;5;178;38;5;95m▄[48;5;222;38;5;222m█[38;5;95m▄[48;5;136;38;5;222m▄[48;5;222m██[38;5;136m▄[38;5;222m██[48;5;178m▄[38;5;178m███[48;5;136;38;5;95m▄[48;5;16;38;5;74m▄[49;38;5;16m▄[39m              [00m
              [38;5;60m▄[48;5;60;38;5;74m▄[48;5;117;38;5;231m▄[48;5;74;38;5;16m▄[48;5;95;38;5;117m▄▄[48;5;136;38;5;136m█[48;5;222;38;5;95m▄▄[48;5;95m█[48;5;222m▄▄▄[48;5;178m▄[48;5;95;38;5;117m▄▄[48;5;117m█[48;5;74;38;5;74m██[48;5;16;38;5;16m█[49;39m             [00m
            [38;5;16m▄▄[48;5;60;38;5;60m█[48;5;117;38;5;117m█[48;5;16;38;5;238m▄[38;5;117m▄[48;5;117m█████[38;5;238m▄[48;5;238;38;5;231m▄[48;5;117;38;5;16m▄[38;5;117m█████[48;5;74;38;5;74m███[48;5;16;38;5;16m█[49;39m            [00m
        [38;5;60m▄▄[48;5;16;38;5;74m▄▄[48;5;74m█[38;5;16m▄[48;5;60m▄[48;5;209;38;5;167m▄[48;5;117;38;5;209m▄[38;5;117m██████[48;5;16m▄[38;5;238m▄[38;5;117m▄[48;5;117m████[48;5;74;38;5;74m████[48;5;16;38;5;16m█[49m▄[39m           [00m
      [38;5;60m▄[48;5;60;38;5;117m▄[48;5;117m█[38;5;16m▄[49m▀▀▀[39m  [38;5;16m▀[48;5;117m▄[38;5;117m█████[48;5;209;38;5;209m█[48;5;117m▄▄[48;5;209m█[38;5;167m▄[48;5;117;38;5;117m█[38;5;74m▄[48;5;74m████[48;5;16;38;5;16m█[48;5;74;38;5;60m▄▄[48;5;16;38;5;74m▄▄[49;38;5;16m▄▄[39m       [00m
     [48;5;60;38;5;60m█[48;5;117;38;5;117m█[38;5;16m▄[49m▀[39m       [38;5;60m▄[48;5;16m▄[48;5;74;38;5;16m▄[48;5;117m▄▄[38;5;74m▄▄[48;5;209m▄[48;5;167;38;5;60m▄[38;5;74m▄[48;5;74m█[38;5;60m▄[38;5;74m██[38;5;16m▄▄[49m▀[39m   [38;5;60m▀▀[48;5;74m▄▄[48;5;16;38;5;74m▄[49;38;5;16m▄[39m     [00m
    [48;5;60;38;5;60m█[48;5;117;38;5;117m█[48;5;16;38;5;16m█[49;39m        [38;5;60m▄[48;5;60m█[38;5;16m▄[49m▀[39m  [38;5;16m▀▀▀▀[48;5;74;38;5;60m▄[38;5;74m█[48;5;60m▄[48;5;16;38;5;16m█[49m▀[39m          [38;5;60m▀[48;5;74m▄[48;5;16;38;5;74m▄[49;38;5;16m▄[39m   [00m
   [38;5;60m▄[48;5;60;38;5;117m▄[48;5;117;38;5;16m▄[49m▀[39m        [48;5;60;38;5;60m█[48;5;16;38;5;16m█[49;39m         [38;5;60m▀[48;5;117m▄[38;5;117m█[48;5;16;38;5;16m█[49;39m            [38;5;60m▀[48;5;74m▄[48;5;16;38;5;16m█[49;39m  [00m
   [48;5;60;38;5;60m█[48;5;74;38;5;74m█[48;5;16;38;5;16m█[49;39m          [38;5;16m▀[39m          [38;5;60m▀[48;5;117m▄[38;5;117m█[48;5;16;38;5;16m█[49;39m            [38;5;60m▀[48;5;74m▄[48;5;16;38;5;16m█[49;39m [00m
   [48;5;60;38;5;60m█[48;5;74;38;5;74m█[48;5;16;38;5;16m█[49;39m                      [38;5;60m▀[48;5;117m▄[48;5;16;38;5;117m▄[49;38;5;16m▄[39m             [48;5;60;38;5;60m█[48;5;16;38;5;16m█[49;39m[00m
    [48;5;60;38;5;60m█[48;5;74;38;5;74m█[48;5;16;38;5;16m█[49;39m                      [48;5;60;38;5;60m█[48;5;117;38;5;117m█[48;5;16;38;5;16m█[49;39m             [38;5;16m▀[48;5;16m█[49;39m[00m
     [38;5;16m▀▀[39m                      [48;5;60;38;5;60m█[48;5;74;38;5;74m█[48;5;16;38;5;16m█[49;39m               [00m
                             [48;5;60;38;5;60m█[48;5;74;38;5;74m█[48;5;16;38;5;16m█[49;39m               [00m
                             [48;5;60;38;5;60m█[48;5;74;38;5;74m█[48;5;16;38;5;16m█[49;39m               [00m
                            [38;5;60m▄[48;5;60;38;5;74m▄[48;5;74;38;5;16m▄[49m▀[39m               [00m
                            [48;5;60;38;5;60m█[48;5;74;38;5;74m█[48;5;16;38;5;16m█[49;39m                [00m
                             [38;5;16m▀[39m                 [00m
                                               [00m
