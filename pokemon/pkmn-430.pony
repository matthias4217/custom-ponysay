$$$
NAME: Honchkrow
OTHER NAMES: ドンカラス, Corboss, Kramshef, 烏鴉頭頭, 돈크로우, Donkarasu
APPEARANCE: Pokémon Generation IV
KIND: Big Boss Pokémon
GROUP: flying
BALLOON: top
COAT: black
DISPLAY: full
BALLOON TOP: 3
BALLOON BOTTOM: 0
POSE: master
BASED ON: Honchkrow
SOURCE: PokéAPI Sprites
MEDIA: Pokémon
LICENSE: Copyright The Pokémon Company
FREE: no
WIDTH: 75
HEIGHT: 31


$$$
$balloon5$
                $\$                                                             [00m
                 $\$                                                            [00m
                  $\$                                                           [00m
                   $\$                                     [00m
                   [38;5;235m▄[48;5;235;38;5;61m▄▄[49;38;5;235m▄▄[48;5;235;38;5;61m▄▄[49;38;5;235m▄[39m                              [00m
                  [48;5;235;38;5;235m█[48;5;61;38;5;61m██[38;5;237m▄[48;5;237;38;5;61m▄[48;5;61m████[48;5;235;38;5;235m█[49;39m                             [00m
                [38;5;237m▄[48;5;235m▄[48;5;61;38;5;61m██[48;5;237m▄[48;5;61m███████[48;5;235;38;5;235m█[49;39m       [38;5;233m▄▄[48;5;233;38;5;237m▄[38;5;233m█[49;39m                 [00m
        [38;5;235m▄▄▄▄▄[38;5;233m▄▄▄[48;5;235m▄[48;5;61m▄[38;5;237m▄[38;5;61m██████[38;5;237m▄▄[48;5;237m█[38;5;233m▄[48;5;235m▄[49m▄▄▄[48;5;233;38;5;61m▄▄▄[48;5;61;38;5;237m▄[48;5;237m█[48;5;235;38;5;233m▄[38;5;237m▄[48;5;233;38;5;233m█[49;39m                [00m
    [38;5;235m▄[48;5;235m█[38;5;61m▄▄[48;5;237;38;5;237m█[48;5;61m▄▄▄▄▄▄▄[38;5;61m██[48;5;237m▄[48;5;233m▄▄▄▄▄▄▄▄[48;5;237m▄[48;5;61m█[38;5;237m▄▄▄▄▄[48;5;237m█[38;5;235m▄[38;5;233m▄[48;5;233;38;5;237m▄[48;5;237;38;5;235m▄[48;5;233;38;5;233m█[49;39m                 [00m
    [38;5;235m▀[48;5;61;38;5;233m▄[48;5;237;38;5;237m████████████████████████████[48;5;233m▄[48;5;235m▄[48;5;237m█[38;5;235m▄[48;5;233;38;5;233m█[49;39m                  [00m
      [38;5;233m▀▀[48;5;237m▄▄[38;5;237m██[38;5;233m▄▄[48;5;233;38;5;136m▄▄[38;5;248m▄[48;5;248;38;5;237m▄[48;5;237m██[38;5;231m▄[38;5;237m████████████[38;5;235m▄▄[48;5;235;38;5;237m▄[38;5;233m▄[49m▀[39m                   [00m
         [38;5;240m▄[48;5;233m▄[38;5;220m▄[48;5;220m██[48;5;136m▄[48;5;248;38;5;248m█[38;5;231m▄[48;5;237;38;5;237m█[48;5;233;38;5;231m▄[48;5;167m▄[48;5;231;38;5;237m▄[48;5;237m█[38;5;248m▄[38;5;237m██████████[38;5;233m▄▄[49m▀[39m                     [00m
       [38;5;240m▄[48;5;240;38;5;220m▄[48;5;220m██[38;5;240m▄▄▄[48;5;136m▄[48;5;240;38;5;220m▄[48;5;231;38;5;248m▄[38;5;231m█[48;5;248m▄▄[38;5;233m▄▄[48;5;237m▄[38;5;255m▄▄[38;5;239m▄[38;5;237m██[48;5;235m▄[48;5;237;38;5;233m▄[49;38;5;237m▀[38;5;233m▀▀[39m                        [00m
      [48;5;240;38;5;233m▄[48;5;220m▄[48;5;240;38;5;240m█[49;38;5;233m▀▀[48;5;240m▄[48;5;220m▄▄▄▄▄[38;5;239m▄[48;5;233;38;5;255m▄▄[48;5;255m██[38;5;237m▄▄[48;5;237m█[48;5;233;38;5;61m▄▄▄▄▄[38;5;237m▄[49;38;5;233m▄▄[39m                        [00m
       [38;5;233m▄[38;5;248m▄[48;5;233;38;5;255m▄[48;5;248m▄[48;5;255m██[38;5;233m▄[48;5;239;38;5;239m█[48;5;233;38;5;255m▄[48;5;255m████[38;5;237m▄[48;5;237;38;5;61m▄[48;5;61m██████████[48;5;237m▄[48;5;233m▄[49;38;5;233m▄[39m        [38;5;235m▄[48;5;235;38;5;167m▄[38;5;235m█[49m▄▄[48;5;235;38;5;167m▄[49;38;5;235m▄[39m [38;5;235m▄▄[39m    [00m
     [38;5;239m▄[48;5;233;38;5;255m▄[48;5;255m████[38;5;248m▄[48;5;233;38;5;255m▄[48;5;255m██████[48;5;237;38;5;237m█[48;5;61;38;5;61m██████████████[38;5;237m▄[48;5;233;38;5;61m▄[49;38;5;235m▄[39m    [38;5;235m▄[48;5;235;38;5;167m▄▄[48;5;167;38;5;95m▄[38;5;167m█[48;5;235m▄[48;5;167m██[48;5;235;38;5;95m▄[38;5;167m▄[48;5;95m▄[48;5;167m█[48;5;235;38;5;235m█[49;39m   [00m
    [48;5;239;38;5;239m█[48;5;255;38;5;255m█████████████[48;5;233;38;5;233m█[48;5;61;38;5;61m██████████████[48;5;237m▄[48;5;61;38;5;237m▄[48;5;237;38;5;61m▄[48;5;233;38;5;237m▄[49;38;5;233m▄[39m   [48;5;235;38;5;235m█[48;5;167;38;5;233m▄▄[38;5;167m██[38;5;233m▄▄[38;5;167m██[38;5;95m▄▄[48;5;235;38;5;235m█[49m▄[39m   [00m
   [48;5;239;38;5;239m█[48;5;255;38;5;255m█████████████[48;5;248;38;5;235m▄[48;5;233;38;5;233m█[48;5;61;38;5;61m█████████████[38;5;237m▄[48;5;237;38;5;61m▄[48;5;61;38;5;237m▄[48;5;237;38;5;61m▄[48;5;61;38;5;237m▄[48;5;233m▄[49;38;5;233m▄[39m [38;5;235m▄[48;5;233m▄[48;5;237;38;5;237m█[48;5;233;38;5;233m█[48;5;95m▄[48;5;233;38;5;237m▄[48;5;237m█[48;5;233;38;5;233m█[48;5;95;38;5;95m███[48;5;235m▄[48;5;95m██[48;5;235m▄[38;5;235m█[49;39m [00m
   [48;5;239;38;5;239m█[48;5;255;38;5;248m▄[38;5;255m███████████[48;5;248;38;5;235m▄[48;5;235;38;5;61m▄[48;5;61m█[48;5;233;38;5;233m█[48;5;61;38;5;61m███████████[38;5;237m▄▄[48;5;237m█[48;5;61m▄[48;5;237m███[48;5;233;38;5;233m█[49;38;5;235m▄[48;5;233m▄[48;5;237;38;5;237m█[38;5;233m▄[48;5;233;38;5;237m▄[48;5;237m█[38;5;235m▄[48;5;233;38;5;233m█[48;5;95m▄[48;5;233;38;5;237m▄▄[38;5;233m█[48;5;95;38;5;95m██[38;5;235m▄[48;5;235m█[49;39m  [00m
   [38;5;239m▀[48;5;248m▄[38;5;248m█[48;5;255m▄▄[38;5;255m████[38;5;248m▄[38;5;255m█[38;5;248m▄[48;5;248m█[48;5;235m▄[48;5;61;38;5;235m▄[38;5;61m█[48;5;233m▄[48;5;61;38;5;233m▄[38;5;61m████████[38;5;237m▄[48;5;237m████████[48;5;235m▄[48;5;233;38;5;233m█[48;5;237;38;5;237m█[48;5;235;38;5;233m▄[38;5;237m▄[48;5;237m█[38;5;233m▄[48;5;233;38;5;237m▄[48;5;235m▄[48;5;237m██[48;5;233;38;5;233m█[48;5;95;38;5;95m█████[48;5;235m▄[49;38;5;235m▄[39m[00m
    [38;5;233m▀[48;5;248m▄[38;5;248m████[48;5;255m▄[48;5;248;38;5;255m▄[48;5;255;38;5;248m▄[48;5;248;38;5;255m▄[48;5;255;38;5;248m▄[48;5;248m█[38;5;235m▄[38;5;61m▄[48;5;61m██[48;5;233m▄[48;5;61;38;5;233m▄[38;5;61m████[38;5;237m▄▄[48;5;237m███████████[48;5;233;38;5;233m█[48;5;235;38;5;237m▄[48;5;237m██[48;5;235m▄[48;5;237m███[38;5;233m▄[48;5;233;38;5;248m▄[48;5;248m█[48;5;235m▄[48;5;95;38;5;95m█[48;5;235m▄[48;5;95;38;5;235m▄[49m▀▀[39m [00m
     [38;5;233m▀[48;5;248m▄[38;5;248m█[38;5;239m▄[38;5;248m██████[38;5;233m▄[38;5;235m▄[48;5;235;38;5;61m▄[48;5;61m███[48;5;233m▄[48;5;237;38;5;233m▄[38;5;237m████████████████[48;5;235m▄[48;5;237m███[38;5;235m▄[38;5;233m▄▄[48;5;233;38;5;235m▄[38;5;237m▄▄▄▄[48;5;235;38;5;233m▄[48;5;95;38;5;235m▄[49m▀[39m   [00m
     [38;5;233m▄[48;5;233;38;5;248m▄[48;5;248;38;5;239m▄[48;5;239;38;5;248m▄[48;5;248m███[38;5;239m▄[38;5;233m▄[48;5;233;38;5;61m▄[48;5;61m██████[38;5;237m▄▄[48;5;233m▄[48;5;237;38;5;233m▄[38;5;235m▄[38;5;237m███████████████[38;5;235m▄[48;5;233;38;5;237m▄[48;5;235m▄[48;5;237m████[38;5;233m▄[48;5;235;38;5;235m█[49;38;5;233m▀[39m      [00m
    [38;5;233m▀[48;5;239m▄▄[48;5;248m▄▄▄[49;38;5;239m▀[38;5;233m▀[48;5;237m▄[48;5;61m▄[38;5;237m▄▄▄▄▄[48;5;237m███████[48;5;233m▄[48;5;235;38;5;235m█[48;5;237;38;5;233m▄[38;5;237m██████████████[38;5;235m▄[38;5;233m▄▄[49m▀▀[39m         [00m
              [38;5;233m▀▀[48;5;237m▄▄▄[38;5;237m█████████[48;5;235;38;5;235m█[48;5;233;38;5;237m▄▄[48;5;235m▄[48;5;237;38;5;233m▄▄▄▄▄▄▄▄▄[49m▀▀[38;5;235m▀[39m             [00m
                   [38;5;233m▀▀▀[48;5;235m▄▄[38;5;237m▄[48;5;237m███[38;5;233m▄[48;5;235m▄[48;5;237m▄[38;5;237m██████[48;5;235;38;5;233m▄[49;38;5;235m▀[39m                  [00m
                        [38;5;233m▀[48;5;233m█[38;5;236m▄▄[38;5;233m█[49;39m  [38;5;233m▀▀[48;5;237m▄[38;5;236m▄[48;5;235;38;5;233m▄[49m▀[39m                    [00m
                    [38;5;233m▄▄▄[48;5;233;38;5;239m▄[38;5;236m▄[48;5;239;38;5;239m██[48;5;233;38;5;233m█[49m▄[39m    [48;5;233;38;5;236m▄[48;5;236m█[48;5;233;38;5;233m█[49;39m                     [00m
                [38;5;233m▄[48;5;233;38;5;248m▄[38;5;239m▄[38;5;231m▄[48;5;239;38;5;239m█[38;5;233m▄▄[48;5;236;38;5;239m▄[48;5;239m██[38;5;236m▄[48;5;236;38;5;233m▄▄[48;5;233;38;5;248m▄[49;38;5;233m▄▄[48;5;233;38;5;236m▄[48;5;236;38;5;239m▄[38;5;236m█[48;5;233m▄[49;39m                     [00m
                [38;5;233m▀[48;5;233m█[48;5;231m▄[48;5;233m█[38;5;231m▄[48;5;231m█[48;5;236;38;5;236m█[48;5;239;38;5;233m▄[49m▀▀▀[48;5;233m█[38;5;239m▄▄[48;5;239;38;5;236m▄[48;5;236;38;5;239m▄[48;5;239m█[38;5;233m▄[48;5;236;38;5;236m██[48;5;233;38;5;233m█[49;39m                    [00m
                   [38;5;233m▀▀▀▀[39m   [48;5;233;38;5;233m█[48;5;239;38;5;248m▄[38;5;233m▄[48;5;236;38;5;236m█[48;5;239;38;5;231m▄[38;5;248m▄[38;5;233m▄[48;5;233m█[48;5;239;38;5;239m█[38;5;236m▄[48;5;233;38;5;233m█[49;39m                    [00m
                           [38;5;233m▀▀▀[48;5;231m▄[48;5;233m█[49m▀▀[48;5;231m▄[48;5;233m█[49;39m                     [00m
                                                         [00m
