$$$
NAME: Persian
OTHER NAMES: ペルシアン, 貓老大, 페르시온, Snobilikat
APPEARANCE: Pokémon Generation I
KIND: Classy Cat Pokémon
GROUP: ground
BALLOON: top
COAT: yellow
DISPLAY: full
BALLOON TOP: 3
BALLOON BOTTOM: 0
POSE: master
BASED ON: Persian
SOURCE: PokéAPI Sprites
MEDIA: Pokémon
LICENSE: Copyright The Pokémon Company
FREE: no
WIDTH: 75
HEIGHT: 34


$$$
$balloon5$
                  $\$                                                          [00m
                   $\$                                                         [00m
                    $\$                                                        [00m
                     $\$                                                  [00m
                    [38;5;233m▄▄[48;5;233m████[49m▄[39m                                             [00m
                   [48;5;233;38;5;233m███[38;5;137m▄[48;5;137m██[48;5;233m▄[38;5;233m██[49;39m                                            [00m
   [38;5;233m▄[48;5;233m████[49m▄[38;5;94m▄[39m       [38;5;94m▄[48;5;233;38;5;233m██[38;5;137m▄[48;5;137m██[38;5;179m▄▄[48;5;179m█[48;5;233;38;5;233m██[49;39m                                            [00m
   [48;5;233;38;5;233m██[48;5;137;38;5;137m██[48;5;233m▄[38;5;233m█[48;5;222m▄[48;5;94;38;5;222m▄[38;5;222m▄[49;38;5;94m▄▄▄[48;5;94;38;5;222m▄[38;5;222m▄[48;5;222;38;5;233m▄[48;5;233m█[38;5;222m▄[48;5;137m▄▄[48;5;179;38;5;179m███[38;5;137m▄[48;5;233;38;5;233m█[49;39m                                             [00m
    [48;5;233;38;5;233m█[48;5;94m▄[48;5;137;38;5;137m█[38;5;179m▄▄[48;5;233;38;5;137m▄[38;5;233m█[48;5;222;38;5;222m██████[48;5;233m▄[48;5;222m███[48;5;222m▄[38;5;222m█[48;5;179m▄▄[48;5;233;38;5;233m█[49;39m   [38;5;137m▄[38;5;94m▄[38;5;233m▄[39m                                        [00m
     [38;5;233m▀▀[48;5;179m▄▄[48;5;222;38;5;222m▄[48;5;222m█[38;5;203m▄[48;5;203;38;5;231m▄[38;5;216m▄[48;5;222;38;5;124m▄[38;5;222m████[38;5;137m▄[38;5;233m▄▄[38;5;222m█[48;5;222;38;5;222m███[48;5;233;38;5;94m▄[49m▄[48;5;94;38;5;222m▄[48;5;222;38;5;233m▄▄[49m▀[39m                                        [00m
        [38;5;233m▀[48;5;222m▄[38;5;222m█[48;5;203m▄[48;5;216;38;5;124m▄[48;5;203m▄[48;5;124;38;5;222m▄[48;5;222m██[38;5;94m▄[48;5;94;38;5;253m▄[48;5;253;38;5;231m▄[38;5;253m█[48;5;233;38;5;94m▄[48;5;222;38;5;222m██[38;5;137m▄[48;5;94;38;5;222m▄[48;5;222;38;5;233m▄▄[49m▀[39m                                           [00m
    [48;5;233;38;5;233m█[48;5;94;38;5;222m▄▄[49;38;5;94m▄▄[48;5;233;38;5;233m█[48;5;94;38;5;253m▄[48;5;222;38;5;94m▄[38;5;222m████[48;5;94;38;5;222m▄[48;5;231;38;5;231m█[48;5;124;38;5;203m▄[48;5;231;38;5;231m██[48;5;222;38;5;222m███[48;5;233m▄[48;5;94m▄[48;5;233m▄[49;38;5;233m▄[38;5;94m▄▄▄▄▄[39m                                       [00m
     [38;5;233m▀▀[48;5;222m▄▄[48;5;233m█[48;5;231;38;5;222m▄[48;5;124;38;5;203m▄[48;5;222;38;5;222m█[48;5;222;38;5;222m████[48;5;222m▄[48;5;233m▄[48;5;222m▄▄[38;5;222m███[48;5;137m▄[48;5;94;38;5;222m▄▄[48;5;137m▄[48;5;222;38;5;233m▄▄▄▄▄[49m▀[39m                                      [00m
      [38;5;94m▄▄▄[38;5;233m▄[48;5;233;38;5;222m▄▄[48;5;222;38;5;233m▄▄[38;5;222m▄[38;5;222m█████[38;5;222m▄[48;5;222m██[38;5;137m▄[48;5;233;38;5;94m▄▄[38;5;233m█[49m▀[39m                            [38;5;94m▄▄[48;5;94;38;5;222m▄[38;5;222m▄▄▄▄▄[38;5;222m▄[49;38;5;94m▄▄[39m     [00m
     [38;5;233m▀[48;5;222m▄▄[48;5;222m▄[48;5;233;38;5;222m▄[48;5;222;38;5;233m▄[38;5;222m▄[48;5;222m█[48;5;94;38;5;233m▄[48;5;222m▄▄▄[48;5;222;38;5;231m▄[48;5;94;38;5;222m▄[48;5;222m████[48;5;222;38;5;233m▄▄▄[48;5;94;38;5;222m▄▄▄[49;38;5;94m▄▄[39m                      [38;5;94m▄[48;5;94;38;5;222m▄[38;5;222m▄[48;5;222m▄[48;5;222m█████████[48;5;222m▄[48;5;94;38;5;222m▄[49;38;5;233m▄[39m   [00m
        [38;5;233m▄[48;5;233;38;5;222m▄[48;5;222;38;5;233m▄[48;5;233m█[49m▀[48;5;222m▄[38;5;222m█████[38;5;233m▄[38;5;94m▄[48;5;137;38;5;222m▄[48;5;222m████[48;5;233;38;5;94m▄[49;38;5;233m▀▀[48;5;222m▄▄[48;5;94m▄[49;39m                   [38;5;94m▄[48;5;94;38;5;222m▄[48;5;222m▄[48;5;222m█[38;5;222m▄[38;5;233m▄[48;5;222m▄[49m▀▀▀▀▀[48;5;222m▄[38;5;222m█[48;5;222m▄[38;5;222m██[48;5;222m▄[48;5;233;38;5;222m▄[49;38;5;233m▄[39m [00m
        [38;5;233m▀▀[39m    [38;5;233m▀▀[48;5;233m█[38;5;222m▄▄[48;5;222m████████[48;5;94m▄▄[49;38;5;94m▄▄[39m                   [48;5;94;38;5;94m█[48;5;222;38;5;222m▄[48;5;222m█[38;5;222m▄[48;5;222;38;5;233m▄[49m▀[39m     [38;5;233m▄[48;5;233;38;5;94m▄[38;5;222m▄▄[38;5;222m▄[48;5;222;38;5;233m▄[48;5;222;38;5;222m▄[38;5;222m██[48;5;233;38;5;222m▄[49;38;5;233m▄[39m[00m
                [48;5;233;38;5;233m█[48;5;222;38;5;222m██████████[38;5;222m▄[48;5;222m██[48;5;222m▄[48;5;94m▄▄▄▄[49;38;5;94m▄▄[38;5;233m▄▄▄▄[39m        [48;5;94;38;5;94m█[48;5;222;38;5;222m▄[48;5;222m█[38;5;222m▄[48;5;233;38;5;233m█[49;39m      [38;5;233m▄[48;5;233;38;5;222m▄[48;5;222m████[48;5;222m▄[48;5;233;38;5;233m█[48;5;222;38;5;222m███[48;5;233;38;5;233m█[49;39m[00m
                [48;5;233;38;5;233m█[48;5;222;38;5;222m█████████[48;5;222;38;5;222m██████████████[48;5;222m▄[48;5;233m▄▄▄[38;5;222m▄[49;38;5;233m▄▄[38;5;94m▄[48;5;94;38;5;222m▄[48;5;222m███[48;5;233;38;5;233m█[49;39m       [48;5;233;38;5;233m█[48;5;222;38;5;222m▄[38;5;222m███[38;5;222m▄[38;5;94m▄[48;5;233;38;5;233m█[48;5;222;38;5;222m██[38;5;222m▄[48;5;233;38;5;233m█[49;39m[00m
               [48;5;233;38;5;233m█[48;5;222;38;5;222m█████████[48;5;222;38;5;222m█████████████████████[48;5;222m▄[48;5;233m▄[38;5;222m▄[48;5;222;38;5;233m▄▄[49m▀[39m         [48;5;233;38;5;233m█[48;5;222;38;5;222m▄[38;5;222m█[48;5;233m▄▄▄[48;5;222m██[38;5;222m▄[48;5;233;38;5;233m█[49;39m [00m
               [48;5;233;38;5;233m█[48;5;222;38;5;222m█[38;5;94m▄[38;5;222m███████[38;5;222m▄[48;5;222;38;5;222m▄[38;5;222m██████████████████████[48;5;222m▄[48;5;233;38;5;233m█[49;39m           [38;5;233m▀▀[48;5;222m▄▄▄[48;5;222m▄[49m▀▀[39m  [00m
               [48;5;233;38;5;233m█[48;5;222;38;5;222m█[48;5;94;38;5;94m█[48;5;222;38;5;222m███████[38;5;222m▄[48;5;222;38;5;222m▄[48;5;222;38;5;222m▄[48;5;222m██████████████████████[48;5;222;38;5;222m█[48;5;233;38;5;233m█[49;39m                    [00m
                [48;5;233;38;5;233m█[48;5;222;38;5;222m█[48;5;94;38;5;94m█[48;5;222;38;5;222m███████[48;5;222m▄[48;5;222m█[48;5;222m▄[38;5;222m███[38;5;222m▄[38;5;222m██████[48;5;94;38;5;222m▄[48;5;222;38;5;94m▄[38;5;222m█████████[48;5;222;38;5;222m██[48;5;233;38;5;233m█[49;39m                   [00m
                [38;5;233m▀[48;5;222m▄[38;5;222m█[48;5;94m▄[48;5;222;38;5;94m▄[38;5;222m█[38;5;94m▄[38;5;222m███████[48;5;222m▄[48;5;233;38;5;233m█[48;5;222;38;5;222m▄[38;5;222m███[38;5;222m▄[48;5;222;38;5;233m▄▄▄[48;5;94;38;5;94m█[48;5;222;38;5;222m████████[38;5;222m▄[48;5;222m██[48;5;233;38;5;233m█[49;39m                   [00m
                 [38;5;233m▀[48;5;222m▄[38;5;222m██[48;5;94m▄[48;5;233;38;5;233m█[48;5;222;38;5;222m████████[48;5;233;38;5;233m█[48;5;222m▄▄[49m▀[48;5;233m█[38;5;222m▄[48;5;222m███[48;5;233m▄[48;5;222;38;5;233m▄[48;5;222;38;5;222m██████[38;5;222m▄[48;5;222m██[38;5;233m▄[49m▀[39m                   [00m
                  [48;5;233;38;5;233m█[48;5;222;38;5;222m███[48;5;233m▄[48;5;222;38;5;233m▄[38;5;222m███████[48;5;233;38;5;233m█[49;39m   [48;5;233;38;5;233m█[48;5;222;38;5;222m█████[48;5;233;38;5;233m█[48;5;222;38;5;222m█[48;5;222m▄▄▄[48;5;222m█████[48;5;233;38;5;233m█[49;39m                    [00m
                  [48;5;233;38;5;233m█[48;5;222;38;5;222m████[48;5;233;38;5;233m█[48;5;222;38;5;222m██████[48;5;233;38;5;233m█[49;39m     [48;5;233;38;5;233m█[48;5;222;38;5;222m█████[48;5;233;38;5;233m█[48;5;222;38;5;222m████████[48;5;233;38;5;233m█[49;39m                    [00m
                  [38;5;233m▀[48;5;222m▄[38;5;222m███[48;5;233m▄[48;5;222;38;5;233m▄[38;5;222m█████[48;5;233;38;5;233m█[49;39m      [38;5;233m▀[48;5;222m▄[38;5;222m████[48;5;233m▄[48;5;222;38;5;233m▄▄[38;5;222m██████[48;5;233;38;5;233m█[49;39m                   [00m
                   [48;5;233;38;5;233m█[48;5;222;38;5;222m████[48;5;233;38;5;233m█[48;5;222;38;5;222m█████[48;5;233;38;5;233m█[49;39m       [48;5;233;38;5;233m█[48;5;222;38;5;222m████[38;5;233m▄[49m▀[39m [38;5;233m▀[48;5;222m▄[38;5;222m█████[48;5;233;38;5;233m█[49;39m                  [00m
                   [48;5;233;38;5;233m█[48;5;222;38;5;222m████[48;5;233;38;5;233m█[48;5;222;38;5;222m█████[48;5;233;38;5;233m█[49;39m      [38;5;233m▄[48;5;233;38;5;222m▄[48;5;222m████[48;5;233;38;5;233m█[49;39m   [48;5;233;38;5;233m█[48;5;222;38;5;222m█████[48;5;233;38;5;233m█[49;39m                  [00m
                   [48;5;233;38;5;233m█[48;5;222;38;5;222m████[48;5;233;38;5;233m█[48;5;222;38;5;222m█████[48;5;233;38;5;233m█[49;39m    [38;5;233m▄[48;5;233;38;5;222m▄▄[48;5;222m████[48;5;233;38;5;233m█[49;39m    [48;5;233;38;5;233m█[48;5;222;38;5;222m████[48;5;233;38;5;233m█[49;39m                   [00m
                 [38;5;233m▄▄[48;5;233;38;5;222m▄[48;5;222m██[48;5;222m▄[38;5;233m▄[48;5;233;38;5;222m▄[48;5;222m█████[48;5;233;38;5;233m█[49;39m    [48;5;233;38;5;233m█[48;5;222;38;5;222m█[48;5;94;38;5;94m█[48;5;222;38;5;222m██[38;5;233m▄[49m▀[39m    [48;5;233;38;5;233m█[48;5;222;38;5;222m████[38;5;233m▄[49m▀[39m                   [00m
                [48;5;233;38;5;233m█[48;5;222;38;5;222m█[38;5;94m▄[48;5;94;38;5;222m▄[48;5;222;38;5;233m▄[48;5;233;38;5;222m▄[38;5;222m▄[48;5;222m████[48;5;222m▄[38;5;222m█[38;5;233m▄[49m▀[39m     [38;5;233m▀▀▀▀[39m    [38;5;233m▄[48;5;233;38;5;222m▄[48;5;222m█████[48;5;233;38;5;233m█[49;39m                    [00m
                [38;5;233m▀[48;5;222m▄[48;5;94m▄[48;5;222m▄[48;5;233m█[48;5;222;38;5;222m█[38;5;94m▄[48;5;94;38;5;222m▄[48;5;222m█[38;5;94m▄[48;5;94;38;5;222m▄[48;5;222m██[48;5;233;38;5;233m█[49;39m              [48;5;233;38;5;233m█[48;5;222;38;5;222m█[48;5;94;38;5;94m█[48;5;222;38;5;222m█[48;5;94;38;5;94m█[48;5;222;38;5;222m█[38;5;233m▄[49m▀[39m                    [00m
                     [38;5;233m▀▀[48;5;222m▄▄[48;5;94m▄[48;5;222m▄▄[49m▀[39m                [38;5;233m▀▀▀▀▀[39m                      [00m
                                                                        [00m
