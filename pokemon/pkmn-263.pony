$$$
NAME: Zigzagoon
OTHER NAMES: 지그제구리, Zigzaton, Zigzachs, ジグザグマ, Ziguzaguma, 蛇紋熊
APPEARANCE: Pokémon Generation III
KIND: Tiny Raccoon Pokémon
GROUP: ground
BALLOON: top
COAT: brown
DISPLAY: full
BALLOON TOP: 3
BALLOON BOTTOM: 0
POSE: master
BASED ON: Zigzagoon
SOURCE: PokéAPI Sprites
MEDIA: Pokémon
LICENSE: Copyright The Pokémon Company
FREE: no
WIDTH: 74
HEIGHT: 23


$$$
$balloon5$
                           $\$                                              [00m
                            $\$                                             [00m
                             $\$                                            [00m
                              $\$                           [00m
                               $\$ [38;5;95m▄[38;5;16m▄[39m             [38;5;145m▄▄▄[39m       [00m
                          [38;5;95m▄[38;5;16m▄[39m  [38;5;95m▄▄[48;5;95;38;5;253m▄[48;5;253m█[48;5;16;38;5;16m█[49;39m  [38;5;16m▄▄▄[39m    [38;5;145m▄▄[48;5;145;38;5;253m▄▄[48;5;253m█[38;5;95m▄[49m▀[39m       [00m
       [38;5;95m▄[48;5;95;38;5;138m▄[49;38;5;16m▄[39m  [38;5;95m▄[38;5;16m▄[39m    [38;5;95m▄[48;5;95;38;5;253m▄[38;5;16m▄[49;38;5;95m▄[48;5;95;38;5;253m▄[38;5;16m▄[49;38;5;95m▄[48;5;95;38;5;138m▄[48;5;138m█[48;5;95m▄[48;5;16;38;5;16m█[48;5;95;38;5;145m▄[48;5;253;38;5;253m█[38;5;138m▄▄▄[48;5;95;38;5;145m▄[48;5;16;38;5;253m▄▄[48;5;253m█[38;5;16m▄[49m▀[39m  [38;5;95m▄[48;5;145m▄[48;5;253;38;5;138m▄▄[38;5;253m█[38;5;95m▄[48;5;95;38;5;145m▄[49m▄[48;5;145;38;5;253m▄▄▄▄[38;5;95m▄[49;39m   [00m
       [48;5;95;38;5;95m█[48;5;138;38;5;138m█[48;5;16m▄[49;38;5;16m▄[48;5;95;38;5;95m█[48;5;138;38;5;138m█[48;5;16;38;5;16m█[49;39m   [48;5;95;38;5;95m█[48;5;145;38;5;253m▄[48;5;253m█[48;5;16;38;5;145m▄[48;5;253;38;5;253m██[48;5;16;38;5;16m█[48;5;138;38;5;95m▄[48;5;95;38;5;253m▄[38;5;16m▄[48;5;138;38;5;138m█[48;5;16;38;5;138m▄[48;5;138;38;5;138m▄▄[48;5;138m█[38;5;16m▄[48;5;16;38;5;253m▄[48;5;253m███[48;5;16m▄[49;38;5;16m▄▄[38;5;95m▄[48;5;95;38;5;138m▄[48;5;138m▄[48;5;138;38;5;138m▄[48;5;138;38;5;253m▄[48;5;253m███████[38;5;95m▄[49m▀[39m    [00m
     [48;5;95;38;5;95m█[38;5;138m▄[38;5;95m█[48;5;138;38;5;138m██[48;5;16m▄[48;5;138m██[48;5;16;38;5;95m▄[49;39m [38;5;95m▄[48;5;95m█[48;5;145;38;5;16m▄[48;5;253;38;5;253m████[38;5;145m▄[48;5;16;38;5;253m▄[48;5;253m██[48;5;16;38;5;16m█[48;5;138;38;5;95m▄▄[38;5;16m▄[38;5;138m█[48;5;138m▄[48;5;16m▄[48;5;253;38;5;138m▄▄▄▄[38;5;16m▄[38;5;253m██[48;5;16m▄[38;5;145m▄[48;5;138;38;5;16m▄[38;5;138m█[48;5;138m▄[48;5;95m▄[48;5;138m▄[38;5;95m▄[38;5;253m▄[48;5;253m████[48;5;145m▄▄▄[49;38;5;95m▄[39m [00m
     [48;5;95;38;5;95m█[48;5;138;38;5;138m██[48;5;138;38;5;138m██[38;5;95m▄▄[48;5;95;38;5;138m▄[48;5;16m▄▄[48;5;138m██[48;5;16;38;5;95m▄[48;5;253m▄[48;5;95;38;5;138m▄▄[48;5;16;38;5;16m█[48;5;253;38;5;253m███[48;5;145;38;5;95m▄[48;5;253m▄[38;5;253m█[38;5;16m▄[48;5;16;38;5;138m▄[48;5;138m██████[38;5;16m▄[48;5;16;38;5;145m▄[48;5;253m▄[48;5;145m█[38;5;16m▄[48;5;16;38;5;138m▄[48;5;138m▄[38;5;138m██[38;5;95m▄[48;5;95;38;5;253m▄[48;5;253m██████[38;5;145m▄[38;5;95m▄[48;5;95m█[49;39m  [00m
      [48;5;95;38;5;95m█[48;5;138;38;5;138m█[38;5;95m▄[48;5;95;38;5;138m▄[48;5;138m██████[48;5;138m▄▄[48;5;138m███[48;5;16;38;5;16m█[48;5;253;38;5;95m▄▄[48;5;95;38;5;138m▄[48;5;138m█[48;5;16;38;5;16m█[48;5;253;38;5;253m█[48;5;16m▄[48;5;95m▄▄▄▄[48;5;138;38;5;16m▄[38;5;138m█[48;5;16;38;5;240m▄[48;5;145;38;5;145m█[38;5;253m▄▄[48;5;16;38;5;145m▄[48;5;138;38;5;16m▄▄[48;5;138;38;5;138m▄▄[48;5;138m██[48;5;95m▄▄▄▄▄[48;5;145;38;5;95m▄[48;5;253;38;5;145m▄▄[38;5;95m▄▄[49m▀[39m [00m
      [48;5;95;38;5;16m▄[48;5;16;38;5;238m▄[48;5;138m▄▄[38;5;240m▄[38;5;138m▄[38;5;138m███████[38;5;138m▄[38;5;95m▄[48;5;95;38;5;138m▄[48;5;138m███[48;5;138m▄▄[48;5;95m▄▄[48;5;253;38;5;95m▄[38;5;145m▄[38;5;16m▄[48;5;16;38;5;138m▄[48;5;138;38;5;138m▄[48;5;138m██[48;5;240m▄[48;5;145;38;5;240m▄[48;5;253m▄[38;5;145m▄▄[48;5;145;38;5;16m▄[48;5;16;38;5;138m▄[48;5;138m████[38;5;95m▄▄[48;5;95;38;5;145m▄▄[48;5;145m█████[48;5;95m▄[49;38;5;16m▄[39m[00m
     [38;5;16m▄[48;5;16;38;5;238m▄[48;5;238m█████[48;5;240m▄[48;5;138m▄[38;5;240m▄[38;5;138m███[48;5;138m▄[48;5;138m███████[38;5;138m▄[38;5;16m▄[48;5;16;38;5;145m▄[48;5;145m█[48;5;16m▄[48;5;138;38;5;16m▄[38;5;138m██[48;5;138m▄▄[48;5;138;38;5;16m▄▄[48;5;16;38;5;145m▄[48;5;145m███[48;5;16m▄▄[48;5;138;38;5;16m▄[38;5;138m█[48;5;95m▄[48;5;145;38;5;95m▄▄[38;5;145m███[38;5;95m▄▄▄[49;38;5;16m▀▀[39m [00m
    [38;5;95m▄[48;5;16;38;5;138m▄[48;5;238;38;5;138m▄[38;5;240m▄[38;5;238m███████[48;5;240m▄[48;5;138;38;5;240m▄[38;5;138m████[38;5;95m▄[38;5;16m▄▄▄[38;5;95m▄[38;5;138m█[48;5;95m▄[48;5;145;38;5;16m▄[48;5;253;38;5;145m▄[38;5;253m██[48;5;16m▄[48;5;138;38;5;16m▄[38;5;138m█[48;5;16;38;5;240m▄[48;5;95;38;5;145m▄[48;5;145m████[38;5;16m▄[48;5;16;38;5;95m▄[38;5;138m▄[48;5;138m█████[48;5;95m▄[38;5;95m█[48;5;145;38;5;145m████[48;5;95m▄[49;38;5;95m▄[39m [00m
   [48;5;95;38;5;238m▄[48;5;138m▄[38;5;138m▄[38;5;138m███[48;5;240m▄[48;5;238;38;5;240m▄[38;5;238m█[38;5;95m▄[48;5;231;38;5;238m▄[48;5;238;38;5;16m▄[48;5;238;38;5;238m▄[38;5;234m▄[48;5;240;38;5;240m█[48;5;138;38;5;138m▄[48;5;138m█████[48;5;16m▄[48;5;145;38;5;16m▄[48;5;16;38;5;145m▄▄[48;5;145m██[38;5;95m▄[38;5;16m▄[48;5;16;38;5;138m▄[48;5;138m███[48;5;16m▄[48;5;145;38;5;16m▄[38;5;145m████[48;5;16m▄[48;5;95;38;5;16m▄[49;38;5;95m▀▀[48;5;138m▄[38;5;16m▄[48;5;95m▄[48;5;145m▄[38;5;95m▄[38;5;145m█[48;5;95m▄[48;5;16;38;5;95m▄[49;38;5;16m▀▀▀[39m [00m
   [48;5;238;38;5;238m█[48;5;240m▄▄[48;5;238m█[48;5;138;38;5;138m██[38;5;138m▄▄[48;5;234;38;5;240m▄[48;5;95;38;5;234m▄[48;5;16;38;5;95m▄[38;5;238m▄[48;5;238;38;5;234m▄[48;5;234m█[48;5;240;38;5;240m█[48;5;138;38;5;138m██████[38;5;16m▄▄[48;5;16;38;5;145m▄[48;5;145m████[48;5;16m▄[48;5;138;38;5;16m▄[38;5;138m███[38;5;16m▄▄[48;5;16;38;5;145m▄[48;5;145m██[38;5;16m▄[49m▀[39m       [38;5;95m▀[38;5;16m▀▀▀[38;5;95m▀[39m   [00m
    [48;5;238;38;5;95m▄[38;5;138m▄[48;5;138m██████[48;5;240m▄[48;5;234;38;5;240m▄▄▄[48;5;240;38;5;138m▄[48;5;138m█████[48;5;95m▄[48;5;16;38;5;95m▄[48;5;145m▄[38;5;145m█████[38;5;95m▄[38;5;16m▄▄[48;5;16;38;5;138m▄[48;5;138m█[48;5;16m▄[48;5;145;38;5;16m▄▄▄[38;5;145m███[48;5;16m▄[49;38;5;16m▄[39m              [00m
     [38;5;16m▀[48;5;138m▄[48;5;16;38;5;138m▄[48;5;138;38;5;16m▄[38;5;138m█[38;5;238m▄[48;5;238;38;5;138m▄[48;5;138;38;5;16m▄[38;5;138m██[38;5;238m▄[48;5;238;38;5;138m▄[48;5;138m█████[38;5;95m▄[38;5;16m▄▄[48;5;16;38;5;145m▄[48;5;145m██[48;5;16m▄[38;5;16m█[38;5;138m▄[48;5;138m███[38;5;16m▄[38;5;138m██[48;5;16m▄[48;5;95;38;5;16m▄[48;5;16;38;5;95m▄▄[38;5;16m█[49m▀▀[39m              [00m
       [38;5;16m▀[48;5;138m▄[48;5;238m▄[48;5;138m▄[38;5;138m██[48;5;16m▄[48;5;238m▄[48;5;138m██[38;5;95m▄[38;5;138m██[48;5;95m▄[48;5;16;38;5;16m█[38;5;145m▄[48;5;145m███[38;5;95m▄▄[38;5;145m██[48;5;16m▄[48;5;138;38;5;16m▄▄[38;5;138m██[48;5;16;38;5;16m█[38;5;95m▄▄[48;5;95m██[38;5;16m▄[49m▀[39m                [00m
        [38;5;95m▄[48;5;95;38;5;145m▄[48;5;145m█[48;5;16m▄▄▄▄[48;5;138;38;5;16m▄▄[38;5;138m█[48;5;16m▄[48;5;95;38;5;16m▄[48;5;138m▄[38;5;138m█[48;5;16;38;5;16m█[38;5;145m▄[48;5;145;38;5;16m▄▄[38;5;145m█[48;5;95m▄[38;5;16m▄[49m▀▀[48;5;16m█[48;5;95;38;5;251m▄[48;5;16;38;5;95m▄▄▄[48;5;95m███[38;5;16m▄[49m▀[39m                 [00m
        [48;5;95;38;5;95m█[48;5;253;38;5;253m██[48;5;145m▄▄[38;5;145m█[48;5;16;38;5;16m█[49;39m  [48;5;16;38;5;95m▄[48;5;138;38;5;16m▄[48;5;16;38;5;253m▄[48;5;145m▄[48;5;16m▄▄[48;5;145;38;5;145m██[38;5;16m▄[49m▀[48;5;145m▄[48;5;16m█[49;39m   [38;5;16m▀[48;5;251m▄[48;5;95m▄[48;5;251m▄[48;5;95m▄[49m▀▀[39m                   [00m
        [48;5;95;38;5;95m█[48;5;145;38;5;231m▄[48;5;253;38;5;145m▄[38;5;231m▄[48;5;231;38;5;145m▄[48;5;16;38;5;16m█[49;39m [38;5;95m▄[48;5;95;38;5;253m▄[48;5;253m████[38;5;145m▄[48;5;145m██[38;5;16m▄[49m▀[39m                                [00m
         [38;5;95m▀[38;5;16m▀▀▀[39m  [48;5;95;38;5;16m▄[48;5;231m▄[48;5;145;38;5;231m▄[48;5;253;38;5;145m▄[48;5;145;38;5;231m▄[38;5;95m▄[38;5;16m▄▄[49m▀[39m                                  [00m
                 [38;5;16m▀▀▀▀[39m                                     [00m
                                                          [00m
