$$$
NAME: Elekid
OTHER NAMES: 電擊怪, 에레키드, エレキッド, Élekid
APPEARANCE: Pokémon Generation II
KIND: Electric Pokémon
GROUP: no-eggs
BALLOON: top
COAT: yellow
DISPLAY: full
BALLOON TOP: 3
BALLOON BOTTOM: 0
POSE: master
BASED ON: Elekid
SOURCE: PokéAPI Sprites
MEDIA: Pokémon
LICENSE: Copyright The Pokémon Company
FREE: no
WIDTH: 75
HEIGHT: 23


$$$
$balloon5$
               $\$                                                           [00m
                $\$                                                          [00m
                 $\$                                                         [00m
                  $\$                           [00m
                  [38;5;173m▄[48;5;94;38;5;221m▄[38;5;172m▄▄[49;38;5;233m▄[39m   [38;5;173m▄[48;5;94;38;5;221m▄[38;5;172m▄▄[49;38;5;94m▄[39m               [00m
        [38;5;248m▄[39m        [48;5;94;38;5;94m█[48;5;221;38;5;221m█[48;5;172;38;5;172m█[38;5;94m▄[38;5;172m█[48;5;233;38;5;233m█[49;39m  [48;5;94;38;5;94m█[48;5;221;38;5;221m█[48;5;172;38;5;172m█[38;5;94m▄[38;5;172m█[48;5;233;38;5;233m█[49;39m               [00m
      [38;5;248m▄[48;5;248;38;5;252m▄[48;5;231;38;5;231m█[48;5;233;38;5;233m█[49;38;5;94m▄[39m      [48;5;94;38;5;94m█[48;5;221;38;5;221m█[48;5;172;38;5;172m█[48;5;233;38;5;233m█[48;5;172;38;5;172m█[48;5;233;38;5;233m█[49;39m  [48;5;94;38;5;94m█[48;5;221;38;5;221m█[48;5;172;38;5;172m█[48;5;233;38;5;233m█[48;5;172;38;5;172m█[48;5;233;38;5;233m█[49;39m               [00m
   [38;5;248m▄[48;5;248;38;5;231m▄[38;5;242m▄[48;5;221;38;5;221m█[48;5;242m▄[48;5;233m▄[48;5;172m▄[48;5;178;38;5;242m▄[48;5;94;38;5;231m▄[49;38;5;248m▄[39m    [48;5;94;38;5;94m█[48;5;221;38;5;221m█[48;5;172;38;5;172m█[48;5;94m▄[48;5;172m█[48;5;233;38;5;233m█[49;39m  [48;5;94;38;5;172m▄[48;5;221;38;5;221m█[48;5;172;38;5;172m█[48;5;94m▄[48;5;172m█[49;38;5;94m▀[39m         [38;5;248m▄▄[39m    [00m
   [48;5;248;38;5;94m▄[48;5;231;38;5;233m▄[38;5;242m▄[48;5;242;38;5;172m▄[48;5;221;38;5;221m██[48;5;242m▄[48;5;231;38;5;172m▄[38;5;242m▄[48;5;233;38;5;233m█[49;39m    [48;5;94;38;5;94m█[48;5;221;38;5;221m█[48;5;172;38;5;172m███[48;5;94;38;5;94m█[49m▄[48;5;172m▄[48;5;221;38;5;221m█[48;5;172;38;5;172m███[48;5;94;38;5;94m█[49;39m      [38;5;248m▄[38;5;94m▄▄[48;5;248;38;5;233m▄[48;5;231;38;5;231m█[38;5;242m▄[48;5;242;38;5;172m▄[49;38;5;94m▄[39m  [00m
   [48;5;94;38;5;94m█[38;5;178m▄[48;5;221m▄[38;5;221m██[38;5;178m▄[48;5;178m██[48;5;172;38;5;172m█[48;5;233;38;5;233m█[49;39m    [48;5;94;38;5;94m█[48;5;221;38;5;178m▄[48;5;172;38;5;172m█[38;5;94m▄[48;5;94;38;5;172m▄[48;5;173m▄[48;5;172m█[48;5;94;38;5;94m█[48;5;221;38;5;221m█[48;5;172;38;5;172m██[38;5;173m▄[48;5;94;38;5;94m█[49m▄[39m    [48;5;248;38;5;248m█[48;5;231;38;5;231m█[48;5;242m▄[48;5;178;38;5;242m▄[48;5;172;38;5;221m▄[48;5;233m▄[48;5;178;38;5;178m██[38;5;242m▄[48;5;242;38;5;231m▄[49;38;5;233m▄[39m[00m
   [48;5;236;38;5;233m▄[38;5;236m█[48;5;178m▄[38;5;94m▄▄[38;5;236m▄▄[48;5;172m▄[48;5;236;38;5;233m▄[48;5;233m█[49;39m  [38;5;173m▄[48;5;173;38;5;221m▄[48;5;94;38;5;94m█[48;5;178;38;5;178m█[48;5;172;38;5;221m▄[48;5;173m▄[48;5;221m██[48;5;178m▄[48;5;94;38;5;94m█[48;5;221;38;5;178m▄[48;5;172;38;5;172m█[48;5;94;38;5;173m▄[48;5;172;38;5;172m██[48;5;173m▄[48;5;94;38;5;94m█[49;39m    [48;5;233;38;5;94m▄[48;5;242;38;5;178m▄[48;5;172;38;5;221m▄[48;5;221m███[48;5;242;38;5;172m▄[48;5;231;38;5;242m▄▄[48;5;233;38;5;233m█[49;39m[00m
   [48;5;233;38;5;233m█[48;5;94;38;5;172m▄[48;5;236;38;5;94m▄[38;5;236m███[38;5;94m▄[38;5;172m▄[48;5;233;38;5;94m▄[49;38;5;233m▀[39m [38;5;173m▄[48;5;172;38;5;221m▄[48;5;221m█[38;5;173m▄[38;5;221m███████[48;5;178m▄▄[48;5;221m██[48;5;172m▄[38;5;172m██[48;5;94;38;5;94m█[49;39m   [48;5;94;38;5;94m█[48;5;221;38;5;221m█████[38;5;178m▄[48;5;178m█[48;5;172;38;5;172m█[48;5;233;38;5;233m█[49;39m[00m
    [48;5;94;38;5;233m▄[48;5;172;38;5;236m▄▄[38;5;94m▄▄[48;5;173;38;5;236m▄[48;5;94m▄[49;38;5;233m▀[39m  [48;5;173;38;5;172m▄[48;5;221;38;5;221m█[38;5;172m▄[48;5;231;38;5;231m█[48;5;173m▄[48;5;221;38;5;173m▄[38;5;221m████[38;5;173m▄[38;5;178m▄[48;5;173;38;5;231m▄▄[38;5;178m▄[48;5;221;38;5;221m█[48;5;178m▄[48;5;172;38;5;172m██[48;5;94;38;5;94m█[49;39m  [48;5;236;38;5;236m█[48;5;242m▄[48;5;221m▄[38;5;172m▄▄[48;5;178m▄[38;5;94m▄[48;5;172;38;5;236m▄[48;5;236m█[48;5;233;38;5;233m█[49;39m[00m
     [48;5;236;38;5;233m▄[48;5;94;38;5;172m▄[48;5;236m▄▄[38;5;94m▄[48;5;233;38;5;173m▄[49;38;5;94m▄▄[48;5;94m█[48;5;221;38;5;221m██[48;5;178m▄[48;5;172m▄[48;5;233m▄[48;5;231m▄[48;5;221m███[48;5;172;38;5;231m▄[48;5;231;38;5;233m▄[38;5;231m█[38;5;172m▄[48;5;172;38;5;221m▄[48;5;221m███[48;5;172;38;5;172m██[48;5;94;38;5;94m█[49;39m  [48;5;94;38;5;94m█[38;5;172m▄[48;5;236m▄[38;5;94m▄[38;5;236m███[38;5;94m▄[38;5;233m▄[49m▀[39m[00m
      [38;5;233m▀[38;5;94m▀[48;5;94;38;5;233m▄[48;5;172m▄▄[38;5;94m▄[38;5;172m█[48;5;94;38;5;233m▄[48;5;221;38;5;221m████[48;5;94;38;5;178m▄[48;5;221;38;5;94m▄[48;5;94;38;5;221m▄▄[48;5;178;38;5;172m▄[48;5;221;38;5;221m█[38;5;94m▄[38;5;221m█████[38;5;178m▄[48;5;172;38;5;172m███[48;5;94m▄▄[38;5;94m█[48;5;236;38;5;236m█[48;5;172m▄[38;5;94m▄[38;5;172m██[38;5;94m▄[38;5;233m▄[49m▀[39m [00m
            [38;5;233m▀[48;5;233m█[48;5;178;38;5;172m▄[48;5;221;38;5;221m████████[48;5;94m▄[48;5;178m▄[48;5;221m████[38;5;172m▄[48;5;172m███[38;5;94m▄[38;5;233m▄▄[48;5;173m▄[48;5;242;38;5;172m▄[48;5;236m▄[38;5;242m▄[38;5;233m▄▄▄[49m▀[39m  [00m
             [38;5;233m▀[48;5;236;38;5;236m██[48;5;178;38;5;172m▄[48;5;221;38;5;178m▄[38;5;172m▄[48;5;236;38;5;236m████[48;5;221;38;5;221m█[38;5;178m▄▄[38;5;172m▄[48;5;178;38;5;236m▄▄[48;5;94m▄[48;5;236m███[48;5;94;38;5;94m█[49;39m   [38;5;94m▀[38;5;233m▀▀▀[39m     [00m
              [38;5;233m▀[48;5;172m▄[38;5;172m██[48;5;236;38;5;236m███[38;5;172m▄[48;5;172m█████[48;5;236m▄▄[48;5;94m▄[48;5;172;38;5;236m▄▄[48;5;94;38;5;94m█[49;39m             [00m
               [38;5;233m▀[48;5;233m█[48;5;172;38;5;172m█[48;5;236m▄▄▄[38;5;236m██[38;5;94m▄[48;5;172;38;5;172m██[48;5;236;38;5;236m████[38;5;233m▄[49m▀[39m              [00m
                 [48;5;233;38;5;233m█[48;5;94m▄[48;5;172m▄▄[48;5;236;38;5;236m█[38;5;172m▄[48;5;172m███[38;5;233m▄▄[48;5;233m██[49;39m                [00m
              [38;5;94m▄▄[48;5;233;38;5;236m▄[38;5;233m██[49;39m  [38;5;233m▀▀▀▀▀[39m [38;5;233m▀[48;5;233m█[38;5;236m▄[38;5;233m█[49;38;5;94m▄[39m              [00m
           [38;5;173m▄[48;5;173;38;5;172m▄[38;5;221m▄[48;5;178m▄▄[48;5;236;38;5;178m▄[38;5;172m▄[48;5;172m█[48;5;94;38;5;94m█[49;39m       [48;5;173;38;5;94m▄[48;5;233;38;5;172m▄[48;5;236;38;5;236m██[38;5;94m▄[48;5;94;38;5;178m▄[49;38;5;173m▄[39m            [00m
           [48;5;173;38;5;233m▄[48;5;221;38;5;221m███[38;5;178m▄[48;5;178;38;5;172m▄[48;5;172;38;5;233m▄▄[49;38;5;94m▀[39m       [48;5;94;38;5;94m█[48;5;172;38;5;178m▄[48;5;178;38;5;221m▄[48;5;221m████[48;5;94;38;5;94m█[49;39m           [00m
            [38;5;233m▀▀▀▀▀[39m           [38;5;233m▀[48;5;221;38;5;94m▄[38;5;233m▄▄▄[48;5;172;38;5;94m▄[49;38;5;233m▀[39m           [00m
                                              [00m
