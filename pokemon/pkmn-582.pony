$$$
NAME: Vanillite
OTHER NAMES: Sorbébé, 바닐프티, Gelatini, バニプッチ, 迷你冰
APPEARANCE: Pokémon Generation V
KIND: Fresh Snow Pokémon
GROUP: mineral
BALLOON: top
COAT: white
DISPLAY: full
BALLOON TOP: 3
BALLOON BOTTOM: 0
POSE: master
BASED ON: Vanillite
SOURCE: PokéAPI Sprites
MEDIA: Pokémon
LICENSE: Copyright The Pokémon Company
FREE: no
WIDTH: 63
HEIGHT: 21


$$$
$balloon5$
           $\$                                                   [00m
            $\$                                                  [00m
             $\$                                                 [00m
              $\$            [00m
              [38;5;103m▄[48;5;103;38;5;231m▄[48;5;60m▄▄[49;38;5;60m▄[39m        [00m
            [38;5;103m▄[48;5;103;38;5;231m▄[48;5;231m███[48;5;60;38;5;16m▄[49;39m         [00m
          [38;5;103m▄[48;5;103;38;5;231m▄[38;5;103m█[48;5;231;38;5;231m█████[48;5;16;38;5;60m▄[48;5;60;38;5;231m▄[49;38;5;60m▄[39m      [00m
        [38;5;103m▄[48;5;103;38;5;146m▄[48;5;231;38;5;231m███[48;5;146m▄[48;5;231m███[48;5;103m▄[48;5;231m███[48;5;60;38;5;146m▄[49;38;5;103m▄[39m    [00m
        [48;5;60;38;5;60m█[48;5;231;38;5;231m█████████████[48;5;16;38;5;16m█[49;39m    [00m
       [38;5;60m▄[48;5;60;38;5;146m▄[48;5;189;38;5;103m▄[48;5;231;38;5;231m███████████[48;5;146;38;5;60m▄[48;5;16;38;5;103m▄[49;39m    [00m
     [38;5;103m▄[48;5;103;38;5;231m▄[48;5;189m▄[48;5;231m██[48;5;146m▄[48;5;231m████████[48;5;189m▄[48;5;146m▄[48;5;189m▄[38;5;189m█[48;5;60m▄[49;38;5;16m▄[39m  [00m
    [38;5;103m▄[48;5;103;38;5;231m▄[48;5;231m███[38;5;117m▄[38;5;110m▄[38;5;231m█████████████[48;5;103m▄[48;5;60;38;5;60m█[49;39m [00m
   [38;5;103m▄[48;5;103;38;5;231m▄[48;5;231;38;5;111m▄[38;5;231m█[38;5;110m▄[48;5;110;38;5;117m▄[48;5;117;38;5;103m▄[38;5;237m▄[48;5;110;38;5;110m█[48;5;231;38;5;231m█████[38;5;110m▄[48;5;110;38;5;117m▄▄[48;5;231;38;5;110m▄[38;5;231m████[48;5;189m▄[48;5;103;38;5;60m▄[49;39m[00m
   [48;5;103;38;5;103m█[48;5;117;38;5;111m▄[38;5;117m█[48;5;111;38;5;111m█[48;5;110;38;5;110m█[48;5;117;38;5;117m█[48;5;237;38;5;237m█[48;5;60;38;5;60m██[48;5;231;38;5;231m█████[48;5;110;38;5;60m▄[48;5;237m▄[48;5;103;38;5;237m▄[48;5;117;38;5;117m█[48;5;110;38;5;110m█[48;5;231;38;5;117m▄[48;5;111m▄[48;5;231;38;5;111m▄[38;5;231m█[48;5;60;38;5;60m█[49;39m[00m
   [48;5;60;38;5;60m█[48;5;231;38;5;146m▄[48;5;111;38;5;231m▄[48;5;231m██[48;5;110;38;5;146m▄[48;5;117;38;5;110m▄[48;5;237m▄[48;5;117;38;5;231m▄[48;5;231m█████[48;5;60;38;5;117m▄[38;5;237m▄[48;5;237;38;5;117m▄[48;5;117;38;5;110m▄[48;5;146;38;5;231m▄[48;5;111m▄[48;5;117;38;5;111m▄[48;5;111;38;5;231m▄[48;5;231m█[48;5;16;38;5;16m█[49;39m[00m
    [48;5;60;38;5;60m█[48;5;189;38;5;146m▄[48;5;231;38;5;189m▄[38;5;231m████[48;5;60;38;5;103m▄[48;5;103;38;5;60m▄[48;5;231m▄▄[48;5;103m▄[48;5;60;38;5;103m▄[48;5;231;38;5;231m█[48;5;110m▄▄[48;5;231m████[38;5;146m▄[48;5;16;38;5;16m█[49;39m [00m
     [38;5;16m▀[48;5;146m▄[38;5;103m▄[48;5;189;38;5;146m▄[48;5;231m▄▄[38;5;189m▄[48;5;146;38;5;231m▄[48;5;103m▄▄[48;5;146m▄[48;5;231m███[38;5;189m▄[38;5;146m▄▄[48;5;189m▄[48;5;146;38;5;16m▄[49m▀[39m  [00m
       [38;5;60m▀[38;5;16m▀[48;5;103;38;5;60m▄[48;5;146;38;5;16m▄[38;5;103m▄[38;5;146m█[38;5;103m▄[38;5;146m████[38;5;103m▄[38;5;60m▄[38;5;16m▄[49m▀▀[39m    [00m
       [38;5;111m▄[48;5;111;38;5;117m▄[48;5;25m▄[48;5;146;38;5;60m▄▄[48;5;60;38;5;16m▄[48;5;146;38;5;103m▄[38;5;146m███[48;5;103;38;5;60m▄[48;5;146;38;5;16m▄▄[48;5;16;38;5;111m▄[49;38;5;25m▄[39m     [00m
      [38;5;25m▀[48;5;117;38;5;24m▄▄▄[48;5;25;38;5;25m█[48;5;117;38;5;231m▄[48;5;231m█[48;5;60;38;5;111m▄[48;5;16;38;5;117m▄[38;5;25m▄[38;5;117m▄[48;5;111m▄[38;5;111m█[48;5;25;38;5;24m▄[48;5;117;38;5;117m██[48;5;25m▄[49;38;5;24m▄[39m   [00m
          [48;5;25;38;5;111m▄[48;5;117;38;5;231m▄[48;5;111m▄[38;5;25m▄[48;5;117;38;5;111m▄[38;5;25m▄[48;5;25;38;5;111m▄[48;5;117;38;5;25m▄[48;5;111;38;5;24m▄[49m▀[38;5;16m▀▀[38;5;24m▀[39m    [00m
           [38;5;111m▀[48;5;231;38;5;24m▄▄[48;5;25;38;5;16m▄[48;5;111m▄▄[48;5;24m▄[49m▀[39m        [00m
                           [00m
