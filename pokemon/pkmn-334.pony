$$$
NAME: Altaria
OTHER NAMES: 파비코리, チルタリス, 七夕青鳥, Tyltalis
APPEARANCE: Pokémon Generation III
KIND: Humming Pokémon
GROUP: dragon, flying
BALLOON: top
COAT: blue
DISPLAY: full
BALLOON TOP: 3
BALLOON BOTTOM: 0
POSE: master
BASED ON: Altaria
SOURCE: PokéAPI Sprites
MEDIA: Pokémon
LICENSE: Copyright The Pokémon Company
FREE: no
WIDTH: 75
HEIGHT: 35


$$$
$balloon5$
                                          $\$                                     [00m
                                           $\$                                    [00m
                                            $\$                                   [00m
                                             $\$               [00m
                                            [38;5;60m▄▄[48;5;60;38;5;16m▄▄[49m▀[39m            [00m
                                         [38;5;60m▄▄[48;5;60;38;5;74m▄[48;5;74;38;5;16m▄[49m▀[39m               [00m
                                     [38;5;60m▄▄[48;5;60;38;5;74m▄▄[48;5;74;38;5;16m▄▄[49m▀[39m              [38;5;60m▄▄[38;5;16m▄[39m[00m
                               [38;5;60m▄▄▄[48;5;60;38;5;74m▄▄▄[48;5;74;38;5;16m▄▄[49m▀▀[39m              [38;5;60m▄[48;5;60;38;5;74m▄▄[48;5;74;38;5;16m▄[49m▀[39m [00m
                            [38;5;60m▄[48;5;60;38;5;74m▄[48;5;74;38;5;60m▄[38;5;16m▄▄[49m▀▀▀▀[39m [38;5;74m▄▄▄▄▄[39m       [38;5;60m▄▄▄[48;5;60;38;5;74m▄▄[48;5;74;38;5;16m▄[49m▀▀[39m   [00m
                           [48;5;60;38;5;60m█[48;5;74m▄[48;5;60;38;5;16m▄[49m▀[39m [38;5;74m▄▄[48;5;74;38;5;74m▄▄▄▄[48;5;74m█████[48;5;74m▄▄▄[48;5;60m▄▄[38;5;74m▄▄[48;5;74m██[38;5;16m▄▄[49m▀[39m      [00m
                         [38;5;60m▄▄[48;5;237m▄[48;5;60m█[48;5;16;38;5;74m▄[49m▄[48;5;74m█[48;5;74;38;5;16m▄[48;5;74m▄[49;38;5;237m▀[38;5;16m▀▀▀[38;5;237m▀[48;5;74;38;5;16m▄[48;5;74m▄▄▄▄▄▄[48;5;74m▄▄▄[49m▀▀▀[39m         [00m
                      [38;5;74m▄[48;5;74;38;5;74m▄[38;5;117m▄[48;5;117m████[48;5;74m▄[38;5;74m█[48;5;60m▄[48;5;237m▄[49;38;5;237m▄[39m                           [00m
                     [38;5;60m▄[48;5;74;38;5;74m█[48;5;74;38;5;74m██[48;5;117m▄▄▄▄[48;5;74m█████[48;5;60;38;5;60m█[49;39m                          [00m
                    [38;5;103m▄[48;5;60;38;5;60m█[48;5;231;38;5;16m▄[48;5;74;38;5;74m███████[38;5;16m▄[48;5;74;38;5;231m▄[48;5;74;38;5;74m███[48;5;237;38;5;16m▄[49;39m                         [00m
                   [48;5;103;38;5;60m▄[48;5;231;38;5;254m▄[48;5;103;38;5;231m▄[48;5;16;38;5;60m▄[48;5;60;38;5;231m▄▄▄[48;5;74;38;5;254m▄[48;5;74;38;5;60m▄[38;5;74m██[48;5;16;38;5;60m▄[38;5;237m▄[48;5;74;38;5;74m███[48;5;16;38;5;16m█[49;39m                         [00m
                    [38;5;60m▀[48;5;103;38;5;237m▄[48;5;231;38;5;60m▄[38;5;231m███[38;5;249m▄[48;5;254;38;5;103m▄[48;5;249m▄[48;5;103;38;5;231m▄[48;5;231m██[48;5;254;38;5;254m█[48;5;74;38;5;249m▄[38;5;16m▄[49m▀[39m                         [00m
                    [38;5;103m▄▄[48;5;237m▄[48;5;103;38;5;237m▄[38;5;60m▄[48;5;249m▄[48;5;60;38;5;74m▄▄[48;5;74m█[48;5;103m▄[48;5;254m▄▄[48;5;103;38;5;16m▄[49m▀[39m                           [00m
                  [38;5;103m▄[48;5;103;38;5;189m▄[48;5;189m████[48;5;16;38;5;16m█[48;5;74;38;5;74m██████[38;5;16m▄[48;5;16;38;5;189m▄[48;5;103m▄▄[49;38;5;103m▄[39m   [38;5;103m▄▄▄▄[39m                  [00m
             [38;5;103m▄▄▄[48;5;103;38;5;189m▄[38;5;103m█[48;5;189;38;5;189m█[38;5;231m▄▄[38;5;189m███[48;5;237;38;5;60m▄[48;5;74;38;5;74m█████[38;5;16m▄[48;5;16;38;5;189m▄[48;5;189m████[48;5;103m▄[49;38;5;103m▄[48;5;103;38;5;254m▄[48;5;189m▄▄[38;5;189m██[48;5;103m▄[49;38;5;103m▄[39m                [00m
           [38;5;103m▄[48;5;103;38;5;231m▄[48;5;231m████[48;5;189;38;5;254m▄[48;5;254;38;5;231m▄[48;5;231m███[48;5;189;38;5;189m█[48;5;60;38;5;60m█[48;5;74;38;5;74m▄[38;5;74m████[38;5;16m▄[48;5;16;38;5;189m▄[48;5;189m█████[48;5;103;38;5;103m█[48;5;254;38;5;231m▄[48;5;231m███[48;5;254m▄[48;5;189;38;5;254m▄[38;5;189m█[48;5;103m▄[38;5;103m█[38;5;189m▄▄▄[49;38;5;103m▄▄[39m          [00m
          [48;5;103;38;5;103m█[48;5;254;38;5;254m█[48;5;231;38;5;231m█████████[48;5;254;38;5;60m▄[48;5;60;38;5;74m▄[48;5;74m███[48;5;74m▄▄[38;5;74m█[48;5;16;38;5;16m█[48;5;189;38;5;189m█[38;5;103m▄▄[48;5;103;38;5;231m▄▄[38;5;103m█[48;5;254m▄[48;5;231;38;5;231m██████[48;5;189;38;5;189m██[48;5;103m▄[48;5;189m█████[48;5;103;38;5;103m█[49;39m         [00m
         [48;5;103;38;5;103m█[48;5;189;38;5;189m██[48;5;231;38;5;254m▄[38;5;231m██████[38;5;74m▄[48;5;74;38;5;74m▄[48;5;74m███████[48;5;16;38;5;16m█[48;5;189;38;5;103m▄[48;5;103;38;5;231m▄[48;5;231m██████[48;5;254m▄[48;5;231m███[48;5;254m▄[48;5;189m▄[38;5;254m▄[38;5;189m████████[48;5;103;38;5;103m█[49;39m        [00m
        [38;5;103m▄[48;5;103;38;5;189m▄[48;5;189m███[48;5;254m▄[48;5;231m▄▄[38;5;231m█[38;5;254m▄[48;5;74;38;5;60m▄[48;5;74;38;5;74m████████[38;5;74m▄[48;5;74m█[48;5;16m▄[48;5;189;38;5;16m▄[48;5;231;38;5;103m▄[38;5;189m▄[38;5;231m███████████[48;5;254m▄[48;5;189;38;5;189m██████[38;5;103m▄[48;5;103m█[49;39m        [00m
       [48;5;103;38;5;103m█[48;5;189;38;5;189m████[38;5;103m▄[48;5;103;38;5;231m▄[48;5;231m█████[48;5;103;38;5;103m█[48;5;74m▄[48;5;103;38;5;231m▄▄▄▄▄[48;5;74;38;5;103m▄[48;5;74m▄[38;5;74m██[48;5;103;38;5;103m█[48;5;189;38;5;231m▄[48;5;231m█████████████[48;5;189;38;5;189m█████[48;5;103m▄[48;5;189m██[48;5;103m▄[49;38;5;103m▄[39m      [00m
       [48;5;103;38;5;103m█[48;5;189;38;5;189m█[38;5;249m▄[38;5;103m▄[48;5;103;38;5;231m▄[48;5;231m██████[48;5;103m▄[48;5;231m████████[48;5;254m▄[48;5;103;38;5;254m▄[48;5;74;38;5;103m▄▄[48;5;103;38;5;189m▄[48;5;189;38;5;231m▄▄[48;5;231;38;5;189m▄[38;5;231m████████[48;5;254;38;5;189m▄[48;5;189;38;5;254m▄[38;5;189m█████████[48;5;103;38;5;103m█[49;39m      [00m
       [38;5;103m▀[48;5;249m▄[48;5;103m█[38;5;189m▄[48;5;231;38;5;231m█████████████████[48;5;103;38;5;103m█[48;5;254;38;5;231m▄[48;5;231m████████[38;5;189m▄[48;5;189;38;5;231m▄[48;5;231;38;5;189m▄[48;5;254m▄[48;5;189;38;5;254m▄[48;5;254;38;5;189m▄[48;5;189;38;5;254m▄[38;5;189m█████████[48;5;103;38;5;103m█[49;39m      [00m
         [48;5;103;38;5;103m█[48;5;189;38;5;189m█[48;5;254m▄[48;5;231;38;5;231m███████████████████████████[48;5;189;38;5;254m▄[38;5;189m█████████████[48;5;103;38;5;103m█[49;39m       [00m
     [38;5;60m▄▄[39m [48;5;103;38;5;103m█[48;5;189;38;5;189m███[48;5;231;38;5;254m▄[38;5;231m███[38;5;254m▄[48;5;254;38;5;231m▄[48;5;231m████████████████████[38;5;254m▄[48;5;189;38;5;189m███████████[38;5;103m▄▄[49m▀[39m        [00m
    [48;5;60;38;5;60m█[48;5;117;38;5;74m▄[38;5;74m▄[48;5;60;38;5;74m▄[48;5;103;38;5;103m█[48;5;189;38;5;189m█████[48;5;254m▄[48;5;231m▄[48;5;254m▄[48;5;231;38;5;254m▄[38;5;231m█████[38;5;254m▄▄[48;5;254;38;5;189m▄[48;5;189m█[48;5;231m▄[38;5;254m▄[38;5;231m██████[38;5;254m▄[38;5;189m▄[48;5;254m▄[48;5;189m███████████[48;5;103;38;5;103m█[49m▀[39m          [00m
   [38;5;60m▄[48;5;16;38;5;74m▄[48;5;237m▄[48;5;74;38;5;74m██[48;5;103m▄[48;5;189;38;5;103m▄[38;5;189m████████[48;5;254m▄[48;5;231m▄▄▄[48;5;254m▄[48;5;189m███████[48;5;254m▄[48;5;231m▄▄[48;5;254m▄[48;5;189m██████████████[38;5;103m▄[49m▀[39m           [00m
   [48;5;60;38;5;16m▄[48;5;74m▄[49m▀▀[48;5;74m▄[38;5;74m█[48;5;103m▄[48;5;189;38;5;103m▄[38;5;189m████[38;5;249m▄[38;5;189m█████████[38;5;60m▄▄▄[48;5;60;38;5;237m▄[38;5;74m▄▄[48;5;189;38;5;60m▄[38;5;189m██████[48;5;103m▄[48;5;189;38;5;103m▄[38;5;189m██[38;5;103m▄▄▄▄[49m▀▀[39m             [00m
        [48;5;16;38;5;16m█[48;5;74;38;5;74m█[48;5;237;38;5;16m▄[49;38;5;103m▀[48;5;189m▄▄▄[48;5;103m█[48;5;189;38;5;189m████████[48;5;16m▄[48;5;117;38;5;16m▄[48;5;74;38;5;74m▄[48;5;237;38;5;16m▄[48;5;117m▄[48;5;74;38;5;237m▄[48;5;237;38;5;74m▄[48;5;60m▄[48;5;189;38;5;16m▄[38;5;189m██████[38;5;103m▄[48;5;103;38;5;74m▄[48;5;60m▄[48;5;237m▄[48;5;60;38;5;237m▄[38;5;60m█[48;5;16m▄▄▄▄[49;38;5;237m▄[38;5;16m▄▄[38;5;60m▄[39m        [00m
         [38;5;16m▀[39m      [38;5;103m▀[48;5;189m▄[38;5;189m████████[48;5;16;38;5;237m▄[48;5;74;38;5;60m▄[38;5;74m█[48;5;237;38;5;16m▄[48;5;117m▄[48;5;60m▄[48;5;16;38;5;189m▄[48;5;189m███[38;5;103m▄▄[38;5;237m▄[48;5;103;38;5;60m▄[48;5;237m▄[48;5;74;38;5;16m▄[38;5;237m▄[38;5;74m██[48;5;16m▄[38;5;237m▄▄[49;38;5;16m▀[48;5;60m▄▄[38;5;74m▄[48;5;74m█[48;5;60m▄▄▄[49;38;5;60m▄▄[39m   [00m
                  [38;5;103m▀▀[48;5;189m▄▄▄▄▄▄[49m▀[48;5;237;38;5;237m█[48;5;74;38;5;74m██[48;5;16;38;5;16m█[49;38;5;103m▀▀▀▀▀[38;5;16m▀[48;5;60m▄▄[49m▀[48;5;60m▄[38;5;60m█[48;5;16m▄▄[48;5;237;38;5;16m▄[48;5;74;38;5;237m▄[38;5;74m██[48;5;237m▄[49;38;5;237m▄[39m [38;5;16m▀▀[48;5;74m▄[38;5;237m▄[38;5;74m███[48;5;60m▄[49;38;5;16m▄[39m [00m
                           [48;5;237;38;5;16m▄[48;5;74m▄[49m▀[39m           [38;5;16m▀[48;5;60m▄▄[48;5;16m█[49m▀▀[48;5;74m▄[38;5;74m██[48;5;16;38;5;16m█[49;39m    [38;5;16m▀▀▀▀▀[39m [00m
                                                [38;5;16m▀▀[39m           [00m
                                                             [00m
