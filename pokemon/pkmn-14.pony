$$$
NAME: Kakuna
OTHER NAMES: 鐵殼蛹, 딱충이, Coconfort, Kokuna, コクーン, Cocoon
APPEARANCE: Pokémon Generation I
KIND: Cocoon Pokémon
GROUP: bug
BALLOON: top
COAT: yellow
DISPLAY: full
BALLOON TOP: 3
BALLOON BOTTOM: 0
POSE: master
BASED ON: Kakuna
SOURCE: PokéAPI Sprites
MEDIA: Pokémon
LICENSE: Copyright The Pokémon Company
FREE: no
WIDTH: 63
HEIGHT: 23


$$$
$balloon5$
    $\$                                                          [00m
     $\$                                                         [00m
      $\$                                                        [00m
       $\$                [00m
        $\$  [38;5;179m▄[38;5;136m▄▄▄▄[39m        [00m
       [38;5;179m▄▄[48;5;179;38;5;221m▄[38;5;230m▄[48;5;221m▄▄[38;5;221m███[48;5;136m▄[38;5;179m▄[49;38;5;136m▄▄[39m    [00m
     [38;5;179m▄[48;5;179;38;5;221m▄[48;5;221;38;5;230m▄[48;5;230m██████[48;5;221;38;5;221m████[48;5;179m▄[38;5;179m█[48;5;94m▄[49;38;5;94m▄[39m  [00m
    [48;5;179;38;5;136m▄[48;5;221;38;5;221m█[48;5;230;38;5;230m██████[38;5;221m▄[48;5;221m██████[48;5;179m▄[38;5;179m██[48;5;58;38;5;94m▄[49;39m [00m
   [48;5;136;38;5;58m▄[48;5;179;38;5;242m▄[48;5;221;38;5;221m██[48;5;230m▄▄▄[48;5;221m███[38;5;179m▄[48;5;179;38;5;237m▄[48;5;237;38;5;233m▄▄[48;5;179m▄[48;5;221;38;5;221m██[48;5;179;38;5;179m███[48;5;58;38;5;58m█[49;39m[00m
   [48;5;58;38;5;58m█[48;5;237;38;5;231m▄[48;5;179;38;5;237m▄[48;5;221;38;5;221m██████[38;5;179m▄[48;5;237;38;5;231m▄[48;5;242;38;5;242m█[48;5;237;38;5;237m█[48;5;233m▄[38;5;233m█[48;5;221;38;5;221m█[38;5;179m▄[48;5;179m██[38;5;136m▄[48;5;233;38;5;233m█[49;39m[00m
   [38;5;94m▀[48;5;237m▄[38;5;179m▄[48;5;179;38;5;221m▄[48;5;221m█████[48;5;237m▄▄▄▄[48;5;179;38;5;179m█[48;5;221;38;5;136m▄[48;5;179m▄▄[48;5;136;38;5;179m▄[38;5;233m▄[49m▀[39m [00m
     [48;5;94;38;5;136m▄[48;5;136;38;5;94m▄[48;5;221;38;5;136m▄▄▄▄▄[48;5;136m██[38;5;179m▄▄▄[48;5;179;38;5;58m▄▄[48;5;233;38;5;221m▄[38;5;179m▄[48;5;58m▄[49;38;5;58m▄[39m [00m
   [38;5;94m▄[48;5;94;38;5;230m▄[48;5;221;38;5;221m█[48;5;136;38;5;179m▄[48;5;94;38;5;94m█[48;5;179;38;5;221m▄▄[48;5;136;38;5;179m▄[38;5;136m█[48;5;179;38;5;58m▄▄[48;5;58;38;5;179m▄[38;5;221m▄[48;5;94m▄[48;5;221m███[38;5;179m▄[48;5;179m█[38;5;136m▄[48;5;58;38;5;233m▄[49;39m[00m
   [38;5;58m▀[48;5;136m▄[48;5;179m▄[38;5;94m▄[38;5;179m█[48;5;94;38;5;58m▄[48;5;221;38;5;179m▄[48;5;179;38;5;58m▄[48;5;233m▄[48;5;58;38;5;179m▄[48;5;94m▄[48;5;179;38;5;94m▄▄[38;5;58m▄[38;5;233m▄[48;5;136m▄[38;5;58m▄[38;5;94m▄[48;5;58;38;5;179m▄[48;5;233;38;5;233m█[49;39m [00m
    [48;5;94;38;5;58m▄[48;5;221;38;5;230m▄[38;5;179m▄[48;5;94;38;5;136m▄[48;5;221;38;5;221m█[48;5;58;38;5;179m▄[48;5;179m██[48;5;58;38;5;94m▄[48;5;179;38;5;179m█[48;5;221;38;5;221m█████[38;5;179m▄[48;5;179;38;5;233m▄[48;5;233;38;5;179m▄[38;5;233m█[49;39m [00m
    [48;5;58;38;5;58m█[48;5;221m▄[48;5;136;38;5;136m█[48;5;221;38;5;221m█[38;5;230m▄[48;5;230m█[48;5;179;38;5;179m███[48;5;58;38;5;58m█[38;5;179m▄[48;5;179;38;5;94m▄[48;5;221;38;5;58m▄[48;5;179m▄[48;5;94;38;5;179m▄[48;5;58m▄[48;5;179m██[48;5;233;38;5;233m█[49;39m [00m
    [48;5;58;38;5;94m▄[48;5;221;38;5;221m█[48;5;136;38;5;136m█[48;5;221;38;5;221m█[48;5;230;38;5;230m██[48;5;179;38;5;179m███[48;5;58;38;5;58m█[48;5;179;38;5;221m▄[48;5;221m██[48;5;136;38;5;136m█[48;5;179;38;5;179m███[38;5;233m▄[49m▀[39m [00m
     [48;5;94;38;5;58m▄[48;5;221;38;5;221m█[48;5;136;38;5;94m▄[48;5;221;38;5;221m█[48;5;230m▄[48;5;179;38;5;179m██[48;5;94;38;5;58m▄[48;5;179;38;5;179m█[48;5;221;38;5;221m██[48;5;136;38;5;136m█[48;5;179;38;5;179m████[48;5;233;38;5;233m█[49;39m  [00m
     [38;5;58m▀[48;5;221m▄[48;5;58m█[48;5;94;38;5;221m▄[48;5;221;38;5;94m▄[48;5;179;38;5;179m█[48;5;94;38;5;58m▄[48;5;179;38;5;179m█[48;5;58m▄[48;5;221;38;5;58m▄[48;5;136;38;5;136m█[48;5;179;38;5;179m████[48;5;233;38;5;233m█[49;39m   [00m
      [48;5;58;38;5;58m█[48;5;94;38;5;94m█[48;5;221;38;5;179m▄[38;5;136m▄[48;5;94m▄[48;5;179m▄[48;5;221;38;5;179m▄[48;5;179m██[48;5;58m▄[48;5;179;38;5;58m▄[38;5;179m██[38;5;233m▄[49m▀[39m   [00m
       [48;5;58;38;5;58m█[48;5;136;38;5;94m▄[38;5;230m▄[48;5;221;38;5;221m██[48;5;136;38;5;179m▄[48;5;94m▄[48;5;179;38;5;94m▄[38;5;179m█[48;5;58m▄[48;5;179;38;5;58m▄[38;5;233m▄[49m▀[39m    [00m
        [48;5;58;38;5;58m█[48;5;221;38;5;136m▄[48;5;136;38;5;221m▄▄[48;5;179;38;5;94m▄[38;5;179m██[48;5;94m▄[48;5;179;38;5;58m▄[48;5;233;38;5;233m█[49m▀[39m     [00m
         [38;5;58m▀[48;5;221m▄[38;5;179m▄[48;5;179m█[48;5;94m▄[48;5;179;38;5;58m▄[38;5;233m▄[49m▀[39m       [00m
           [38;5;58m▀[48;5;179m▄[38;5;233m▄[49m▀[39m         [00m
                        [00m
