$$$
NAME: Pineco
OTHER NAMES: Tannza, クヌギダマ, 榛果球, Kunugidama, 피콘, Pomdepik
APPEARANCE: Pokémon Generation II
KIND: Bagworm Pokémon
GROUP: bug
BALLOON: top
COAT: gray
DISPLAY: full
BALLOON TOP: 3
BALLOON BOTTOM: 0
POSE: master
BASED ON: Pineco
SOURCE: PokéAPI Sprites
MEDIA: Pokémon
LICENSE: Copyright The Pokémon Company
FREE: no
WIDTH: 70
HEIGHT: 24


$$$
$balloon5$
                 $\$                                                    [00m
                  $\$                                                   [00m
                   $\$                                                  [00m
                    $\$                 [00m
                    [38;5;67m▄[48;5;67;38;5;116m▄[49;38;5;60m▄[39m               [00m
               [38;5;73m▄▄▄[39m [38;5;67m▄[48;5;67;38;5;116m▄[48;5;116m█[48;5;60m▄[49;38;5;60m▄[39m              [00m
       [38;5;67m▄[48;5;67;38;5;116m▄▄▄▄[49;38;5;67m▄▄[48;5;73;38;5;116m▄[48;5;116m███[48;5;73m▄[48;5;67;38;5;60m▄[48;5;116;38;5;116m███[48;5;60;38;5;60m█[49;39m  [38;5;73m▄▄[48;5;73;38;5;116m▄▄▄▄[49;38;5;73m▄[39m     [00m
      [38;5;60m▄[48;5;67m▄[48;5;116m▄▄▄▄▄[38;5;73m▄[48;5;67m▄[48;5;116;38;5;67m▄[38;5;116m█[38;5;73m▄[48;5;60;38;5;60m█[48;5;116;38;5;116m████[48;5;73;38;5;73m█[48;5;60;38;5;60m█[48;5;73;38;5;116m▄[48;5;116m██[38;5;60m▄[38;5;233m▄▄[48;5;67m▄▄[38;5;73m▄▄▄[49;38;5;67m▄[39m [00m
     [48;5;60;38;5;233m▄[48;5;73;38;5;73m███████[48;5;60;38;5;116m▄[38;5;159m▄[48;5;73;38;5;233m▄[48;5;67;38;5;73m▄[48;5;73;38;5;60m▄[48;5;60;38;5;73m▄[48;5;73m█[48;5;116m▄▄[48;5;73m█[38;5;60m▄[48;5;60m█[38;5;159m▄[38;5;116m▄[38;5;73m▄[48;5;73m█████[48;5;60m▄[48;5;233m▄[48;5;73;38;5;60m▄[49;38;5;67m▀[39m [00m
      [38;5;233m▀▀[48;5;60m▄[38;5;60m███[48;5;67m▄[48;5;159;38;5;67m▄[38;5;159m██[48;5;233;38;5;233m█[48;5;60;38;5;60m█[48;5;73;38;5;73m███[38;5;60m▄[48;5;60;38;5;159m▄[48;5;159m██[38;5;67m▄[38;5;60m▄[48;5;67m▄[48;5;60m████[48;5;67m▄[48;5;73;38;5;233m▄▄[49m▀[39m  [00m
         [38;5;233m▀[48;5;60m▄[38;5;60m███[48;5;67m▄[48;5;159;38;5;159m██[48;5;233;38;5;67m▄[48;5;73;38;5;233m▄[38;5;73m█[48;5;60;38;5;60m█[48;5;159;38;5;159m██[38;5;60m▄[48;5;60m██████[38;5;233m▄[49m▀▀[39m     [00m
    [38;5;60m▄▄▄[48;5;60;38;5;116m▄▄▄[48;5;233m▄[38;5;233m█[48;5;60m▄[48;5;233;38;5;252m▄▄[48;5;60;38;5;88m▄[48;5;159;38;5;73m▄[38;5;159m█[48;5;233;38;5;67m▄[48;5;60m▄[48;5;159;38;5;159m█[38;5;116m▄[48;5;67;38;5;233m▄[48;5;60;38;5;88m▄[48;5;233;38;5;252m▄▄[48;5;60;38;5;233m▄[38;5;60m██[38;5;233m▄[48;5;233;38;5;60m▄[49m▄▄▄[39m    [00m
   [48;5;60;38;5;60m█[48;5;116;38;5;116m██[38;5;73m▄[38;5;67m▄▄[48;5;73;38;5;233m▄[48;5;67m▄[48;5;233;38;5;60m▄[48;5;252;38;5;231m▄[48;5;231m█[48;5;203;38;5;203m█[38;5;233m▄[48;5;60;38;5;60m█[48;5;116;38;5;73m▄[48;5;159;38;5;159m██[48;5;116;38;5;73m▄[48;5;60;38;5;60m█[48;5;88;38;5;233m▄[48;5;203m▄[38;5;203m█[48;5;231;38;5;231m█[48;5;252m▄[48;5;233;38;5;233m█[48;5;60;38;5;60m█[48;5;233m▄[48;5;73m▄[38;5;67m▄[38;5;73m██[48;5;60;38;5;116m▄▄▄[49;38;5;233m▄[39m[00m
    [48;5;60;38;5;233m▄[48;5;67m▄▄[48;5;233;38;5;73m▄[38;5;159m▄[48;5;73;38;5;116m▄[48;5;116m█[48;5;73m▄[48;5;67;38;5;73m▄[48;5;252m▄[48;5;203m▄[48;5;233m▄[48;5;67m▄[48;5;73m████[48;5;60m▄[48;5;233m▄▄[48;5;252m▄[48;5;67m▄▄[48;5;73m█[38;5;116m▄[48;5;233m▄▄[48;5;60;38;5;233m▄▄[38;5;60m█[48;5;67m▄[48;5;116m▄▄[48;5;233;38;5;233m█[49;39m[00m
    [38;5;233m▄[48;5;60;38;5;60m█[48;5;159m▄[38;5;159m███[48;5;116m▄▄[38;5;60m▄[48;5;67m▄[48;5;60;38;5;73m▄[48;5;116;38;5;116m█[48;5;73m▄▄▄▄▄[48;5;116m██[48;5;60;38;5;233m▄[48;5;73;38;5;60m▄[48;5;116m▄[38;5;159m▄▄▄[48;5;159m███[48;5;116m▄[48;5;233m▄[38;5;60m▄[48;5;60;38;5;233m▄[49m▀[39m [00m
    [38;5;233m▀[48;5;60m▄[48;5;67m▄[48;5;60;38;5;67m▄▄[48;5;159;38;5;60m▄▄[48;5;60m█[38;5;233m▄[48;5;233;38;5;116m▄[48;5;116;38;5;159m▄[48;5;159m███████[48;5;116;38;5;116m█[48;5;233;38;5;73m▄[48;5;60;38;5;233m▄[48;5;67;38;5;67m█[48;5;60;38;5;73m▄[48;5;159;38;5;67m▄[38;5;159m██[38;5;116m▄[38;5;60m▄[48;5;116m▄[48;5;60;38;5;67m▄▄[48;5;233;38;5;233m█[49;39m  [00m
     [38;5;60m▄[48;5;60;38;5;116m▄[48;5;233;38;5;73m▄▄[48;5;67;38;5;233m▄▄[48;5;233m█[48;5;60;38;5;60m█[38;5;73m▄[48;5;67m▄▄▄▄▄▄▄▄[48;5;60m▄[38;5;60m█[48;5;233;38;5;233m██[48;5;67m▄[48;5;73;38;5;67m▄[48;5;60;38;5;73m▄[38;5;60m█[38;5;67m▄[48;5;67;38;5;233m▄▄[48;5;233;38;5;73m▄[38;5;233m█[49;39m   [00m
    [48;5;60;38;5;60m█[48;5;116m▄▄[38;5;116m█[48;5;73m▄[38;5;73m█[38;5;60m▄[48;5;233m▄[48;5;67;38;5;233m▄▄[48;5;73m▄▄▄▄▄▄▄▄[48;5;67m▄▄[48;5;233;38;5;60m▄[48;5;60;38;5;233m▄[38;5;60m█[48;5;233m▄[38;5;73m▄▄▄[48;5;73m█[38;5;116m▄▄[38;5;60m▄[48;5;233;38;5;233m█[49;39m  [00m
    [38;5;233m▀[48;5;73m▄▄[48;5;60;38;5;73m▄▄[38;5;60m█[48;5;67;38;5;233m▄[48;5;233;38;5;73m▄▄[48;5;73m██[48;5;67m▄▄▄[38;5;60m▄[48;5;60;38;5;67m▄[38;5;73m▄▄▄[48;5;67m▄▄[48;5;60;38;5;67m▄[48;5;233m▄[48;5;67;38;5;233m▄[48;5;60;38;5;67m▄[48;5;73;38;5;60m▄[48;5;116m▄▄[48;5;60;38;5;73m▄▄[48;5;73;38;5;233m▄[49m▀[39m  [00m
       [38;5;60m▀[38;5;233m▀[48;5;233m█[48;5;60;38;5;73m▄▄[48;5;73;38;5;60m▄▄▄[38;5;73m██[48;5;67;38;5;60m▄[48;5;233;38;5;233m█[48;5;60;38;5;60m█[48;5;73m▄[38;5;73m██████[48;5;233;38;5;60m▄[48;5;67;38;5;233m▄[48;5;60m▄[48;5;73m▄▄[48;5;233;38;5;60m▄[38;5;233m█[49;39m    [00m
        [48;5;233;38;5;233m█[48;5;60;38;5;60m█[48;5;233m▄▄[48;5;73;38;5;233m▄▄▄[48;5;60;38;5;67m▄▄[48;5;233;38;5;233m█[48;5;60;38;5;60m█[48;5;233;38;5;233m█[48;5;60;38;5;60m██[48;5;73m▄▄[38;5;67m▄[48;5;60;38;5;73m▄▄[48;5;67;38;5;67m█[38;5;233m▄[48;5;233m█[48;5;60;38;5;60m████[48;5;233;38;5;233m█[49;39m   [00m
        [38;5;233m▀[48;5;60m▄▄▄▄[49m▀[39m [38;5;233m▀▀▀[48;5;60m▄[38;5;60m█[48;5;233m▄[48;5;60;38;5;233m▄[48;5;73m▄▄▄[49m▀▀▀[39m [38;5;233m▀[48;5;60m▄▄▄▄[49m▀[39m   [00m
                  [48;5;233;38;5;233m█[48;5;60;38;5;67m▄[38;5;60m████[48;5;233;38;5;233m█[49;39m             [00m
                   [48;5;60;38;5;233m▄[48;5;67;38;5;67m█[48;5;60m▄[38;5;60m█[48;5;233;38;5;233m█[49;39m              [00m
                    [38;5;233m▀▀▀[39m               [00m
                                      [00m
