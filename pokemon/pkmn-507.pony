$$$
NAME: Herdier
OTHER NAMES: 하데리어, Terribark, Ponchien, ハーデリア, 哈約克
APPEARANCE: Pokémon Generation V
KIND: Loyal Dog Pokémon
GROUP: ground
BALLOON: top
COAT: gray
DISPLAY: full
BALLOON TOP: 3
BALLOON BOTTOM: 0
POSE: master
BASED ON: Herdier
SOURCE: PokéAPI Sprites
MEDIA: Pokémon
LICENSE: Copyright The Pokémon Company
FREE: no
WIDTH: 75
HEIGHT: 30


$$$
$balloon5$
       $\$                                                                      [00m
        $\$                                                                     [00m
         $\$                                                                    [00m
          $\$                                           [00m
           $\$[38;5;95m▄▄[39m                                        [00m
            [48;5;95;38;5;95m█[48;5;173;38;5;173m█[48;5;95;38;5;95m█[49;39m            [38;5;95m▄▄[38;5;236m▄[39m                        [00m
           [48;5;95;38;5;95m█[48;5;173;38;5;173m███[48;5;95;38;5;95m█[49;39m         [38;5;95m▄[48;5;95;38;5;173m▄[48;5;173m██[48;5;16;38;5;16m█[49;39m                        [00m
          [38;5;95m▄[48;5;95;38;5;173m▄[48;5;173m███[38;5;95m▄[48;5;236;38;5;223m▄[49;38;5;16m▄[39m     [38;5;95m▄[48;5;95;38;5;173m▄[48;5;173m█[38;5;95m▄[38;5;173m██[48;5;16;38;5;16m█[49;39m                        [00m
       [48;5;95;38;5;95m█[38;5;173m▄[49;38;5;95m▄[48;5;95m█[48;5;173;38;5;173m███[48;5;236;38;5;236m█[48;5;223;38;5;223m██[48;5;16;38;5;16m█[49;39m    [38;5;95m▄[48;5;95;38;5;173m▄[48;5;173m█[48;5;95;38;5;95m█[48;5;173;38;5;173m███[48;5;16;38;5;16m█[49;39m                        [00m
       [48;5;95;38;5;95m█[48;5;173;38;5;173m██[48;5;95;38;5;95m█[48;5;173;38;5;173m██[48;5;236;38;5;236m█[48;5;223;38;5;223m███[48;5;16;38;5;95m▄[49;38;5;236m▄[39m  [38;5;95m▄[48;5;95;38;5;173m▄[48;5;173m█[48;5;95;38;5;95m█[48;5;173;38;5;173m███[48;5;95;38;5;16m▄[49m▀[39m               [38;5;235m▄▄[39m       [00m
    [38;5;236m▄▄▄[39m [48;5;95;38;5;95m█[48;5;173;38;5;173m█[48;5;95m▄[48;5;173m█[48;5;236;38;5;236m█[48;5;223;38;5;223m███[48;5;95;38;5;137m▄[48;5;223;38;5;223m█[48;5;16;38;5;16m█[49;38;5;95m▄[39m [48;5;95;38;5;95m█[48;5;173;38;5;173m█[48;5;95;38;5;95m█[48;5;173;38;5;173m████[48;5;16;38;5;236m▄[49;38;5;16m▄[39m             [38;5;235m▄[48;5;235;38;5;60m▄[48;5;60m█[48;5;235;38;5;235m█[49;39m       [00m
    [38;5;236m▀[48;5;223m▄[38;5;223m█[48;5;236m▄▄[48;5;173;38;5;236m▄[38;5;173m█[38;5;236m▄[48;5;236;38;5;223m▄[48;5;223m████[38;5;236m▄[48;5;236;38;5;173m▄[48;5;173;38;5;95m▄[48;5;95m█[48;5;173m▄[48;5;95;38;5;223m▄▄▄▄[48;5;173;38;5;16m▄[48;5;95;38;5;173m▄[48;5;173m█[48;5;16;38;5;16m█[49;39m            [38;5;235m▄[48;5;235;38;5;60m▄[48;5;60m██[48;5;235;38;5;235m█[49;39m       [00m
     [38;5;137m▄[48;5;236m▄[48;5;223m▄[38;5;223m██[48;5;236m▄[38;5;236m█[48;5;223;38;5;223m████[38;5;137m▄[48;5;95;38;5;223m▄▄[48;5;223m████[38;5;16m▄[38;5;95m▄[48;5;16;38;5;173m▄[48;5;95m▄[48;5;173m█[48;5;16;38;5;16m█[49;39m             [48;5;235;38;5;235m█[48;5;60;38;5;60m███[48;5;235;38;5;235m█[49;39m       [00m
     [38;5;236m▀[48;5;223m▄[38;5;180m▄[48;5;137;38;5;223m▄[48;5;223m██[48;5;137m▄[48;5;223m███[48;5;180m▄[48;5;223m█████[48;5;137m▄[48;5;236m▄[48;5;95;38;5;16m▄[48;5;173;38;5;173m██[38;5;16m▄[49m▀[39m             [48;5;235;38;5;235m█[48;5;60;38;5;60m███[48;5;235;38;5;235m█[49;39m [38;5;235m▄▄[39m     [00m
     [38;5;95m▄▄[48;5;236m▄[48;5;95;38;5;248m▄[48;5;180;38;5;16m▄[48;5;137;38;5;231m▄[48;5;223;38;5;137m▄[38;5;223m█████[38;5;180m▄[38;5;95m▄[38;5;236m▄[48;5;180m▄[48;5;236;38;5;173m▄▄[48;5;173m█[38;5;95m▄[48;5;236m▄[49;39m              [38;5;235m▄[48;5;235;38;5;60m▄[48;5;60m███[48;5;235;38;5;235m█[38;5;60m▄[48;5;60m█[48;5;16;38;5;16m█[49;39m     [00m
     [38;5;95m▀[48;5;173m▄[48;5;95;38;5;173m▄[48;5;251;38;5;95m▄[48;5;235;38;5;235m█[48;5;16;38;5;16m█[48;5;180;38;5;180m███[48;5;137m▄[38;5;95m▄[38;5;231m▄[48;5;16;38;5;16m█[48;5;231m▄[48;5;248;38;5;231m▄[48;5;95;38;5;95m█[48;5;173;38;5;173m████[48;5;95m▄[48;5;236m▄▄[49;38;5;16m▄[39m           [48;5;235;38;5;235m█[48;5;60;38;5;60m███[48;5;235m▄[48;5;60m█[38;5;236m▄[48;5;16;38;5;16m█[49;39m      [00m
      [48;5;95;38;5;236m▄[48;5;236;38;5;223m▄▄[48;5;223;38;5;60m▄[38;5;235m▄▄[38;5;137m▄[48;5;180;38;5;223m▄[48;5;137m▄[48;5;95m▄[48;5;231;38;5;137m▄[48;5;235;38;5;236m▄[38;5;251m▄[48;5;231;38;5;248m▄[48;5;95;38;5;95m█[48;5;173;38;5;173m██[38;5;95m▄[38;5;16m▄▄[48;5;95m▄[49m▀[39m            [48;5;235;38;5;235m█[48;5;236;38;5;236m█[48;5;60m▄▄[48;5;236m█[38;5;16m▄[49m▀[39m       [00m
    [38;5;137m▄[48;5;137;38;5;223m▄[48;5;223m███[48;5;16m▄[48;5;235;38;5;16m▄▄[48;5;16;38;5;223m▄[48;5;223m█████[48;5;236m▄[48;5;95;38;5;236m▄[48;5;173;38;5;173m███[38;5;95m▄[38;5;16m▄[49m▀[39m      [38;5;235m▄▄▄▄[48;5;235;38;5;60m▄▄[48;5;16m▄▄▄▄[48;5;236;38;5;236m█[38;5;16m▄[49m▀[39m         [00m
   [38;5;137m▄[48;5;137;38;5;223m▄[48;5;223m█[38;5;137m▄[48;5;137;38;5;223m▄[48;5;223m████████████[48;5;236;38;5;236m█[48;5;95;38;5;16m▄[48;5;16;38;5;236m▄▄[48;5;236m█[48;5;16m▄[49;38;5;16m▄[38;5;235m▄▄[48;5;235;38;5;60m▄▄▄[48;5;60m████████████[48;5;235m▄[49;38;5;235m▄[39m        [00m
   [48;5;137;38;5;137m█[48;5;223;38;5;223m█[38;5;236m▄[48;5;236m█[48;5;223;38;5;223m██[48;5;137;38;5;16m▄[48;5;223;38;5;223m█[48;5;137;38;5;16m▄[48;5;223;38;5;223m███[38;5;137m▄[38;5;223m██[48;5;137m▄[48;5;223;38;5;137m▄[38;5;223m█[48;5;16;38;5;137m▄[48;5;236;38;5;16m▄[38;5;236m███[38;5;60m▄[48;5;60m███████████████████[48;5;235m▄[49;38;5;16m▄[39m      [00m
   [38;5;236m▀▀[39m [48;5;16;38;5;16m█[48;5;223m▄[49m▀[39m [38;5;16m▀[39m [38;5;16m▀[48;5;223m▄[48;5;16;38;5;235m▄[38;5;60m▄[48;5;223;38;5;16m▄[38;5;223m██[48;5;137m▄[48;5;236;38;5;16m▄[48;5;223m▄[48;5;16m█[48;5;236;38;5;60m▄[48;5;60m██████████████████████[38;5;236m▄[48;5;236m█[48;5;16m▄▄[49;38;5;16m▄▄[39m  [00m
           [38;5;235m▄[48;5;235;38;5;60m▄[48;5;60m██[38;5;235m▄[38;5;60m█[48;5;16m▄▄[48;5;223;38;5;16m▄[48;5;16m█[48;5;60;38;5;60m████[38;5;236m▄[38;5;60m█████████████[38;5;236m▄▄▄▄▄[48;5;236m███[38;5;16m▄▄[38;5;236m███[48;5;16m▄[49;38;5;16m▄[39m[00m
          [38;5;235m▄[48;5;235;38;5;60m▄[48;5;60m█[38;5;16m▄[48;5;235;38;5;236m▄[48;5;16;38;5;16m█[48;5;60;38;5;236m▄[38;5;60m██[38;5;235m▄[38;5;16m▄[38;5;236m▄[38;5;60m███[48;5;236m▄[48;5;60;38;5;16m▄[38;5;236m▄▄▄▄▄▄[48;5;236m██████████[38;5;16m▄[38;5;236m█████[48;5;16m▄[38;5;16m█[49m▀▀▀[39m[00m
          [38;5;16m▀▀[48;5;16;38;5;235m▄[48;5;236;38;5;236m███[48;5;16;38;5;236m▄[48;5;60;38;5;16m▄[38;5;60m█[48;5;16;38;5;16m█[48;5;95;38;5;95m█[48;5;16m▄[48;5;236;38;5;16m▄[38;5;236m███[48;5;16m▄[48;5;236;38;5;16m▄[38;5;236m█████████[48;5;16m▄[48;5;236;38;5;16m▄[38;5;236m█████[48;5;16;38;5;16m█[38;5;95m▄[48;5;236;38;5;16m▄▄[38;5;236m███[48;5;16m▄[49;38;5;16m▄[39m [00m
           [48;5;235;38;5;235m█[48;5;236;38;5;236m█[38;5;16m▄[48;5;16m█[48;5;236;38;5;235m▄[48;5;236;38;5;16m▄[48;5;95;38;5;95m█[48;5;16m▄▄[48;5;95m███[48;5;16m▄▄[48;5;236;38;5;16m▄[38;5;236m█[48;5;16;38;5;16m█[48;5;236m▄[38;5;236m███[48;5;16m▄[48;5;236;38;5;16m▄[38;5;236m████[48;5;16m▄[38;5;16m█[38;5;95m▄[48;5;236;38;5;16m▄▄[38;5;236m██[48;5;16;38;5;16m█[48;5;95;38;5;95m██[48;5;16;38;5;16m█[49m▀▀▀▀[39m [00m
           [38;5;235m▀[38;5;16m▀[39m [38;5;16m▀[48;5;235m▄[49m▀[48;5;236;38;5;236m█[48;5;95;38;5;95m███[48;5;236m▄[48;5;95;38;5;236m▄▄[48;5;236;38;5;16m▄[48;5;95;38;5;95m█[48;5;16m▄▄[48;5;95m█[48;5;16m▄[48;5;236;38;5;16m▄▄[38;5;236m██[48;5;16;38;5;16m█[38;5;95m▄[48;5;236;38;5;16m▄▄[38;5;236m█[48;5;16;38;5;16m█[48;5;95;38;5;95m█[48;5;16;38;5;236m▄[48;5;95;38;5;16m▄[48;5;16;38;5;95m▄▄▄[48;5;95m██[48;5;16m▄[49;38;5;16m▄[39m    [00m
                 [48;5;236;38;5;236m█[48;5;95;38;5;95m█████[48;5;16;38;5;16m█[49;39m [48;5;16;38;5;16m█[48;5;95;38;5;95m██████[48;5;16;38;5;236m▄[48;5;236;38;5;16m▄[48;5;16m█[49;38;5;236m▀[48;5;95m▄[38;5;95m█[48;5;16m▄▄[48;5;95m█[48;5;236;38;5;236m█[49;39m [38;5;16m▀[48;5;95m▄[38;5;95m████[48;5;16;38;5;16m█[49;39m    [00m
                  [48;5;236;38;5;236m█[48;5;95;38;5;95m████[48;5;16;38;5;16m█[49;39m  [48;5;16;38;5;16m█[48;5;95;38;5;95m████[48;5;16;38;5;16m█[49;39m    [38;5;236m▄[48;5;236m█[48;5;95;38;5;95m███[48;5;16;38;5;16m█[49;39m  [48;5;16;38;5;16m█[48;5;95;38;5;95m███[38;5;236m▄[49;38;5;16m▀[39m    [00m
                 [38;5;236m▄[48;5;236;38;5;95m▄[48;5;173;38;5;173m███[48;5;16;38;5;16m█[49;39m   [48;5;16;38;5;236m▄[48;5;95;38;5;173m▄▄[38;5;95m█[38;5;236m▄[49;38;5;16m▀[39m   [48;5;16;38;5;16m█[48;5;95;38;5;236m▄[38;5;95m█[38;5;236m▄[38;5;95m█[48;5;236;38;5;16m▄[49m▀[39m [38;5;16m▄[48;5;236;38;5;95m▄▄[48;5;95m██[48;5;16;38;5;16m█[49;39m     [00m
                [48;5;236;38;5;16m▄[48;5;173;38;5;173m█[38;5;236m▄[48;5;95;38;5;173m▄[48;5;173m█[38;5;95m▄[48;5;16;38;5;16m█[49;39m [38;5;236m▄[48;5;236;38;5;173m▄[48;5;95m▄▄[48;5;173m██[48;5;16;38;5;16m█[49;39m     [38;5;16m▀▀▀▀[39m   [38;5;16m▀[48;5;236m▄[48;5;95m▄[48;5;236m▄[48;5;95m▄[49m▀[39m     [00m
                 [38;5;236m▀[38;5;16m▀▀▀▀[39m  [38;5;236m▀[48;5;173;38;5;16m▄[48;5;95m▄[48;5;173m▄[48;5;236m▄[48;5;95m▄[49m▀[39m                       [00m
                                                      [00m
