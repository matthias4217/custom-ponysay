$$$
NAME: Girafarig
OTHER NAMES: キリンリキ, Kirinriki, 麒麟奇, 키링키
APPEARANCE: Pokémon Generation II
KIND: Long Neck Pokémon
GROUP: ground
BALLOON: top
COAT: yellow
DISPLAY: full
BALLOON TOP: 3
BALLOON BOTTOM: 0
POSE: master
BASED ON: Girafarig
SOURCE: PokéAPI Sprites
MEDIA: Pokémon
LICENSE: Copyright The Pokémon Company
FREE: no
WIDTH: 75
HEIGHT: 38


$$$
$balloon5$
  $\$                                                                          [00m
   $\$                                                                         [00m
    $\$                                                                        [00m
     $\$                                                [00m
      $\$ [38;5;60m▄▄[39m      [38;5;60m▄▄[39m                                    [00m
      [38;5;60m▄[48;5;60;38;5;231m▄[48;5;231m██[48;5;60m▄[49;38;5;60m▄[39m  [38;5;60m▄[48;5;60;38;5;231m▄[48;5;231m██[48;5;60m▄[49;38;5;60m▄[39m                                  [00m
      [38;5;60m▀[48;5;146m▄[48;5;231;38;5;146m▄▄[48;5;146;38;5;103m▄[49;38;5;60m▀[39m  [38;5;60m▀[48;5;146;38;5;103m▄[48;5;231;38;5;146m▄▄[48;5;146;38;5;60m▄[49m▀[39m                                  [00m
        [48;5;60;38;5;60m█[48;5;146;38;5;146m█[48;5;60;38;5;60m█[49;39m    [48;5;60;38;5;60m█[48;5;146;38;5;146m█[48;5;60;38;5;60m█[49;39m                                    [00m
    [38;5;237m▄▄▄▄[48;5;60;38;5;103m▄[48;5;146;38;5;146m█[38;5;231m▄[48;5;60m▄[48;5;103m▄▄[48;5;60;38;5;60m█[48;5;231;38;5;231m█[38;5;60m▄[48;5;60;38;5;237m▄[49m▄▄[38;5;137m▄[39m                                 [00m
   [48;5;237;38;5;237m█[48;5;179;38;5;179m███[48;5;60;38;5;60m█[48;5;146;38;5;231m▄[48;5;231m████[48;5;103m▄[48;5;231m██[48;5;60m▄[48;5;103;38;5;146m▄[48;5;184;38;5;184m██[48;5;179m▄[48;5;137m▄▄▄[49;38;5;237m▄[39m                             [00m
    [48;5;237;38;5;237m█[48;5;179;38;5;179m█[48;5;60;38;5;60m█[48;5;231;38;5;231m█████████[38;5;146m▄[48;5;146;38;5;184m▄[48;5;184m███[38;5;175m▄[48;5;131m▄[38;5;233m▄[49m▀[39m                             [00m
     [48;5;60;38;5;60m█[48;5;231;38;5;231m██████[38;5;146m▄[48;5;146;38;5;138m▄[48;5;237;38;5;146m▄[38;5;237m█[48;5;146m▄[48;5;184;38;5;184m██[38;5;175m▄[48;5;175;38;5;131m▄[38;5;233m▄[49m▀[39m                               [00m
     [38;5;103m▀[48;5;231;38;5;233m▄[38;5;231m█████[48;5;138;38;5;237m▄[48;5;233;38;5;231m▄[48;5;60;38;5;233m▄[48;5;231;38;5;231m█[48;5;237;38;5;137m▄[48;5;184;38;5;184m██[48;5;233m▄[38;5;237m▄[49;38;5;131m▄▄[39m                       [38;5;237m▄▄▄▄[39m    [00m
      [48;5;233;38;5;60m▄[48;5;231;38;5;231m████[38;5;146m▄[48;5;237;38;5;233m▄[48;5;233m█[38;5;237m▄[48;5;231m▄[48;5;137;38;5;184m▄[48;5;184m███[48;5;237;38;5;237m█[48;5;175;38;5;175m██[48;5;131m▄[38;5;233m▄[49;39m                  [38;5;237m▄[48;5;237;38;5;240m▄[48;5;240;38;5;138m▄[48;5;138m██[38;5;184m▄▄[48;5;240;38;5;138m▄[48;5;237m▄[49;38;5;237m▄[39m [00m
     [38;5;60m▄[48;5;103;38;5;231m▄[48;5;231m███[38;5;146m▄[48;5;146;38;5;184m▄[48;5;138m▄[48;5;237m▄[48;5;184m████[38;5;237m▄[38;5;184m█[48;5;237m▄[48;5;175;38;5;240m▄[48;5;240;38;5;131m▄[48;5;233m▄[49;39m                  [48;5;237;38;5;237m█[48;5;240;38;5;240m█[48;5;138;38;5;138m███[48;5;185;38;5;185m█[48;5;184m▄[48;5;60;38;5;231m▄[48;5;184;38;5;233m▄[48;5;138;38;5;138m██[49;38;5;237m▄[39m[00m
    [38;5;131m▄[48;5;237;38;5;175m▄[48;5;175m█[48;5;131m▄[48;5;231;38;5;131m▄[38;5;146m▄[48;5;146;38;5;184m▄[48;5;184m████[38;5;137m▄▄[48;5;237;38;5;179m▄[48;5;179;38;5;184m▄[48;5;184m██[48;5;237;38;5;237m█[48;5;175;38;5;175m██[48;5;131m▄[38;5;233m▄[49;39m               [48;5;237;38;5;233m▄[48;5;240;38;5;240m███[48;5;138m▄[38;5;138m█[48;5;185m▄[38;5;185m█[48;5;60m▄[48;5;233;38;5;138m▄[48;5;138;38;5;240m▄[48;5;240m█[48;5;233;38;5;233m█[49;39m[00m
    [48;5;131;38;5;237m▄[48;5;175;38;5;175m███[48;5;131m▄[48;5;179;38;5;131m▄[38;5;237m▄[48;5;184;38;5;138m▄[38;5;184m█[38;5;237m▄[48;5;237;38;5;179m▄[48;5;179m██[38;5;184m▄[48;5;184m█[38;5;138m▄[48;5;138;38;5;240m▄[48;5;237;38;5;237m█[48;5;175;38;5;240m▄[48;5;240;38;5;131m▄[48;5;233m▄[49;39m                [48;5;233;38;5;233m█[48;5;240;38;5;240m██████[38;5;231m▄[38;5;138m▄[48;5;231;38;5;231m█[48;5;138;38;5;138m█[48;5;231;38;5;233m▄[49m▀[39m[00m
     [38;5;237m▀[38;5;131m▀[48;5;175;38;5;237m▄[48;5;131m▄[38;5;233m▄[48;5;179m▄[49;38;5;237m▀▀[48;5;179m▄[38;5;179m██[38;5;184m▄[48;5;184m██[48;5;240;38;5;240m██[48;5;237m▄[48;5;175;38;5;237m▄[38;5;175m██[48;5;131m▄[38;5;233m▄[49;39m               [48;5;237;38;5;233m▄[48;5;240;38;5;240m█████[38;5;237m▄[38;5;233m▄▄[49m▀[39m  [00m
             [48;5;237;38;5;237m█[48;5;179;38;5;179m█[48;5;184;38;5;184m████[48;5;138m▄[48;5;240;38;5;138m▄▄[48;5;237;38;5;237m█[48;5;175;38;5;175m█[38;5;233m▄[49m▀[39m               [38;5;237m▄[48;5;233;38;5;240m▄[48;5;240m█[48;5;233;38;5;233m█[49m▀▀▀[39m      [00m
              [48;5;137;38;5;237m▄[48;5;184;38;5;184m██████[38;5;138m▄[38;5;240m▄[48;5;131;38;5;237m▄[48;5;240;38;5;131m▄[48;5;131;38;5;175m▄▄[38;5;131m█[49;39m [38;5;131m▄[38;5;233m▄[39m   [38;5;131m▄[38;5;233m▄[39m    [38;5;237m▄[48;5;237;38;5;138m▄[48;5;138m█[48;5;233;38;5;233m█[49;39m          [00m
              [48;5;237;38;5;237m█[48;5;184;38;5;184m█████[48;5;240;38;5;240m████[48;5;237;38;5;138m▄[48;5;175;38;5;237m▄▄[48;5;131m▄[38;5;131m█[48;5;175;38;5;175m██[48;5;233m▄[49;38;5;233m▄[48;5;131;38;5;175m▄[48;5;175m█[48;5;131m▄[48;5;233;38;5;131m▄[49;38;5;233m▄[39m [38;5;237m▄[48;5;237;38;5;138m▄[48;5;138m█[48;5;237;38;5;233m▄[49;39m           [00m
              [48;5;237;38;5;237m█[48;5;184;38;5;184m█████[48;5;138m▄[48;5;240;38;5;138m▄[38;5;240m█[38;5;184m▄[48;5;184;38;5;138m▄[38;5;240m▄[38;5;138m▄[38;5;184m██[48;5;237m▄▄▄[48;5;131;38;5;138m▄[48;5;237m▄▄▄[48;5;233m▄▄[49;38;5;233m▄[48;5;237m▄[48;5;240;38;5;240m█[48;5;237;38;5;233m▄[49;39m            [00m
             [38;5;237m▄[48;5;237;38;5;184m▄[48;5;184m████████[48;5;138;38;5;240m▄[48;5;240m███[48;5;138m▄[48;5;184;38;5;184m██[38;5;138m▄▄[38;5;184m█[48;5;138;38;5;138m█[38;5;184m▄[48;5;184m██[48;5;138m▄[38;5;138m█[38;5;184m▄[48;5;233;38;5;179m▄[38;5;233m█[49;39m            [00m
             [48;5;237;38;5;237m█[48;5;184;38;5;179m▄[38;5;184m████████[48;5;138m▄[48;5;240;38;5;240m██[38;5;138m▄[48;5;138;38;5;184m▄[48;5;184m█[48;5;138;38;5;240m▄[48;5;240m██[38;5;138m▄[48;5;138;38;5;184m▄[48;5;179;38;5;138m▄[48;5;184;38;5;179m▄[38;5;184m█[38;5;179m▄[48;5;138;38;5;138m█[48;5;184m▄[38;5;179m▄[48;5;138;38;5;138m█[48;5;233m▄[49;38;5;233m▄[39m          [00m
            [48;5;237;38;5;237m█[48;5;184;38;5;184m█[48;5;179m▄[48;5;184m███████████████[48;5;138m▄▄[48;5;184m██[48;5;138;38;5;138m████████[38;5;240m▄[48;5;240m█[48;5;233;38;5;233m█[49;39m         [00m
            [48;5;237;38;5;237m█[48;5;184;38;5;184m██[48;5;179;38;5;137m▄[48;5;184;38;5;184m██████████████████[48;5;138;38;5;138m█████[38;5;240m▄▄[48;5;240;38;5;137m▄[48;5;137;38;5;240m▄[48;5;240;38;5;137m▄[38;5;240m█[48;5;233;38;5;233m█[49;39m        [00m
            [48;5;237;38;5;237m█[48;5;184;38;5;179m▄▄[38;5;184m█[48;5;137;38;5;179m▄[48;5;179m█[48;5;184m▄▄[38;5;137m▄[38;5;184m████████[38;5;137m▄[38;5;184m██[38;5;179m▄[48;5;138;38;5;240m▄▄▄▄[48;5;240m███[48;5;137m▄[48;5;240m█[48;5;137m▄[48;5;240m██[48;5;233m▄[49;38;5;233m▄[39m       [00m
            [38;5;237m▀[48;5;137m▄[48;5;179;38;5;179m███[48;5;137m▄▄[48;5;179;38;5;233m▄[48;5;137m▄[48;5;184;38;5;240m▄[38;5;184m███████[48;5;137;38;5;137m█[48;5;179;38;5;237m▄▄▄[48;5;240;38;5;233m▄▄[48;5;233;38;5;240m▄▄[48;5;240m█████████[48;5;233;38;5;233m█[49;39m       [00m
             [48;5;237;38;5;237m█[48;5;179;38;5;137m▄[38;5;179m█████[38;5;137m▄[48;5;237;38;5;233m▄[48;5;184;38;5;184m███████[48;5;237;38;5;237m█[49;39m [48;5;233;38;5;233m█[48;5;240;38;5;240m█████[48;5;233;38;5;233m█[48;5;240;38;5;240m███████[38;5;237m▄[49m▀[39m       [00m
              [48;5;237;38;5;237m█[48;5;179;38;5;179m████[38;5;137m▄[49m▀[39m [48;5;137;38;5;237m▄[48;5;184;38;5;184m█████[48;5;137;38;5;237m▄[49;39m  [38;5;233m▀[48;5;240;38;5;240m████[48;5;237;38;5;237m█[49;39m [48;5;233;38;5;233m█[48;5;240;38;5;240m██████[48;5;233;38;5;233m█[49;39m        [00m
               [48;5;237;38;5;237m█[48;5;179;38;5;179m███[48;5;237;38;5;237m█[49;39m  [48;5;237;38;5;237m█[48;5;184;38;5;179m▄[38;5;184m███[38;5;179m▄[48;5;237;38;5;237m█[49;39m   [48;5;233;38;5;233m█[48;5;240;38;5;240m███[48;5;237m▄[49;38;5;237m▄[39m [38;5;233m▀[48;5;240m▄[38;5;240m████[48;5;233;38;5;237m▄[49;39m        [00m
              [38;5;137m▄[48;5;137;38;5;184m▄[48;5;184m█[48;5;179m▄[38;5;179m█[48;5;137m▄[48;5;237;38;5;237m█[49;39m  [48;5;237;38;5;237m█[48;5;184;38;5;184m███[48;5;237;38;5;237m█[49;39m     [48;5;233;38;5;233m█[48;5;240;38;5;240m██[38;5;237m▄[49;38;5;233m▀[39m   [48;5;233;38;5;233m█[48;5;237;38;5;240m▄[48;5;240m███[48;5;233;38;5;233m█[49;39m       [00m
              [38;5;237m▀[48;5;184m▄[38;5;179m▄[48;5;179m██[38;5;137m▄[49;38;5;237m▀[39m [48;5;137;38;5;237m▄[48;5;184;38;5;179m▄[38;5;184m██[38;5;179m▄[48;5;179m█[48;5;237;38;5;237m█[49;39m   [38;5;233m▄[48;5;233;38;5;240m▄[48;5;240m██[49;38;5;233m▀[39m    [38;5;233m▀[48;5;237m▄[48;5;240;38;5;240m██[38;5;233m▄[49;38;5;237m▀[39m       [00m
                [48;5;237;38;5;237m█[48;5;179;38;5;179m██[48;5;237;38;5;237m█[49;39m  [38;5;237m▀[48;5;179;38;5;137m▄[38;5;179m██[38;5;237m▄[49m▀[39m   [38;5;237m▄[48;5;237;38;5;240m▄[48;5;240m██[48;5;237;38;5;233m▄[49;39m      [48;5;233;38;5;233m█[48;5;240;38;5;240m██[48;5;233;38;5;233m█[49;39m        [00m
                [48;5;237;38;5;237m█[48;5;179;38;5;179m██[48;5;237;38;5;237m█[49;39m   [48;5;237;38;5;237m█[48;5;179;38;5;179m██[48;5;237;38;5;237m█[49;39m   [38;5;233m▄[48;5;60;38;5;60m█[48;5;240;38;5;233m▄[38;5;60m▄[48;5;60m█[48;5;233;38;5;233m█[49;39m     [38;5;237m▄[48;5;233;38;5;240m▄[48;5;240;38;5;138m▄▄[38;5;240m█[48;5;237;38;5;233m▄[49;39m       [00m
               [48;5;137;38;5;237m▄[48;5;179;38;5;179m████[48;5;233;38;5;237m▄[49;38;5;233m▄[48;5;237;38;5;179m▄[48;5;179m███[48;5;237m▄[49;38;5;237m▄[39m  [38;5;233m▀[48;5;60m▄[48;5;233m█[48;5;60m▄▄[49m▀[39m    [38;5;233m▄[48;5;237;38;5;60m▄[48;5;138;38;5;233m▄[38;5;60m▄▄▄[49;38;5;233m▀[39m       [00m
              [48;5;233;38;5;233m█[48;5;103;38;5;103m█[48;5;137;38;5;233m▄[48;5;179;38;5;60m▄▄[48;5;233;38;5;233m█[49;39m [38;5;233m▀[48;5;179m▄[38;5;179m██[38;5;137m▄[38;5;237m▄[49m▀[39m            [48;5;233;38;5;233m██[48;5;60;38;5;60m███[48;5;233;38;5;233m█[49;39m        [00m
              [38;5;233m▀[48;5;233m█[48;5;60m▄▄▄[49m▀[39m [48;5;233;38;5;233m█[48;5;103;38;5;103m█[48;5;233;38;5;233m█[48;5;60;38;5;60m███[48;5;237;38;5;237m█[49;39m             [38;5;233m▀▀▀▀[39m         [00m
                     [38;5;233m▀[48;5;233m█[48;5;60m▄▄▄[49m▀[39m                           [00m
                                                      [00m
