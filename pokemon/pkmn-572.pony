$$$
NAME: Minccino
OTHER NAMES: Picochilla, チラーミィ, 치라미, 泡沫栗鼠, Chinchidou
APPEARANCE: Pokémon Generation V
KIND: Chinchilla Pokémon
GROUP: ground
BALLOON: top
COAT: gray
DISPLAY: full
BALLOON TOP: 3
BALLOON BOTTOM: 0
POSE: master
BASED ON: Minccino
SOURCE: PokéAPI Sprites
MEDIA: Pokémon
LICENSE: Copyright The Pokémon Company
FREE: no
WIDTH: 75
HEIGHT: 28


$$$
$balloon5$
                               $\$                                              [00m
                                $\$                                             [00m
                                 $\$                                            [00m
                                  $\$                         [00m
                                 [38;5;102m▄[38;5;239m▄[48;5;102;38;5;231m▄[48;5;239m▄▄[49;38;5;239m▄[39m                     [00m
         [38;5;102m▄▄[48;5;102;38;5;231m▄▄[49;38;5;239m▄[39m                 [38;5;102m▄[48;5;102;38;5;231m▄[48;5;231m██████[48;5;239m▄[49;38;5;239m▄[39m                   [00m
       [38;5;102m▄[48;5;102;38;5;231m▄[48;5;231m█████[48;5;239m▄[49;38;5;239m▄[39m             [38;5;102m▄[48;5;102;38;5;252m▄[48;5;231;38;5;231m██[38;5;252m▄[38;5;231m██████[48;5;239m▄[49;38;5;239m▄[39m                  [00m
      [48;5;102;38;5;239m▄[48;5;231;38;5;252m▄[38;5;231m███[38;5;252m▄[38;5;231m███[38;5;252m▄[48;5;239;38;5;239m█[49;39m           [48;5;102;38;5;239m▄[48;5;249;38;5;249m██[48;5;252m▄▄[48;5;249m██[48;5;252m▄[48;5;231m▄[38;5;252m▄▄▄[48;5;252;38;5;249m▄[48;5;239;38;5;239m█[49;39m                  [00m
     [48;5;239;38;5;239m█[48;5;249;38;5;249m██[48;5;252m▄▄[48;5;249m███[48;5;252m▄▄[48;5;249m██[48;5;239;38;5;239m█[49;39m [38;5;239m▄[48;5;239;38;5;249m▄▄[49;38;5;239m▄[39m    [48;5;239;38;5;239m█[48;5;249;38;5;249m███████[38;5;102m▄[38;5;239m▄▄[38;5;102m▄[38;5;249m███[48;5;239;38;5;239m█[49;39m                 [00m
    [48;5;239;38;5;239m█[48;5;249;38;5;249m█████[48;5;239;38;5;102m▄[48;5;249;38;5;239m▄[38;5;249m█████[38;5;239m▄[48;5;239;38;5;249m▄[48;5;249m███[48;5;233;38;5;233m█[49;39m   [48;5;239;38;5;233m▄[48;5;249;38;5;249m██████[38;5;239m▄[48;5;239;38;5;167m▄[48;5;167m▄[48;5;102;38;5;167m▄[38;5;102m██[48;5;249;38;5;249m███[48;5;239;38;5;239m█[49;39m                 [00m
   [38;5;239m▄[48;5;239;38;5;249m▄[48;5;249m████[48;5;102;38;5;102m█[38;5;167m▄[48;5;167;38;5;167m▄[48;5;233;38;5;233m█[48;5;249;38;5;249m███[38;5;239m▄[48;5;239;38;5;249m▄[48;5;249m███[48;5;239;38;5;233m▄[49;39m   [38;5;239m▄[48;5;233;38;5;249m▄[48;5;249m█████[48;5;239;38;5;239m█[48;5;167;38;5;233m▄▄[38;5;167m█[38;5;210m▄[48;5;167;38;5;167m█[48;5;249;38;5;249m█████[48;5;239;38;5;233m▄[49;39m                [00m
   [48;5;239;38;5;239m█[48;5;249;38;5;249m█████[38;5;167m▄[48;5;167;38;5;210m▄[48;5;167m▄[38;5;167m█[48;5;239;38;5;239m█[48;5;249;38;5;249m██[48;5;239;38;5;239m█[48;5;249;38;5;249m███[38;5;233m▄[49m▀[39m   [48;5;233;38;5;233m█[48;5;249;38;5;249m████[38;5;239m▄[48;5;233;38;5;249m▄▄[48;5;249m██[48;5;233;38;5;233m█[48;5;210;38;5;210m██[48;5;167;38;5;167m█[48;5;249;38;5;249m████[48;5;233;38;5;233m█[49;39m       [38;5;102m▄▄[39m       [00m
   [48;5;239;38;5;239m█[48;5;249;38;5;249m█████[48;5;167;38;5;167m█[48;5;210;38;5;239m▄[48;5;239;38;5;249m▄[48;5;233m▄[48;5;239;38;5;233m▄[48;5;249;38;5;239m▄[38;5;249m█[38;5;239m▄[48;5;239;38;5;233m▄[48;5;249;38;5;249m██[48;5;233;38;5;239m▄[49m▄▄[48;5;239;38;5;249m▄▄[48;5;233;38;5;233m█[48;5;249;38;5;249m███[38;5;233m▄[48;5;239;38;5;249m▄[48;5;249m███[38;5;233m▄[48;5;233;38;5;210m▄[48;5;210m██[48;5;167;38;5;167m█[48;5;249;38;5;249m████[48;5;233;38;5;233m█[49;39m      [48;5;102;38;5;239m▄[48;5;231;38;5;231m██[48;5;239m▄[49;38;5;239m▄[39m     [00m
   [38;5;239m▀[48;5;249m▄[38;5;249m████[48;5;167m▄[48;5;233;38;5;233m█[48;5;249;38;5;249m███[48;5;233;38;5;239m▄[48;5;249;38;5;233m▄[48;5;233m█[48;5;249;38;5;249m█[48;5;239m▄[48;5;249;38;5;239m▄[38;5;249m█[48;5;102m▄[48;5;249m███████[48;5;233;38;5;239m▄[48;5;249;38;5;249m███[38;5;233m▄[48;5;233;38;5;239m▄[48;5;210m▄[38;5;210m█[38;5;167m▄[48;5;167;38;5;249m▄[48;5;249m███[38;5;233m▄[49m▀[39m      [38;5;239m▀[48;5;231m▄[38;5;231m██[48;5;239m▄[49;38;5;239m▄[39m    [00m
    [48;5;239;38;5;239m█[48;5;249;38;5;249m█████[38;5;239m▄[48;5;233;38;5;233m█[48;5;249;38;5;249m██[38;5;233m▄[48;5;233;38;5;249m▄[48;5;249m█[48;5;233m▄[48;5;249m███████[38;5;233m▄[38;5;249m█████[38;5;239m▄[48;5;233;38;5;249m▄[48;5;249m███[48;5;239;38;5;233m▄[48;5;167;38;5;249m▄[48;5;249m████[48;5;233;38;5;233m█[49;39m        [48;5;239;38;5;239m█[48;5;231;38;5;231m███[48;5;239m▄[49;38;5;239m▄[39m   [00m
     [48;5;239;38;5;239m█[48;5;249;38;5;102m▄[38;5;249m██[38;5;102m▄[48;5;239;38;5;233m▄[48;5;249;38;5;249m█[48;5;233m▄[48;5;249;38;5;233m▄[48;5;233;38;5;249m▄[48;5;249m█████████[48;5;102;38;5;239m▄[48;5;231;38;5;231m█[48;5;102;38;5;239m▄[48;5;249;38;5;102m▄[38;5;249m█████[38;5;239m▄[38;5;233m▄[48;5;233;38;5;249m▄[48;5;249m█████[48;5;233;38;5;233m█[49;39m         [48;5;239;38;5;239m█[48;5;252;38;5;249m▄[48;5;231;38;5;252m▄▄[38;5;249m▄[48;5;233;38;5;233m█[49;39m   [00m
      [48;5;239;38;5;239m█[48;5;102;38;5;102m████[48;5;239m▄[48;5;249m▄[48;5;233;38;5;233m█[48;5;249;38;5;102m▄[48;5;239;38;5;231m▄[48;5;249;38;5;102m▄[38;5;249m███████[48;5;239;38;5;239m███[48;5;233;38;5;233m█[48;5;249;38;5;249m████[48;5;239m▄[48;5;249m█████[38;5;233m▄▄[49m▀[39m          [48;5;239;38;5;239m█[48;5;249;38;5;249m█████[48;5;239;38;5;233m▄[49;39m  [00m
       [38;5;239m▀[48;5;102m▄▄[38;5;102m██[48;5;233;38;5;233m█[48;5;249;38;5;249m█[48;5;239;38;5;233m▄[48;5;231;38;5;239m▄[48;5;239m█[48;5;249;38;5;249m███████[48;5;233m▄[48;5;239;38;5;233m▄▄[48;5;233;38;5;249m▄[48;5;249m██████[38;5;233m▄▄[49m▀▀[39m            [48;5;239;38;5;239m█[48;5;249;38;5;249m██████[48;5;233;38;5;233m█[49;39m  [00m
          [38;5;239m▀▀[48;5;233;38;5;233m█[48;5;249;38;5;249m█[48;5;233m▄[48;5;239;38;5;233m▄[48;5;233;38;5;249m▄[48;5;249m█[48;5;239m▄[48;5;249m███[38;5;239m▄[38;5;249m███████[38;5;102m▄[48;5;239m▄▄[38;5;233m▄[49;39m               [38;5;239m▄[48;5;239;38;5;249m▄[48;5;249m██████[48;5;233;38;5;233m█[49;39m  [00m
             [48;5;239;38;5;233m▄[48;5;249;38;5;249m███[48;5;239m▄[48;5;102m▄[48;5;249;38;5;239m▄▄[48;5;102;38;5;249m▄[48;5;249m██████[38;5;102m▄[48;5;102m███[38;5;239m▄[49m▀[39m              [38;5;239m▄[48;5;239;38;5;249m▄[48;5;249m██████[48;5;233;38;5;233m█[49;39m   [00m
              [38;5;233m▀[48;5;102m▄[48;5;249m▄[38;5;102m▄[38;5;249m█████[38;5;102m▄▄▄[48;5;102m███[38;5;239m▄[38;5;233m▄▄[48;5;233;38;5;249m▄[38;5;102m▄[38;5;249m▄[49;38;5;233m▄[39m           [38;5;239m▄[48;5;239;38;5;249m▄[48;5;249m████[38;5;239m▄▄▄[48;5;239;38;5;249m▄▄[48;5;233m▄[49;38;5;233m▄[39m[00m
                 [38;5;239m▀▀[48;5;239;38;5;233m▄[48;5;233;38;5;249m▄▄[48;5;102;38;5;233m▄[38;5;239m▄[38;5;102m██[38;5;249m▄[48;5;239m▄▄[48;5;249m█████[48;5;102;38;5;233m▄[49m▀[39m         [38;5;239m▄[48;5;239;38;5;249m▄[48;5;249m███[38;5;239m▄[48;5;239;38;5;249m▄▄[48;5;249m█████[38;5;233m▄[49m▀[39m[00m
               [38;5;239m▄▄[48;5;239;38;5;102m▄[38;5;239m█[38;5;249m▄[48;5;233m▄[48;5;249m██████████[38;5;233m▄▄[49m▀[39m         [38;5;239m▄[48;5;239;38;5;249m▄[48;5;249m██[38;5;102m▄▄[48;5;239;38;5;249m▄[48;5;249m███████[48;5;233;38;5;233m█[49;39m  [00m
              [48;5;239;38;5;233m▄[48;5;249;38;5;102m▄[48;5;102;38;5;249m▄[48;5;233m▄[48;5;249;38;5;233m▄▄[48;5;102m▄[48;5;233;38;5;249m▄[48;5;249m█[38;5;239m▄[48;5;102;38;5;249m▄[48;5;249m█████[48;5;233m▄[49;38;5;233m▄[39m        [38;5;239m▄▄[48;5;239;38;5;249m▄[48;5;249m███[48;5;102m▄[48;5;249m████████[38;5;233m▄[49m▀[39m   [00m
               [38;5;233m▀[38;5;239m▀▀[48;5;233m▄[48;5;249;38;5;249m██[48;5;233m▄[48;5;239m▄[48;5;249m███████[38;5;102m▄[48;5;233m▄[49;38;5;233m▄[38;5;239m▄▄▄▄[48;5;239;38;5;102m▄▄[38;5;249m▄[48;5;249m███████████[38;5;233m▄▄[49m▀▀[39m     [00m
                 [48;5;233;38;5;233m█[48;5;102;38;5;102m█[48;5;249m▄▄[38;5;249m█████[38;5;102m▄▄▄[48;5;102m███[48;5;233;38;5;233m█[48;5;102;38;5;102m████[38;5;239m▄▄[38;5;102m█[48;5;249m▄[38;5;249m██[48;5;239m▄▄▄▄[48;5;249;38;5;239m▄▄[49;38;5;233m▀▀[39m         [00m
                 [38;5;239m▀[48;5;102;38;5;233m▄[38;5;102m████████████[38;5;233m▄[49m▀[48;5;102;38;5;239m▄▄▄[38;5;102m███[48;5;239m▄[38;5;233m▄[48;5;102m▄[48;5;249m▄[38;5;239m▄[38;5;249m█████[48;5;239m▄[49;38;5;239m▄[39m         [00m
                 [48;5;233;38;5;233m█[48;5;239;38;5;239m█[48;5;233;38;5;102m▄[38;5;239m▄[48;5;239;38;5;102m▄[38;5;233m▄[49m▀▀▀[48;5;239m▄[38;5;102m▄▄[48;5;233;38;5;239m▄[38;5;102m▄[48;5;102;38;5;239m▄[48;5;233;38;5;233m█[49;39m   [38;5;233m▀▀▀▀[39m    [38;5;233m▀▀▀▀▀▀[39m          [00m
                  [38;5;233m▀▀▀▀[39m      [38;5;233m▀▀▀▀[39m                            [00m
                                                            [00m
