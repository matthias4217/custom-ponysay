$$$
NAME: Swalot
OTHER NAMES: 꿀꺽몬, 吞食獸, Schlukwech, Avaltout, マルノーム, Marunoom
APPEARANCE: Pokémon Generation III
KIND: Poison Bag Pokémon
GROUP: indeterminate
BALLOON: top
COAT: purple
DISPLAY: full
BALLOON TOP: 3
BALLOON BOTTOM: 0
POSE: master
BASED ON: Swalot
SOURCE: PokéAPI Sprites
MEDIA: Pokémon
LICENSE: Copyright The Pokémon Company
FREE: no
WIDTH: 75
HEIGHT: 33


$$$
$balloon5$
               $\$                                                                [00m
                $\$                                                               [00m
                 $\$                                                              [00m
                  $\$                                          [00m
                   $\$   [38;5;60m▄▄▄▄▄▄▄[39m                               [00m
                  [38;5;60m▄[38;5;141m▄[48;5;60;38;5;183m▄▄[48;5;141m▄[48;5;141m▄[38;5;141m██████[48;5;60m▄▄▄[49;38;5;60m▄▄[39m                          [00m
               [38;5;60m▄[48;5;60;38;5;183m▄[48;5;141m▄[48;5;183m███████[48;5;141m▄[38;5;141m█████████[48;5;60m▄▄[49;38;5;60m▄[39m                       [00m
             [38;5;60m▄[48;5;60;38;5;183m▄[48;5;183m███████████[48;5;141;38;5;141m████████████[48;5;60m▄[49;38;5;60m▄[39m                     [00m
            [48;5;60;38;5;60m█[48;5;141;38;5;141m▄[48;5;183;38;5;183m███████████[38;5;141m▄[48;5;141m██████████████[48;5;60;38;5;141m▄[49;38;5;60m▄[39m                   [00m
           [48;5;60;38;5;231m▄[48;5;141;38;5;161m▄[48;5;141;38;5;141m▄[48;5;183;38;5;141m▄[38;5;183m███████[38;5;141m▄[38;5;141m▄[48;5;141;38;5;231m▄[38;5;161m▄[48;5;141;38;5;60m▄[38;5;141m██████████████[48;5;16m▄[49;38;5;16m▄[39m                  [00m
          [48;5;60;38;5;60m█[48;5;161;38;5;141m▄▄[48;5;141;38;5;60m▄[48;5;60;38;5;183m▄▄[38;5;141m▄[48;5;141;38;5;141m█[48;5;141;38;5;60m▄[38;5;141m████[48;5;141m▄[48;5;161;38;5;60m▄▄[48;5;60;38;5;141m▄[48;5;141m███████████████[48;5;16m▄[49;38;5;16m▄[39m                 [00m
          [48;5;60;38;5;60m█[48;5;141;38;5;141m█[48;5;60;38;5;16m▄[48;5;183;38;5;141m▄▄[48;5;141m████[48;5;141m▄▄[48;5;141m██████████████████████[48;5;16m▄[49;38;5;16m▄[39m                [00m
         [48;5;60;38;5;60m█[48;5;141;38;5;141m█[48;5;141;38;5;141m█[48;5;16m▄[48;5;141;38;5;16m▄[38;5;141m████████[38;5;101m▄▄▄▄[38;5;141m██████████████████[48;5;16;38;5;16m█[49;39m                [00m
         [48;5;60;38;5;60m█[48;5;101;38;5;179m▄[48;5;179;38;5;221m▄[48;5;141;38;5;179m▄[38;5;60m▄[48;5;16;38;5;141m▄[48;5;60m▄[48;5;141m█████[48;5;179;38;5;179m█[48;5;221m▄▄[38;5;221m█[48;5;179m▄[48;5;101m▄[48;5;141;38;5;101m▄[38;5;141m█████████████████[48;5;16;38;5;16m█[49;39m               [00m
        [38;5;101m▄[48;5;101;38;5;179m▄[48;5;221;38;5;221m█[38;5;179m▄[48;5;60;38;5;16m▄[48;5;141;38;5;183m▄▄[38;5;141m███████[48;5;101m▄[48;5;16m▄▄[48;5;179;38;5;16m▄[48;5;221;38;5;221m█[48;5;179;38;5;179m█[48;5;101;38;5;101m█[48;5;141;38;5;141m████████████████[48;5;16m▄[49;38;5;16m▄[39m              [00m
       [38;5;101m▄[48;5;101;38;5;179m▄[48;5;221;38;5;221m█[38;5;101m▄[48;5;101;38;5;141m▄[48;5;16m▄[48;5;183;38;5;16m▄[38;5;141m▄[48;5;141m▄[48;5;141m█[38;5;16m▄[38;5;141m██[48;5;141m▄[38;5;141m████[48;5;16;38;5;16m█[48;5;221;38;5;221m██[48;5;101;38;5;101m█[48;5;141;38;5;141m█████████████████[48;5;16;38;5;16m█[49;39m              [00m
   [38;5;16m▄[48;5;101;38;5;221m▄▄[49;38;5;101m▄[48;5;101;38;5;179m▄[48;5;221;38;5;221m█[38;5;101m▄[48;5;101;38;5;141m▄[48;5;141m███[48;5;16m▄[38;5;141m▄▄[48;5;141m████[48;5;141;38;5;141m████[48;5;16m▄[48;5;179;38;5;16m▄[48;5;221;38;5;221m█[48;5;101m▄[48;5;141;38;5;101m▄▄▄▄[48;5;101;38;5;221m▄▄[48;5;141;38;5;16m▄[38;5;141m██████████[48;5;16;38;5;141m▄[49;38;5;16m▄[39m             [00m
   [38;5;16m▀[48;5;221m▄[38;5;179m▄▄▄[48;5;179;38;5;16m▄[48;5;16m█[48;5;141;38;5;141m█████[48;5;141m▄[38;5;141m███[38;5;141m▄[48;5;141m██████[48;5;16m▄[48;5;179;38;5;16m▄[48;5;221m▄[38;5;179m▄▄▄▄[48;5;179m█[38;5;16m▄[48;5;16;38;5;141m▄[48;5;141m██████████[48;5;141;38;5;141m█[48;5;16;38;5;16m█[49;39m             [00m
     [38;5;16m▀▀▀[38;5;60m▄[48;5;16;38;5;16m█[48;5;141;38;5;141m███████████████████[48;5;16m▄▄▄▄▄[48;5;141m███████████[48;5;141;38;5;141m██[48;5;16;38;5;239m▄[49;38;5;16m▄[39m            [00m
    [38;5;60m▄[48;5;60;38;5;183m▄[38;5;60m█[38;5;141m▄[48;5;141;38;5;141m█[48;5;16;38;5;60m▄[48;5;141;38;5;141m███████████████████[38;5;60m▄[38;5;141m██[38;5;60m▄[38;5;141m████[38;5;60m▄[38;5;141m███[38;5;60m▄[38;5;141m██[38;5;141m▄[48;5;141m██[48;5;239;38;5;239m█[48;5;236;38;5;236m█[49;38;5;16m▄[39m           [00m
   [48;5;60;38;5;60m█[48;5;183;38;5;183m█[48;5;60;38;5;16m▄[48;5;183;38;5;183m██[48;5;141;38;5;141m█[48;5;60;38;5;141m▄[48;5;141m▄[38;5;141m█████████████████[48;5;60;38;5;16m▄[48;5;183;38;5;183m█[48;5;141;38;5;141m█[48;5;60;38;5;60m█[48;5;141;38;5;141m█[48;5;183;38;5;183m██[48;5;141;38;5;141m██[38;5;141m▄[48;5;60;38;5;60m█[48;5;141;38;5;141m██[48;5;141;38;5;141m█[48;5;60;38;5;16m▄[48;5;141;38;5;141m▄[48;5;141m███[48;5;239;38;5;239m██[48;5;16;38;5;236m▄[49;38;5;16m▄[39m          [00m
   [48;5;16;38;5;16m█[48;5;183;38;5;141m▄[48;5;16;38;5;16m█[48;5;141;38;5;141m▄[38;5;141m███[48;5;60;38;5;16m▄[48;5;141;38;5;141m███[38;5;239m▄[48;5;239;38;5;242m▄[48;5;141;38;5;239m▄[38;5;141m███████████[48;5;16;38;5;16m█[48;5;141;38;5;141m▄[38;5;141m█[48;5;16;38;5;16m█[48;5;141;38;5;141m█[48;5;183;38;5;141m▄▄[48;5;141m██[48;5;141;38;5;141m█[48;5;16;38;5;16m█[48;5;141;38;5;141m▄▄[48;5;141m█[48;5;16;38;5;16m█[48;5;141;38;5;141m████[48;5;239m▄[38;5;239m██[48;5;16;38;5;236m▄[49;38;5;16m▄[39m         [00m
    [38;5;16m▀[48;5;141m▄[48;5;16m█[48;5;141m▄[48;5;141m▄[49m▀[48;5;16m█[48;5;141;38;5;141m██[38;5;239m▄[48;5;239;38;5;242m▄[48;5;242m██[48;5;239m▄[48;5;141;38;5;239m▄[38;5;141m██████████[48;5;16m▄[48;5;141;38;5;16m▄▄[48;5;16;38;5;141m▄[48;5;141;38;5;16m▄[48;5;141m▄▄[48;5;141m▄[48;5;16;38;5;236m▄[48;5;141;38;5;16m▄▄▄[48;5;16;38;5;141m▄[48;5;141m██████[48;5;239;38;5;239m███[48;5;16m▄[49;38;5;16m▄[39m        [00m
          [38;5;16m▀[48;5;60m▄[48;5;141;38;5;141m█[48;5;239;38;5;239m█[48;5;242;38;5;242m██████[48;5;141;38;5;239m▄[38;5;141m█████████[38;5;141m▄[48;5;239;38;5;242m▄[48;5;242m██[48;5;239;38;5;239m█[48;5;236;38;5;236m█████[48;5;239m▄[48;5;141;38;5;239m▄[38;5;141m██████[48;5;239;38;5;239m████[48;5;16m▄[49;38;5;16m▄[39m       [00m
           [48;5;16;38;5;16m█[48;5;239;38;5;239m█[48;5;242;38;5;242m████████[48;5;239m▄[48;5;141;38;5;239m▄[38;5;141m██████[38;5;239m▄[48;5;239;38;5;242m▄[48;5;242m███[48;5;239;38;5;239m█[48;5;236;38;5;236m███████[48;5;239m▄[48;5;141;38;5;141m████████[48;5;239m▄▄▄[48;5;16;38;5;239m▄[49;38;5;16m▄[39m     [00m
            [48;5;16;38;5;16m█[48;5;242;38;5;242m██████████[48;5;239;38;5;239m█[48;5;141;38;5;141m█████[48;5;239m▄[48;5;242;38;5;239m▄[38;5;242m█[38;5;239m▄[48;5;239m█[48;5;236;38;5;236m████████[38;5;141m▄[48;5;141m█████████████[48;5;16m▄▄[49;38;5;16m▄[39m  [00m
            [48;5;16;38;5;16m█[48;5;239;38;5;239m█[48;5;242;38;5;242m███████[38;5;239m▄[48;5;239;38;5;141m▄[48;5;141m████[38;5;141m▄▄[48;5;141m██[48;5;239m▄[38;5;239m█[48;5;236;38;5;236m███████[38;5;239m▄[48;5;141;38;5;141m██████████████[48;5;141m▄[38;5;141m██[48;5;141m▄[48;5;16;38;5;16m█[49;39m [00m
         [38;5;16m▄▄[48;5;16;38;5;141m▄[48;5;141m█[48;5;239m▄[38;5;236m▄[48;5;242;38;5;239m▄▄▄[38;5;236m▄[48;5;239;38;5;141m▄[48;5;141m█[48;5;141m▄▄▄[48;5;141m█████████[48;5;239;38;5;239m█[48;5;236;38;5;236m████[38;5;141m▄[48;5;141m██████[38;5;141m▄[48;5;141;38;5;141m▄[48;5;141;38;5;141m▄[48;5;141;38;5;141m▄[48;5;141;38;5;141m▄[38;5;141m█████████[48;5;141m▄[48;5;16;38;5;16m█[49;39m[00m
       [38;5;16m▄[48;5;16;38;5;141m▄[48;5;141m██████[48;5;236m▄[48;5;239;38;5;236m▄[48;5;236;38;5;141m▄[48;5;141m███████████████[38;5;141m▄[38;5;141m█[48;5;236;38;5;141m▄[38;5;141m▄[48;5;141m████████[38;5;141m▄[48;5;141;38;5;141m▄[48;5;141;38;5;141m▄[48;5;141;38;5;141m▄[48;5;141;38;5;141m▄[38;5;141m█████████[48;5;97;38;5;16m▄[49m▀[39m[00m
       [48;5;16;38;5;16m█[48;5;141;38;5;141m███████████████████████[38;5;141m▄[48;5;141;38;5;141m▄[48;5;141;38;5;141m▄[48;5;141;38;5;141m▄[48;5;141;38;5;141m▄[48;5;141;38;5;141m▄[48;5;141;38;5;141m▄[38;5;141m████████[48;5;141m▄[48;5;141m█[48;5;141m▄[48;5;141m██[48;5;97m▄[48;5;141;38;5;97m▄▄▄[38;5;16m▄▄[48;5;97m▄[49m▀[39m  [00m
       [38;5;16m▀[48;5;141m▄[38;5;141m███████████████████████[48;5;141m▄[48;5;141;38;5;141m▄[48;5;141;38;5;141m▄[48;5;141;38;5;141m▄[48;5;141;38;5;141m▄[48;5;141m███████[38;5;97m▄[38;5;141m██████[38;5;97m▄[48;5;97;38;5;16m▄[49m▀▀[39m      [00m
         [38;5;16m▀▀▀[48;5;97m▄[48;5;141;38;5;141m██████[38;5;97m▄[48;5;97m█[48;5;141;38;5;141m█████████[38;5;97m▄[38;5;141m████████████[48;5;97;38;5;16m▄[49m▀▀[48;5;141m▄▄[48;5;97m▄▄▄[49m▀[39m         [00m
            [38;5;16m▀[48;5;97m▄[48;5;141m▄▄▄▄[49m▀▀▀[48;5;141m▄[38;5;141m████████[48;5;97m▄[48;5;141;38;5;16m▄▄[38;5;141m████████[38;5;16m▄▄[49m▀[39m                 [00m
                      [38;5;16m▀▀[48;5;141m▄▄▄▄▄▄[49m▀[39m  [38;5;16m▀▀▀▀▀▀▀▀[39m                    [00m
                                                             [00m
