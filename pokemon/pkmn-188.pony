$$$
NAME: Skiploom
OTHER NAMES: 毽子花, Popocco, Hubelupf, ポポッコ, Floravol, 두코
APPEARANCE: Pokémon Generation II
KIND: Cottonweed Pokémon
GROUP: plant, fairy
BALLOON: top
COAT: green
DISPLAY: full
BALLOON TOP: 3
BALLOON BOTTOM: 0
POSE: master
BASED ON: Skiploom
SOURCE: PokéAPI Sprites
MEDIA: Pokémon
LICENSE: Copyright The Pokémon Company
FREE: no
WIDTH: 73
HEIGHT: 18


$$$
$balloon5$
                  $\$                                                      [00m
                   $\$                                                     [00m
                    $\$                                                    [00m
                     $\$                     [00m
                 [38;5;16m▄▄[39m [38;5;94m▄▄[48;5;94;38;5;227m▄▄▄[49;38;5;94m▄[39m  [38;5;94m▄▄▄[39m            [00m
              [38;5;94m▄[48;5;94;38;5;227m▄▄[48;5;227m██[48;5;16;38;5;220m▄▄[48;5;220;38;5;16m▄[48;5;227;38;5;220m▄[38;5;215m▄▄[48;5;220;38;5;16m▄[48;5;16;38;5;227m▄[48;5;94m▄[48;5;227m██[38;5;94m▄[48;5;94;38;5;16m▄[49m▄[39m          [00m
            [38;5;16m▄[48;5;16;38;5;227m▄▄[48;5;94m▄[38;5;215m▄[48;5;220;38;5;94m▄[48;5;215m▄▄[38;5;16m▄[48;5;94;38;5;152m▄[48;5;16m▄[38;5;231m▄▄[48;5;94;38;5;152m▄[48;5;215;38;5;16m▄[38;5;94m▄[48;5;94;38;5;215m▄[38;5;220m▄[48;5;227;38;5;227m██[48;5;94m▄▄[49;38;5;16m▄[39m        [00m
       [38;5;65m▄▄▄[48;5;65;38;5;77m▄▄[48;5;16m▄[48;5;227;38;5;16m▄[38;5;227m██[48;5;220;38;5;94m▄[48;5;215;38;5;16m▄[48;5;16;38;5;220m▄[38;5;215m▄▄[38;5;94m▄▄[48;5;152;38;5;16m▄▄▄▄[48;5;16;38;5;215m▄▄▄[48;5;94;38;5;220m▄[48;5;215;38;5;94m▄[48;5;227m▄[48;5;215;38;5;16m▄[49m▀[39m        [00m
    [38;5;65m▄[48;5;65;38;5;119m▄▄[48;5;119m█[38;5;77m▄[48;5;77m█████[48;5;16m▄▄[48;5;220;38;5;16m▄[48;5;227m▄[38;5;220m▄[38;5;227m█[48;5;220m▄[48;5;215;38;5;16m▄[48;5;16;38;5;227m▄[48;5;94m▄[48;5;215m▄▄[48;5;94m▄[48;5;16m▄[48;5;215;38;5;16m▄[48;5;220m▄[48;5;227;38;5;227m███[38;5;16m▄[48;5;16m█[49;39m        [00m
   [48;5;65;38;5;16m▄[48;5;77;38;5;77m█[48;5;119m▄▄[48;5;77m█████[38;5;34m▄[38;5;77m██[38;5;119m▄▄[48;5;34m▄[48;5;16m▄▄[38;5;77m▄▄[48;5;94;38;5;16m▄[48;5;227m▄▄▄▄[48;5;94m▄[48;5;16;38;5;77m▄[48;5;77m█[48;5;16m▄▄▄[48;5;77m██[48;5;65m▄▄[49;38;5;65m▄▄[39m    [00m
   [38;5;16m▀[48;5;34m▄[48;5;77;38;5;34m▄[38;5;77m████[38;5;65m▄[48;5;65;38;5;77m▄[48;5;77m█[48;5;119;38;5;227m▄[38;5;202m▄[38;5;119m███████[48;5;77m▄▄[38;5;77m██████████████[38;5;34m▄[48;5;65m▄▄[49;38;5;65m▄[39m [00m
     [38;5;16m▀[38;5;65m▀[48;5;34m▄▄▄[48;5;65;38;5;77m▄[48;5;77m██[48;5;202m▄▄[48;5;119m▄▄[38;5;119m█████[48;5;227;38;5;202m▄[48;5;202m█[48;5;77;38;5;77m████[38;5;34m▄[38;5;77m████████[38;5;34m▄[48;5;34m████[48;5;65;38;5;16m▄[49;39m[00m
         [48;5;65;38;5;16m▄[48;5;77;38;5;77m██[48;5;34m▄[48;5;77;38;5;65m▄▄[38;5;77m█████████[38;5;34m▄[38;5;77m████[48;5;65m▄[48;5;77;38;5;16m▄[38;5;77m███[38;5;34m▄▄[48;5;34m█████[38;5;16m▄[49m▀[39m[00m
         [38;5;16m▀[48;5;34;38;5;65m▄[48;5;77;38;5;34m▄[38;5;77m███[48;5;65m▄[48;5;77;38;5;65m▄[38;5;16m▄▄[48;5;65;38;5;77m▄[48;5;16m▄▄[48;5;65m▄[48;5;34m▄[48;5;77m██████[38;5;34m▄[48;5;16m▄[48;5;65m▄[48;5;34;38;5;16m▄▄[38;5;34m███[38;5;16m▄▄[49m▀[39m  [00m
         [38;5;16m▄[48;5;65;38;5;77m▄[48;5;34;38;5;65m▄[38;5;34m██[48;5;77m▄▄[38;5;77m█████[38;5;34m▄▄[38;5;65m▄▄[38;5;77m██[38;5;34m▄[48;5;34m██████[48;5;65;38;5;65m█[49;38;5;16m▀▀▀[39m     [00m
        [48;5;65;38;5;65m█[48;5;77;38;5;77m██[38;5;34m▄[48;5;34;38;5;65m▄[38;5;16m▄[38;5;34m████████[48;5;65;38;5;65m█[48;5;34;38;5;77m▄[48;5;77m██[48;5;34;38;5;65m▄[48;5;65;38;5;34m▄[48;5;34m█████[48;5;16;38;5;16m█[49;39m         [00m
         [38;5;16m▀▀▀[39m  [38;5;65m▀[48;5;34;38;5;16m▄▄[38;5;34m█████[48;5;65m▄[48;5;34;38;5;65m▄[48;5;77;38;5;16m▄[48;5;34m▄[48;5;16;38;5;34m▄[48;5;34m███[38;5;16m▄▄[48;5;16;38;5;65m▄[49;39m          [00m
                 [38;5;16m▀▀[48;5;16;38;5;65m▄[38;5;34m▄[48;5;34;38;5;16m▄▄▄▄▄▄[49m▀[48;5;16;38;5;65m▄[38;5;34m▄[48;5;34m███[48;5;65;38;5;16m▄[49;39m         [00m
                    [38;5;65m▀[48;5;34m▄▄[49m▀[39m     [38;5;65m▀[48;5;34m▄[38;5;16m▄[48;5;65m▄[49m▀[39m         [00m
                                           [00m
