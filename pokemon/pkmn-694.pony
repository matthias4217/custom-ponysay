$$$
NAME: Helioptile
OTHER NAMES: 목도리키텔, Eguana, エリキテル, 傘電蜥, Galvaran
APPEARANCE: Pokémon Generation VI
KIND: Generator Pokémon
GROUP: monster, dragon
BALLOON: top
COAT: yellow
DISPLAY: full
BALLOON TOP: 3
BALLOON BOTTOM: 0
POSE: master
BASED ON: Helioptile
SOURCE: PokéAPI Sprites
MEDIA: Pokémon
LICENSE: Copyright The Pokémon Company
FREE: no
WIDTH: 68
HEIGHT: 22


$$$
$balloon5$
         $\$                                                          [00m
          $\$                                                         [00m
           $\$                                                        [00m
            $\$                        [00m
             $\$  [38;5;232m▄▄▄▄▄[39m                [00m
            [38;5;232m▄▄[48;5;232;38;5;60m▄▄[48;5;60m█████[48;5;232m▄▄[49;38;5;232m▄▄[39m            [00m
          [38;5;232m▄[48;5;232;38;5;60m▄[48;5;60m█████████████[48;5;232m▄[49;38;5;232m▄[39m          [00m
        [38;5;234m▄[48;5;232;38;5;60m▄[48;5;60;38;5;240m▄[38;5;234m▄▄[38;5;238m▄▄▄▄▄▄▄▄[38;5;234m▄▄[38;5;60m████[48;5;232m▄[49;38;5;232m▄[39m        [00m
       [38;5;232m▄[48;5;234;38;5;234m█[48;5;238;38;5;60m▄[48;5;234;38;5;188m▄[48;5;188m█[38;5;60m▄[48;5;234;38;5;231m▄[48;5;66;38;5;66m█[38;5;221m▄[48;5;221m█[48;5;60m▄[38;5;60m██[48;5;234;38;5;68m▄[48;5;188;38;5;60m▄[38;5;188m█[48;5;234m▄[48;5;238;38;5;234m▄[38;5;60m▄▄▄[48;5;232m▄[49;38;5;232m▄[39m       [00m
      [38;5;232m▄[48;5;232;38;5;238m▄[48;5;234;38;5;232m▄[48;5;60;38;5;234m▄[48;5;231;38;5;231m█[48;5;68;38;5;68m█[48;5;231;38;5;231m█[48;5;68;38;5;68m█[48;5;221;38;5;137m▄▄▄▄▄[48;5;68;38;5;68m█[48;5;231;38;5;231m█[48;5;68;38;5;68m█[48;5;60;38;5;60m█[48;5;231;38;5;231m█[48;5;234;38;5;234m█[48;5;60;38;5;60m███[48;5;238;38;5;238m█[48;5;60;38;5;60m█[48;5;232m▄[49;38;5;232m▄[39m     [00m
      [48;5;232;38;5;232m█[48;5;238;38;5;238m█[48;5;232;38;5;232m█[48;5;221;38;5;221m█[48;5;231;38;5;231m█[48;5;68m▄[48;5;110;38;5;60m▄[48;5;68;38;5;231m▄[48;5;221;38;5;221m█████[48;5;68;38;5;188m▄[48;5;110;38;5;68m▄[48;5;68;38;5;60m▄[48;5;60;38;5;231m▄[48;5;231m█[48;5;95;38;5;95m█[48;5;238;38;5;221m▄[38;5;137m▄▄[38;5;238m█[48;5;60;38;5;60m██[48;5;232m▄[49;38;5;232m▄[39m    [00m
     [48;5;232;38;5;232m█[48;5;238;38;5;238m██[48;5;232;38;5;232m█[48;5;221;38;5;137m▄[48;5;137;38;5;221m▄[48;5;231;38;5;137m▄▄[48;5;188;38;5;221m▄[48;5;221m█[48;5;137m▄[48;5;221m█[48;5;137m▄[48;5;221m██[48;5;188m▄[48;5;231;38;5;137m▄▄[48;5;137;38;5;221m▄[48;5;221m███[48;5;137;38;5;232m▄[48;5;238;38;5;238m█[48;5;60;38;5;60m███[48;5;232m▄[49;38;5;232m▄[39m   [00m
    [38;5;232m▄[48;5;232;38;5;238m▄[48;5;238m██[48;5;232;38;5;232m█[49;38;5;95m▀[48;5;221m▄[38;5;221m███[48;5;137m▄[48;5;221;38;5;137m▄[38;5;95m▄▄[48;5;137;38;5;221m▄[48;5;95m▄[48;5;221m████[38;5;137m▄[38;5;232m▄[49m▀[39m [48;5;238;38;5;238m█[48;5;60;38;5;60m████[48;5;232m▄[49;38;5;232m▄[39m  [00m
    [48;5;232;38;5;232m█[48;5;238;38;5;238m███[48;5;232;38;5;232m█[49;39m  [38;5;95m▀▀[48;5;221;38;5;232m▄▄[38;5;137m▄▄▄▄▄▄[38;5;232m▄[48;5;137m▄[49m▀▀[39m   [38;5;238m▀[48;5;60m▄[38;5;60m████[48;5;232m▄[49;38;5;232m▄[39m [00m
   [48;5;232;38;5;232m█[48;5;238;38;5;238m████[48;5;232;38;5;232m█[49;39m      [38;5;232m▀▀[48;5;95;38;5;95m█[48;5;137;38;5;137m███[48;5;232m▄[49;38;5;232m▄[39m      [38;5;238m▀[48;5;60m▄[38;5;60m████[48;5;232;38;5;232m█[49;39m [00m
   [48;5;232;38;5;232m█[48;5;238;38;5;137m▄▄[38;5;238m██[48;5;232;38;5;232m█[49;39m      [38;5;95m▄[48;5;95;38;5;221m▄[48;5;221m█[48;5;137m▄▄[48;5;221m█[48;5;137m▄[48;5;232;38;5;137m▄[49;38;5;232m▄[39m      [38;5;238m▀[48;5;60m▄[38;5;221m▄[48;5;221m███[48;5;58;38;5;58m█[49;39m[00m
    [48;5;232;38;5;232m█[48;5;137;38;5;137m███[48;5;232;38;5;232m█[49;39m     [38;5;95m▄[48;5;95;38;5;221m▄[48;5;221m█[38;5;137m▄[38;5;221m█████[48;5;137;38;5;137m█[48;5;232;38;5;221m▄[49;38;5;232m▄▄▄▄[48;5;232;38;5;137m▄▄[48;5;137m█[48;5;58m▄[48;5;221;38;5;58m▄[38;5;221m██[48;5;58;38;5;58m█[49;39m[00m
    [38;5;232m▀[48;5;137m▄[38;5;137m█[48;5;232;38;5;232m█[49;39m      [48;5;95;38;5;95m█[48;5;221;38;5;221m██[48;5;137;38;5;95m▄[48;5;221;38;5;221m██[48;5;137;38;5;95m▄[48;5;221;38;5;221m██[48;5;95;38;5;95m█[48;5;221;38;5;221m██[48;5;95m▄[48;5;137;38;5;95m▄[38;5;137m████[38;5;232m▄[49m▀[38;5;58m▀[48;5;221m▄[48;5;58m█[49;39m[00m
     [38;5;232m▀▀[39m       [48;5;95;38;5;95m█[48;5;221;38;5;221m██[48;5;95;38;5;95m█[48;5;137;38;5;137m██[48;5;95;38;5;95m█[48;5;221;38;5;221m██[48;5;95;38;5;95m█[48;5;221;38;5;221m███[48;5;95m▄[48;5;137;38;5;95m▄[38;5;137m█[38;5;232m▄[49m▀[39m     [00m
             [48;5;232;38;5;232m█[48;5;137;38;5;137m█[48;5;95;38;5;238m▄[48;5;221;38;5;242m▄[48;5;95;38;5;95m█[48;5;137;38;5;137m██[48;5;95;38;5;238m▄[48;5;221;38;5;242m▄[48;5;95;38;5;232m▄[48;5;221;38;5;137m▄[38;5;221m████[48;5;95;38;5;232m▄[49m▀[39m       [00m
             [38;5;232m▀[48;5;137m▄[48;5;238;38;5;137m▄[48;5;242;38;5;232m▄[48;5;232m█[49;38;5;95m▀▀[48;5;238;38;5;232m▄[48;5;242m▄[49m▀[38;5;95m▀[48;5;221;38;5;232m▄[38;5;221m██[48;5;95;38;5;232m▄[49;39m         [00m
              [38;5;232m▄[48;5;232;38;5;242m▄[48;5;137m▄[38;5;238m▄[48;5;232;38;5;232m█[49;39m    [38;5;232m▄[48;5;232;38;5;242m▄[48;5;137;38;5;238m▄[48;5;232;38;5;242m▄[49;38;5;232m▄[39m         [00m
              [38;5;232m▀▀▀▀[39m     [38;5;232m▀▀▀▀▀[39m         [00m
                                     [00m
