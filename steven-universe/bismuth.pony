$$$
APPEARANCE: Steven Universe S03E03 : Same Old World
BALLOON: top
BALLOON BOTTOM: 0
BALLOON TOP: 3
BASED ON: Bismuth
COAT: periwinkle
DISPLAY: full, front
EYE: black
GROUP: gem
KIND: bismuth
MANE: rainbow
MASTER: bismuth
MEDIA: Steven Universe
NAME: Bismuth
OTHER NAMES: Biz
POSE: movie
SOURCE: [⛧]
WIDTH: 39
HEIGHT: 52


$$$
$balloon17$[00m
                  $\$                       [00m
                   $\$                      [00m
                    $\$                     [00m
                   [38;5;16m▄▄[48;5;16;38;5;180m▄▄[38;5;169m▄▄[38;5;134m▄▄[49;38;5;16m▄[39m              [00m
                 [38;5;16m▄[48;5;16;38;5;49m▄[48;5;229m▄[38;5;139m▄[48;5;180m▄▄[48;5;169;38;5;169m█[38;5;139m▄[48;5;134m▄[38;5;80m▄[48;5;80m█[48;5;16m▄[49;38;5;16m▄[39m            [00m
                [38;5;16m▄[48;5;16;38;5;49m▄[48;5;49;38;5;80m▄[48;5;139;38;5;110m▄[48;5;16m▄▄[48;5;139m▄▄[48;5;16m▄▄[48;5;139m▄[48;5;49;38;5;139m▄[38;5;49m█[48;5;16m▄[49;38;5;16m▄[39m           [00m
               [38;5;16m▄[48;5;16;38;5;49m▄[48;5;80;38;5;80m█[48;5;139;38;5;110m▄[48;5;110;38;5;231m▄[48;5;231;38;5;16m▄[38;5;231m█[48;5;110;38;5;110m█[38;5;231m▄[48;5;231;38;5;16m▄[38;5;231m█[48;5;110;38;5;110m█[48;5;139;38;5;139m█[48;5;229;38;5;229m█[48;5;49m▄[38;5;49m█[48;5;16;38;5;16m█[49;39m          [00m
              [48;5;16;38;5;16m█[48;5;49;38;5;80m▄[48;5;80m█[38;5;110m▄[48;5;110m█[48;5;231m▄[48;5;16m▄[48;5;110m███[48;5;16m▄[48;5;231m▄[48;5;110m█[48;5;139m▄[48;5;212;38;5;139m▄[48;5;229;38;5;229m█[48;5;49m▄[48;5;16;38;5;16m█[49;39m          [00m
             [48;5;16;38;5;16m█[48;5;49;38;5;49m█[48;5;80;38;5;80m██[48;5;110;38;5;110m███████[38;5;231m▄▄▄[38;5;110m█[48;5;139;38;5;229m▄[48;5;229m█[38;5;212m▄[48;5;49;38;5;49m█[48;5;16;38;5;16m█[49;39m         [00m
           [38;5;16m▄▄[48;5;16m█[48;5;49;38;5;49m█[48;5;80;38;5;80m██[48;5;110;38;5;110m██[48;5;231m▄▄▄▄▄▄[48;5;110m██[38;5;110m▄[48;5;229;38;5;229m██[48;5;212;38;5;125m▄[38;5;212m█[48;5;49;38;5;49m█[48;5;16;38;5;16m█[49;39m        [00m
        [38;5;16m▄[48;5;16;38;5;110m▄▄[48;5;110m███[48;5;49m▄[48;5;80;38;5;80m██[48;5;125;38;5;125m█[48;5;110;38;5;110m▄▄[48;5;110;38;5;110m▄▄▄▄▄▄[48;5;110;38;5;110m▄[48;5;110m█[48;5;229;38;5;229m██[48;5;125;38;5;125m█[48;5;110m▄[38;5;110m█[48;5;16m▄▄[49;38;5;16m▄[39m      [00m
       [48;5;16;38;5;16m█[48;5;110;38;5;110m███████[48;5;125;38;5;125m███[48;5;110;38;5;110m██[38;5;139m▄▄▄▄▄▄▄[38;5;110m█[48;5;229m▄[38;5;125m▄[48;5;125m██[48;5;110;38;5;110m██[38;5;235m▄[48;5;235m█[48;5;16m▄[49;38;5;16m▄[39m    [00m
      [38;5;16m▄[48;5;16;38;5;110m▄[48;5;110m███████[48;5;125;38;5;125m███[48;5;110;38;5;110m██[48;5;139;38;5;139m█[48;5;80;38;5;229m▄[38;5;85m▄[48;5;134m▄[38;5;87m▄[38;5;212m▄[48;5;139;38;5;139m█[48;5;110;38;5;110m██[48;5;125;38;5;125m███[48;5;110;38;5;110m███[48;5;235;38;5;235m█[38;5;110m▄[48;5;16m▄[49;38;5;16m▄[39m   [00m
     [38;5;16m▄[48;5;16;38;5;110m▄[48;5;110m███████[38;5;178m▄[48;5;178m██[48;5;228;38;5;228m█[48;5;110m▄[38;5;110m█[48;5;139;38;5;139m██[48;5;229m▄[48;5;157m▄[48;5;87m▄[48;5;212m▄[48;5;139m█[48;5;110;38;5;110m█[38;5;178m▄[48;5;178m██[48;5;228;38;5;228m█[48;5;110m▄[38;5;110m████[48;5;235m▄[48;5;16m▄[49;38;5;16m▄[39m  [00m
    [48;5;16;38;5;16m█[48;5;110;38;5;110m█████████[48;5;178;38;5;125m▄[38;5;178m█[38;5;228m▄[48;5;228m█[38;5;125m▄[48;5;110m▄[38;5;89m▄[38;5;125m▄▄▄[38;5;89m▄[38;5;125m▄▄▄[48;5;178;38;5;89m▄[38;5;178m█[48;5;228;38;5;228m██[38;5;125m▄[48;5;110;38;5;110m██████[48;5;16m▄[49;38;5;16m▄[39m [00m
   [48;5;16;38;5;16m█[48;5;110;38;5;110m████████[38;5;16m▄[48;5;125;38;5;125m███[48;5;228;38;5;89m▄[48;5;125;38;5;125m███[48;5;89;38;5;89m█[48;5;125;38;5;125m███[48;5;89;38;5;89m█[48;5;125;38;5;125m███[48;5;89;38;5;89m█[48;5;125;38;5;125m█[48;5;228;38;5;89m▄[48;5;125;38;5;125m█[38;5;16m▄[48;5;110;38;5;110m███████[48;5;16;38;5;16m█[49;39m [00m
   [48;5;16;38;5;16m█[48;5;110;38;5;110m███████[48;5;16;38;5;16m█[49m▀[48;5;125m▄[38;5;125m██[48;5;89;38;5;89m█[48;5;125;38;5;125m███[48;5;89;38;5;89m█[48;5;125;38;5;125m█[38;5;89m▄▄[48;5;89;38;5;125m▄[48;5;125;38;5;89m▄▄[38;5;125m█[48;5;89;38;5;89m█[48;5;125;38;5;125m█[48;5;89;38;5;89m█[48;5;125;38;5;125m█[48;5;16;38;5;16m█[48;5;110;38;5;110m████████[48;5;16;38;5;16m█[49;39m[00m
   [48;5;16;38;5;16m█[48;5;110;38;5;110m███████[48;5;16;38;5;16m█[49;39m [48;5;16;38;5;16m█[48;5;125;38;5;125m██[48;5;89;38;5;89m█[48;5;125m▄▄▄[48;5;89m██[48;5;125m▄▄▄▄▄[48;5;89m██[48;5;125m▄[48;5;89;38;5;125m▄[48;5;16;38;5;16m██[48;5;110;38;5;110m███████[38;5;16m▄[49m▀[39m[00m
   [48;5;16;38;5;16m█[48;5;110;38;5;110m██████[48;5;16;38;5;16m█[49m▄[39m [48;5;16;38;5;16m█[48;5;89;38;5;125m▄▄[48;5;125m█[38;5;89m▄▄▄▄▄▄▄▄▄▄▄▄[38;5;125m█[48;5;89m▄[48;5;16;38;5;16m█[49;39m [48;5;16;38;5;16m█[48;5;110;38;5;110m██████[48;5;16;38;5;16m█[49;39m [00m
   [38;5;16m▀[48;5;110m▄[38;5;110m████[38;5;110m▄▄[38;5;110m█[48;5;16;38;5;16m█[48;5;89;38;5;125m▄▄▄▄[48;5;125m█████[48;5;89;38;5;89m█[48;5;125;38;5;125m█[48;5;89;38;5;89m█[48;5;125;38;5;125m████[48;5;89m▄▄[48;5;16;38;5;89m▄[49;38;5;16m▄[48;5;16m█[48;5;110;38;5;110m██████[48;5;16;38;5;16m█[49;39m [00m
    [48;5;16;38;5;16m█[48;5;110;38;5;110m█[38;5;110m▄[48;5;110m█[48;5;110m▄▄[48;5;110;38;5;110m▄[48;5;110;38;5;16m▄[48;5;16m█[48;5;125;38;5;125m█████████[48;5;89m▄[48;5;125;38;5;89m▄[48;5;89m█[48;5;125;38;5;125m███████[48;5;16;38;5;16m█[48;5;110;38;5;110m███████[48;5;16;38;5;16m█[49;39m [00m
     [38;5;16m▀[48;5;110m▄▄▄[49m▀▀▄[48;5;16;38;5;125m▄[48;5;125m███████[38;5;16m▄▄▄▄[48;5;89m▄[48;5;125;38;5;125m███████[48;5;16;38;5;16m█[48;5;110m▄▄[38;5;110m█[38;5;110m▄[38;5;110m█[38;5;110m▄[38;5;16m▄[49m▀[39m [00m
           [48;5;16;38;5;16m█[48;5;125;38;5;125m████████[48;5;16;38;5;16m█[49;39m   [48;5;16;38;5;16m█[48;5;125;38;5;125m███████[48;5;16;38;5;16m█[49;39m  [38;5;16m▀▀▀▀[39m   [00m
           [48;5;16;38;5;16m█[48;5;125;38;5;125m███████[48;5;16;38;5;16m█[49;39m     [48;5;16;38;5;16m█[48;5;125;38;5;125m██████[48;5;16;38;5;16m█[49;39m         [00m
          [38;5;16m▄[48;5;16;38;5;125m▄[48;5;125m███████[48;5;16;38;5;16m█[49;39m     [48;5;16;38;5;16m█[48;5;125;38;5;125m██████[48;5;16;38;5;16m█[49;39m         [00m
          [48;5;16;38;5;16m█[48;5;125;38;5;163m▄▄▄▄▄▄▄[48;5;16;38;5;16m█[49;39m      [48;5;16;38;5;16m█[48;5;125;38;5;163m▄▄▄▄▄▄[48;5;16;38;5;16m█[49;39m         [00m
          [48;5;16;38;5;16m█[48;5;163;38;5;232m▄▄▄▄▄▄▄[48;5;16;38;5;16m█[49;39m      [48;5;16;38;5;16m█[48;5;163;38;5;232m▄▄▄▄▄▄[48;5;16;38;5;16m█[49;39m         [00m
         [38;5;16m▄[48;5;16;38;5;232m▄[48;5;232m███████[48;5;16;38;5;16m█[49;39m      [48;5;16;38;5;16m█[48;5;232;38;5;232m██████[48;5;16m▄[49;38;5;16m▄[39m        [00m
         [38;5;16m▀[48;5;232m▄▄▄▄▄▄[49m▀▀[39m        [38;5;16m▀[48;5;232m▄▄▄▄▄▄[49m▀[39m        [00m
                                          [00m
