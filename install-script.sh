#! /bin/bash

###
#
# I've decided to separate all the folders, to allow users to choose
# what they want
#
###


# Borderlands
echo "Copying Borderlands ponies..."
sudo cp borderlands/*.pony /usr/share/ponysay/extraponies
echo "Done"

# Duel Masters
echo "Copying Duel Masters ponies..."
sudo cp duel-masters/*.pony /usr/share/ponysay/extraponies
echo "Done"

# Nichijou
echo "Copying Nichijou ponies..."
sudo cp nichijou/*.pony /usr/share/ponysay/extraponies
echo "Done"

# Pokemon
echo "Copying Pokemon ponies..."
sudo cp pokemon/*.pony /usr/share/ponysay/extraponies
echo "Done"

# Steven Universe
echo "Copying Steven Universe ponies..."
sudo cp steven-universe/*.pony /usr/share/ponysay/extraponies
echo "Done"
