Images come from :
  * [Hollulu](https://www.deviantart.com/hollulu) : 
    Steven Universe, Lion
  * [Dragon97586](https://www.deviantart.com/dragon97586) :
    Sapphire
  * [NightjarArt](https://www.deviantart.com/nightjarart) :
    Sardonyx
  * [Wangertron](https://wangertron.wordpress.com) :
    Claptrap
  * MadEwink : Peridot sitting
