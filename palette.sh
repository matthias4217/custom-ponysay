#! /bin/bash

c=16
while ((c < 256)); do
	echo -en "\e[48;5;${c}m  \e[49m"
	c=$(( $c + 1 ))
	if (( $(( c % 36 )) == 16 )); then
		echo
	fi
done; echo
